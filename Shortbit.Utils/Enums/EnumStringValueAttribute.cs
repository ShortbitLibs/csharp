﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shortbit.Utils.Enums
{
	[AttributeUsage(AttributeTargets.Field)]
	public class EnumStringValueAttribute : Attribute
	{
		public EnumStringValueAttribute(string value)
		{
			this.Value = value;
		}

		public string Value { get; }
	}
}
