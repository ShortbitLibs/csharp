﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shortbit.Utils.Enums
{
	[AttributeUsage(AttributeTargets.Field)]
	public class EnumValueAttribute : Attribute
	{
		public EnumValueAttribute(object value)
		{
			this.Value = value;
		}

		public object Value { get; }
	}
}
