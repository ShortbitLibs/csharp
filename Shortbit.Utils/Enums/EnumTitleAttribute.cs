﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shortbit.Utils.Enums
{
	[AttributeUsage(AttributeTargets.Field)]
	public class EnumTitleAttribute : Attribute
	{
		public EnumTitleAttribute(string title)
		{
			this.Title = title;
		}

		public string Title { get; }
	}
}
