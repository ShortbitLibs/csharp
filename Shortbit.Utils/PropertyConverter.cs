﻿// /*
//  * PropertyConverter.cs
//  *
//  *  Created on: 05:16
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

//using System.Xaml;

namespace Shortbit.Utils
{
	public class PropertyConverter : TypeConverter
	{
		private static List<Func<ITypeDescriptorContext, Type>> instanceTypeSelectors = new List<Func<ITypeDescriptorContext, Type>>();

		public static void AddInstanceTypeSelector(Func<ITypeDescriptorContext, Type> typeSelector)
		{
			PropertyConverter.instanceTypeSelectors.Add(typeSelector);
		}

		private static Type getInstanceType(ITypeDescriptorContext context)
		{
			if (context == null) return null;

			return PropertyConverter.instanceTypeSelectors.Select(selector => selector(context)).FirstOrDefault(type => type != null);
		}


		private readonly Type instanceType;

		public PropertyConverter()
		{
			this.instanceType = null;
		}

		public PropertyConverter(Type instanceType)
		{
			this.instanceType = instanceType;
		}

		private Type GetInstanceType(ITypeDescriptorContext context)
		{
			if (this.instanceType != null)
				return this.instanceType;
			
			return PropertyConverter.getInstanceType(context);
		}

		private TypeConverter GetInnerTypeConverter(Type instanceType)
		{
			if (instanceType == null) return null;

			if (instanceType.IsGenericType
			    && instanceType.GetGenericTypeDefinition() == typeof (Property<>)
			    && instanceType.GetGenericArguments().Length == 1)
			{
				var innerType = instanceType.GetGenericArguments()[0];
				return TypeDescriptor.GetConverter(innerType);
			}
			else
			{
				throw new ArgumentException("Incompatible type", "type");
			}
		}

		/// <summary>
		///     Gibt zurück, ob dieser Konverter ein Objekt des angegebenen Typs mithilfe des angegebenen Kontexts in den Typ des
		///     Konverters konvertieren kann.
		/// </summary>
		/// <returns>
		///     true, wenn dieser Konverter die Konvertierung durchführen kann, andernfalls false.
		/// </returns>
		/// <param name="context">
		///     Eine <see cref="T:System.ComponentModel.ITypeDescriptorContext" />-Schnittstelle, die einen
		///     Formatierungskontext bereitstellt.
		/// </param>
		/// <param name="sourceType">Eine <see cref="T:System.Type" />-Klasse, die den Ausgangstyp der Konvertierung darstellt.</param>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (string)) return true;

			Type instanceType = this.GetInstanceType(context);
			var innerTypeConverter = this.GetInnerTypeConverter(instanceType);

			if (innerTypeConverter == null) return false;


			return innerTypeConverter.CanConvertFrom(context, sourceType);
		}

		/// <summary>
		///     Konvertiert das angegebene Objekt unter Verwendung des angegebenen Kontexts und der Kulturinformationen in den Typ
		///     dieses Konverters.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Object" />, das den konvertierten Wert darstellt.
		/// </returns>
		/// <param name="context">
		///     Eine <see cref="T:System.ComponentModel.ITypeDescriptorContext" />-Schnittstelle, die einen
		///     Formatierungskontext bereitstellt.
		/// </param>
		/// <param name="culture">Die als aktuelle Kultur zu verwendenden <see cref="T:System.Globalization.CultureInfo" />.</param>
		/// <param name="value">Die zu konvertierende <see cref="T:System.Object" />-Klasse.</param>
		/// <exception cref="T:System.NotSupportedException">Die Konvertierung kann nicht durchgeführt werden.</exception>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			Type instanceType = this.GetInstanceType(context);
			var innerTypeConverter = this.GetInnerTypeConverter(instanceType);

			if (innerTypeConverter == null) return null;


			object propValue = innerTypeConverter.ConvertFrom(context, culture, value);
			object propertyInstance = Activator.CreateInstance(instanceType, propValue);

			return propertyInstance;
		}

		/// <summary>
		///     Gibt zurück, ob dieser Konverter das Objekt mithilfe des angegebenen Kontexts in den angegebenen Typ konvertieren
		///     kann.
		/// </summary>
		/// <returns>
		///     true, wenn dieser Konverter die Konvertierung durchführen kann, andernfalls false.
		/// </returns>
		/// <param name="context">
		///     Eine <see cref="T:System.ComponentModel.ITypeDescriptorContext" />-Schnittstelle, die einen
		///     Formatierungskontext bereitstellt.
		/// </param>
		/// <param name="destinationType">Ein <see cref="T:System.Type" />, der den Zieltyp der Konvertierung darstellt.</param>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof (string)) return true;

			Type instanceType = this.GetInstanceType(context);
			var innerTypeConverter = this.GetInnerTypeConverter(instanceType);

			if (innerTypeConverter == null) return false;


			return innerTypeConverter.CanConvertTo(context, destinationType);
		}

		/// <summary>
		///     Konvertiert das angegebene Wertobjekt unter Verwendung des angegebenen Kontexts und der angegebenen
		///     Kulturinformationen in den angegebenen Typ.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Object" />, das den konvertierten Wert darstellt.
		/// </returns>
		/// <param name="context">
		///     Eine <see cref="T:System.ComponentModel.ITypeDescriptorContext" />-Schnittstelle, die einen
		///     Formatierungskontext bereitstellt.
		/// </param>
		/// <param name="culture">
		///     <see cref="T:System.Globalization.CultureInfo" /> Wenn null übergeben wird, wird von der
		///     aktuellen Kultur ausgegangen.
		/// </param>
		/// <param name="value">Die zu konvertierende <see cref="T:System.Object" />-Klasse.</param>
		/// <param name="destinationType">
		///     Der <see cref="T:System.Type" />, in den der <paramref name="value" />-Parameter
		///     konvertiert werden soll.
		/// </param>
		/// <exception cref="T:System.ArgumentNullException">Der <paramref name="destinationType" />-Parameter ist null.</exception>
		/// <exception cref="T:System.NotSupportedException">Die Konvertierung kann nicht durchgeführt werden.</exception>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
			Type destinationType)
		{
			Type instanceType = this.GetInstanceType(context);
			var innerTypeConverter = this.GetInnerTypeConverter(instanceType);

			if (innerTypeConverter == null) return null;


			var prop = (PropertyBase) value;
			return innerTypeConverter.ConvertTo(context, culture, prop.RawValue, destinationType);
		}
	}
}