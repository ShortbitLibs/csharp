﻿using System;
using System.Text.RegularExpressions;

namespace Shortbit.Utils

{
	public sealed class VersionRange
	{
		private static readonly Regex rangeMatchRegex = new Regex(
			@"^\s*(?<lower_exclude>!?)(?<lower>.*)\s+(-|->|=>)\s+(?<upper_exclude>!?)(?<upper>.*)\s*$");

		public Version Lower { get; set; }
		public Version Upper { get; set; }

		public bool IncludeLower { get; set; }
		public bool IncludeUpper { get; set; }

		public bool IsWildcard => this.Lower == null && this.Upper == null;
		public bool IsUpperBound => this.Upper != null && this.Lower == null;
		public bool IsLowerBound => this.Lower != null && this.Upper == null;
		public bool IsOneBound => this.IsLowerBound || this.IsUpperBound;


		public VersionRange(Version lower = null, Version upper = null, bool includeLower = true,
							bool includeUpper = true)
		{
			this.Lower = lower;
			this.Upper = upper;
			this.IncludeLower = includeLower;
			this.IncludeUpper = includeUpper;
		}

		public bool Contains(Version version)
		{
			Throw.IfNull(version, nameof(version));

			if (this.Lower != null)
			{
				if (version == this.Lower)
					return this.IncludeLower;
				if (version < this.Lower)
					return false;
			}

			if (this.Upper != null)
			{
				if (version == this.Upper)
					return this.IncludeUpper;
				if (version > this.Upper)
					return false;

			}

			return true;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			if (this.Lower != null && this.Upper != null)
				return $"{(this.IncludeLower ? "" : "!")}{this.Lower} -> {(this.IncludeUpper ? "" : "!")}{this.Upper}";

			if (this.Lower != null)
				return $">{(this.IncludeLower ? "=" : "")} {this.Lower}";

			if (this.Upper != null)
				return $"<{(this.IncludeUpper ? "=" : "")} {this.Upper}";

			return "*";
		}


		public static VersionRange Parse(string value)
		{
			value = value.Trim();
			
			if (Version.TryParse(value, out var v2))
				return new VersionRange(lower: v2, upper: v2);
			
			if (value == "*")
				return new VersionRange();

			if (value[0] == '=')
			{
				var v = Version.Parse(value.Substring(1));
				return new VersionRange(lower: v, upper: v);
			}

			if (value[0] == '>')
			{
				int index = 1;
				bool include = false;
				if (value[1] == '=')
				{
					index = 2;
					include = true;
				}
				var vStr = value.Substring(index).Trim();
				var v = Version.Parse(vStr);
				return new VersionRange(lower: v, includeLower: include);
			}

			if (value[0] == '<')
			{
				int index = 1;
				bool include = false;
				if (value[1] == '=')
				{
					index = 2;
					include = true;
				}
				var vStr = value.Substring(index).Trim();
				var v = Version.Parse(vStr);
				return new VersionRange(upper: v, includeUpper: include);
			}

			var match = VersionRange.rangeMatchRegex.Match(value);
			if (match.Success)
			{
				var lower = match.Groups["lower"].Value.Trim();
				var upper = match.Groups["upper"].Value.Trim();
				var lowerExclude = match.Groups["lower_exclude"].Value.Trim();
				var upperExclude = match.Groups["upper_exclude"].Value.Trim();

				if (lower == "*" && upper == "*")
					throw new ArgumentException("Either lower or upper version must be specified.");

				if (lower == "*")
					return new VersionRange(upper: Version.Parse(upper), includeUpper: upperExclude.Length == 0);
				if (upper == "*")
					return new VersionRange(lower: Version.Parse(lower), includeLower: lowerExclude.Length == 0);

				var vLower = Version.Parse(lower);
				var vUpper = Version.Parse(upper);
				if (vLower > vUpper)
				{
					var tmp = vLower;
					vLower = vUpper;
					vUpper = tmp;
				}

				return new VersionRange(lower: vLower, upper: vUpper, includeLower: lowerExclude.Length == 0, includeUpper: upperExclude.Length == 0);

			}

			throw new ArgumentException($"Could not parse version range string \"{value}\".");
		}
	}
}
