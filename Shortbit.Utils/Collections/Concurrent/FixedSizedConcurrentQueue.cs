﻿// /*
//  * FixedSizedConcurrentQueue.cs
//  *
//  *  Created on: 17:37
//  *         Author: 
//  */

using System.Collections.Concurrent;

namespace Shortbit.Utils.Collections.Concurrent
{
	public class FixedSizedConcurrentQueue<T> : ConcurrentQueue<T>
	{
		private readonly object syncObject = new object();

		public int Size { get; set; }

		public FixedSizedConcurrentQueue(int size)
		{
			Size = size;
		}

		public new void Enqueue(T obj)
		{
			base.Enqueue(obj);
			lock (syncObject)
			{
				while (base.Count > Size)
				{
					T outObj;
					base.TryDequeue(out outObj);
				}
			}
		}
	}
}