﻿// /*
//  * CountingList.cs
//  *
//  *  Created on: 07:22
//  *         Author: 
//  */

using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Shortbit.Utils.Collections.Generic
{
	/// <summary>
	///     Implementation of the List class using a HashSet for faster content check
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class CountingList<T> : IList<T>
	{
		private readonly List<T> values;
		private readonly Dictionary<T, int> contentCounter;


		/// <summary>
		///     Initialisiert eine neue, leere Instanz der <see cref="T:Shortbit.Utils.Collections.Generic.CountingList`1" />-Klasse,
		///     die die Standardanfangskapazität aufweist.
		/// </summary>
		public CountingList()
		{
			this.values = new List<T>();
			this.contentCounter = new Dictionary<T, int>();
		}

		/// <summary>
		///     Initialisiert eine neue, leere Instanz der <see cref="T:Shortbit.Utils.Collections.Generic.CountingList`1" />-Klasse,
		///     die die angegebene Anfangskapazität aufweist.
		/// </summary>
		/// <param name="capacity">Die Anzahl von Elementen, die anfänglich in der neuen Liste gespeichert werden können.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="capacity" /> ist kleiner als 0 (null).</exception>
		public CountingList(int capacity)
		{
			this.values = new List<T>(capacity);
			this.contentCounter = new Dictionary<T, int>(capacity);
		}

		/// <summary>
		///     Initialisiert eine neue Instanz der <see cref="T:Shortbit.Utils.Collections.Generic.CountingList`1" />-Klasse, die aus
		///     der angegebenen Auflistung kopierte Elemente enthält und eine ausreichende Kapazität für die Anzahl der kopierten
		///     Elemente aufweist.
		/// </summary>
		/// <param name="collection">Die Auflistung, deren Elemente in die neue Liste kopiert werden.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="collection" /> ist null.</exception>
		public CountingList(IEnumerable<T> collection)
		{
			var other = collection.ToListSafe();

			this.values = new List<T>(other);
			this.contentCounter = new Dictionary<T, int>();
			foreach (var item in other)
				this.Increment(item);
		}

		/// <summary>
		///     Gibt einen Enumerator zurück, der die Auflistung durchläuft.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Collections.Generic.IEnumerator`1" />, der zum Durchlaufen der Auflistung verwendet werden
		///     kann.
		/// </returns>
		public IEnumerator<T> GetEnumerator()
		{
			return this.values.GetEnumerator();
		}

		/// <summary>
		///     Gibt einen Enumerator zurück, der eine Auflistung durchläuft.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Collections.IEnumerator" />-Objekt, das zum Durchlaufen der Auflistung verwendet werden
		///     kann.
		/// </returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.values.GetEnumerator();
		}

		/// <summary>
		///     Fügt der <see cref="T:System.Collections.Generic.ICollection`1" /> ein Element hinzu.
		/// </summary>
		/// <param name="item">Das Objekt, das <see cref="T:System.Collections.Generic.ICollection`1" /> hinzugefügt werden soll.</param>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void Add(T item)
		{
			this.values.Add(item);
			this.Increment(item);
		}

		/// <summary>
		///     Entfernt alle Elemente aus <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void Clear()
		{
			this.values.Clear();
			this.contentCounter.Clear();
		}

		/// <summary>
		///     Ermittelt, ob die <see cref="T:System.Collections.Generic.ICollection`1" /> einen bestimmten Wert enthält.
		/// </summary>
		/// <returns>
		///     true, wenn sich <paramref name="item" /> in <see cref="T:System.Collections.Generic.ICollection`1" /> befindet,
		///     andernfalls false.
		/// </returns>
		/// <param name="item">Das im <see cref="T:System.Collections.Generic.ICollection`1" /> zu suchende Objekt.</param>
		public bool Contains(T item)
		{
			return this.contentCounter.ContainsKey(item);
		}

		/// <summary>
		///     Kopiert die Elemente von <see cref="T:System.Collections.Generic.ICollection`1" /> in ein
		///     <see cref="T:System.Array" />, beginnend bei einem bestimmten <see cref="T:System.Array" />-Index.
		/// </summary>
		/// <param name="array">
		///     Das eindimensionale <see cref="T:System.Array" />, das das Ziel der aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> kopierten Elemente ist. Für das
		///     <see cref="T:System.Array" /> muss eine nullbasierte Indizierung verwendet werden.
		/// </param>
		/// <param name="arrayIndex">Der nullbasierte Index in <paramref name="array" />, an dem das Kopieren beginnt.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="array" /> ist null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="arrayIndex" /> ist kleiner als 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///     Die Anzahl der Elemente in der Quell-
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist größer als der verfügbare Platz von
		///     <paramref name="arrayIndex" /> bis zum Ende des Ziel-<paramref name="array" />.
		/// </exception>
		public void CopyTo(T[] array, int arrayIndex)
		{
			this.values.CopyTo(array, arrayIndex);
		}

		/// <summary>
		///     Entfernt das erste Vorkommen eines angegebenen Objekts aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <returns>
		///     true, wenn das <paramref name="item" /> erfolgreich aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> entfernt wurde, andernfalls false. Diese Methode gibt
		///     auch dann false zurück, wenn <paramref name="item" /> nicht in der ursprünglichen
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> gefunden wurde.
		/// </returns>
		/// <param name="item">Das aus dem <see cref="T:System.Collections.Generic.ICollection`1" /> zu entfernende Objekt.</param>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public bool Remove(T item)
		{
			if (!this.contentCounter.ContainsKey(item))
				return false;

			this.Decrement(item);

			return this.values.Remove(item);
		}

		/// <summary>
		///     Ruft die Anzahl der Elemente ab, die in <see cref="T:System.Collections.Generic.ICollection`1" /> enthalten sind.
		/// </summary>
		/// <returns>
		///     Die Anzahl der Elemente, die in <see cref="T:System.Collections.Generic.ICollection`1" /> enthalten sind.
		/// </returns>
		public int Count
		{
			get { return this.values.Count; }
		}

		/// <summary>
		///     Ruft einen Wert ab, der angibt, ob das <see cref="T:System.Collections.Generic.ICollection`1" /> schreibgeschützt
		///     ist.
		/// </summary>
		/// <returns>
		///     true, wenn das <see cref="T:System.Collections.Generic.ICollection`1" /> schreibgeschützt ist, andernfalls false.
		/// </returns>
		public bool IsReadOnly
		{
			get { return (this.values as IList).IsReadOnly; }
		}

		/// <summary>
		///     Bestimmt den Index eines bestimmten Elements in der <see cref="T:System.Collections.Generic.IList`1" />.
		/// </summary>
		/// <returns>
		///     Der Index von <paramref name="item" />, wenn das Element in der Liste gefunden wird, andernfalls -1.
		/// </returns>
		/// <param name="item">Das im <see cref="T:System.Collections.Generic.IList`1" /> zu suchende Objekt.</param>
		public int IndexOf(T item)
		{
			return this.values.IndexOf(item);
		}

		/// <summary>
		///     Fügt am angegebenen Index ein Element in die <see cref="T:System.Collections.Generic.IList`1" /> ein.
		/// </summary>
		/// <param name="index">Der nullbasierte Index, an dem <paramref name="item" /> eingefügt werden soll.</param>
		/// <param name="item">Das in die <see cref="T:System.Collections.Generic.IList`1" /> einzufügende Objekt.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///     <paramref name="index" /> ist kein gültiger Index in der
		///     <see cref="T:System.Collections.Generic.IList`1" />.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		///     Die <see cref="T:System.Collections.Generic.IList`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void Insert(int index, T item)
		{
			this.values.Insert(index, item);
			this.Increment(item);
		}

		/// <summary>
		///     Entfernt das <see cref="T:System.Collections.Generic.IList`1" />-Element am angegebenen Index.
		/// </summary>
		/// <param name="index">Der nullbasierte Index des zu entfernenden Elements.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///     <paramref name="index" /> ist kein gültiger Index in der
		///     <see cref="T:System.Collections.Generic.IList`1" />.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		///     Die <see cref="T:System.Collections.Generic.IList`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void RemoveAt(int index)
		{
			var item = this.values[index];
			this.values.RemoveAt(index);

			this.Decrement(item);
		}

		/// <summary>
		///     Ruft das Element am angegebenen Index ab oder legt dieses fest.
		/// </summary>
		/// <returns>
		///     Das Element am angegebenen Index.
		/// </returns>
		/// <param name="index">Der nullbasierte Index des Elements, das abgerufen oder festgelegt werden soll.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///     <paramref name="index" /> ist kein gültiger Index in der
		///     <see cref="T:System.Collections.Generic.IList`1" />.
		/// </exception>
		/// <exception cref="T:System.NotSupportedException">
		///     Die Eigenschaft wird festgelegt, und die
		///     <see cref="T:System.Collections.Generic.IList`1" /> ist schreibgeschützt.
		/// </exception>
		public T this[int index]
		{
			get { return this.values[index]; }
			set
			{
				var item = this.values[index];
				this.values[index] = value;

				this.Increment(value);
				this.Decrement(item);
			}
		}

		/// <summary>
		///     Gibt einen schreibgeschützten <see cref="T:System.Collections.Generic.IList`1" />-Wrapper für die aktuelle
		///     Auflistung zurück.
		/// </summary>
		/// <returns>
		///     Eine <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" />, die als schreibgeschützter Wrapper um
		///     die aktuelle <see cref="T:System.Collections.Generic.List`1" /> fungiert.
		/// </returns>
		public ReadOnlyCollection<T> AsReadOnly()
		{
			return this.values.AsReadOnly();
		}


		private void Increment(T item)
		{
			if (this.contentCounter.ContainsKey(item))
				this.contentCounter[item]++;
			else
				this.contentCounter.Add(item, 1);
		}

		private void Decrement(T item)
		{
			if (this.contentCounter.ContainsKey(item)) return;

			this.contentCounter[item]--;
			if (this.contentCounter[item] == 0)
				this.contentCounter.Remove(item);
		}
	}
}