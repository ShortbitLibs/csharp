﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

#if NETCOREAPP
namespace Shortbit.Utils.Collections.Generic
{
	public readonly struct WrappedAsyncEnumerator<T> : IAsyncEnumerator<T>, IEnumerator<T>
	{
		private readonly IEnumerator<T> enumerator;

		public WrappedAsyncEnumerator(IEnumerator<T> enumerator)
		{
			this.enumerator = enumerator;
		}

		/// <inheritdoc />
		public void Dispose()
		{
			this.enumerator.Dispose();
		}

		/// <inheritdoc />
		bool IEnumerator.MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <inheritdoc />
		void IEnumerator.Reset()
		{
			this.enumerator.Reset();
		}

		public ValueTask<bool> MoveNextAsync()
		{
			return new ValueTask<bool>(this.enumerator.MoveNext());
		}

		public ValueTask DisposeAsync()
		{
			this.enumerator.Dispose();
			return new ValueTask();
		}

		/// <inheritdoc />
		public T Current => this.enumerator.Current;

		/// <inheritdoc />
		object IEnumerator.Current => this.Current;
	}
}
#endif