﻿// /*
//  * IPriorityQueue.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System.Collections.Generic;

namespace Shortbit.Utils.Collections.Generic
{
	public interface IPriorityQueue<T> : IEnumerable<T>
	{
		int Count { get; }

		void Enqueue(T value);

		T Dequeue();
		T Peek();

		void Remove(T value);
	}
}