﻿// /*
//  * BiDictionary.cs
//  *
//  *  Created on: 05:54
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils.Collections.Generic
{
	public class BiDictionary<TFirst, TSecond> : ICollection<Tuple<TFirst, TSecond>>, IEnumerable<Tuple<TFirst, TSecond>>
	{
		private readonly IDictionary<TFirst, TSecond> firstToSecond = new Dictionary<TFirst, TSecond>();
		private readonly IDictionary<TSecond, TFirst> secondToFirst = new Dictionary<TSecond, TFirst>();


		/// <summary>
		///     Ruft die Anzahl der Elemente ab, die in <see cref="T:System.Collections.Generic.ICollection`1" /> enthalten sind.
		/// </summary>
		/// <returns>
		///     Die Anzahl der Elemente, die in <see cref="T:System.Collections.Generic.ICollection`1" /> enthalten sind.
		/// </returns>
		public int Count
		{
			get { return this.firstToSecond.Count; }
		}

		/// <summary>
		///     Ruft einen Wert ab, der angibt, ob das <see cref="T:System.Collections.Generic.ICollection`1" /> schreibgeschützt
		///     ist.
		/// </summary>
		/// <returns>
		///     true, wenn das <see cref="T:System.Collections.Generic.ICollection`1" /> schreibgeschützt ist, andernfalls false.
		/// </returns>
		public bool IsReadOnly
		{
			get { return false; }
		}

		public ICollection<TFirst> Firsts
		{
			get { return this.firstToSecond.Keys; }
		}

		public ICollection<TSecond> Seconds
		{
			get { return this.secondToFirst.Keys; }
		}

		// Note potential ambiguity using indexers (e.g. mapping from int to int)
		// Hence the methods as well...
		public TSecond this[TFirst first]
		{
			get { return GetByFirst(first); }
		}

		public TFirst this[TSecond second]
		{
			get { return GetBySecond(second); }
		}

		/// <summary>
		///     Fügt der <see cref="T:System.Collections.Generic.ICollection`1" /> ein Element hinzu.
		/// </summary>
		/// <param name="item">Das Objekt, das <see cref="T:System.Collections.Generic.ICollection`1" /> hinzugefügt werden soll.</param>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void Add(Tuple<TFirst, TSecond> item)
		{
			this.Add(item.Item1, item.Item2);
		}


		public void Add(TFirst first, TSecond second)
		{
			if (firstToSecond.ContainsKey(first) ||
			    secondToFirst.ContainsKey(second))
			{
				throw new ArgumentException("Duplicate first or second");
			}
			firstToSecond.Add(first, second);
			secondToFirst.Add(second, first);
		}


		public bool RemoveByFirst(TFirst first)
		{
			if (!this.firstToSecond.ContainsKey(first))
				return false;

			var second = this.firstToSecond[first];
			this.firstToSecond.Remove(first);
			this.secondToFirst.Remove(second);

			return true;
		}

		public bool RemoveBySecond(TSecond second)
		{
			if (!this.secondToFirst.ContainsKey(second))
				return false;

			var first = this.secondToFirst[second];
			this.firstToSecond.Remove(first);
			this.secondToFirst.Remove(second);

			return true;
		}

		/// <summary>
		///     Entfernt alle Elemente aus <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public void Clear()
		{
			this.firstToSecond.Clear();
			this.secondToFirst.Clear();
		}

		/// <summary>
		///     Ermittelt, ob die <see cref="T:System.Collections.Generic.ICollection`1" /> einen bestimmten Wert enthält.
		/// </summary>
		/// <returns>
		///     true, wenn sich <paramref name="item" /> in <see cref="T:System.Collections.Generic.ICollection`1" /> befindet,
		///     andernfalls false.
		/// </returns>
		/// <param name="item">Das im <see cref="T:System.Collections.Generic.ICollection`1" /> zu suchende Objekt.</param>
		public bool Contains(Tuple<TFirst, TSecond> item)
		{
			if ((item == null) || (item.Item1 == null) || (item.Item2 == null)) throw new ArgumentNullException("item");

			var first = item.Item1;
			var second = item.Item2;

			return this.firstToSecond.ContainsKey(first) && this.firstToSecond[first].Equals(second);
		}


		/// <summary>
		///     Entfernt das erste Vorkommen eines angegebenen Objekts aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" />.
		/// </summary>
		/// <returns>
		///     true, wenn das <paramref name="item" /> erfolgreich aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> entfernt wurde, andernfalls false. Diese Methode gibt
		///     auch dann false zurück, wenn <paramref name="item" /> nicht in der ursprünglichen
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> gefunden wurde.
		/// </returns>
		/// <param name="item">Das aus dem <see cref="T:System.Collections.Generic.ICollection`1" /> zu entfernende Objekt.</param>
		/// <exception cref="T:System.NotSupportedException">
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist
		///     schreibgeschützt.
		/// </exception>
		public bool Remove(Tuple<TFirst, TSecond> item)
		{
			if (item == null) throw new ArgumentNullException("item");

			return this.RemoveByFirst(item.Item1);
		}

		public TSecond GetByFirst(TFirst first)
		{
			return this.firstToSecond[first];
		}

		public TFirst GetBySecond(TSecond second)
		{
			return this.secondToFirst[second];
		}

		public bool TryGetByFirst(TFirst first, out TSecond second)
		{
			return firstToSecond.TryGetValue(first, out second);
		}

		public bool TryGetBySecond(TSecond second, out TFirst first)
		{
			return secondToFirst.TryGetValue(second, out first);
		}

		public bool ContainsByFirst(TFirst first)
		{
			return this.firstToSecond.ContainsKey(first);
		}
		public bool ContainsBySecond(TSecond second)
		{
			return this.secondToFirst.ContainsKey(second);
		}



		/// <summary>
		///     Gibt einen Enumerator zurück, der die Auflistung durchläuft.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Collections.Generic.IEnumerator`1" />, der zum Durchlaufen der Auflistung verwendet werden
		///     kann.
		/// </returns>
		IEnumerator<Tuple<TFirst, TSecond>> IEnumerable<Tuple<TFirst, TSecond>>.GetEnumerator()
		{
			return this.firstToSecond.Select(p => new Tuple<TFirst, TSecond>(p.Key, p.Value)).GetEnumerator();
		}

		/// <summary>
		///     Gibt einen Enumerator zurück, der eine Auflistung durchläuft.
		/// </summary>
		/// <returns>
		///     Ein <see cref="T:System.Collections.IEnumerator" />-Objekt, das zum Durchlaufen der Auflistung verwendet werden
		///     kann.
		/// </returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return (this as IEnumerable<Tuple<TFirst, TSecond>>).GetEnumerator();
		}


		/// <summary>
		///     Kopiert die Elemente von <see cref="T:System.Collections.Generic.ICollection`1" /> in ein
		///     <see cref="T:System.Array" />, beginnend bei einem bestimmten <see cref="T:System.Array" />-Index.
		/// </summary>
		/// <param name="array">
		///     Das eindimensionale <see cref="T:System.Array" />, das das Ziel der aus der
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> kopierten Elemente ist. Für das
		///     <see cref="T:System.Array" /> muss eine nullbasierte Indizierung verwendet werden.
		/// </param>
		/// <param name="arrayIndex">Der nullbasierte Index in <paramref name="array" />, an dem das Kopieren beginnt.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="array" /> ist null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="arrayIndex" /> ist kleiner als 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///     Die Anzahl der Elemente in der Quell-
		///     <see cref="T:System.Collections.Generic.ICollection`1" /> ist größer als der verfügbare Platz von
		///     <paramref name="arrayIndex" /> bis zum Ende des Ziel-<paramref name="array" />.
		/// </exception>
		public void CopyTo(Tuple<TFirst, TSecond>[] array, int arrayIndex)
		{
			int i = arrayIndex;
			foreach (var pair in this.firstToSecond)
			{
				array[i] = Tuple.Create(pair.Key, pair.Value);
				i++;
			}
		}
	}
}