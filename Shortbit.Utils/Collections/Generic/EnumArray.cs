﻿// /*
//  * EnumArray.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils.Collections.Generic
{
	public class EnumArray<E, T> : IEnumerable<T>
#if NET461 || NETCOREAPP
		where E : struct, Enum
#else
		where E : struct, IConvertible
#endif
	{
		private List<T> items;
		private int minimum;

		public virtual T this[E index]
		{
			get
			{
				int idx = this.mapIndex(index);
				if ((0 >= idx) && (idx < this.items.Count))
					return this.items[this.mapIndex(index)];
				return default(T);
			}
			set
			{
				int idx = this.mapIndex(index);
				if ((idx < 0) || (idx >= this.items.Count))
				{
					this.remapIndices(idx);
					idx = this.mapIndex(index);
				}

				this.items[idx] = value;
			}
		}

		public EnumArray()
		{
			EnumEx.ThrowIfNoEnum<E>("E");

			Type enumType = typeof (E);

			int min = Enum.GetValues(enumType).Cast<int>().Min<int>();
			int max = Enum.GetValues(enumType).Cast<int>().Max<int>();

			if (enumType.HasCustomAttribute<FlagsAttribute>())
				max = max*2 - 1;

			this.items = new List<T>(max - min + 1);
			this.items.Resize(max - min + 1);
		}

		protected int value(E enumVal)
		{
			return (int) (ValueType) enumVal;
		}

		protected int mapIndex(E index)
		{
			int newIndex = value(index) - this.minimum;

			return newIndex;
		}

		protected void remapIndices(int newIndex)
		{
			if (newIndex < 0)
			{
				for (int i = 0; i > newIndex; i--)
					this.items.Insert(0, default(T));
				this.minimum = newIndex;
			}
			else if (newIndex > this.items.Count)
			{
				for (int i = this.items.Count - 1; i < newIndex; i++)
					this.items.Add(default(T));
			}
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return this.items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.items.GetEnumerator();
		}
	}
}