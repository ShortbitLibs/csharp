﻿// /*
//  * BinaryHeap.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Shortbit.Utils.Collections.Generic
{
	public class BinaryHeap<T> : IPriorityQueue<T>
	{
		private List<T> storage;
		private IComparer<T> comparer;


		public BinaryHeap()
			: this(Comparer<T>.Default)
		{
		}

		public BinaryHeap(IComparer<T> comparer)
		{
			this.storage = new List<T>();
			this.comparer = comparer;
		}

		public int Count
		{
			get { return this.storage.Count; }
		}

		public void Enqueue(T value)
		{
			int i = this.storage.Count;
			this.storage.Add(value);
			this.HeapifyUp(i);
		}

		public T Dequeue()
		{
			if (this.Count == 0)
				throw new InvalidOperationException();

			T temp = this.storage[0];

			this.Remove(0);

			return temp;
		}

		public T Peek()
		{
			if (this.Count == 0)
				throw new InvalidOperationException();

			return this.storage[0];
		}

		public void Remove(T value)
		{
			int index = -1;
			for (int i = 0; i < this.storage.Count; i++)
				if (this.storage[i].Equals(value))
					index = i;

			if (index != -1)
				Remove(index);
		}

		private void Remove(int i)
		{
			int last = this.Count - 1;
			this.storage[i] = this.storage[last];
			this.storage.RemoveAt(last);
			this.HeapifyDown(i);
		}

		#region IEnumer<T> Implementierung

		public IEnumerator<T> GetEnumerator()
		{
			return this.storage.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion

		private bool Exists(int i)
		{
			return (i < this.storage.Count);
		}

		private int Parent(int i)
		{
			if (i == 0)
				return -1;

			return (int) Math.Floor((i - 1)/2.0);
		}

		private int Left(int i)
		{
			return 2*i + 1;
		}

		private int Right(int i)
		{
			return 2*i + 2;
		}

		private bool IsHeap(int i)
		{
			if (!this.Exists(i))
				return true;

			int left = this.Left(i);
			int right = this.Right(i);

			return (this.comparer.Compare(this.storage[i], this.storage[left]) <= 0)
			       && (this.comparer.Compare(this.storage[i], this.storage[right]) <= 0)
			       && (this.IsHeap(left)) && (this.IsHeap(right));
		}

		private void HeapifyDown(int i)
		{
			int x = i;
			int left = this.Left(i);
			int right = this.Right(i);

			if ((this.Exists(left)) && (this.comparer.Compare(this.storage[left], this.storage[x]) < 0))
				x = left;
			if ((this.Exists(right)) && (this.comparer.Compare(this.storage[right], this.storage[x]) < 0))
				x = right;

			if (i != x)
			{
				this.Swap(x, i);
				this.HeapifyDown(x);
			}
		}

		private void HeapifyUp(int i)
		{
			while ((i > 0) && (this.comparer.Compare(this.storage[i], this.storage[this.Parent(i)]) < 0))
			{
				this.Swap(i, this.Parent(i));
				i = this.Parent(i);
			}
		}

		private void Swap(int a, int b)
		{
			T temp = this.storage[a];
			this.storage[a] = this.storage[b];
			this.storage[b] = temp;
		}
	}
}