﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Collections.Generic
{
	public class OrderedDictionary <TKey, TValue> : IOrderedDictionary<TKey, TValue>
	{
		private readonly Dictionary<TKey, TValue> elements = new Dictionary<TKey, TValue>();
		private readonly List<TKey> keyOrder = new List<TKey>();


		int ICollection.Count => this.elements.Count;
		public int Count => this.elements.Count;

		public object SyncRoot => ((IDictionary) this.elements).SyncRoot;
		public bool IsSynchronized => ((IDictionary) this.elements).IsSynchronized;
		public bool IsFixedSize => ((IDictionary) this.elements).IsFixedSize;

		bool IDictionary.IsReadOnly => ((IDictionary) this.elements).IsReadOnly;

		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => ((IDictionary) this.elements).IsReadOnly;


		public ICollection<TKey> Keys => this.elements.Keys;

		ICollection IDictionary.Keys => ((IDictionary)this.elements).Keys;

		public ICollection<TValue> Values => this.elements.Values;

		ICollection IDictionary.Values => ((IDictionary) this.elements).Values;


		public TValue this[int index]
		{
			get => this.GetByIndex(index);
			set => this.SetByIndex(index, value);
		}
		object IOrderedDictionary.this[int index]
		{
			get => this.GetByIndex(index);
			set => this.SetByIndex(index, (TValue) value);
		}
		
		public TValue this[TKey key]
		{
			get => this.GetByKey(key);
			set => this.SetByKey(key, value);
		}

		object IDictionary.this[object key]
		{
			get => this.GetByKey((TKey)key);
			set => this.SetByKey((TKey)key, (TValue)value);
		}

		
		public TValue GetByIndex(int index)
		{
			if (this.TryGetByIndex(index, out var value))
				return value;
			throw new ArgumentOutOfRangeException(nameof(index));
		}

		public bool TryGetByIndex(int index, out TValue value)
		{
			if (index < 0 || index >= this.keyOrder.Count)
			{
				value = default(TValue);
				return false;
			}

			var key = this.keyOrder[index];
			return this.elements.TryGetValue(key, out value);
		}

		public void SetByIndex(int index, TValue value)
		{
			var key = this.keyOrder[index];
			this.elements[key] = value;
		}

		public TValue GetByKey(TKey key) => this.elements[key];

		public bool TryGetByKey(TKey key, out TValue value) => this.elements.TryGetValue(key, out value);

		public void SetByKey(TKey key, TValue value) => this.elements[key] = value;

		public bool TryGetValue(TKey key, out TValue value) => this.TryGetByKey(key, out value);

		void IDictionary.Clear() => this.Clear();

		public void Clear()
		{
			this.elements.Clear();
			this.keyOrder.Clear();
		}

		public void Add(KeyValuePair<TKey, TValue> item) => this.Add(item.Key, item.Value);
		public void Add(object key, object value) => this.Add((TKey) key, (TValue) value);
		public void Add(TKey key, TValue value)
		{
			if (this.elements.ContainsKey(key))
				throw new ArgumentException("An element with the same key already exists in the OrderedDictionary.");

			this.keyOrder.Add(key);
			this.elements.Add(key, value);
		}

		public void Insert(int index, object key, object value) => this.Insert(index, (TKey) key, (TValue) value);
		public void Insert(int index, TKey key, TValue value)
		{
			if (this.elements.ContainsKey(key))
				throw new ArgumentException("An element with the same key already exists in the OrderedDictionary.");

			this.keyOrder.Insert(index, key);
			this.elements.Add(key, value);
		}

		public bool Remove(KeyValuePair<TKey, TValue> item) => this.Remove(item.Key);
		public void Remove(object key) => this.Remove((TKey) key);
		public bool Remove(TKey key)
		{
			if (!this.elements.ContainsKey(key))
				return false;

			this.keyOrder.Remove(key);
			this.elements.Remove(key);
			return true;
		}

		
		public void RemoveAt(int index)
		{
			if (index < 0 || index >= this.Count)
				throw new ArgumentOutOfRangeException(nameof(index));
			throw new NotImplementedException();
		}


		public bool Contains(KeyValuePair<TKey, TValue> item) => this.ContainsKey(item.Key);
		public bool Contains(object key) => this.ContainsKey((TKey) key);

		public bool ContainsKey(TKey key) => this.elements.ContainsKey(key);

		public bool ContainsValue(TValue value) => this.elements.ContainsValue(value);

		
		public void CopyTo(Array array, int index) => ((IDictionary) this.elements).CopyTo(array, index);

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) =>
			((IDictionary<TKey, TValue>) this.elements).CopyTo(array, arrayIndex);



		IDictionaryEnumerator IOrderedDictionary<TKey, TValue>.GetEnumerator() => this.elements.GetEnumerator();

		IDictionaryEnumerator IOrderedDictionary.GetEnumerator() => this.elements.GetEnumerator();

		IDictionaryEnumerator IDictionary.GetEnumerator() => this.elements.GetEnumerator();
		
		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => this.elements.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => this.elements.GetEnumerator();
		
	}
}