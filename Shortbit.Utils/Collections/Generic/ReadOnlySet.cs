﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Collections.Generic
{
	public class ReadOnlySet<T> : IReadOnlySet<T>
	{
		private readonly ISet<T> source;

		public ReadOnlySet(ISet<T> source)
		{

			if (source == null)
				throw new ArgumentNullException(nameof(source));
			this.source = source;
		}

		/// <summary>Returns an enumerator that iterates through the collection.</summary>
		/// <returns>An enumerator that can be used to iterate through the collection.</returns>
		public IEnumerator<T> GetEnumerator() => this.source.GetEnumerator();

		/// <summary>Returns an enumerator that iterates through a collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <summary>Gets the number of elements in the collection.</summary>
		/// <returns>The number of elements in the collection. </returns>
		public int Count => this.source.Count;

		/// <summary>Determines whether a set is a subset of a specified collection.</summary>
		/// <returns>true if the current set is a subset of <paramref name="other" />; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool IsSubsetOf(IEnumerable<T> other) => this.source.IsSubsetOf(other);

		/// <summary>Determines whether the current set is a superset of a specified collection.</summary>
		/// <returns>true if the current set is a superset of <paramref name="other" />; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool IsSupersetOf(IEnumerable<T> other) => this.source.IsSupersetOf(other);

		/// <summary>Determines whether the current set is a proper (strict) superset of a specified collection.</summary>
		/// <returns>true if the current set is a proper superset of <paramref name="other" />; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool IsProperSupersetOf(IEnumerable<T> other) => this.source.IsProperSupersetOf(other);

		/// <summary>Determines whether the current set is a proper (strict) subset of a specified collection.</summary>
		/// <returns>true if the current set is a proper subset of <paramref name="other" />; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool IsProperSubsetOf(IEnumerable<T> other) => this.source.IsProperSubsetOf(other);

		/// <summary>Determines whether the current set overlaps with the specified collection.</summary>
		/// <returns>true if the current set and <paramref name="other" /> share at least one common element; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool Overlaps(IEnumerable<T> other) => this.source.Overlaps(other);

		/// <summary>Determines whether the current set and the specified collection contain the same elements.</summary>
		/// <returns>true if the current set is equal to <paramref name="other" />; otherwise, false.</returns>
		/// <param name="other">The collection to compare to the current set.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="other" /> is null.</exception>
		public bool SetEquals(IEnumerable<T> other) => this.source.SetEquals(other);
	}
}
