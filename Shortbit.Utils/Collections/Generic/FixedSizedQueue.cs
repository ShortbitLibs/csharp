﻿// /*
//  * FixedSizedQueue.cs
//  *
//  *  Created on: 17:35
//  *         Author: 
//  */

using System.Collections.Generic;

namespace Shortbit.Utils.Collections.Generic
{
	public class FixedSizedQueue<T> : Queue<T>
	{
		public int Size { get; set; }

		public FixedSizedQueue(int size)
		{
			Size = size;
		}

		public new void Enqueue(T obj)
		{
			base.Enqueue(obj);
			while (base.Count > Size)
			{
				base.Dequeue();
			}
		}
	}
}