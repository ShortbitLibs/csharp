﻿// /*
//  * PriorityQueue.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Shortbit.Utils.Collections.Generic
{
	[Obsolete("This class is deprecated. Please use the Heap class, instead.")]
	public class PriorityQueue<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
	{
		private SortedDictionary<TKey, List<TValue>> internalList;

		public int Count { get; private set; }

		public PriorityQueue()
		{
			internalList = new SortedDictionary<TKey, List<TValue>>();
			Count = 0;
		}

		public void Enqueue(TKey key, TValue value)
		{
			if (!internalList.ContainsKey(key))
				internalList.Add(key, new List<TValue>());

			internalList[key].Add(value);
			Count++;
		}

		public TValue Dequeue()
		{
			if (this.Count == 0)
				throw new InvalidOperationException();

			KeyValuePair<TKey, List<TValue>> firstEntry = this.internalList.GetEnumerator().Current;

			TValue val = firstEntry.Value[0];
			firstEntry.Value.RemoveAt(0);

			// Once the Queue gets empty, remove it from the Dictionary...
			if (firstEntry.Value.Count == 0)
				internalList.Remove(firstEntry.Key);

			this.Count--;
			return val;
		}

		public TValue Peek()
		{
			if (this.Count == 0)
				throw new InvalidOperationException();

			return this.internalList.GetEnumerator().Current.Value[0];
		}

		public void Remove(KeyValuePair<TKey, TValue> pair)
		{
			this.Remove(pair.Key, pair.Value);
		}

		public void Remove(TKey key, TValue value)
		{
			if (!this.internalList.ContainsKey(key))
				throw new InvalidOperationException();

			List<TValue> currentQueue = this.internalList[key];
			if (!currentQueue.Contains(value))
				throw new InvalidOperationException();

			currentQueue.Remove(value);
		}


		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		public class Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>
		{
			private IEnumerator<KeyValuePair<TKey, List<TValue>>> dictEnumerator;
			private IEnumerator<TValue> listEnumerator;

			private PriorityQueue<TKey, TValue> queue;

			public Enumerator(PriorityQueue<TKey, TValue> queue)
			{
				this.queue = queue;

				this.Reset();
			}

			public KeyValuePair<TKey, TValue> Current
			{
				get { return new KeyValuePair<TKey, TValue>(dictEnumerator.Current.Key, listEnumerator.Current); }
			}

			public void Dispose()
			{
			}

			object IEnumerator.Current
			{
				get { return this.Current; }
			}

			public bool MoveNext()
			{
				if (listEnumerator.MoveNext())
					return true;

				if (dictEnumerator.MoveNext())
				{
					listEnumerator = dictEnumerator.Current.Value.GetEnumerator();
					return true;
				}

				return false;
			}

			public void Reset()
			{
				dictEnumerator = queue.internalList.GetEnumerator();
				listEnumerator = dictEnumerator.Current.Value.GetEnumerator();
			}
		}
	}
}