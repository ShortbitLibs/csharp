﻿using System;
using System.Collections.Generic;

namespace Shortbit.Utils.Collections.Generic
{
    public class CollectionDifference<T>
    {
        public IEnumerable<Tuple<T, T>> Shared { get; set; }

        public IEnumerable<T> OnlyInSelf { get; set; }

        public IEnumerable<T> OnlyInOther { get; set; }
    }
}
