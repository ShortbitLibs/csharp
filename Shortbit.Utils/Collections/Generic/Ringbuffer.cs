﻿// /*
//  * Ringbuffer.cs
//  *
//  *  Created on: 08:32
//  *         Author: 
//  */

using System.Threading;

namespace Shortbit.Utils.Collections.Generic
{
	public class Ringbuffer<T> : Disposable
	{
		private T[] buffer;

		private int read;
		private int write;

		private int used;

		private readonly AutoResetEvent readEvent = new AutoResetEvent(false);
		private readonly AutoResetEvent writeEvent = new AutoResetEvent(false);

		private readonly object lockSync = new object();

		public int Size
		{
			get { return this.buffer.Length; }
		}

		public int Used
		{
			get { return this.used; }
		}

		public bool DataAvailable
		{
			get { return this.Used > 0; }
		}

		public bool SpaceAvailable
		{
			get { return this.Used < this.Size; }
		}

		public Ringbuffer(int size)
		{
			this.buffer = new T[size];

			this.used = 0;
			this.read = 0;
			this.write = 0;
		}

		public void Write(T value)
		{
			if (!this.SpaceAvailable)
				this.readEvent.WaitOne();

			this.readEvent.WaitOne(0); // Resets the event

			lock (this.lockSync)
			{
				this.buffer[this.write] = value;
				this.write++;
				this.write %= this.Size;

				this.used++;
			}
			this.writeEvent.Set();
		}

		public T Read()
		{
			while (!this.DataAvailable)
				this.writeEvent.WaitOne();
			this.writeEvent.WaitOne(0); // Resets the event

			T ret;
			lock (this.lockSync)
			{
				ret = this.buffer[this.read];
				this.read++;
				this.read %= this.Size;

				this.used--;
			}
			this.readEvent.Set();

			return ret;
		}
	}
}