﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils.Collections.Generic
{
	public class MemberKeyedDictionary <TKey, TElement> : ICollection<TElement>, IReadOnlyDictionary<TKey, TElement>
	{
		private readonly Func<TElement, TKey> memberAccess;
		private readonly Dictionary<TKey, TElement> elements;

		public MemberKeyedDictionary(Func<TElement, TKey> memberAccess, int capacity = 0)
		{
			this.memberAccess = memberAccess;
			this.elements = new Dictionary<TKey, TElement>(capacity);
		}
		public MemberKeyedDictionary(Func<TElement, TKey> memberAccess, ICollection<TElement> source)
		 : this(memberAccess, source?.Count ?? 0)
		{
			source = Throw.IfNull(source, nameof(source));
			foreach (var e in source)
				this.elements[this.memberAccess(e)] = e;
		}
		

		/// <inheritdoc />
		IEnumerator<KeyValuePair<TKey, TElement>> IEnumerable<KeyValuePair<TKey, TElement>>.GetEnumerator() =>
			this.elements.GetEnumerator();

		/// <inheritdoc />
		public IEnumerator<TElement> GetEnumerator() => this.elements.Values.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <inheritdoc />
		public void Add(TElement item)
		{
			this.elements.Add(this.memberAccess(item), item);
		}

		public void AddOrSet(TElement item)
		{
			this.elements[this.memberAccess(item)] = item;
		}

		/// <inheritdoc />
		public void Clear()
		{
			this.elements.Clear();
		}

		/// <inheritdoc />
		public bool Contains(TElement item) => this.elements.ContainsValue(item);

		/// <inheritdoc />
		public void CopyTo(TElement[] array, int arrayIndex)
		{
			this.elements.Values.CopyTo(array, arrayIndex);
		}

		/// <inheritdoc />
		public bool Remove(TElement item) => throw new NotImplementedException();

		/// <inheritdoc />
		public int Count => this.elements.Count;

		/// <inheritdoc />
		public bool IsReadOnly => false;

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.elements.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TElement value)
		{
			return this.elements.TryGetValue(key, out value);
		}

		/// <inheritdoc />
		public TElement this[TKey key] => this.elements[key];

		/// <inheritdoc />
		public IEnumerable<TKey> Keys => this.elements.Keys;

		/// <inheritdoc />
		public IEnumerable<TElement> Values => this.elements.Values;
	}
}
