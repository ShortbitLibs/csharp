﻿// /*
//  * WeakReference.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Shortbit.Utils
{
#if NET40
	public sealed class WeakReference<T> : ISerializable
	{
		private readonly WeakReference storage;

		public bool IsAlive => storage.IsAlive;

		public T Target
		{
			set => storage.Target = value;
			get => (T) storage.Target;
		}

		public bool TrackResurrection => storage.TrackResurrection;

		public WeakReference(T target)
		{
			storage = new WeakReference(target);
		}

		public WeakReference(T target, bool trackResurrection)
		{
			storage = new WeakReference(target, trackResurrection);
		}
		

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with all the data necessary to serialize the current <see cref="T:System.WeakReference`1" /> object.</summary>
		/// <param name="info">An object that holds all the data necessary to serialize or deserialize the current <see cref="T:System.WeakReference`1" /> object.</param>
		/// <param name="context">The location where serialized data is stored and retrieved.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="info" /> is <see langword="null" />.</exception>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			storage.GetObjectData(info, context);
		}

		/// <summary>Sets the target object that is referenced by this <see cref="T:System.WeakReference`1" /> object.</summary>
		/// <param name="target">The new target object.</param>
		public void SetTarget(T target)
		{
			this.Target = target;
		}

		/// <summary>Tries to retrieve the target object that is referenced by the current <see cref="T:System.WeakReference`1" /> object.</summary>
		/// <param name="target">When this method returns, contains the target object, if it is available. This parameter is treated as uninitialized.</param>
		/// <returns>
		/// <see langword="true" /> if the target was retrieved; otherwise, <see langword="false" />.</returns>
		public bool TryGetTarget(out T target)
		{
			var alive = this.IsAlive;
			target = alive ? (T)this.storage.Target : default(T);
			return alive;
		}
	}
#endif
}