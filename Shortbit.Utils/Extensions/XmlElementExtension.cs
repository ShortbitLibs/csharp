﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Shortbit.Utils
{
    public static class XmlElementExtension
    {
        public static IEnumerable<XmlElement> GetXmlElementsByTagName(this XmlElement self, string tagName)
        {
            return self.GetElementsByTagName(tagName).Cast<XmlElement>();
        }
        public static XmlElement GetElementByTagName(this XmlElement self, string tagName)
        {
            var elements = self.GetElementsByTagName(tagName);
            if (elements.Count == 0)
                return null;
            return (XmlElement)elements[0];
        }

        public static IEnumerable<XmlElement> ChildElements(this XmlElement self)
        {
            return self.ChildNodes.OfType<XmlElement>();
        }
        public static XmlElement FirstChildElement(this XmlElement self)
        {
            IEnumerable<XmlElement> children = self.ChildElements();
            return children.FirstOrDefault();
        }

        public static XmlElement LastChildElement(this XmlElement self)
        {
            IEnumerable<XmlElement> children = self.ChildElements();
            return children.LastOrDefault();
        }
    }
}
