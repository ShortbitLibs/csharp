﻿// /*
//  * NumericsExtension.cs
//  *
//  *  Created on: 22:56
//  *         Author: 
//  */

namespace Shortbit.Utils
{
	public static class NumericsExtension
	{
		public static bool Within(this short value, short lower, short upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this int value, int lower, int upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this long value, long lower, long upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this ushort value, ushort lower, ushort upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this uint value, uint lower, uint upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this ulong value, ulong lower, ulong upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this float value, float lower, float upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this double value, double lower, double upper)
		{
			return MathEx.Within(value, lower, upper);
		}

		public static bool Within(this decimal value, decimal lower, decimal upper)
		{
			return MathEx.Within(value, lower, upper);
		}


		public static short Clamp(this short value, short lower, short upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static int Clamp(this int value, int lower, int upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static long Clamp(this long value, long lower, long upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static ushort Clamp(this ushort value, ushort lower, ushort upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static uint Clamp(this uint value, uint lower, uint upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static ulong Clamp(this ulong value, ulong lower, ulong upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static float Clamp(this float value, float lower, float upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static double Clamp(this double value, double lower, double upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}

		public static decimal Clamp(this decimal value, decimal lower, decimal upper)
		{
			return MathEx.Clamp(value, lower, upper);
		}
	}
}