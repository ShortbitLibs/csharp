﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace Shortbit.Utils
{
	public static class NameValueCollectionExtension
	{
		public static Dictionary<string, List<String>> ToDictionary(this NameValueCollection self)
		{
			var res = new Dictionary<string, List<string>>();

			foreach (var key in self.AllKeys)
			{
				foreach (var value in self.GetValues(key))
				{ 
					if (string.IsNullOrWhiteSpace(value))
						continue;

					if (!res.ContainsKey(key))
						res.Add(key, new List<string>());
					res[key].Add(value);
				}
			}
			return res;
		}

		#if !NET40
		public static string UrlEncode(this NameValueCollection self)
		{
			var array = (from key in self.AllKeys
					from value in self.GetValues(key)
					select $"{WebUtility.UrlEncode(key)}={WebUtility.UrlEncode(value)}")
				.ToArray();

			return string.Join("&", array);
		}

		public static void UrlDecode(this NameValueCollection self, string query)
		{
			if (string.IsNullOrEmpty(query))
				return;
			if (query.StartsWith("?"))
				query = query.Substring(1);

			var parts = query.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries);
			foreach (var part in parts)
			{
				var index = part.IndexOf("=");
				var key = WebUtility.UrlDecode(part.Substring(0, index));
				var value = WebUtility.UrlDecode(part.Substring(index + 1));
				self.Add(key, value);
			}
		}
		#endif
	}
}
