﻿// /*
//  * IListExtension.cs
//  *
//  *  Created on: 05:58
//  *         Author: 
//  */

using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils
{
	public static class IListExtension
	{
		public static void RemoveFirst<T>(this IList<T> me)
		{
			if (me.Any())
				me.RemoveAt(0);
		}

		public static void RemoveLast<T>(this IList<T> me)
		{
			if (me.Any())
				me.RemoveAt(me.Count - 1);
		}

		public static T Choice<T>(this IList<T> input, bool removeUsed = true, System.Random random = null)
		{
			if (input == null || input.Count == 0)
				return default(T);

			if (random == null)
				random = new System.Random();

			int index = random.Next(input.Count);
			var item = input[index];
			if (removeUsed)
				input.RemoveAt(index);

			return item;
		}

		public static void Shuffle<T>(this IList<T> me, System.Random random = null)
		{
			if (me == null || me.Count == 0)
				return;

			if (random == null)
				random = new System.Random();

			int n = me.Count;
			while (n > 1)
			{
				n--;
				int k = random.Next(n + 1);
				T value = me[k];
				me[k] = me[n];
				me[n] = value;
			}
		}
	}
}