﻿// /*
//  * TypeExtension.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Shortbit.Utils
{
	public static class TypeExtension
	{
		public static IEnumerable<T> GetCustomAttributes<T>(this ICustomAttributeProvider t) where T : Attribute
		{
			return t.GetCustomAttributes<T>(true);
		}

		public static IEnumerable<T> GetCustomAttributes<T>(this ICustomAttributeProvider t, bool inherit) where T : Attribute
		{
			return t.GetCustomAttributes(typeof (T), inherit).Cast<T>();
        }


        public static bool HasCustomAttribute<T>(this ICustomAttributeProvider t) where T : Attribute
		{
			return t.HasCustomAttribute<T>(true);
		}

		public static bool HasCustomAttribute<T>(this ICustomAttributeProvider t, bool inherit) where T : Attribute
		{
			return t.GetCustomAttributes<T>(inherit).Any();
		}

		public static T GetCustomAttribute<T>(this ICustomAttributeProvider t) where T : Attribute
		{
			return t.GetCustomAttribute<T>(true);
		}

		public static T GetCustomAttribute<T>(this ICustomAttributeProvider t, bool inherit) where T : Attribute
		{
			return t.GetCustomAttributes<T>(inherit).First();
		}

		public static bool CanConvertTo(this Type fromType, Type toType)
		{
			try
			{
				// Throws an exception if there is no conversion from fromType to toType
				Expression.Convert(Expression.Parameter(fromType, null), toType);
				return true;
			}
			catch
			{
				return false;
			}
		}


		public static Boolean IsAnonymousType(this Type type)
		{
			var hasCompilerGeneratedAttribute = type.GetCustomAttributes(typeof (CompilerGeneratedAttribute), false).Any();
			var isSealed = type.IsSealed;
			var namespaceNull = type.Namespace == null;
			var isAnonymousType = hasCompilerGeneratedAttribute && isSealed && namespaceNull;

			return isAnonymousType;
		}


	    public static ConstructorInfo GetConstructor(this Type type, params Type[] types)
	    {
	        return type.GetConstructor(types);
	    }
	    public static MethodInfo GetMethod(this Type type, string name, params Type[] types)
	    {
	        return type.GetMethod(name, types);
	    }
        

        public static MethodInfo GetMethod(this Type me, string name, int numGenericParameters,
            params Type[] types)
        {
            return GetMethod(me.GetMethods().Where(m => m.Name == name), numGenericParameters, types);
        }
        public static MethodInfo GetMethod(this Type me, string name, BindingFlags flags, int numGenericParameters,
            params Type[] types)
	    {
	        return GetMethod(me.GetMethods(flags).Where(m => m.Name == name), numGenericParameters, types);
        }

        /// <summary>
        /// Kudos to http://stackoverflow.com/a/9496602
        /// </summary>
        /// <param name="methods"></param>
        /// <param name="numGenericParameters"></param>
        /// <param name="types"></param>
        /// <returns></returns>

        private static MethodInfo GetMethod(IEnumerable<MethodInfo> methods, int numGenericParameters,
	        params Type[] types)
	    {
            foreach (var method in methods)
            {
                // set a flag here, which will eventually be false if the method isn't a match.
                var correct = true;
                if (method.IsGenericMethodDefinition)
                {
                    // map the "private" Type objects which are the type parameters to
                    // my public "Tx" classes...
                    var d = new Dictionary<Type, Type>();
                    var args = method.GetGenericArguments();
                    if (args.Length > Refl.T.Length)
                        throw new NotSupportedException("Too many type parameters.");

                    // Number of generic arguments does not match...
                    if (args.Length != numGenericParameters)
                        continue;

                    for (int i = 0; i < args.Length; i++)
                        d[Refl.T[i]] = args[i];

                    var p = method.GetParameters();
                    for (var i = 0; i < p.Length; i++)
                    {
                        // Find the Refl.TX classes and replace them with the 
                        // actual type parameters.
                        var pt = Substitute(types[i], d);
                        // Then it's a simple equality check on two Type instances.
                        if (pt != p[i].ParameterType)
                        {
                            correct = false;
                            break;
                        }
                    }
                    if (correct)
                        return method;
                }
                else
                {
                    var p = method.GetParameters();
                    for (var i = 0; i < p.Length; i++)
                    {
                        var pt = types[i];
                        if (pt != p[i].ParameterType)
                        {
                            correct = false;
                            break;
                        }
                    }
                    if (correct)
                        return method;
                }
            }
            return null;
        }

        private static Type Substitute(Type t, IDictionary<Type, Type> env)
        {

            if (t.IsGenericParameter)
            {
                t = Refl.T[t.GenericParameterPosition];
            }

            // We only really do something if the type 
            // passed in is a (constructed) generic type.
            if (t.IsGenericType)
            {
                var targs = t.GetGenericArguments();
                for (int i = 0; i < targs.Length; i++)
                    targs[i] = Substitute(targs[i], env); // recursive call
                t = t.GetGenericTypeDefinition();
                t = t.MakeGenericType(targs);
            }
            // see if the type is in the environment and sub if it is.
            return env.ContainsKey(t) ? env[t] : t;
        }
       


        public static T GetConstructorDelegate<T>(this Type type)
	    {
            var invoke = typeof (T).GetMethod("Invoke");
	        var parameterTypes = invoke.GetParameters().Select(pi => pi.ParameterType).ToArraySafe();

	        return type.GetConstructor(parameterTypes).CreateDelegate<T>();
	    }

	    public static T GetMethodDelegate<T>(this Type type, string method)
        {
            var invoke = typeof(T).GetMethod("Invoke");
            var parameterTypes = invoke.GetParameters().Select(pi => pi.ParameterType).ToArraySafe();
            
	        return type.GetMethod(method, parameterTypes).CreateDelegate<T>();
        }

		/*	public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo t) where T : Attribute { return GetCustomAttributes<T>((ICustomAttributeProvider)t); }
			public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo t, bool inherit) where T : Attribute { return GetCustomAttributes<T>((ICustomAttributeProvider)t, inherit); }

			public static bool HasCustomAttribute<T>(this MemberInfo t) where T : Attribute { return HasCustomAttribute<T>((ICustomAttributeProvider)t); }
			public static bool HasCustomAttribute<T>(this MemberInfo t, bool inherit) where T : Attribute { return HasCustomAttribute<T>((ICustomAttributeProvider)t, inherit); }

			public static T GetCustomAttribute<T>(this MemberInfo t) where T : Attribute { return GetCustomAttribute<T>((ICustomAttributeProvider)t); }
			public static T GetCustomAttribute<T>(this MemberInfo t, bool inherit) where T : Attribute { return GetCustomAttribute<T>((ICustomAttributeProvider)t, inherit); }


			public static IEnumerable<T> GetCustomAttributes<T>(this System.Type t) where T : Attribute { return GetCustomAttributes<T>((ICustomAttributeProvider)t); }
			public static IEnumerable<T> GetCustomAttributes<T>(this System.Type t, bool inherit) where T : Attribute { return GetCustomAttributes<T>((ICustomAttributeProvider)t, inherit); }

			public static bool HasCustomAttribute<T>(this System.Type t) where T : Attribute { return HasCustomAttribute<T>((ICustomAttributeProvider)t); }
			public static bool HasCustomAttribute<T>(this System.Type t, bool inherit) where T : Attribute { return HasCustomAttribute<T>((ICustomAttributeProvider)t, inherit); }

			public static T GetCustomAttribute<T>(this System.Type t) where T : Attribute { return GetCustomAttribute<T>((ICustomAttributeProvider)t); }
			public static T GetCustomAttribute<T>(this System.Type t, bool inherit) where T : Attribute { return GetCustomAttribute<T>((ICustomAttributeProvider)t, inherit); }*/
	}
}