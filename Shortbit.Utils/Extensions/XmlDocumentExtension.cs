﻿// /*
//  * XmlDocumentExtension.cs
//  *
//  *  Created on: 21:01
//  *         Author: 
//  */

using System.Xml;

namespace Shortbit.Utils
{
	public static class XmlDocumentExtension
	{
		public static void AppendDefaultXmlDeclaration(this XmlDocument doc)
		{
			doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));
		}
	}
}