﻿using System.Linq;
using System.Xml.Linq;

namespace Shortbit.Utils
{
    public static class XContainerExtension
    {
        public static XElement Descendant(this XContainer me)
        {
            return me.Descendants().First();
        }
        public static XElement Descendant(this XContainer me, XName name)
        {
            return me.Descendants(name).First();
        }
        public static XElement DescendantOrDefault(this XContainer me)
        {
            return me.Descendants().FirstOrDefault();
        }
        public static XElement DescendantOrDefault(this XContainer me, XName name)
        {
            return me.Descendants(name).FirstOrDefault();
        }
    }
}
