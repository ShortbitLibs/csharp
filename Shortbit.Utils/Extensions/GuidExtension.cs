﻿using System;

namespace Shortbit.Utils
{
	public static class GuidExtension
	{
		public static string ToShortString(this Guid self, int segments = 1)
		{
			if (segments < 1 || segments > 5)
				throw new ArgumentOutOfRangeException(nameof(segments));


			var txt = self.ToString("D");
			if (segments == 5)
				return txt;

			int idx = 0;
			for (int i = 0; i < segments; i++)
				idx = txt.IndexOf("-", idx + 1, StringComparison.Ordinal);

			return txt.Substring(0, idx);
		}
	}
}
