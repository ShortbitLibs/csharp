﻿// /*
//  * DirectoryInfoExtension.cs
//  *
//  *  Created on: 22:34
//  *         Author: 
//  */

using System.IO;

namespace Shortbit.Utils
{
	public static class DirectoryInfoExtension
	{
		public static void Empty(this DirectoryInfo directory)
		{
			directory.DeleteFiles();
			directory.DeleteDirectories();
		}

		public static void DeleteFiles(this DirectoryInfo directory)
		{
			foreach (FileInfo file in directory.GetFiles()) file.Delete();
		}

		public static void DeleteDirectories(this DirectoryInfo directory)
		{
			foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
		}
	}
}