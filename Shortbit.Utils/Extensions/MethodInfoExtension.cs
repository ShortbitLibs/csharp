﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Shortbit.Utils
{
    public static class MethodInfoExtension
    {
        public static T CreateDelegate<T>(this MethodInfo method)
        {
            return ExpressionEx.CreateDelegate<T, MethodInfo>(method, Expression.Call);
        }

        public static Type[] GetParameterTypes(this MethodInfo me)
        {
            return me.GetParameters().Select(pt => pt.ParameterType).ToArray();
        }
    }
}
