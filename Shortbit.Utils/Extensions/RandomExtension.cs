﻿// /*
//  * RandomExtension.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils
{
	public static class RandomExtension
	{
		/// <summary>
		///     Generates a random boolean with a 50% chace for either true or false.
		/// </summary>
		/// <param name="random">The random number generator.</param>
		/// <returns>A random boolean value.</returns>
		public static bool NextBoolean(this Random random)
		{
			return random.NextBoolean(0.5);
		}

		/// <summary>
		///     Generates a random boolean where
		///     <value>true</value>
		///     has the given chance.
		/// </summary>
		/// <param name="random">The random number generator.</param>
		/// <param name="chance">The chance for a
		///     <value>true</value>
		///     value.
		/// </param>
		/// <returns>A random boolean value.</returns>
		public static bool NextBoolean(this Random random, double chance)
		{
			if ((chance <= 0.0) || (chance >= 1.0))
				throw new ArgumentException("chance argument must be between 0.0 and 1.0");

			return random.NextDouble() < chance; // NextDouble is 0.0 <= x < 1.0
		}


		/// <summary>
		///     Selects a random element of the given list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="random"></param>
		/// <param name="enumerable"></param>
		/// <returns></returns>
		[Obsolete("Use IList.Choice instead.")]
		public static T SelectRandom<T>(this Random random, IEnumerable<T> enumerable)
		{
			var list = enumerable as IList<T> ?? enumerable.ToList();
			int chance = random.Next(list.Count); // Don't skip through last element...
			return list[chance];
		}

		/// <summary>
		///     Select a random element from the given enumerable considering the weight of each element.
		///     The chance of each element getting selected is its weight divied by the total weight of all elements.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="random"></param>
		/// <param name="enumerable"></param>
		/// <param name="weightFunc"></param>
		/// <returns></returns>
		public static T SelectRouletteWheel<T>(this Random random, IEnumerable<T> enumerable, Func<T, double> weightFunc)
		{
			var list = enumerable as IList<T> ?? enumerable.ToList();

			var cumulativeWeight = new double[list.Count()];
			cumulativeWeight[0] = weightFunc(list[0]);
			for (int i = 1; i < list.Count; i++)
				cumulativeWeight[i] = cumulativeWeight[i - 1] + weightFunc(list[i]);


			double selection = random.NextDouble()*cumulativeWeight[list.Count - 1];

			int index = Array.BinarySearch(cumulativeWeight, selection);
			if (index < 0) // value itself not found, so the bitwise-complement index of the next bigger item was returned...
				index = ~index;

			// NOTE: No need to check for (index == list.Count).
			// It can't happen cause Random.NextDouble() generates a value < 1 (therefore selection < cumulativeWeight[list.Count - 1])

			return list[index];
		}


		/// <summary>
		///     Generates a randum number that follows Benford's law of empirical numbers.
		/// </summary>
		/// <param name="random">The random number generator.</param>
		/// <param name="length">The number of digits.</param>
		/// <returns></returns>
		public static int NextEmpirical(this Random random, uint length)
		{
			int ret = 0;

			var digits = new List<int>();
			for (int i = 0; i < 10; i++)
				digits.Add(i);

			for (int i = 0; i < length; i++)
			{
				uint n = (uint) i;
				ret += random.SelectRouletteWheel(digits, d => MathEx.BenfordsProbability((uint) d, n));

				ret *= 10;
			}

			return ret;
		}
	}
}