﻿// /*
//  * ObjectExtension.cs
//  *
//  *  Created on: 00:54
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils
{
	public static class ObjectExtension
	{
		public static Dictionary<string, object> ToDictionary(this object me)
		{
			if (!me.GetType().IsAnonymousType())
				throw new ArgumentException("Given object must be anonymous type.");
			return me.GetType().GetProperties().ToDictionary(x => x.Name, x => x.GetValue(me, null));
		}
	}
}