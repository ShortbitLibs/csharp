﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Shortbit.Utils
{
    public static class HashAlgorithmExtension
    {
        public static void Update(this HashAlgorithm me, string input, bool final = false)
        {
            me.Update(Encoding.UTF8.GetBytes(input), final);
        }

        public static void Update(this HashAlgorithm me, float input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        public static void Update(this HashAlgorithm me, double input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }

        public static void Update(this HashAlgorithm me, bool input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }

        // public static void Update(this HashAlgorithm me, sbyte input, bool final = false)
        // {
        //     me.Update(BitConverter.GetBytes(input), final);
        // }
        public static void Update(this HashAlgorithm me, short input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        public static void Update(this HashAlgorithm me, int input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        public static void Update(this HashAlgorithm me, long input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        // public static void Update(this HashAlgorithm me, byte input, bool final = false)
        // {
        //     me.Update(BitConverter.GetBytes(input), final);
        // }
        public static void Update(this HashAlgorithm me, ushort input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        public static void Update(this HashAlgorithm me, uint input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }
        public static void Update(this HashAlgorithm me, ulong input, bool final = false)
        {
            me.Update(BitConverter.GetBytes(input), final);
        }

        public static void Update(this HashAlgorithm me, byte[] input, bool final = false)
        {
            if (final)
                me.TransformFinalBlock(input, 0, input.Length);
            else
                me.TransformBlock(input, 0, input.Length, input, 0);
        }

    }
}
