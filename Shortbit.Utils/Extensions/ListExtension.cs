﻿// /*
//  * ListExtension.cs
//  *
//  *  Created on: 05:58
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Shortbit.Utils
{
// ReSharper disable once InconsistentNaming
	public static class ListExtension
	{
		public static List<T> PadFront<T>(this List<T> me, int size)
		{
			return me.PadFront(size, default(T));
		}

		public static List<T> PadFront<T>(this List<T> me, int size, T @default)
		{
			int num = Math.Max(size - me.Count, 0);
			for (int i = 0; i < num; i++)
				me.Insert(0, @default);

			return me;
		}

		public static List<T> PadBack<T>(this List<T> me, int size)
		{
			return me.PadBack(size, default(T));
		}

		public static List<T> PadBack<T>(this List<T> me, int size, T @default)
		{
			int num = Math.Max(size - me.Count, 0);
			for (int i = 0; i < num; i++)
				me.Add(@default);
			return me;
		}


		public static List<T> TrimFront<T>(this List<T> me, int size)
		{
			int num = Math.Max(me.Count - size, 0);
			me.RemoveRange(0, num);
			return me;
		}

		public static List<T> TrimBack<T>(this List<T> me, int size)
		{
			int num = Math.Max(me.Count - size, 0);
			me.RemoveRange(me.Count - num, num);
			return me;
		}

		public static void Resize<T>(this List<T> list, int sz, T c = default(T))
		{
			int cur = list.Count;
			if (sz < cur)
				list.RemoveRange(sz, cur - sz);
			else if (sz > cur)
				list.AddRange(Enumerable.Repeat(c, sz - cur));
		}
	}
}