﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Utils.Collections.Generic;

#if NETCOREAPP
namespace Shortbit.Utils
{
	public static class IAsyncEnumerableExtension
	{
		
		
		public static async Task<List<T>> ToListSafe<T>(this IAsyncEnumerable<T> self, [EnumeratorCancellation] CancellationToken token = default)
		{
			var res = new List<T>();
			await foreach (var element in self.WithCancellation(token))
			{
				res.Add(element);
			}

			return res;
		}
		/*
		
		public static IAsyncEnumerable<T> Where<T>(this IAsyncEnumerable<T> self, Func<T, bool> predicate, [EnumeratorCancellation] CancellationToken token = default)
		{
			return self.Where((t, i) => predicate(t), token);
		}
		public static async IAsyncEnumerable<T> Where<T>(this IAsyncEnumerable<T> self, Func<T, int, bool> predicate, [EnumeratorCancellation] CancellationToken token = default)
		{
			if (self == null)
				throw new ArgumentNullException(nameof(self));

			int i = 0;
			await foreach (var e in self.WithCancellation(token))
			{
				if (predicate(e, i))
					yield return e;
				i++;
			}
		}


		public static IAsyncEnumerable<TResult> Select<TSource, TResult>(this IAsyncEnumerable<TSource> self, Func<TSource, TResult> selector, [EnumeratorCancellation] CancellationToken token = default)
		{
			return self.Select((t, i) => selector(t), token);
		}
		public static async IAsyncEnumerable<TResult> Select<TSource, TResult>(this IAsyncEnumerable<TSource> self, Func<TSource, int, TResult> selector, [EnumeratorCancellation] CancellationToken token = default)
		{
			if (self == null)
				throw new ArgumentNullException(nameof(self));

			int i = 0;
			await foreach (var e in self.WithCancellation(token))
			{
				yield return selector(e, i);
				i++;
			}
		}

		public static async IAsyncEnumerable<T> Concat<T>(this IAsyncEnumerable<T> self, IAsyncEnumerable<T> other, [EnumeratorCancellation] CancellationToken token = default)
		{
			if (self == null)
				throw new ArgumentNullException(nameof(self));
			if (other == null)
				throw new ArgumentNullException(nameof(other));
			await foreach (var e in self.WithCancellation(token))
				yield return e;
			await foreach (var e in other.WithCancellation(token))
				yield return e;
		}
		

		
		public static async Task<TSource> Aggregate<TSource>(this IAsyncEnumerable<TSource> self,
																  Func<TSource, TSource, TSource> func, CancellationToken token)
		{
			if (self == null)
				throw new ArgumentNullException(nameof(self));
			if (func == null)
				throw new ArgumentNullException(nameof(func));

			TSource accumulate = default;
			var accumulateSet = false;
			await foreach (var s in self.WithCancellation(token))
			{
				if (!accumulateSet)
				{
					accumulate = s;
					accumulateSet = true;
				}
				else
					accumulate = func(accumulate, s);

			}

			return accumulate;
		}
		
		
		public static async Task<TAccumulate> Aggregate<TSource, TAccumulate>(this IAsyncEnumerable<TSource> self,
																 TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func, CancellationToken token)
		{
			if (self == null)
				throw new ArgumentNullException(nameof(self));
			if (func == null)
				throw new ArgumentNullException(nameof(func));
			
			TAccumulate accumulate = seed;
			await foreach (var s in self.WithCancellation(token))
			{
				accumulate = func(accumulate, s);
			}

			return accumulate;
		}
		*/
		
	}
}
#endif