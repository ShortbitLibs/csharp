﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Shortbit.Utils
{
	public static class PropertyInfoExtension
	{

		public static bool IsStatic(this PropertyInfo source, bool nonPublic = false) 
			=> source.GetAccessors(nonPublic).Any(x => x.IsStatic);

		public static Func<T, TReturn> BuildTypedGetter<T, TReturn>(this PropertyInfo propertyInfo)
		{
			Func<T, TReturn> reflGet = (Func<T, TReturn>)
				Delegate.CreateDelegate(typeof(Func<T, TReturn>), propertyInfo.GetGetMethod());
			return reflGet;
		}

		public static Action<T, TProperty> BuildTypedSetter<T, TProperty>(this PropertyInfo propertyInfo)
		{
			Action<T, TProperty> reflSet = (Action<T, TProperty>)Delegate.CreateDelegate(
				typeof(Action<T, TProperty>), propertyInfo.GetSetMethod());
			return reflSet;
		}

		public static Action<T, object> BuildUntypedSetter<T>(this PropertyInfo propertyInfo)
		{
			var targetType = propertyInfo.DeclaringType;
			var methodInfo = propertyInfo.GetSetMethod();
			var exTarget = Expression.Parameter(targetType, "t");
			var exValue = Expression.Parameter(typeof(object), "p");
			// wir betreiben ein anObject.SetPropertyValue(object)
			var exBody = Expression.Call(exTarget, methodInfo,
									   Expression.Convert(exValue, propertyInfo.PropertyType));
			var lambda = Expression.Lambda<Action<T, object>>(exBody, exTarget, exValue);
			// (t, p) => t.set_StringValue(Convert(p))

			var action = lambda.Compile();
			return action;
		}
		public static Action<object, object> BuildUntypedSetter(this PropertyInfo propertyInfo)
		{
			var targetType = propertyInfo.DeclaringType;
			var methodInfo = propertyInfo.GetSetMethod();
			var exTarget = Expression.Parameter(typeof(object), "t");
			var exTargetCasted = Expression.Convert(exTarget, targetType);
			var exValue = Expression.Parameter(typeof(object), "p");

			// wir betreiben ein anObject.SetPropertyValue(object)
			var exBody = Expression.Call(exTargetCasted, methodInfo,
									   Expression.Convert(exValue, propertyInfo.PropertyType));
			var lambda = Expression.Lambda<Action<object, object>>(exBody, exTarget, exValue);
			// (t, p) => t.set_StringValue(Convert(p))

			var action = lambda.Compile();
			return action;
		}

		public static Func<T, object> BuildUntypedGetter<T>(this PropertyInfo propertyInfo)
		{
			var targetType = propertyInfo.DeclaringType;
			var methodInfo = propertyInfo.GetGetMethod();
			var returnType = methodInfo.ReturnType;

			var exTarget = Expression.Parameter(targetType, "t");
			var exBody = Expression.Call(exTarget, methodInfo);
			var exBody2 = Expression.Convert(exBody, typeof(object));

			var lambda = Expression.Lambda<Func<T, object>>(exBody2, exTarget);
			// t => Convert(t.get_Foo())

			var action = lambda.Compile();
			return action;
		}
		public static Func<object, object> BuildUntypedGetter(this PropertyInfo propertyInfo)
		{
			var targetType = propertyInfo.DeclaringType;
			var methodInfo = propertyInfo.GetGetMethod();
			var returnType = methodInfo.ReturnType;

			var exTarget = Expression.Parameter(typeof(object), "t");
			var exTargetCasted = Expression.Convert(exTarget, targetType);
			var exBody = Expression.Call(exTargetCasted, methodInfo);
			var exBody2 = Expression.Convert(exBody, typeof(object));

			var lambda = Expression.Lambda<Func<object, object>>(exBody2, exTarget);
			// t => Convert(((T)t).get_Foo())

			var action = lambda.Compile();
			return action;
		}
	}
}
