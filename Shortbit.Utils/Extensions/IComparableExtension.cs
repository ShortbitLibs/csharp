﻿// /*
//  * IComparableExtension.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;

namespace Shortbit.Utils
{
	public static class IComparableExtension
	{
		public static bool IsLessThan<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) < 0;
		}

		public static bool IsLessOrEqual<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) <= 0;
		}

		public static bool IsMoreThan<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) > 0;
		}

		public static bool IsMoreOrEqual<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) >= 0;
		}

		public static bool IsEqual<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) == 0;
		}

		public static bool IsNotEqual<T>(this T me, T other) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.CompareTo(other) != 0;
		}

		public static bool IsBetween<T>(this T me, T max, T min) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.IsMoreOrEqual(min) && me.IsLessOrEqual(max);
		}

		public static bool IsStrictBetween<T>(this T me, T max, T min) where T : IComparable<T>
		{
			if (me == null) throw new ArgumentNullException("this");

			return me.IsMoreThan(min) && me.IsLessThan(max);
		}


		public static T Max<T>(this T me, params T[] others) where T : IComparable<T>
		{
			T max = me;
			foreach (T other in others)
				if (max.IsLessThan(other))
					max = other;
			return max;
		}

		public static T Min<T>(this T me, params T[] others) where T : IComparable<T>
		{
			T min = me;
			foreach (T other in others)
				if (min.IsMoreThan(other))
					min = other;
			return min;
		}

		public static T Clamp<T>(this T me, T max, T min) where T : IComparable<T>
		{
			return me.Max(min).Min(max);
		}
	}
}