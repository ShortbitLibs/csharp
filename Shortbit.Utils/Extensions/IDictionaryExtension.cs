﻿// /*
//  * IDictionaryExtension.cs
//  *
//  *  Created on: 07:23
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
//using System.IO.Ports;
using System.Linq;

namespace Shortbit.Utils
{
	public static class IDictionaryExtension
	{
		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key)
		{
			return me.GetOrDefault(key, default(TValue));
		}

		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key, TValue @default)
		{
			return me.GetOrDefault(key, () => @default);
		}
		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key, Func<TValue> @default)
		{
			if (!me.ContainsKey(key))
				return @default();
			return me[key];
		}

		public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key, TValue @default)
		{
			return me.GetOrCreate(key, () => @default);
		}
		public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> me, TKey key, Func<TValue> @default)
		{
			if (!me.ContainsKey(key))
				me[key] = @default();
			return me[key];
		}


		public static void AddOrSet<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value)
		{
			if (dict.ContainsKey(key))
				dict[key] = value;
			else
				dict.Add(key, value);
		}

		public static void AddRangeOverride<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
		{
			foreach (var pair in dicToAdd)
				dic[pair.Key] = pair.Value;
		}

		public static void AddRangeNewOnly<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
		{
			foreach (var pair in dicToAdd)
			{
				if (!dic.ContainsKey(pair.Key))
					dic.Add(pair.Key, pair.Value);
			}
		}

		public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
		{
			foreach (var pair in dicToAdd)
			{
				dic.Add(pair.Key, pair.Value);
			}
		}

		public static bool ContainsAnyKey<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<TKey> keys)
		{
			foreach (var key in keys)
				if (dic.ContainsKey(key))
					return true;
			return false;
		}
		public static bool ContainsAllKeys<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<TKey> keys)
		{
			foreach (var key in keys)
				if (!dic.ContainsKey(key))
					return false;
			return true;
		}

		#if !NET40
		public static ReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(this IDictionary<TKey, TValue> self)
		{
			return new ReadOnlyDictionary<TKey, TValue>(self);
		}
		#endif
		private static string AssignmentValue<TValue>(TValue value)
		{
			// ReSharper disable once CompareNonConstrainedGenericWithNull
			if (value == null)
				return "null";

			if (value is string)
				return (value as string);

			if (value is IEnumerable)
				return (value as IEnumerable).ToListString();
			return value.ToString();
		}

		public static string ToAssignmentString<TKey, TValue>(this IDictionary<TKey, TValue> dict)
		{
			return "{" + string.Join(", ", dict.Select(pair => pair.Key + "=" + AssignmentValue(pair.Value))) + "}";
		}
	}
}