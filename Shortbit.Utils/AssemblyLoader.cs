﻿// /*
//  * AssemblyLoader.cs
//  *
//  *  Created on: 16:11
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using Shortbit.Utils.WinApi;

namespace Shortbit.Utils
{
	public static class AssemblyLoader
	{
		private abstract class Entry
		{
			public string Name { get; private set; }

			public Entry(String name)
			{
				this.Name = name;
			}

			public abstract String[] Plattforms { get; }
		}

		private abstract class ManagedAssemblyEntry : Entry
		{
			public ManagedAssemblyEntry(String name)
				: base(name)
			{
			}

			public abstract Assembly Load(string plattform);
		}

		private class ManagedAssemblyResource : ManagedAssemblyEntry
		{
			private readonly Dictionary<string, string> plattformAssemblies = new Dictionary<string, string>();


			public override string[] Plattforms
			{
				get { return plattformAssemblies.Keys.ToArray(); }
			}


			public Assembly ContainingAssembly { get; private set; }

			public ManagedAssemblyResource(string name, Assembly containingAssembly)
				: base(name)
			{
				this.ContainingAssembly = containingAssembly;
			}


			public void Add(string plattform, string resource)
			{
				this.plattformAssemblies.Add(plattform, resource);
			}

			public string getResource(string plattform)
			{
				return this.plattformAssemblies[plattform];
			}

			public override Assembly Load(string plattform)
			{
				if (!this.plattformAssemblies.ContainsKey(plattform))
					return null;

				using (Stream stream = this.ContainingAssembly.GetManifestResourceStream(this.plattformAssemblies[plattform]))
				{
					if (stream == null) return null;

					var assemblyData = new Byte[stream.Length];
					stream.Read(assemblyData, 0, assemblyData.Length);
					return Assembly.Load(assemblyData);
				}
			}
		}


		private abstract class NativeAssemblyEntry : Entry
		{
			protected IntPtr handle = IntPtr.Zero;

			public NativeAssemblyEntry(string name)
				: base(name)
			{
			}


			public void Load(string plattform)
			{
				if (this.handle != IntPtr.Zero) return;
				

				string file = this.InitLibrary(plattform);
				this.handle = Kernel32.LoadLibrary(file);
			}

			public abstract string InitLibrary(string plattform);

			public void Unload()
			{
				if (this.handle == IntPtr.Zero) return;

				Kernel32.FreeLibrary(this.handle);
				this.Dispose();
			}

			protected virtual void Dispose()
			{
			}
		}

		private class NativeAssemblyResource : NativeAssemblyEntry
		{
			private readonly Dictionary<string, string> plattformAssemblies = new Dictionary<string, string>();

			public Assembly ContainingAssembly { get; private set; }

			public NativeAssemblyResource(string name, Assembly containingAssembly)
				: base(name)
			{
				this.ContainingAssembly = containingAssembly;
			}

			public override string[] Plattforms
			{
				get { return plattformAssemblies.Keys.ToArray(); }
			}


			public override string InitLibrary(string plattform)
			{
				if (!this.plattformAssemblies.ContainsKey(plattform))
					return null;

				string filename = this.Name + ".dll";

				Byte[] assemblyData;
				using (Stream stream = this.ContainingAssembly.GetManifestResourceStream(this.plattformAssemblies[plattform]))
				{
					if (stream == null) return null;

					assemblyData = new Byte[stream.Length];
					stream.Read(assemblyData, 0, assemblyData.Length);
				}

				bool fileOk;
				string tempFile;

				string tempPath;
				do
				{
					tempPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
				} while (Directory.Exists(tempPath));

				using (var sha1 = new SHA1CryptoServiceProvider())
				{
					// GetRaw the hash value of the Embedded DLL
					string fileHash = BitConverter.ToString(sha1.ComputeHash(assemblyData)).Replace("-", string.Empty);

					// The full path of the DLL that will be saved
					tempFile = tempPath + filename;

					// Check if the DLL is already existed or not?
					if (File.Exists(tempFile))
					{
						// GetRaw the file hash value of the existed DLL
						byte[] bb = File.ReadAllBytes(tempFile);
						string fileHash2 = BitConverter.ToString(sha1.ComputeHash(bb)).Replace("-", string.Empty);

						// Compare the existed DLL with the Embedded DLL
						fileOk = fileHash == fileHash2;
					}
					else
					{
						// The DLL is not existed yet
						fileOk = false;
					}
				}

				// Create the file on disk
				if (!fileOk)
				{
					File.WriteAllBytes(tempFile, assemblyData);
				}

				return tempFile;
			}
		}


		private static Dictionary<string, ManagedAssemblyEntry> managedEntries =
			new Dictionary<string, ManagedAssemblyEntry>();

		public static void AddNativeAssemblyFolder(string folder)
		{
		}

		public static void AddManagedAssemblyResources(Assembly containingAssembly, string prefix)
		{
			var assemblyName = new AssemblyName(containingAssembly.FullName);

			if (!prefix.StartsWith(assemblyName.Name))
				prefix = assemblyName.Name + "." + prefix;

			var assemblies = GetManagedResourcePlattforms(containingAssembly.GetManifestResourceNames(), prefix);

			foreach (var pair in assemblies)
			{
				foreach (var assm in pair.Value)
				{
					if (!assm.EndsWith(".dll"))
						continue;

					var name = assm.Remove(assm.Length - 4);


					ManagedAssemblyResource entry = null;
					if (managedEntries.ContainsKey(name))
						entry = (ManagedAssemblyResource) managedEntries[name];
					else
						entry = new ManagedAssemblyResource(name, containingAssembly);

					entry.Add(pair.Key, prefix + "." + pair.Key + "." + assm);

					if (!managedEntries.ContainsKey(name))
						managedEntries.Add(name, entry);
				}
			}

			Console.WriteLine("Contained assemblies:");
			foreach (var entry in managedEntries.Values)
			{
				Console.WriteLine("  " + entry.Name + ": ");
				foreach (var plattform in entry.Plattforms)
				{
					Console.WriteLine("    " + plattform + ": " + (entry as ManagedAssemblyResource).getResource(plattform));
				}
			}
		}


		public static void Initialize()
		{
			AppDomain.CurrentDomain.AssemblyResolve += Resolver;
		}


		private static Dictionary<String, List<String>> GetManagedResourcePlattforms(IEnumerable<string> names, string prefix)
		{
			var ret = new Dictionary<string, List<String>>();
			foreach (String resource in names)
			{
				if (!resource.StartsWith(prefix))
					continue;
				string suffix = resource.Remove(0, prefix.Length + 1);

				int dotIndex = suffix.IndexOf(".", StringComparison.Ordinal);
				if (dotIndex < 0) continue;


				string plattform = suffix.Substring(0, dotIndex);
				string name = suffix.Substring(dotIndex + 1);

				if (!ret.ContainsKey(plattform))
					ret.Add(plattform, new List<string>());

				ret[plattform].Add(name);
			}
			return ret;
		}

		private static Assembly Resolver(object sender, ResolveEventArgs args)
		{
			return null;
		}
	}
}