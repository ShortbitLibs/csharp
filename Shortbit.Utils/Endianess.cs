﻿// /*
//  * Endianess.cs
//  *
//  *  Created on: 00:43
//  *         Author: 
//  */

namespace Shortbit.Utils
{
	public enum Endianess
	{
		LittleEndian,
		BigEndian
	}
}