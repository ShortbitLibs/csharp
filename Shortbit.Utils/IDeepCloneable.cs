﻿// /*
//  * IDeepCloneable.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils
{
	internal interface IDeepCloneable<T>
		where T : IDeepCloneable<T>
	{
		T DeepClone();
	}
}