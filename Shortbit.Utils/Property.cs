﻿// /*
//  * Property.cs
//  *
//  *  Created on: 03:42
//  *         Author: 
//  */

using System;
using System.ComponentModel;

namespace Shortbit.Utils
{
	[TypeConverter(typeof (PropertyConverter))]
	public class Property<T> : PropertyBase
	{
		public override object RawValue
		{
			get { return this.Value; }
		}

		public T Value
		{
			get { return this.getter(); }
			set { this.setter(value); }
		}

		private readonly Func<T> getter;
		private readonly Action<T> setter;
		private T value;

		public Property()
			: this(default(T))
		{
		}

		public Property(T value)
		{
			this.value = value;
			this.getter = () => this.value;
			this.setter = v => this.value = v;
		}

		public Property(Func<T> getter, Action<T> setter)
		{
			Throw.IfNull(getter, nameof(getter));
			Throw.IfNull(setter, nameof(setter));

			this.getter = getter;
			this.setter = setter;
		}

		public T Get()
		{
			return this.Value;
		}


		public void Set(T value)
		{
			this.Value = value;
		}

		public static implicit operator T(Property<T> property)
		{
			if (property == null)
				return default(T);
			return property.Value;
		}

		public static implicit operator Property<T>(T value)
		{
			return new Property<T>(value);
		}
	}
}