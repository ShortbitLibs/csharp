﻿// /*
//  * Statistic.cs
//  *
//  *  Created on: 20:10
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Linq;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils
{
	public static class Statistic
	{
		public static double Mean([NotNull]IEnumerable<double> list)
		{
			Throw.IfNull(list, nameof(list));

			var xs = list.ToListSafe();
			if (xs.Count == 0)
				return 0;

			return xs.Sum(x => (x/xs.Count));
		}

		public static double Variance([NotNull]IEnumerable<double> list)
		{
			Throw.IfNull(list, nameof(list));

			var xs = list.ToListSafe();
			if (xs.Count == 0)
				return 0;

			double mean = 0;
			double sqrVal = 0;

			foreach (var x in xs)
			{
				sqrVal += (Math.Pow(x, 2)/xs.Count);
				mean += (x/xs.Count);
			}
			mean = Math.Pow(mean, 2);

			return sqrVal - mean;
		}

		public static double StandardDeviation([NotNull]IEnumerable<double> list)
		{
			return Math.Sqrt(Variance(list));
		}

		public static double Covariance([NotNull]IEnumerable<double> listX, [NotNull]IEnumerable<double> listY)
		{
			Throw.IfNull(listX, nameof(listX));
			Throw.IfNull(listY, nameof(listY));

			var xs = listX.ToListSafe();
			var ys = listY.ToListSafe();

			if (xs.Count != ys.Count) throw new ArgumentException("Variable sets for Covariance must have same length.");
			if (xs.Count == 0) return 0;

			double meanX = Mean(xs);
			double meanY = Mean(ys);


			return xs.Zip(ys, (x, y) => (x - meanX)*(y - meanY)).Sum(x => x/xs.Count);
		}

		public static double Correlation([NotNull]IEnumerable<double> listX, [NotNull]IEnumerable<double> listY)
		{
			Throw.IfNull(listX, nameof(listX));
			Throw.IfNull(listY, nameof(listY));

			var xs = listX.ToListSafe();
			var ys = listY.ToListSafe();

			double varX = Variance(xs);
			double varY = Variance(ys);
			double cov = Covariance(xs, ys);

			if (xs.Count == 0) return 0; // Covariance checks for different list lengths, no need here...

			return cov/(varX*varY);
		}


		public static double Quantil([NotNull]IEnumerable<double> list, double percent)
		{
			Throw.IfNull(list, nameof(list));
			Throw.IfNotWithin(percent, 0.0, 1.0, nameof(percent), includeLower: false, includeUpper: false);

			var xs = list.ToArraySafe();

			Array.Sort(xs);

			double i = xs.Length*percent;
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if ((i%1) == 0)
				return 0.5*(xs[(int) i] + xs[(int) i + 1]);
			return xs[(int) Math.Ceiling(i)];
		}

		public static double Median(IEnumerable<double> list)
		{
			return Quantil(list, 0.5);
		}

		public static double Quartil1(IEnumerable<double> list)
		{
			return Quantil(list, 0.25);
		}

		public static double Quartil3(IEnumerable<double> list)
		{
			return Quantil(list, 0.75);
		}
	}
}