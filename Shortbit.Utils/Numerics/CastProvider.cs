﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using MiscUtils.Annotations;
using MiscUtils;
using MiscUtils.Numerics.ValueTypes;

namespace MiscUtils.Numerics
{

	public abstract class CastProvider<T>
	{
		[NotNull]private static Dictionary<Tuple<Type, Type>, UnaryExpression> expressionMap = new Dictionary<Tuple<Type, Type>, UnaryExpression>();


		public virtual bool CanCastFrom<TIn>()
		{
			var source = typeof (TIn);
			return ((source.CanConvertTo(typeof(T)))
					|| typeof(IConvertible).IsAssignableFrom(source));
		}

		public abstract T CastFrom<TIn>(TIn value);


		public abstract T Zero { get; }
		public abstract T One { get; }
		public abstract T Two { get; }


		protected static Func<TFrom, T> ConvertFromExpression<TFrom>()
		{
			//Type toType = typeof (TTo);
			var expr = GetExpression(typeof(TFrom), typeof(T));
			if (expr == null)
				return null;

			return Expression.Lambda<Func<TFrom, T>>(expr, (ParameterExpression)expr.Operand).Compile();

		}
		protected static Func<T, TTo> ConvertToExpression<TTo>()
		{
			//Type toType = typeof (TTo);
			var expr = GetExpression(typeof(T), typeof(TTo));
			if (expr == null)
				return null;

			return Expression.Lambda<Func<T, TTo>>(expr, (ParameterExpression)expr.Operand).Compile();

		}

		protected static Func<TFrom, TTo> ConvertExpression<TFrom, TTo>()
		{
			//Type toType = typeof (TTo);
			var expr = GetExpression(typeof(TFrom), typeof(TTo));
			if (expr == null)
				return null;

			return Expression.Lambda<Func<TFrom, TTo>>(expr, (ParameterExpression)expr.Operand).Compile();

		}

		protected static UnaryExpression GetExpression(Type fromType, Type toType)
		{
			var key = new Tuple<Type, Type>(fromType, toType);
			if (!expressionMap.ContainsKey(key))
			{
				UnaryExpression expr = null;
				try
				{
					expr = Expression.Convert(Expression.Parameter(fromType, null), toType);

				}
				catch (InvalidOperationException)
				{
				}
				expressionMap.Add(key, expr);
			}

			return expressionMap[key];
		}
	}


	public class CastProvider<T, TBase> : CastProvider<T> where TBase : struct
	{

		private static readonly Func<TBase, T> FromBase = ConvertFromExpression<TBase>();
		private static readonly Func<T, TBase> ToBase = ConvertToExpression<TBase>();


		public override bool CanCastFrom<TIn>()
		{
			var source = typeof(TIn);
			return base.CanCastFrom<TIn>() 
				|| source.CanConvertTo(typeof(TBase));
		}

		public override T CastFrom<TIn>(TIn x)
		{
			if (x == null) throw new ArgumentNullException("x");

			var source = x.GetType();
			if (!this.CanCastFrom<TIn>()) throw new InvalidCastException("Can't cast " + source.Name + " to " + typeof(T).Name + ".");


			if (source.CanConvertTo(typeof (T)))
				return ConvertFromExpression<TIn>()(x);

			if (source.CanConvertTo(typeof(TBase)))
				return FromBase(ConvertFromBaseExpression<TIn>()(x));
			

			return FromBase((TBase) Convert.ChangeType(x, typeof (TBase)));
		}
		
		public override T Zero
		{
			get { return CastFrom(0); }
		}
		
		public override T One
		{
			get { return CastFrom(1); }
		}
		public override T Two
		{
			get { return CastFrom(2); }
		}


		protected static Func<TFrom, TBase> ConvertFromBaseExpression<TFrom>()
		{
			//Type toType = typeof (TTo);
			var expr = GetExpression(typeof(TFrom), typeof(TBase));
			if (expr == null)
				return null;

			return Expression.Lambda<Func<TFrom, TBase>>(expr, (ParameterExpression)expr.Operand).Compile();

		}
		protected static Func<TBase, TTo> ConvertToBaseExpression<TTo>()
		{
			//Type toType = typeof (TTo);
			var expr = GetExpression(typeof(TBase), typeof(TTo));
			if (expr == null)
				return null;

			return Expression.Lambda<Func<TBase, TTo>>(expr, (ParameterExpression)expr.Operand).Compile();

		}

	}

}
