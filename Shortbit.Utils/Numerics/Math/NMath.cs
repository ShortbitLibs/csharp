﻿// /*
//  * NMath.cs
//  *
//  *  Created on: 20:10
//  *         Author: 
//  */

using System.Collections.Generic;
using Shortbit.Utils.Numerics.Operators;

namespace Shortbit.Utils.Numerics.Math
{
	public static class NMath
	{
		public static TB Pow<TB, TE, OP>(OP provider, TB x, TE exponent)
			where OP : IMultiply<TB>, IDivide<TB>, IConstant<TB>, IIncrement<TE>, IDecrement<TE>, IConstant<TE>, IComparer<TE>
		{
			return Pow(provider, provider, x, exponent);
		}

		public static TB Pow<TB, TE, OPB, OPE>(OPB baseProvider, OPE exponentProvider, TB x, TE exponent)
			where OPB : IMultiply<TB>, IDivide<TB>, IConstant<TB>
			where OPE : IIncrement<TE>, IDecrement<TE>, IConstant<TE>, IComparer<TE>
		{
			int diff = exponentProvider.Compare(exponent, exponentProvider.Zero());

			TB result = baseProvider.One();

			if (diff == 0)
				return result;

			if (diff > 0)
			{
				for (TE i = exponentProvider.Zero();
					exponentProvider.Compare(i, exponent) < 0;
					exponentProvider.ReadAndIncrement(ref i))
					baseProvider.AssignMultiply(ref result, ref x);
			}
			else
			{
				for (TE i = exponentProvider.Zero();
					exponentProvider.Compare(i, exponent) > 0;
					exponentProvider.ReadAndDecrement(ref i))
					baseProvider.AssignDivide(ref result, ref x);
			}

			return result;
		}

		/*	public static Double Exp<T, OP>(OP provider, T d) where OP : IIncrement<T>, IDecrement<T>, IConstant<T>, IComparer<T>
		{
			return Pow<Double, T, FOOOO, OP>(null, provider, System.Math.E, d); // TODO: Use provider of double here...
		}*/

		public static T Abs<T, OP>(OP provider, T x) where OP : INegate<T>, IComparer<T>, IConstant<T>
		{
			if (provider.Compare(x, provider.Zero()) < 0)
				return provider.Negate(ref x);
			return x;
		}

		public static T Max<T, OP>(OP provider, T x, T y) where OP : IComparer<T>
		{
			if (provider.Compare(x, y) > 0)
				return x;
			return y;
		}

		public static T Min<T, OP>(OP provider, T x, T y) where OP : IComparer<T>
		{
			if (provider.Compare(x, y) < 0)
				return x;
			return y;
		}

		public static int Sign<T, OP>(OP provider, T x) where OP : IComparer<T>, IConstant<T>
		{
			int cmp = provider.Compare(x, provider.Zero());
			return System.Math.Sign(cmp);
		}

		public static T Sqrt<T, OP>(OP provider, ref T s)
			where OP : IAdd<T>, ISubtract<T>, IMultiply<T>, IDivide<T>, IConstant<T>, IComparer<T>, INegate<T>
		{
			T one = provider.One();
			T two = provider.Add(ref one, ref one);

			T x = provider.Divide(ref s, ref two);

			for (int i = 0; i < 20; i++)
			{
				/*	double n = fg / 2.0;
				double lstX = 0.0;
				while (n != lstX)
				{
					lstX = n;
					n = (n + fg / n) / 2.0;
				}*/
				T temp = provider.Divide(ref s, ref x);
				provider.AssignAdd(ref x, ref temp);
				provider.AssignDivide(ref x, ref two);

				/*	// (x*x - s) / 2x = (x * x - s) / x / 2
				T change = x;

				change.AssignMultiply(ref x);
				change.AssignSubtract(ref s);
				change.AssignDivide(ref x);
				change.AssignDivide(ref two);

				x.AssignSubtract(ref change);*/
			}
			return x;
		}

		public static double Sqrt(double s)
		{
			double x = 2;
			for (int i = 0; i < 20; i++)
			{
				x -= (x*x - s)/(2*x);
			}
			return x;
		}
	}
}