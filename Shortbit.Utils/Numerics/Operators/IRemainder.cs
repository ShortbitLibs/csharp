﻿// /*
//  * IRemainder.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IRemainder<T> : IOperator<T>
	{
		T Remainder(ref T a, ref T b);

		void AssignRemainder(ref T first, ref T other);
	}
}