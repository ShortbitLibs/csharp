﻿// /*
//  * IConstant.cs
//  *
//  *  Created on: 16:45
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IConstant<T> : IOperator<T>
	{
		T Zero();

		T One();
	}
}