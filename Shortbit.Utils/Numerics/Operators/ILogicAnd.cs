﻿// /*
//  * ILogicAnd.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface ILogicAnd<T> : IOperator<T>
	{
		T LogicAnd(ref T a, ref T b);

		void AssignLogicAnd(ref T first, ref T other);
	}
}