﻿// /*
//  * IMultiply.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IMultiply<T> : IOperator<T>
	{
		T Multiply(ref T a, ref T b);

		void AssignMultiply(ref T first, ref T other);
	}
}