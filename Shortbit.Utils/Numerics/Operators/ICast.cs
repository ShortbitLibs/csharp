﻿// /*
//  * ICast.cs
//  *
//  *  Created on: 22:00
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface ICast<F, T> : IOperator<F>
	{
		T CastTo(ref F from);
	}
}