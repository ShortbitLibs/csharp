﻿// /*
//  * ILogicOr.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface ILogicOr<T> : IOperator<T>
	{
		T LogicOr(ref T a, ref T b);

		void AssignLogicOr(ref T first, ref T other);
	}
}