﻿// /*
//  * ILogicXor.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface ILogicXor<T> : IOperator<T>
	{
		T LogicXor(ref T a, ref T b);

		void AssignLogicXor(ref T first, ref T other);
	}
}