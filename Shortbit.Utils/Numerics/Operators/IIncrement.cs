﻿// /*
//  * IIncrement.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IIncrement<T> : IOperator<T>
	{
		T IncrementAndRead(ref T value);

		T ReadAndIncrement(ref T value);
	}
}