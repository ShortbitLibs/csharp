﻿// /*
//  * INegate.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface INegate<T> : IOperator<T>
	{
		T Negate(ref T value);
	}
}