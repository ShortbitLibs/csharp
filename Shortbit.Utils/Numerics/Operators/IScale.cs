﻿// /*
//  * IScale.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IScale<T, S> : IOperator<T>
	{
		T Scale(ref T value, ref S scalar);

		void AssignScale(ref T first, ref S scalar);
	}
}