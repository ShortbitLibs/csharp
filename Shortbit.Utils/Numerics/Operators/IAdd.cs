﻿// /*
//  * IAdd.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IAdd<T> : IOperator<T>
	{
		T Add(ref T a, ref T b);

		void AssignAdd(ref T first, ref T other);
	}
}