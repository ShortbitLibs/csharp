﻿// /*
//  * IDecrement.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Utils.Numerics.Operators
{
	public interface IDecrement<T> : IOperator<T>
	{
		T DecrementAndRead(ref T value);

		T ReadAndDecrement(ref T value);
	}
}