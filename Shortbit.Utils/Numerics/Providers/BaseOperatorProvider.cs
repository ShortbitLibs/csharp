﻿// /*
//  * BaseOperatorProvider.cs
//  *
//  *  Created on: 18:23
//  *         Author: 
//  */

using System;
using Shortbit.Utils.Numerics.Operators;

namespace Shortbit.Utils.Numerics.Providers
{
	public abstract class BaseOperatorProvider<T> : IOperatorProvider<T>
	{
		public virtual bool Provides<OP>() where OP : class, IOperator<T>
		{
			return (this is OP);
		}

		public virtual OP GetOperator<OP>() where OP : class, IOperator<T>
		{
			if (!this.Provides<OP>())
				throw new ArgumentException(this.GetType().Name + " does not provide the operator " + typeof (OP).Name);
			return (OP) (object) this;
		}
	}
}