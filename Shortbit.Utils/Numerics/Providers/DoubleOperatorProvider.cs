﻿// /*
//  * DoubleOperatorProvider.cs
//  *
//  *  Created on: 17:36
//  *         Author: 
//  */

using System;
using Shortbit.Utils.Numerics.Operators;

namespace Shortbit.Utils.Numerics.Providers
{
	public class DoubleOperatorOperatorProvider : BaseOperatorProvider<Double>, INumericOperators<Double>
	{
		public static DoubleOperatorOperatorProvider Instance
		{
			get { return Singleton<DoubleOperatorOperatorProvider>.ProvideInstance(() => new DoubleOperatorOperatorProvider()); }
		}

		private DoubleOperatorOperatorProvider()
		{
		}

		/// <summary>
		///     Vergleicht zwei Objekte und gibt einen Wert zurück, der angibt, ob ein Wert niedriger, gleich oder größer als der
		///     andere Wert ist.
		/// </summary>
		/// <returns>
		///     Eine ganze Zahl mit Vorzeichen, die die relativen Werte von <paramref name="x" /> und <paramref name="y" /> angibt,
		///     wie in der folgenden Tabelle veranschaulicht. Wert  Bedeutung  Kleiner als 0 <paramref name="x" /> ist kleiner als
		///     <paramref name="y" />. Zero <paramref name="x" /> ist gleich <paramref name="y" />. Größer als 0 (null)
		///     <paramref name="x" /> ist größer als <paramref name="y" />.
		/// </returns>
		/// <param name="x">Das erste zu vergleichende Objekt.</param>
		/// <param name="y">Das zweite zu vergleichende Objekt.</param>
		public int Compare(double x, double y)
		{
			return x.CompareTo(y);
		}

		public double Add(ref double a, ref double b)
		{
			return a + b;
		}

		public void AssignAdd(ref double first, ref double other)
		{
			first += other;
		}

		public double Subtract(ref double a, ref double b)
		{
			return a - b;
		}

		public void AssignSubtract(ref double first, ref double other)
		{
			first -= other;
		}

		public double Multiply(ref double a, ref double b)
		{
			return a*b;
		}

		public void AssignMultiply(ref double first, ref double other)
		{
			first *= other;
		}

		public double Divide(ref double a, ref double b)
		{
			return a/b;
		}

		public void AssignDivide(ref double first, ref double other)
		{
			first /= other;
		}

		public double Zero()
		{
			return 0.0;
		}

		public double One()
		{
			return 1.0;
		}

		public double DecrementAndRead(ref double value)
		{
			return --value;
		}

		public double ReadAndDecrement(ref double value)
		{
			return value--;
		}

		public double IncrementAndRead(ref double value)
		{
			return ++value;
		}

		public double ReadAndIncrement(ref double value)
		{
			return value++;
		}

		public double Remainder(ref double a, ref double b)
		{
			return a%b;
		}

		public void AssignRemainder(ref double first, ref double other)
		{
			first %= other;
		}

		public double Negate(ref double value)
		{
			return -value;
		}
	}
}