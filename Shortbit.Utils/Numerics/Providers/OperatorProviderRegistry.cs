﻿// /*
//  * OperatorProviderRegistry.cs
//  *
//  *  Created on: 18:12
//  *         Author: 
//  */

using Shortbit.Utils.Numerics.Operators;

namespace Shortbit.Utils.Numerics.Providers
{
	public static class OperatorProviderRegistry
	{
		public static IOperatorProvider<T> GetProvider<T>(T value)
		{
			if (typeof (IOperatorProvider<T>).IsAssignableFrom(typeof (T)))
				return (IOperatorProvider<T>) value;

			// TODO: Check for global registered providers

			return null;
		}


		private class SingleOperatorProvider<T, OP> : IOperatorProvider<T> where OP : IOperator<T>
		{
			private readonly OP @operator;

			public SingleOperatorProvider(OP @operator)
			{
				this.@operator = @operator;
			}


			public bool Provides<OP1>() where OP1 : class, IOperator<T>
			{
				return (this.@operator is OP1);
			}

			public OP1 GetOperator<OP1>() where OP1 : class, IOperator<T>
			{
				if (!this.Provides<OP1>())
					return null;
				return (OP1) (object) this.@operator;
			}
		}
	}
}