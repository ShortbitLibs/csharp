﻿// /*
//  * IOperatorProvider.cs
//  *
//  *  Created on: 18:12
//  *         Author: 
//  */

using Shortbit.Utils.Numerics.Operators;

namespace Shortbit.Utils.Numerics.Providers
{
	public interface IOperatorProvider<T>
	{
		bool Provides<OP>() where OP : class, IOperator<T>;

		OP GetOperator<OP>() where OP : class, IOperator<T>;
	}
}