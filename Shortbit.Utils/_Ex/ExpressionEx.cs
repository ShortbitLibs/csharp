﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils
{
    public static class ExpressionEx
	{
		public delegate Expression[] ForeachLoopBodyDelegate(MemberExpression element, LabelTarget breakTarget,
															 LabelTarget continueTarget);



		public static TDelegate CreateDelegate<TDelegate, TMethodBase>(TMethodBase methodBase,
            Func<TMethodBase, IEnumerable<Expression>, Expression> callExpressionGenerator) where TMethodBase : MethodBase
        {
            var delInfo = typeof(TDelegate).GetMethod("Invoke");

            var delParams = delInfo.GetParameters();
            var methodParams = methodBase.GetParameters();

            if (delParams.Length != methodParams.Length)
                throw new ArgumentException("Delegate and Contructor parameters do not match.");

            var eLambdaParams = new ParameterExpression[delParams.Length];
            var eMethodParams = new Expression[delParams.Length];
            for (int i = 0; i < delParams.Length; i++)
            {
                var dP = delParams[i];
                var cP = methodParams[i];

                eLambdaParams[i] = Expression.Parameter(dP.ParameterType, dP.Name);

                if (dP.ParameterType == cP.ParameterType)
                    eMethodParams[i] = eLambdaParams[i];
                else
                    eMethodParams[i] = Expression.Convert(eLambdaParams[i], cP.ParameterType);
            }

            var eMethod = callExpressionGenerator(methodBase, eMethodParams);

            Expression eDelegate = eMethod;
            if (delInfo.ReturnType != eMethod.Type)
                eDelegate = Expression.Convert(eMethod, delInfo.ReturnType);

            var lambda = Expression.Lambda<TDelegate>(eDelegate, eLambdaParams);

            return lambda.Compile();
        }

		public static (ParameterExpression loopVar, Expression[] loopBody) SimpleForLoop(Expression breakValue, Func<ParameterExpression, Expression[]> body)
		{
			var loopVar = Expression.Variable(typeof(int), "i");
			var init = Expression.Assign(loopVar, Expression.Constant(0));

			var loopBreak = Expression.Label("LoopBreak");

			var loopBody = new List<Expression>
			{
				Expression.IfThen(
					Expression.GreaterThanOrEqual(loopVar, breakValue),
					Expression.Break(loopBreak)
				)
			};
			loopBody.AddRange(body(loopVar));
			loopBody.Add(Expression.AddAssign(loopVar, Expression.Constant(1)));
			
			var loopBlock = new Expression[]
			{
				init,
				Expression.Loop(
					Expression.Block(loopBody),
					loopBreak
				)
			};
			return (loopVar, loopBlock);
		}

		public static BlockExpression SimpleForLoopBlock(Expression breakValue,
														 Func<ParameterExpression, Expression[]> body)
		{
			var (loopVar, loopBody) = ExpressionEx.SimpleForLoop(breakValue, body);
			return Expression.Block(new[] {loopVar}, loopBody);
		}

		public static (ParameterExpression enumerator, Expression[] loopBody) ForeachLoop(Type enumerableType, Expression enumerable, ForeachLoopBodyDelegate body)
		{
			var enumerableInterface = enumerableType.GetInterface(typeof(IEnumerable<>).FullName) ??
									  enumerableType.GetInterface(typeof(IEnumerable).FullName);
			if (enumerableInterface == null)
				throw new ArgumentException($"Given type does not implement IEnumerable<T> or IEnumerable.");

			var elementType = enumerableInterface.IsGenericType
				? enumerableInterface.GetGenericArguments()[0]
				: typeof(object);

			var getEnumeratorMethod = enumerableInterface.GetMethod("GetEnumerator");
			Debug.Assert(getEnumeratorMethod != null, nameof(getEnumeratorMethod) + " != null");
			
			var enumeratorType = enumerableInterface != typeof(IEnumerable)
				? typeof(IEnumerator<>).MakeGenericType(elementType)
				: typeof(IEnumerator);
			
			var moveNextMethod = typeof(IEnumerator).GetMethod("MoveNext");
			Debug.Assert(moveNextMethod != null, nameof(moveNextMethod) + " != null");

			var enumeratorVar = Expression.Variable(enumeratorType, "enumerator");
			var enumeratorAssign = Expression.Assign(enumeratorVar, Expression.Call(enumerable, getEnumeratorMethod));

			var callMoveNext = Expression.Call(enumeratorVar, moveNextMethod);
			
			var loopContinue = Expression.Label("LoopContinue");
			var loopBreak = Expression.Label("LoopBreak");

			var loopBody = new List<Expression>
			{
				Expression.IfThen(
					Expression.Equal(callMoveNext, Expression.Constant(false)),
					Expression.Break(loopBreak)
				)
			};
			loopBody.AddRange(body(Expression.Property(enumeratorVar, "Current"), loopBreak, loopContinue));
			
			var loopBlock = new Expression[]
			{
				enumeratorAssign,
				Expression.Loop(
					Expression.Block(loopBody),
					loopBreak,
					loopContinue
				)
			};
			return (enumeratorVar, loopBlock);
		}
        
    }
}
