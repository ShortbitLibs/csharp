﻿// /*
//  * EnumEx.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Shortbit.Utils.Enums;
using Shortbit.Utils.Collections.Generic;

namespace Shortbit.Utils
{
	public static class EnumEx
	{
		/// <summary>
		///     Small helper method that works around a flaw in C# generic constraints.
		///     Checks if given type T is a enum and throws a ArgumentException, if not so.
		/// </summary>
		/// <typeparam name="T">The type to check. Presumable a Enum...</typeparam>
		/// <param name="argumentName">The name of the type argument to use in Exception message. Default to "T".</param>
		/// <exception cref="ArgumentException">Thrown if the type argument T is no Enum.</exception>
#if NET461 || NETCOREAPP
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void ThrowIfNoEnum<T>(String argumentName = "T") where T : struct, Enum
		{
			// Nothing to do here, generic constraint does this check.
			// But we need this function for compatibility sake.
		}
#else
		public static void ThrowIfNoEnum<T>(String argumentName = "T") where T : struct, IConvertible
		{
			if (!typeof (T).IsEnum) throw new ArgumentException(argumentName + " must be an enumerated type.");
		}
#endif

		public static T[] GetValues<T>()
#if NET461 || NETCOREAPP
			where T : struct, Enum
#else
			where T : struct, IConvertible
#endif
		{
			EnumEx.ThrowIfNoEnum<T>("T");

			return Enum.GetValues(typeof (T)).Cast<T>().ToArray();
		}

		public static IEnumerable<T> GetFlags<T>(T flags)
#if NET461 || NETCOREAPP
			where T : struct, Enum
#else
			where T : struct, IConvertible
#endif
		{
			EnumEx.ThrowIfNoEnum<T>("T");

			var e = (Enum) (object) flags;
			foreach (var value in GetValues<T>())
				if (e.HasFlag((Enum) (object) value))
					yield return value;
		}

		public static T Parse<T>(string value)
#if NET461 || NETCOREAPP
			where T : struct, Enum
#else
			where T : struct, IConvertible
#endif
		{
			EnumEx.ThrowIfNoEnum<T>("T");
			return (T) Enum.Parse(typeof (T), value);
		}


		public static IReadOnlyDictionary<TEnum, TValue> LoadMultipleAttributeValues<TEnum, TAttribute, TValue>(Func<TAttribute[], TValue> valueLoader, bool throwOnMissing = false)
#if NET461 || NETCOREAPP
			where TEnum : struct, Enum
#else
			where TEnum : struct, IConvertible
#endif
			where TAttribute : Attribute
		{
			EnumEx.ThrowIfNoEnum<TEnum>();

			var enumType = typeof(TEnum);
			var attrType = typeof(TAttribute);

			var res = new Dictionary<TEnum, TValue>();
			foreach (var value in EnumEx.GetValues<TEnum>())
			{
				var memberInfos = enumType.GetMember(value.ToString());
				var enumValueMemberInfo = memberInfos.FirstOrDefault(m => m.DeclaringType == enumType);
				Debug.Assert(enumValueMemberInfo != null, nameof(enumValueMemberInfo) + " != null");
				var valueAttributes = (TAttribute[])enumValueMemberInfo.GetCustomAttributes(attrType, false);
				if (valueAttributes.Length == 0)
				{
					if (throwOnMissing)
						throw new InvalidOperationException($"Attribute {attrType.FullName} missing on enum field {enumType.FullName}.{value}.");
					continue;
				}

				var v = valueLoader(valueAttributes);
				res[value] = v;
			}
			
			return res.AsReadOnly();
		}

		public static IReadOnlyDictionary<TEnum, TValue> LoadAttributeValues<TEnum, TAttribute, TValue>(Func<TAttribute, TValue> valueLoader, bool throwOnMissing = false)
#if NET461 || NETCOREAPP
			where TEnum : struct, Enum
#else
			where TEnum : struct, IConvertible
#endif
			where TAttribute : Attribute
		{
			return EnumEx.LoadMultipleAttributeValues<TEnum, TAttribute, TValue>(attrs => attrs.Length == 1 ? valueLoader(attrs[0]) : throw new InvalidOperationException());
		}
		

		public static IReadOnlyDictionary<TEnum, string> LoadTitles<TEnum>()
#if NET461 || NETCOREAPP
			where TEnum : struct, Enum
#else
			where TEnum : struct, IConvertible
#endif
		{
			return EnumEx.LoadAttributeValues<TEnum, EnumTitleAttribute, string>(a => a.Title);
		}
		

		public static IReadOnlyDictionary<TEnum, object> LoadValues<TEnum>()
#if NET461 || NETCOREAPP
			where TEnum : struct, Enum
#else
			where TEnum : struct, IConvertible
#endif
		{
			return EnumEx.LoadAttributeValues<TEnum, EnumValueAttribute, object>(a => a.Value);
		}
		

		public static IReadOnlyDictionary<TEnum, string> LoadStringValues<TEnum>()
#if NET461 || NETCOREAPP
			where TEnum : struct, Enum
#else
			where TEnum : struct, IConvertible
#endif
		{
			return EnumEx.LoadAttributeValues<TEnum, EnumStringValueAttribute, string>(a => a.Value);
		}

	}
}