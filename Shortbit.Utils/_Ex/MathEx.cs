﻿// /*
//  * MathEx.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;

namespace Shortbit.Utils
{
	public static class MathEx
	{
		public static class Float
		{
			public const float Pi = 3.14159265358979f;
			public const float PiDouble = Float.Pi * 2;
			public const float PiHalf = Float.Pi / 2;
			public const float PiQuarter = Float.Pi / 4;

			public const float Epsilon = 0.0001f;
			public const float EpsilonSquared = Float.Epsilon * Float.Epsilon;
		}
		
		public static class Double
		{
			public const double Pi = 3.14159265358979;
			public const double PiDouble = Double.Pi * 2;
			public const double PiHalf = Double.Pi / 2;
			public const double PiQuarter = Double.Pi / 4;

			public const double Epsilon = 0.0000001;
			public const double EpsilonSquared = Double.Epsilon * Double.Epsilon;
		}


		
		public static float Rad2Deg(float radian)
		{
			return (radian/Float.Pi)*180.0f;
		}
		public static double Rad2Deg(double radian)
		{
			return (radian/Double.Pi)*180.0;
		}

		public static float Deg2Rad(float degree)
		{
			return (degree/180.0f)*Float.Pi;
		}
		public static double Deg2Rad(double degree)
		{
			return (degree/180.0)*Double.Pi;
		}



		public static sbyte Mod(sbyte value, sbyte div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return m < 0 ? (sbyte)(m + div) : (sbyte)m;
		}
		public static byte Mod(byte value, byte div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return (byte)m;
		}

		public static short Mod(short value, short div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return m < 0 ? (short)(m + div) : (short)m;
		}
		public static ushort Mod(ushort value, ushort div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return (ushort)m;
		}


		public static int Mod(int value, int div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return m < 0 ? (byte)(m + div) : (byte)m;
		}
		public static uint Mod(uint value, uint div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return (byte)m;
		}


		public static long Mod(long value, long div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return m < 0 ? m + div : m;
		}
		public static ulong Mod(ulong value, ulong div)
		{
			if (div <= 0)
				throw new ArgumentOutOfRangeException(nameof(div), "Negative or zero divisor is not allowed");

			var m = value % div;
			return m;
		}
		

		
		

		public static bool NearZero(float x)
		{
			return (x*x) < Float.EpsilonSquared;
		}
		public static bool NearZero(float x, float epsilon)
		{
			return (x*x) < (epsilon * epsilon);
		}

		public static bool NearZero(double x)
		{
			return (x*x) < Double.EpsilonSquared;
		}
		public static bool NearZero(double x, double epsilon)
		{
			return (x*x) < (epsilon * epsilon);
		}

		public static bool NearEqual(float a, float b)
		{
			return MathEx.NearZero(b - a);
		}
		public static bool NearEqual(float a, float b, float epsilon)
		{
			return MathEx.NearZero(b - a, epsilon);
		}

		public static bool NearEqual(double a, double b)
		{
			return MathEx.NearZero(b - a);
		}
		public static bool NearEqual(double a, double b, double epsilon)
		{
			return MathEx.NearZero(b - a, epsilon);
		}


		/// <summary>
		///     Calculates Benford's probability for a given digit at a given position in a empirical number.
		/// </summary>
		/// <param name="Base">The base of the number.</param>
		/// <param name="digit">The digit of which thet probability is calculated.</param>
		/// <param name="position">The position in a empirical number, starting with 0 at first digit (most left).</param>
		/// <returns>The probability for the given digit.</returns>
		/// <see cref="http://en.wikipedia.org/wiki/Benford%27s_law" />
		public static double BenfordsProbability(int Base, uint digit, uint position)
		{
			if (digit >= Base) throw new ArgumentOutOfRangeException("digit", "digit must be between 0 and Base.");

			double ret = 0;
			for (var k = (int) Math.Floor(Math.Pow(Base, position - 1)); k <= Math.Pow(Base, position) - 1; k++)
			{
				ret += Math.Log(1 + k*Base + digit, Base);
			}

			return ret;
		}

		/// <summary>
		///     Calculates Benford's probability for the given digit at a given position in a empirical number to the base of 10.
		/// </summary>
		/// <param name="digit">The digit of which thet probability is calculated.</param>
		/// <param name="position">The position in a empirical number, starting with 0 at first digit (most left).</param>
		/// <returns>The probability for the given digit.</returns>
		/// <see cref="MathEx.BenfordsProbability(int, uint, uint)" />
		/// <see cref="http://en.wikipedia.org/wiki/Benford%27s_law" />
		public static double BenfordsProbability(uint digit, uint position)
		{
			return BenfordsProbability(10, digit, position);
		}

		#region Within Overloading

		public static bool Within(short value, short lower, short upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(int value, int lower, int upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(long value, long lower, long upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(ushort value, ushort lower, ushort upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(uint value, uint lower, uint upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(ulong value, ulong lower, ulong upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(float value, float lower, float upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(double value, double lower, double upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		public static bool Within(decimal value, decimal lower, decimal upper)
		{
			return ((lower <= value) && (value <= upper));
		}

		#endregion

		#region Clamp Overloading

		public static short Clamp(short value, short lower, short upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static int Clamp(int value, int lower, int upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static long Clamp(long value, long lower, long upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static ushort Clamp(ushort value, ushort lower, ushort upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static uint Clamp(uint value, uint lower, uint upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static ulong Clamp(ulong value, ulong lower, ulong upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static float Clamp(float value, float lower, float upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static double Clamp(double value, double lower, double upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		public static decimal Clamp(decimal value, decimal lower, decimal upper)
		{
			if (value < lower) return lower;
			if (value > upper) return upper;
			return value;
		}

		#endregion
	}
}