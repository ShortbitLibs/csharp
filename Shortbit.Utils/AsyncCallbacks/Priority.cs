﻿// /*
//  * Priority.cs
//  *
//  *  Created on: 12:23
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public enum Priority
	{
		Highest,
		High,
		Normal,
		Low,
		Lowest
	}
}