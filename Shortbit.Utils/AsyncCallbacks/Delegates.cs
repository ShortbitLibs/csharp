﻿// /*
//  * Delegates.cs
//  *
//  *  Created on: 13:23
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public delegate void Callback<in A>(A arg);

	public delegate void Callback<in A, in C>(A arg, C context);
}