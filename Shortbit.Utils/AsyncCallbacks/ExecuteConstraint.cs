﻿// /*
//  * ExecuteConstraint.cs
//  *
//  *  Created on: 12:24
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public enum ExecuteConstraint
	{
		Always,
		OnlyAsFirstPriority,
		NeverAsFirstPriority,
	}
}