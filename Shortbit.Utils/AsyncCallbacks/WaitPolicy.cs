﻿// /*
//  * WaitPolicy.cs
//  *
//  *  Created on: 22:23
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public enum WaitPolicy
	{
		DontWait,
		WaitForAny,
		WaitForAll
	}
}