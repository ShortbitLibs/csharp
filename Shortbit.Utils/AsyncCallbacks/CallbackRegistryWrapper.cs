﻿// /*
//  * CallbackRegistryWrapper.cs
//  *
//  *  Created on: 00:36
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public class CallbackRegistryWrapper<BA> : ICallbackRegistry<BA>
	{
		private readonly ICallbackRegistry<BA> hidden;

		public CallbackRegistryWrapper(ICallbackRegistry<BA> hidden)
		{
			this.hidden = hidden;
		}

		#region Implementation of ICallbackRegistry<BA>

		public void Subscribe<A>(Callback<A> callback) where A : BA
		{
			this.hidden.Subscribe(callback);
		}

		public void Subscribe<A>(Callback<A> callback, Priority priority) where A : BA
		{
			hidden.Subscribe(callback, priority);
		}

		public void Subscribe<A>(Callback<A> callback, ExecuteConstraint constraint) where A : BA
		{
			hidden.Subscribe(callback, constraint);
		}

		public void Subscribe<A>(Callback<A> callback, Priority priority, ExecuteConstraint constraint) where A : BA
		{
			hidden.Subscribe(callback, priority, constraint);
		}

		public void Revoke<A>(Callback<A> callback) where A : BA
		{
			hidden.Revoke(callback);
		}

		public void Revoke<A>(Callback<A> callback, Priority priority) where A : BA
		{
			hidden.Revoke(callback, priority);
		}

		#endregion
	}

	public class CallbackRegistryWrapper<BA, C> : ICallbackRegistry<BA, C>
	{
		private readonly ICallbackRegistry<BA, C> hidden;

		public CallbackRegistryWrapper(ICallbackRegistry<BA, C> hidden)
		{
			this.hidden = hidden;
		}

		#region Implementation of ICallbackRegistry<BA, C>

		public void Subscribe<A>(Callback<A, C> callback) where A : BA
		{
			this.hidden.Subscribe(callback);
		}

		public void Subscribe<A>(Callback<A, C> callback, Priority priority) where A : BA
		{
			hidden.Subscribe(callback, priority);
		}

		public void Subscribe<A>(Callback<A, C> callback, ExecuteConstraint constraint) where A : BA
		{
			hidden.Subscribe(callback, constraint);
		}

		public void Subscribe<A>(Callback<A, C> callback, Priority priority, ExecuteConstraint constraint) where A : BA
		{
			hidden.Subscribe(callback, priority, constraint);
		}

		public void Revoke<A>(Callback<A, C> callback) where A : BA
		{
			hidden.Revoke(callback);
		}

		public void Revoke<A>(Callback<A, C> callback, Priority priority) where A : BA
		{
			hidden.Revoke(callback, priority);
		}

		#endregion
	}
}