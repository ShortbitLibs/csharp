﻿// /*
//  * ICallbackRegistry.cs
//  *
//  *  Created on: 00:32
//  *         Author: 
//  */

namespace Shortbit.Utils.AsyncCallbacks
{
	public interface ICallbackRegistry<BA>
	{
		void Subscribe<A>(Callback<A> callback) where A : BA;
		void Subscribe<A>(Callback<A> callback, Priority priority) where A : BA;
		void Subscribe<A>(Callback<A> callback, ExecuteConstraint constraint) where A : BA;
		void Subscribe<A>(Callback<A> callback, Priority priority, ExecuteConstraint constraint) where A : BA;

		void Revoke<A>(Callback<A> callback) where A : BA;
		void Revoke<A>(Callback<A> callback, Priority priority) where A : BA;
	}

	public interface ICallbackRegistry<BA, C>
	{
		void Subscribe<A>(Callback<A, C> callback) where A : BA;
		void Subscribe<A>(Callback<A, C> callback, Priority priority) where A : BA;
		void Subscribe<A>(Callback<A, C> callback, ExecuteConstraint constraint) where A : BA;
		void Subscribe<A>(Callback<A, C> callback, Priority priority, ExecuteConstraint constraint) where A : BA;

		void Revoke<A>(Callback<A, C> callback) where A : BA;
		void Revoke<A>(Callback<A, C> callback, Priority priority) where A : BA;
	}
}