﻿// /*
//  * CallbackRegistry.cs
//  *
//  *  Created on: 12:26
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shortbit.Utils.AsyncCallbacks
{
	public class CallbackRegistry<BA, C> : ICallbackRegistry<BA>, ICallbackRegistry<BA, C>
	{
		public delegate bool PriorityLoopTerminationCallback(BA arg, Priority priority);

		private readonly Dictionary<Priority, ArgumentTypeMap> priorityMap;

		public Priority DefaultPriority { get; set; }
		public ExecuteConstraint DefaulExecuteConstraint { get; set; }

		public WaitPolicy WaitPolicy { get; set; }

		public TimeSpan WaitTimeout { get; set; }

		public PriorityLoopTerminationCallback PriorityLoopTermination { get; set; }

		public CallbackRegistry()
		{
			this.priorityMap = new Dictionary<Priority, ArgumentTypeMap>();
			foreach (Priority priority in EnumEx.GetValues<Priority>())
				priorityMap.Add(priority, new ArgumentTypeMap(this, priority));

			this.DefaultPriority = Priority.Normal;
			this.DefaulExecuteConstraint = ExecuteConstraint.Always;

			this.WaitPolicy = WaitPolicy.WaitForAll;
			this.WaitTimeout = TimeSpan.FromSeconds(2);
		}

		public void Subscribe<A>(Callback<A> callback) where A : BA
		{
			this.Subscribe(callback, this.DefaultPriority, this.DefaulExecuteConstraint);
		}

		public void Subscribe<A>(Callback<A> callback, Priority priority) where A : BA
		{
			this.Subscribe(callback, priority, this.DefaulExecuteConstraint);
		}

		public void Subscribe<A>(Callback<A> callback, ExecuteConstraint constraint) where A : BA
		{
			this.Subscribe(callback, this.DefaultPriority, constraint);
		}

		public void Subscribe<A>(Callback<A> callback, Priority priority, ExecuteConstraint constraint) where A : BA
		{
			priorityMap[priority].Add(typeof (A), callback, (arg, context) => callback((A) arg), constraint);
		}

		public void Subscribe<A>(Callback<A, C> callback) where A : BA
		{
			this.Subscribe(callback, this.DefaultPriority, this.DefaulExecuteConstraint);
		}

		public void Subscribe<A>(Callback<A, C> callback, Priority priority) where A : BA
		{
			this.Subscribe(callback, priority, this.DefaulExecuteConstraint);
		}

		public void Subscribe<A>(Callback<A, C> callback, ExecuteConstraint constraint) where A : BA
		{
			this.Subscribe(callback, this.DefaultPriority, constraint);
		}

		public void Subscribe<A>(Callback<A, C> callback, Priority priority, ExecuteConstraint constraint) where A : BA
		{
			priorityMap[priority].Add(typeof (A), callback, (arg, context) => callback((A) arg, context), constraint);
		}

		public void SubscribeAll(CallbackRegistry<BA, C> that)
		{
			foreach (var pair in that.priorityMap)
			{
				priorityMap[pair.Key].AddAll(pair.Value);
			}
		}


		public void Revoke<A>(Callback<A> callback) where A : BA
		{
			this.Revoke(callback, this.DefaultPriority);
		}

		public void Revoke<A>(Callback<A> callback, Priority priority) where A : BA
		{
			priorityMap[priority].Remove(typeof (A), callback);
		}

		public void Revoke<A>(Callback<A, C> callback) where A : BA
		{
			this.Revoke(callback, this.DefaultPriority);
		}

		public void Revoke<A>(Callback<A, C> callback, Priority priority) where A : BA
		{
			priorityMap[priority].Remove(typeof (A), callback);
		}


		public void Invoke(BA arg)
		{
			this.Invoke(arg, default(C));
		}

		public void Invoke(BA arg, C context)
		{
			bool firstPriority = true;
			for (var p = Priority.Highest;; p++)
				// Explicit for-loop to assure order of execution...
			{
				if ((this.PriorityLoopTermination != null) && (this.PriorityLoopTermination(arg, p)))
					break;
				if (priorityMap[p].Call(arg, context, firstPriority))
					firstPriority = false;

				if (p == Priority.Lowest) break;
			}
		}

		private class CallbackSubscriptionList
		{
			public ExecuteConstraint ExecuteConstraint { get; set; }

			private readonly List<Action<BA, C>> subscriptions;

			public CallbackSubscriptionList()
			{
				this.ExecuteConstraint = ExecuteConstraint.Always;
				this.subscriptions = new List<Action<BA, C>>();
			}

			public void Add(Action<BA, C> wrapper, ExecuteConstraint executeConstraint)
			{
				subscriptions.Add(wrapper);
				this.ExecuteConstraint = executeConstraint;
			}

			public void AddAll(CallbackSubscriptionList that)
			{
				this.subscriptions.AddRange(that.subscriptions);
			}

			public void Remove()
			{
				if (subscriptions.Any())
					return;
				subscriptions.RemoveAt(0);
			}

			public bool Any()
			{
				return this.subscriptions.Count > 0;
			}

			public Task[] InvokeAsync(BA arg, C context)
			{
				var tasks = new Task[this.subscriptions.Count];
				for (int i = 0; i < this.subscriptions.Count; i++)
					tasks[i] = Task.Factory.FromAsync(this.subscriptions[i].BeginInvoke, this.subscriptions[i].EndInvoke, arg, context,
						null);
				return tasks;
			}
		}

		private class CallbackMap
		{
			private readonly CallbackRegistry<BA, C> registry;
			private readonly Dictionary<Delegate, CallbackSubscriptionList> callbacks;

			public CallbackMap(CallbackRegistry<BA, C> registry)
			{
				this.registry = registry;
				this.callbacks = new Dictionary<Delegate, CallbackSubscriptionList>();
			}

			public void Add(Delegate callback, Action<BA, C> wrapper, ExecuteConstraint executeConstraint)
			{
				if (!this.callbacks.ContainsKey(callback))
					callbacks.Add(callback, new CallbackSubscriptionList());

				this.callbacks[callback].Add(wrapper, executeConstraint);
			}

			public void AddAll(CallbackMap that)
			{
				foreach (var pair in that.callbacks)
				{
					if (!this.callbacks.ContainsKey(pair.Key))
						this.callbacks.Add(pair.Key, new CallbackSubscriptionList());
					this.callbacks[pair.Key].AddAll(pair.Value);
				}
			}


			public void Remove(Delegate callback)
			{
				if (!this.callbacks.ContainsKey(callback))
					return;

				this.callbacks[callback].Remove();
				if (!this.callbacks[callback].Any())
					this.callbacks.Remove(callback);
			}

			public bool Call(BA arg, C context, bool firstPriority)
			{
				var tasks = new List<Task>();
				foreach (CallbackSubscriptionList handler in this.callbacks.Values)
				{
					if (((handler.ExecuteConstraint == ExecuteConstraint.NeverAsFirstPriority) && firstPriority)
					    || ((handler.ExecuteConstraint == ExecuteConstraint.OnlyAsFirstPriority) && !firstPriority))
						continue;
					tasks.AddRange(handler.InvokeAsync(arg, context));
				}

				switch (this.registry.WaitPolicy)
				{
					case WaitPolicy.DontWait:
						return tasks.Any();
					case WaitPolicy.WaitForAny:
						return Task.WaitAny(tasks.ToArray(), this.registry.WaitTimeout) != -1;
					case WaitPolicy.WaitForAll:
						return Task.WaitAll(tasks.ToArray(), this.registry.WaitTimeout);
					default:
						throw new InvalidOperationException("Unknown WaitPolicy: " + this.registry.WaitPolicy);
				}
			}
		}

		private class ArgumentTypeMap
		{
			private readonly CallbackRegistry<BA, C> registry;
			private readonly Dictionary<Type, CallbackMap> argumentTypeMap;

// ReSharper disable once UnusedAutoPropertyAccessor.Local
			public Priority Priority { get; private set; }

			public ArgumentTypeMap(CallbackRegistry<BA, C> registry, Priority priority)
			{
				this.registry = registry;
				this.Priority = priority;
				this.argumentTypeMap = new Dictionary<Type, CallbackMap>();
			}

			public void Add(Type type, Delegate callback, Action<BA, C> wrapper, ExecuteConstraint executeConstraint)
			{
				lock (argumentTypeMap)
				{
					if (!argumentTypeMap.ContainsKey(type))
						argumentTypeMap.Add(type, new CallbackMap(this.registry));
					argumentTypeMap[type].Add(callback, wrapper, executeConstraint);
				}
			}

			public void AddAll(ArgumentTypeMap that)
			{
				foreach (var pair in that.argumentTypeMap)
				{
					if (!this.argumentTypeMap.ContainsKey(pair.Key))
						this.argumentTypeMap.Add(pair.Key, new CallbackMap(this.registry));
					argumentTypeMap[pair.Key].AddAll(pair.Value);
				}
			}

			public void Remove(Type type, Delegate callback)
			{
				lock (argumentTypeMap)
				{
					if (!argumentTypeMap.ContainsKey(type))
						return;
					argumentTypeMap[type].Remove(callback);
				}
			}

			public bool Call(BA arg, C context, bool firstPriority)
			{
				bool any = false;
				for (Type t = arg.GetType(); t != typeof (Object); t = t.BaseType)
				{
					Debug.Assert(t != null, "t != null");
					if (!argumentTypeMap.ContainsKey(t))
						continue; // No handler for this class in hierarchy registered.

					if (argumentTypeMap[t].Call(arg, context, firstPriority))
						any = true;
				}
				return any;
			}
		}
	}
}