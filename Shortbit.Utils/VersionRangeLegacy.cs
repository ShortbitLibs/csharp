﻿using System;
using System.Text;

namespace Shortbit.Utils
{
	public class VersionRangeLegacy
	{

		#region Segments
		public abstract class Segment
		{
			public abstract bool Matches(int value);

			public abstract override string ToString();
		}

		public class ValueSegment : Segment
		{
			public int Value { get; set; }

			public ValueSegment(int value) => this.Value = value;

			public override bool Matches(int value) => value == -1 || value == this.Value;

			public override string ToString() => this.Value.ToString();
		}

		public class RangeSegment : Segment
		{
			public int MinValue { get; set; }

			public int MaxValue { get; set; }

			public RangeSegment(int min, int max)
			{
				this.MinValue = min;
				this.MaxValue = max;
			}

			public override bool Matches(int value) => MathEx.Within(value, this.MinValue, this.MaxValue);

			public override string ToString() => this.MinValue + "-" + this.MaxValue;
		}

		public class BelowSegment : Segment
		{
			public int MaxValue { get; set; }

			public BelowSegment(int value) => this.MaxValue = value;

			public override bool Matches(int value) => value <= this.MaxValue;

			public override string ToString() => "<=" + this.MaxValue;
		}

		public class AboveSegment : Segment
		{
			public int MinValue { get; set; }

			public AboveSegment(int value) => this.MinValue = value;

			public override bool Matches(int value) => value >= this.MinValue;

			public override string ToString() => ">=" + this.MinValue;
		}

		public class WildcardSegment : Segment
		{
			public override bool Matches(int value) => true;

			public override string ToString() => "*";
		}

		public class UnspecifiedSegment : Segment
		{
			public override bool Matches(int value) => true;

			public override string ToString() => "*";
		}

		#endregion


		public Segment Major { get; set; } = new WildcardSegment();

		public Segment Minor { get; set; } = new WildcardSegment();

		public Segment Build { get; set; } = new UnspecifiedSegment();

		public Segment Revision { get; set; } = new UnspecifiedSegment();


		public static implicit operator VersionRangeLegacy(Version version)
		{
			var res = new VersionRangeLegacy();
			if (version.Major >= 0)
				res.Major = new ValueSegment(version.Major);
			if (version.Minor >= 0)
				res.Minor = new ValueSegment(version.Minor);
			if (version.Build >= 0)
				res.Build = new ValueSegment(version.Build);
			if (version.Revision >= 0)
				res.Revision = new ValueSegment(version.Revision);

			return res;
		}


		public static VersionRangeLegacy Parse(string value)
		{
			var parts = value.Split(new []{'.'}, StringSplitOptions.RemoveEmptyEntries);

			var res = new VersionRangeLegacy();

			if (parts.Length >= 1)
				res.Major = ParseSegement(parts[0]);

			if (parts.Length >= 2)
				res.Minor = ParseSegement(parts[1]);

			if (parts.Length >= 3)
				res.Build = ParseSegement(parts[2]);

			if (parts.Length >= 4)
				res.Revision = ParseSegement(parts[3]);

			return res;
		}


		private static Segment ParseSegement(string part)
		{

			if (part == "*")
				return new WildcardSegment();

			try
			{
				return new ValueSegment(Convert.ToInt32(part));
			}
			catch (FormatException)
			{}

			if (part.Contains("-"))
			{
				var rangePos = part.IndexOf("-", StringComparison.Ordinal);
				var lower = part.Substring(0, rangePos);
				var upper = part.Substring(rangePos + 1);

				return new RangeSegment(Convert.ToInt32(lower), Convert.ToInt32(upper));
			}

			var ordinal = part[0];
			var num = Convert.ToInt32(part.Substring(part[1] == '=' ? 2 : 1));

			if (ordinal == '<')
				return new BelowSegment(num);
			if (ordinal == '>')
				return new AboveSegment(num);

			throw new ArgumentException($"Unknown segment \"{part}\".");
		}

		public override string ToString()
		{
			var builder = new StringBuilder();
			builder.Append(this.Major);
			builder.Append(".");
			builder.Append(this.Minor);

			if (!(this.Build is UnspecifiedSegment) || !(this.Revision is UnspecifiedSegment))
			{
				builder.Append(".");
				builder.Append(this.Build);
			}
			if (!(this.Revision is UnspecifiedSegment))
			{
				builder.Append(".");
				builder.Append(this.Revision);
			}
			return builder.ToString();
		}

		public bool Matches(Version version)
		{
			return this.Major.Matches(version.Major)
			       && this.Minor.Matches(version.Minor)
			       && this.Build.Matches(version.Build)
			       && this.Revision.Matches(version.Revision);
		}
	}
}
