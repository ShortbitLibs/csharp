﻿// /*
//  * OutputStream.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.IO;
using System.Security.Permissions;

namespace Shortbit.Utils.IO
{
	public class OutputStream
	{
		public OutputStream(Stream source)
		{
			this.Source = source;
		}

		public Stream Source { get; private set; }

		public bool CanWrite
		{
			get { return this.Source.CanWrite; }
		}

		public void Dispose()
		{
			this.Source.Dispose();
		}

		public void Flush()
		{
			this.Source.Flush();
		}

		public void Write(byte[] buffer, int offset, int count)
		{
			this.Source.Write(buffer, offset, count);
		}

		public IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.Source.BeginRead(buffer, offset, count, callback, state);
		}

		public void EndWrite(IAsyncResult asyncResult)
		{
			this.Source.EndWrite(asyncResult);
		}


		public static implicit operator OutputStream(Stream source)
		{
			return new OutputStream(source);
		}

		public static implicit operator Stream(OutputStream outputStream)
		{
			return outputStream.Source;
		}
	}
}