﻿// /*
//  * BitReader.cs
//  *
//  *  Created on: 02:46
//  *         Author: 
//  */

using System;
using System.IO;
using System.Numerics;

namespace Shortbit.Utils.IO
{
	public class BitReader : Disposable
	{
		public Endianess Endianess { get; private set; }

		private Stream input;

		private byte cachedByte;
		private int remainingBits;


		public BitReader(Stream input)
			: this(input, Endianess.LittleEndian)
		{
		}

		public BitReader(Stream input, Endianess endianess)
		{
			this.input = input;
			this.Endianess = endianess;
		}

		public bool ReadBit()
		{
			return this.Read(1) > 0;
		}

		public byte ReadByte()
		{
			return (byte) this.Read(8);
		}


		public BigInteger Read(int bits)
		{
			int resultBits = 0;

			//	Console.WriteLine("bits: " + bits);
			BigInteger result = 0;
			while (resultBits != bits)
			{
				if (this.remainingBits == 0)
					this.ReadByteFromStream();

				int copybits = Math.Min(bits - resultBits, this.remainingBits);
				//	Console.WriteLine("copybits: " + copybits);
				int copy = (this.cachedByte & ((1 << copybits) - 1));
				//	Console.WriteLine("copy: " + copy);
				if (this.Endianess == Endianess.BigEndian)
					result |= copy << (bits - resultBits - copybits);
				else
					result |= copy << resultBits;
				this.cachedByte >>= copybits;
				this.remainingBits -= copybits;
				resultBits += copybits;
				//	Console.WriteLine("remainingBits: " + remainingBits);
				//	Console.WriteLine("resultBits: " + resultBits);
			}
			return result;
		}

		/// <summary>
		///     Aligns the reader to the next full byte in stream, discarding any remaining bits.
		/// </summary>
		public void Align()
		{
			if (remainingBits == 8)
				return;

			this.cachedByte = 0;
			this.remainingBits = 0;
		}

		/// <summary>
		///     Reads the next byte from stream, writes it to the internal buffer and resets the number of remainign bits on
		///     current byte.
		/// </summary>
		private void ReadByteFromStream()
		{
			//	Console.WriteLine("Reading next byte.");
			int next = this.input.ReadByte();
			if (next == -1)
				throw new EndOfStreamException();
			this.cachedByte = (byte) next;
			this.remainingBits = 8;
		}

		/// <summary>
		///     This method is used to call Dipose on any managed resource used by this class.
		///     NOTE: It is called after Dispose(bool fromFinalizer), but before DisposeUnmanaged() and only if not disposed by the
		///     finalizer!
		/// </summary>
		protected override void DisposeManaged()
		{
			base.DisposeManaged();
			this.input.Dispose();
		}
	}
}