﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Utils.IO
{
	public class TimeoutStream<T> : Stream 
		where T : Stream
	{

		private readonly T source;
		private readonly Func<T, bool> dataAvailable;

		public int Timeout { get; set; } = 5000;

		public bool OwnsSource { get; set; }

		public TimeoutStream(T source, Func<T, bool> dataAvailable = null)
		{
			this.source = source;

			if (dataAvailable == null)
				dataAvailable = s => s.Position < s.Length;
			this.dataAvailable = dataAvailable;

		}

		/// <inheritdoc />
		public override void Flush() => this.source.Flush();

		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin) => this.source.Seek(offset, origin);

		/// <inheritdoc />
		public override void SetLength(long value) => this.source.SetLength(value);

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count)
		{
			int time = 0;
			int read = 0;
			while (read < count)
			{
				if (!this.dataAvailable(this.source))
				{
					Thread.Sleep(1);
					time += 1;
					if (time >= this.Timeout)
						throw new TimeoutException();

					continue;
				}

				int curr = this.source.Read(buffer, offset + read, count - read);
				if (curr == 0)
					throw new EndOfStreamException();
				read += curr;
			}
			return read;
		}

		public byte[] ReadBytes(int count)
		{
			var buffer = new byte[count];
			this.Read(buffer, 0, count);
			return buffer;
		}

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.source.Write(buffer, offset, count);
		}

		/// <inheritdoc />
		public override bool CanRead => this.source.CanRead;

		/// <inheritdoc />
		public override bool CanSeek => this.source.CanSeek;

		/// <inheritdoc />
		public override bool CanWrite => this.source.CanWrite;

		/// <inheritdoc />
		public override long Length => this.source.Length;

		/// <inheritdoc />
		public override long Position
		{
			get => this.source.Position;
			set => this.source.Position = value;
		}

		/// <summary>Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream. Instead of calling this method, ensure that the stream is properly disposed.</summary>
		public override void Close()
		{
			if (this.OwnsSource)
				this.source.Close();
		}
	}
}
