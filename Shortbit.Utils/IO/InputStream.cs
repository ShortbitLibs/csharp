﻿// /*
//  * InputStream.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Shortbit.Utils.IO
{
	public class InputStream
	{
		public InputStream(Stream source)
		{
			this.Source = source;
		}


		public Stream Source { get; private set; }

		public bool CanRead
		{
			get { return this.Source.CanRead; }
		}


		public void Flush()
		{
			this.Source.Flush();
		}

		public int Read([In, Out] byte[] buffer, int offset, int count)
		{
			return this.Source.Read(buffer, offset, count);
		}
		
		public IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.Source.BeginRead(buffer, offset, count, callback, state);
		}

		public int EndRead(IAsyncResult asyncResult)
		{
			return this.Source.EndRead(asyncResult);
		}


		public void Dispose()
		{
			this.Source.Dispose();
		}

		public static implicit operator InputStream(Stream source)
		{
			return new InputStream(source);
		}

		public static implicit operator Stream(InputStream inputStream)
		{
			return inputStream.Source;
		}
	}
}