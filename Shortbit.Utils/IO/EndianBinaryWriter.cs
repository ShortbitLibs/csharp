﻿// /*
//  * EndianBinaryWriter.cs
//  *
//  *  Created on: 01:19
//  *         Author: 
//  */

using System;
using System.IO;
using System.Text;

namespace Shortbit.Utils.IO
{
	public class EndianBinaryWriter : BinaryWriter
	{
		public Endianess Order { get; private set; }

		protected EndianBinaryWriter(Endianess order)
		{
			Order = order;
		}

		public EndianBinaryWriter(Stream output, Endianess order)
			: base(output)
		{
			Order = order;
		}

		public EndianBinaryWriter(Stream output, Encoding encoding, Endianess order)
			: base(output, encoding)
		{
			Order = order;
		}

		public override void Write(bool value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(char ch)
		{
			base.Write(EndianBitConverter.GetBytes(ch, Order));
		}

		public override void Write(char[] chars)
		{
			foreach (char c in chars)
				Write(c);
		}

		public override void Write(char[] chars, int index, int count)
		{
			for (int c = index; c < index + count; c++)
				Write(chars[c]);
		}

		public override void Write(double value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(decimal value)
		{
			throw new NotImplementedException();
		}

		public override void Write(short value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(ushort value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(uint value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(int value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(long value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(ulong value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}

		public override void Write(float value)
		{
			base.Write(EndianBitConverter.GetBytes(value, Order));
		}
	}
}