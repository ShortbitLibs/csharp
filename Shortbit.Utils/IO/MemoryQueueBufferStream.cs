﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Utils.IO
{
	/// <summary>
	/// This stream maintains data only until the data is read, then it is purged from the stream.
	/// Source from <see href="https://codereview.stackexchange.com/a/93266">https://codereview.stackexchange.com/a/93266</see>
	/// </summary>
	public class MemoryQueueBufferStream : Stream
	{
		/// <summary>
		/// Represents a single write into the MemoryQueueBufferStream.  Each write is a seperate chunk
		/// </summary>
		private class Chunk
		{
			/// <summary>
			/// As we read through the chunk, the start index will increment.  When we get to the end of the chunk,
			/// we will remove the chunk
			/// </summary>
			public int ChunkReadStartIndex { get; set; }

			/// <summary>
			/// Actual Data
			/// </summary>
			public byte[] Data { get; set; }
		}

		//Maintains the streams data.  The Queue object provides an easy and efficient way to add and remove data
		//Each item in the queue represents each write to the stream.  Every call to write translates to an item in the queue
		private readonly ConcurrentQueue<Chunk> buffers;

		private readonly ManualResetEvent dataAvailable = new ManualResetEvent(false);
		private readonly object readLock = new object();

		public MemoryQueueBufferStream()
		{
			this.buffers = new ConcurrentQueue<Chunk>();
		}

		/// <inheritdoc />
		public override int ReadTimeout { get; set; } = Timeout.Infinite;

		/// <summary>When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.</summary>
		/// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.</returns>
		/// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset" /> and (<paramref name="offset" /> + <paramref name="count" /> - 1) replaced by the bytes read from the current source. </param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin storing the data read from the current stream. </param>
		/// <param name="count">The maximum number of bytes to be read from the current stream. </param>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the buffer length. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		/// <paramref name="buffer" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		/// <paramref name="offset" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		/// <exception cref="T:System.TimeoutException">The method timed out while reading. </exception>
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.ValidateBufferArgs(buffer, offset, count);

			int remainingBytesToRead = count;

			int totalBytesRead = 0;

			lock (this.readLock)
			{
				if (!this.dataAvailable.WaitOne(this.ReadTimeout))
					throw new TimeoutException();

				//Read until we hit the requested count, or until we hav nothing left to read
				while (totalBytesRead <= count && this.buffers.Count > 0)
				{
					//Get first chunk from the queue
					if (!this.buffers.TryPeek(out var chunk))
						break;
				
					//Determine how much of the chunk there is left to read
					int unreadChunkLength = chunk.Data.Length - chunk.ChunkReadStartIndex;

					//Determine how much of the unread part of the chunk we can actually read
					int bytesToRead = Math.Min(unreadChunkLength, remainingBytesToRead);

					if (bytesToRead > 0)
					{
						//Read from the chunk into the buffer
						Buffer.BlockCopy(chunk.Data, chunk.ChunkReadStartIndex, buffer, offset + totalBytesRead,
										 bytesToRead);

						totalBytesRead += bytesToRead;
						remainingBytesToRead -= bytesToRead;

						//If the entire chunk has been read,  remove it
						if (chunk.ChunkReadStartIndex + bytesToRead >= chunk.Data.Length)
						{
							this.buffers.TryDequeue(out _);
						}
						else
						{
							//Otherwise just update the chunk read start index, so we know where to start reading on the next call
							chunk.ChunkReadStartIndex = chunk.ChunkReadStartIndex + bytesToRead;
						}
					}
					else
					{
						break;
					}
				}

				if (this.buffers.Count == 0)
					this.dataAvailable.Reset();
			}

			return totalBytesRead;
		}

		private void ValidateBufferArgs(byte[] buffer, int offset, int count)
		{
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "offset must be non-negative");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "count must be non-negative");
			}
			if ((buffer.Length - offset) < count)
			{
				throw new ArgumentException("requested count exceeds available size");
			}
		}
		
		/// <summary>
		/// Writes data to the stream
		/// </summary>
		/// <param name="buffer">Data to copy into the stream</param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.ValidateBufferArgs(buffer, offset, count);

			// We don't want to use the buffer passed in, as it could be altered by the caller
			byte[] bufSave = new byte[count];
			Buffer.BlockCopy(buffer, offset, bufSave, 0, count);

			// Add the data to the queue
			// Since we're always creating a new chunk, there's no need for locking. ConcurrentQueue does the synchronizing for us.
			this.buffers.Enqueue(new Chunk() { ChunkReadStartIndex = 0, Data = bufSave });
			this.dataAvailable.Set();
		}

		public override bool CanSeek => false;

		/// <summary>
		/// Always returns 0
		/// </summary>
		public override long Position
		{
			
			get => 0; // We're always at the start of the stream, because as the stream purges what we've read
			set => throw new NotSupportedException(this.GetType().Name + " is not seekable");
		}

		public override bool CanWrite => true;

		public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException(this.GetType().Name + " is not seekable");

		public override void SetLength(long value) => throw new NotSupportedException(this.GetType().Name + " length can not be changed");


		public override bool CanRead => true;

		public override long Length
		{
			get
			{

				if (this.buffers == null)
				{
					return 0;
				}

				if (this.buffers.Count == 0)
				{
					return 0;
				}

				return this.buffers.Sum(b => b.Data.Length - b.ChunkReadStartIndex);
			}
		}

		public override void Flush()
		{
		}
	}
}
