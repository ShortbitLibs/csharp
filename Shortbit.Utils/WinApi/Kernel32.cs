﻿// /*
//  * Kernel32.cs
//  *
//  *  Created on: 14:59
//  *         Author: 
//  */

using System;
using System.Runtime.InteropServices;

namespace Shortbit.Utils.WinApi
{
	public static class Kernel32
	{
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr LoadLibrary(string dllToLoad);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern bool FreeLibrary(IntPtr hModule);


		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern void SetDllDirectory(string lpPathName);
	}
}