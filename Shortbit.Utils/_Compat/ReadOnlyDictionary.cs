﻿using System;
using System.Collections.Generic;
using System.Text;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic
{
#if NET40
/*
	public class ReadOnlyDictionary<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
	{
		private readonly IDictionary<TKey, TValue> source;

		public ReadOnlyDictionary(IDictionary<TKey, TValue> source)
		{
			this.source = source;
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value) => this.source.TryGetValue(key, out value);

		/// <inheritdoc />
		public TValue this[TKey key] => this.source[key];

		/// <inheritdoc />
		public IEnumerable<TKey> Keys => this.source.Keys;

		/// <inheritdoc />
		public IEnumerable<TValue> Values => this.source.Values;
	}
	*/
#endif
}
