﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// ReSharper disable once CheckNamespace
namespace System.Threading.Tasks
{
	public class TaskCompat
	{
		public static Task<TResult> FromResult<TResult>(TResult result)
		{
#if NET40
			var source = new TaskCompletionSource<TResult>();
			var t = source.Task;
			source.SetResult(result);
			return t;
#else
			return Task.FromResult(result);
#endif
		}
	}
}
