﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public sealed class SkeletonTypeCollection : SkeletonType
	{

		public static bool IsCollectionType(Type type)
		{
			bool any = false;
			foreach(var genericArgument in GenericsUtils.GetCollectionArgumentTypes(type))
			{
				any = true;

				if (GenericsUtils.GetKeyValuePairArguments(genericArgument) != null)
					return false; // If one of the Collections is KeyValuePair<TK, TV>, it's a Dictionary, not a Collection.
			}

			return any;
		}

		public static SkeletonTypeCollection CreateCollection(Type sourceType) =>
			SkeletonTypeCollection.CreateCollection(new SkeletonTypeCreationContext(), sourceType, true);

		public static SkeletonTypeCollection CreateCollection(SkeletonTypeCreationContext context, Type sourceType)
			=> SkeletonTypeCollection.CreateCollection(context, sourceType, true);

		internal static SkeletonTypeCollection CreateCollection(SkeletonTypeCreationContext context, Type sourceType, bool typeCheck)
		{
			if (typeCheck && !SkeletonTypeCollection.IsCollectionType(sourceType))
				throw new ArgumentException($"Given type \"{sourceType.FullName}\" is not a collection type.");

			if (context.Cache.ContainsKey(sourceType))
				return (SkeletonTypeCollection) context.Cache[sourceType];

			var argT = GenericsUtils.GetCollectionArgumentTypes(sourceType).ToList();
			if (argT.Count > 1)
				throw new ArgumentException($"Can't parse class {sourceType.FullName} as Collection since it implements multiple ICollection<T> interfaces.");

			
			var elementType = SkeletonType.Create(context, argT[0]); 

			var res = new SkeletonTypeCollection(sourceType, elementType);
			context.Cache.Add(res);
			return res;
		}

		private SkeletonTypeCollection(Type sourceType, SkeletonType elementType) : base(sourceType)
		{
			this.ElementType = elementType;
		}


		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString() => $"Collection <{this.ElementType}>";

		[NotNull]
		public SkeletonType ElementType { get; }
	}
	#endif
}
