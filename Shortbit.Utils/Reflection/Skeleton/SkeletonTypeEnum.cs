﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Reflection.Skeleton
{
#if !NET40
	public sealed class SkeletonTypeEnum : SkeletonType
	{
		public static bool IsEnumType(Type type) => type.IsEnum;

		public static SkeletonTypeEnum CreateEnum(Type sourceType)
			=> SkeletonTypeEnum.CreateEnum(new SkeletonTypeCreationContext(), sourceType);

		public static SkeletonTypeEnum CreateEnum(SkeletonTypeCreationContext context, Type sourceType)
			=> SkeletonTypeEnum.CreateEnum(context, sourceType, true);

		internal static SkeletonTypeEnum CreateEnum(SkeletonTypeCreationContext context, Type sourceType, bool typeCheck)
		{
			if (typeCheck && !SkeletonTypeEnum.IsEnumType(sourceType))
				throw new ArgumentException($"Given type \"{sourceType.FullName}\" is not a enum type.");

			if (context.Cache.ContainsKey(sourceType))
				return (SkeletonTypeEnum) context.Cache[sourceType];

			var values = Enum.GetValues(sourceType).Cast<object>().ToList().AsReadOnly();
			var underlyingType = SkeletonTypePrimitive.CreatePrimitive(context, Enum.GetUnderlyingType(sourceType));

			var res = new SkeletonTypeEnum(sourceType, values, underlyingType);
			context.Cache.Add(res);
			return res;
		}

		private SkeletonTypeEnum(Type sourceType, IReadOnlyCollection<object> values, SkeletonTypePrimitive underlyingType) : base(sourceType)
		{
			this.Values = values;
			this.UnderlyingType = underlyingType;
		}
		

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString() => $"Enum <{this.FullName}>";

		public IReadOnlyCollection<Object> Values { get; }
		public SkeletonTypePrimitive UnderlyingType { get; }
	}
	#endif
}
