﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public sealed class SkeletonTypeDictionary : SkeletonType
	{

		public static bool IsDictionaryType(Type type)
		{
			foreach(var genericArgument in GenericsUtils.GetCollectionArgumentTypes(type))
			{
				if (GenericsUtils.GetKeyValuePairArguments(genericArgument) != null)
					return true; // If one of the Collections is KeyValuePair<TK, TV>, it's a Dictionary.
			}

			return false;
		}

		public static SkeletonTypeDictionary CreateDictionary(Type sourceType)
			=> SkeletonTypeDictionary.CreateDictionary(new SkeletonTypeCreationContext(), sourceType);

		public static SkeletonTypeDictionary CreateDictionary(SkeletonTypeCreationContext context, Type sourceType)
			=> SkeletonTypeDictionary.CreateDictionary(context, sourceType, true);

		internal static SkeletonTypeDictionary CreateDictionary(SkeletonTypeCreationContext context, Type sourceType, bool typeCheck )
		{
			if (typeCheck && !SkeletonTypeDictionary.IsDictionaryType(sourceType))
				throw new ArgumentException($"Given type \"{sourceType.FullName}\" is not a Dictionary.");

			if (context.Cache.ContainsKey(sourceType))
				return (SkeletonTypeDictionary) context.Cache[sourceType];


			var kvpT = GenericsUtils.GetCollectionArgumentTypes(sourceType)
									.Select(GenericsUtils.GetKeyValuePairArguments).Where(t => t != null).Select(t => t.Value)
									.ToList();
			if (kvpT.Count > 1)
				throw new ArgumentException($"Can't parse class {sourceType.FullName} as Dictionary since it implements multiple ICollection<KeyValuePair<TK,TV>> interfaces.");

			var keyType = SkeletonType.Create(context, kvpT[0].Key);
			var valueType = SkeletonType.Create(context, kvpT[0].Value);

			var res = new SkeletonTypeDictionary(sourceType, keyType, valueType);
			context.Cache.Add(res);
			return res;
		}

		private SkeletonTypeDictionary(Type sourceType, SkeletonType keyType, SkeletonType valueType) : base(sourceType)
		{
			this.KeyType = keyType;
			this.ValueType = valueType;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString() => $"Dictionary <{this.KeyType}, {this.ValueType}>";


		[NotNull]
		public SkeletonType KeyType { get; }


		[NotNull]
		public SkeletonType ValueType { get; }
	}
	#endif
}
