﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	internal static class GenericsUtils
	{
		public static IEnumerable<Type> GetCollectionArgumentTypes(Type type)
		{
			var interfaces = type.GetInterfaces();

			var collectionInterfaces = interfaces.Where(i => i.IsGenericType && !i.IsGenericTypeDefinition && i.GetGenericTypeDefinition() == typeof(ICollection<>)).ToList();

			if (collectionInterfaces.Count == 0)
				yield break;

			foreach (var @interface in collectionInterfaces)
			{
				var genericArgument = @interface.GetGenericArguments()[0];
				yield return genericArgument;
			}
		}
		
		public static KeyValuePair<Type, Type>? GetKeyValuePairArguments(Type type)
		{
			if (!type.IsGenericType ||
				type.GetGenericTypeDefinition() != typeof(KeyValuePair<,>))
				return null;
			return new KeyValuePair<Type, Type>(type.GetGenericArguments()[0], type.GetGenericArguments()[1]);
		}
	}
}
