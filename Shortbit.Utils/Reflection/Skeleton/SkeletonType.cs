﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public abstract class SkeletonType
	{
		public static SkeletonType Create(Type type) => SkeletonType.Create(new SkeletonTypeCreationContext(), type);

		public static SkeletonType Create(SkeletonTypeCreationContext context, Type type)
		{
			if (SkeletonTypePrimitive.IsPrimitiveType(type))
				return SkeletonTypePrimitive.CreatePrimitive(context, type, false);

			if (SkeletonTypeEnum.IsEnumType(type))
				return SkeletonTypeEnum.CreateEnum(context, type, false);

			if (SkeletonTypeCollection.IsCollectionType(type))
				return SkeletonTypeCollection.CreateCollection(context, type, false);

			if (SkeletonTypeDictionary.IsDictionaryType(type))
				return SkeletonTypeDictionary.CreateDictionary(context, type, false);

			if (SkeletonTypeComplex.IsComplexType(type))
				return SkeletonTypeComplex.CreateComplex(context, type, false);

			throw new ArgumentException("Given type is not Primitive, Enum, Collection, Dictionary or Complex.");
		}


		public string Name => this.SourceType.Name;

		public string FullName => this.SourceType.FullName;

		[NotNull]
		public Type SourceType { get; }


		private protected SkeletonType(Type sourceType)
		{
			this.SourceType = sourceType;
		}

	}
	#endif
}
