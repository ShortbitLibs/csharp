﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Utils.Reflection.Skeleton
{
	public delegate bool PropertyFilterDelegate(Type containingType, PropertyInfo property);

	public delegate bool FieldFilterDelegate(Type containingType, FieldInfo field);

	public static class Filters
	{
		public static PropertyFilterDelegate And(PropertyFilterDelegate a, PropertyFilterDelegate b, 
												 params PropertyFilterDelegate[] other)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));
			if (b == null)
				throw new ArgumentNullException(nameof(b));

			return (containingType, property) =>
			{
				if (!a(containingType, property))
					return false;
				if (!b(containingType, property))
					return false;
				return other.All(d => d(containingType, property));
			};
		}
		public static PropertyFilterDelegate Or(PropertyFilterDelegate a, PropertyFilterDelegate b, 
												 params PropertyFilterDelegate[] other)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));
			if (b == null)
				throw new ArgumentNullException(nameof(b));

			return (containingType, property) =>
			{
				if (a(containingType, property))
					return true;
				if (b(containingType, property))
					return true;
				return other.Any(d => d(containingType, property));
			};
		}
		public static PropertyFilterDelegate Not(PropertyFilterDelegate a)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));

			return (containingType, property) => !a(containingType, property);
		}

		public static FieldFilterDelegate And(FieldFilterDelegate a, FieldFilterDelegate b, 
												 params FieldFilterDelegate[] other)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));
			if (b == null)
				throw new ArgumentNullException(nameof(b));

			return (containingType, field) =>
			{
				if (!a(containingType, field))
					return false;
				if (!b(containingType, field))
					return false;
				return other.All(d => d(containingType, field));
			};
		}

		public static FieldFilterDelegate Or(FieldFilterDelegate a, FieldFilterDelegate b, 
												params FieldFilterDelegate[] other)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));
			if (b == null)
				throw new ArgumentNullException(nameof(b));

			return (containingType, field) =>
			{
				if (a(containingType, field))
					return true;
				if (b(containingType, field))
					return true;
				return other.Any(d => d(containingType, field));
			};
		}

		public static FieldFilterDelegate Not(FieldFilterDelegate a)
		{
			if (a == null)
				throw new ArgumentNullException(nameof(a));

			return (containingType, field) => !a(containingType, field);
		}

		public static class Property
		{
			public static readonly PropertyFilterDelegate All = (containingType, property) => !property.IsStatic();

			public static readonly PropertyFilterDelegate None = (containingType, property) => false;
			
			public static readonly PropertyFilterDelegate ReadAndWrite = (containingType, property) => !property.IsStatic() && property.CanRead && property.CanWrite;

			public static readonly PropertyFilterDelegate ReadOnly = (containingType, property) => !property.IsStatic() && property.CanRead && !property.CanWrite;
		}

		public static class Field
		{
			public static readonly FieldFilterDelegate All = (containingType, field) => !field.IsStatic;

			public static readonly FieldFilterDelegate None = (containingType, field) => false;
			
			public static readonly FieldFilterDelegate Public = (containingType, field) => !field.IsStatic & field.IsPublic;
			
			public static readonly FieldFilterDelegate Private = (containingType, field) => !field.IsStatic & field.IsPrivate;
			
		}
	}
}
