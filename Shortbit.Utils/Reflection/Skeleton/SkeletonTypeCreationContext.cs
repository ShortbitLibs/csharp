﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.Reflection.Skeleton
{
	#if !NET40
	public class SkeletonTypeCreationContext
	{
		private SkeletonTypeCache cache = new SkeletonTypeCache();
		private PropertyFilterDelegate propertyFilter = Filters.Property.ReadAndWrite;
		private FieldFilterDelegate fieldFilter = Filters.Field.Public;

		[NotNull]
		public SkeletonTypeCache Cache
		{
			get => this.cache;
			set => this.cache = value ?? throw new ArgumentNullException(nameof(value));
		}

		[NotNull]
		public PropertyFilterDelegate PropertyFilter
		{
			get => this.propertyFilter;
			set => this.propertyFilter = value ?? throw new ArgumentNullException(nameof(value));
		}

		[NotNull]
		public FieldFilterDelegate FieldFilter
		{
			get => this.fieldFilter;
			set => this.fieldFilter = value ?? throw new ArgumentNullException(nameof(value));
		}
	}
	#endif
}
