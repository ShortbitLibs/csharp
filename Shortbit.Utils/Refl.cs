﻿using System;

namespace Shortbit.Utils
{
    public static class Refl
    {
        public sealed class T1 { }
        public sealed class T2 { }
        public sealed class T3 { }
        public sealed class T4 { }
        public sealed class T5 { }
        public sealed class T6 { }
        public sealed class T7 { }
        public sealed class T8 { }
        public sealed class T9 { }
        public sealed class T10 { }

        public static readonly Type[] T =
        {
            typeof(T1),
            typeof(T2),
            typeof(T3),
            typeof(T4),
            typeof(T5),
            typeof(T6),
            typeof(T7),
            typeof(T8),
            typeof(T9),
            typeof(T10),
        };
    }
}
