﻿using System;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils
{
	public static class Throw
	{
		[NotNull]
		public static T IfNull<T>([CanBeNull]in T arg, [NotNull]string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			return arg;
		}

		[NotNull]
		public static T IfNull<T>([CanBeNull]in T arg, [NotNull]string name, Func<string, Exception> exception)
		{
			if (arg == null) throw exception(name);
			return arg;
		}

		[NotNull]
        public static string IfNullOrEmpty([CanBeNull]string arg, [NotNull]string name)
        {
            if (string.IsNullOrEmpty(arg)) throw new ArgumentNullException(name);
			return arg;
		}
		[NotNull]
		public static string IfNullOrEmpty([CanBeNull]string arg, [NotNull]string name, Func<string, Exception> exception)
		{
			if (string.IsNullOrEmpty(arg)) throw exception(name);
			return arg;
		}

		[NotNull]
	    public static string IfNullOrWhitespace([CanBeNull]string arg, [NotNull]string name)
	    {
	        if (string.IsNullOrWhiteSpace(arg)) throw new ArgumentNullException(name);
			return arg;
		}

		[NotNull]
		public static string IfNullOrWhitespace([CanBeNull]string arg, [NotNull]string name, Func<string, Exception> exception)
		{
			if (string.IsNullOrWhiteSpace(arg)) throw exception(name);
			return arg;
		}
		
		public static T IfNotWithin<T>([NotNull]T val, [NotNull]T min, [NotNull]T max, [NotNull]string name, bool includeLower = true, bool includeUpper = true) where T : IComparable<T>
		{
			bool outside = false;

			Throw.IfNull(val, nameof(val));
			Throw.IfNull(min, nameof(min));
			Throw.IfNull(max, nameof(max));
			Throw.IfNullOrWhitespace(name, nameof(name));

			var cmpMin = val.CompareTo(min);
			var cmpMax = val.CompareTo(max);


			if (cmpMin < 0)
				outside = true;
			if (!includeLower && cmpMin == 0)
				outside = true;
			if (cmpMax > 0)
				outside = true;
			if (!includeUpper && cmpMax == 0)
				outside = true;

			if (outside)
				throw new ArgumentOutOfRangeException(name, val, name+" parameter must be between "+min+" an "+max);
			return val;
		}

		/*
		#if !NETCOREAPP
        public static dynamic IfNotWithin(dynamic val, dynamic min, dynamic max, string name, bool includeLower = true, bool includeUpper = true)
		{
			bool outside = false;

			if (val < min)
				outside = true;
			if (!includeLower && val == min)
				outside = true;
			if (val > max)
				outside = true;
			if (!includeUpper && val == max)
				outside = true;

			if (outside)
				throw new ArgumentOutOfRangeException(name, val, name+" parameter must be between "+min+" an "+max);
			return val;
		}
		#endif
		*/
	}
}
