﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Shortbit.Utils
{
	public class UnsafeByteArray : Disposable
	{ 
		public byte[] BackendArray { get; }

		private GCHandle pinHandle;

		public UnsafeByteArray(byte[] backendArray, GCHandle pinHandle1)
		{
			this.BackendArray = backendArray;
			this.pinHandle = pinHandle1;
		}
	

		protected override void DisposeUnmanaged()
		{
			base.DisposeUnmanaged();
			for (int i = 0; i < this.BackendArray.Length; i++)
				this.BackendArray[i] = 0;

			if (this.pinHandle.IsAllocated)
				this.pinHandle.Free();
		}
	}
}
