﻿// /*
//  * PropertyBase.cs
//  *
//  *  Created on: 04:01
//  *         Author: 
//  */

using System;

namespace Shortbit.Utils
{
	public abstract class PropertyBase
	{
		public abstract object RawValue { get; }

		public Type ValueType
		{
			get
			{
				if (this.RawValue == null)
					return null;
				return this.RawValue.GetType();
			}
		}

		public Object GetRaw()
		{
			return this.RawValue;
		}

		public T GetAs<T>()
		{
			return (T) this.GetRaw();
		}

		/// <summary>
		///     Gibt eine Zeichenfolge zurück, die das aktuelle Objekt darstellt.
		/// </summary>
		/// <returns>
		///     Eine Zeichenfolge, die das aktuelle Objekt darstellt.
		/// </returns>
		public override string ToString()
		{
			if (this.RawValue == null)
				return "";
			return this.RawValue.ToString();
		}

		/// <summary>
		///     Bestimmt, ob das angegebene <see cref="T:System.Object" /> und das aktuelle <see cref="T:System.Object" /> gleich
		///     sind.
		/// </summary>
		/// <returns>
		///     true, wenn das angegebene Objekt und das aktuelle Objekt gleich sind, andernfalls false.
		/// </returns>
		/// <param name="obj">Das Objekt, das mit dem aktuellen Objekt verglichen werden soll.</param>
		public override bool Equals(object obj)
		{
			if (this.RawValue == null)
				return obj == null;
			return this.RawValue.Equals(obj);
		}

		/// <summary>
		///     Fungiert als Hashfunktion für einen bestimmten Typ.
		/// </summary>
		/// <returns>
		///     Ein Hashcode für das aktuelle <see cref="T:System.Object" />.
		/// </returns>
		public override int GetHashCode()
		{
			if (this.RawValue == null)
				return 0;
			return this.RawValue.GetHashCode();
		}
	}
}