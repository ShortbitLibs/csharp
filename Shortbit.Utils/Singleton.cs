﻿// /*
//  * Singleton.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Reflection;

namespace Shortbit.Utils
{
	public class Singleton<T> where T : class
	{
		private static readonly Lazy<Func<T>> DefaultConstructor = new Lazy<Func<T>>(() => () =>
		{
			ConstructorInfo constructor =
				typeof (T).GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null,
					Type.EmptyTypes, null);

			if (constructor == null)
				throw new ArgumentException("Given Singleton type \"" + typeof (T).Name + "\" has no argument-free Contructor.");


			return (T) constructor.Invoke(null);
		});

// ReSharper disable once StaticFieldInGenericType
		private static readonly object SyncRoot = new object();
		private static volatile T instance;

		private Singleton()
		{
		}


		/// <summary>
		///     Gets the Instance for the given class.
		/// </summary>
		public static T Instance
		{
			get { return ProvideInstance(null); }
		}

		public static T ProvideInstance(Func<T> constuctor)
		{
			if (constuctor == null)
				constuctor = DefaultConstructor.Value;

			if (instance == null)
			{
				lock (SyncRoot)
				{
					if (instance == null) // Doublecheck lock pattern. See http://msdn.microsoft.com/en-us/library/ff650316.aspx
					{
						instance = constuctor();
					}
				}
			}
			return instance;
		}
	}
}