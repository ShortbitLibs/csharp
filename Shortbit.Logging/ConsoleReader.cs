﻿using System;
using System.Text;
using System.Threading;

namespace Shortbit.Logging
{
    public static class ConsoleReader
    {
	    public delegate void NewKeyEvent(ConsoleKeyInfo info);

	    public delegate void NewLineEvent(string line);

		
	    private static readonly Thread TReadKey;
		private static readonly ManualResetEvent readKeyEnable;
		private static readonly ManualResetEvent readKeyInput;
	    private static ConsoleKeyInfo lastKey;
		private static readonly ReaderWriterLock lastKeyLock = new ReaderWriterLock();
		
		
	    private static string currentLine;
	    private static int currentLineIndex;
		private static readonly ReaderWriterLock currentLineLock = new ReaderWriterLock();

		private static string lastLine;
		private static readonly ReaderWriterLock lastLineLock = new ReaderWriterLock();
		private static readonly ManualResetEvent readLineInput;

	    private static bool replace;
	    private static bool printKeys;
		private static readonly ReaderWriterLock printKeysLock = new ReaderWriterLock();
		private static bool printNewLine;
		private static readonly ReaderWriterLock printNewLineLock = new ReaderWriterLock();

		public static bool PrintKeys
	    {
		    get
		    {
			    printKeysLock.AcquireReaderLock(-1);
			    bool val = printKeys;
			    printKeysLock.ReleaseLock();
			    return val;
		    }
		    set
		    {
			    printKeysLock.AcquireWriterLock(-1);
			    printKeys = value;
			    printKeysLock.ReleaseLock();
		    }
		}
		public static bool PrintNewLine
		{
			get
			{
				printNewLineLock.AcquireReaderLock(-1);
				bool val = printNewLine;
				printNewLineLock.ReleaseLock();
				return val;
			}
			set
			{
				printNewLineLock.AcquireWriterLock(-1);
				printNewLine = value;
				printNewLineLock.ReleaseLock();
			}
		}

		public static event NewKeyEvent NewKey;

	    public static event NewLineEvent NewLine;

	    public static string CurrentLine
	    {
		    get
		    {
			    currentLineLock.AcquireReaderLock(-1);
			    var line = currentLine;
			    currentLineLock.ReleaseLock();
			    return line;
		    }
	    }

		static ConsoleReader()
		{
			replace = false;
			printKeys = true;
			printNewLine = true;

			TReadKey = new Thread(TReadKey_Run)
			{
				IsBackground = true,
				Name = "TReadKey"
			};
			readKeyEnable = new ManualResetEvent(false);
			readKeyInput = new ManualResetEvent(false);

			TReadKey.Start();

			currentLine = "";
			lastLine = "";
			readLineInput = new ManualResetEvent(false);
		}


		private static void OnNewKey(ConsoleKeyInfo info)
		{
			if (NewKey != null)
			{
				// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
				foreach (NewKeyEvent d in NewKey.GetInvocationList())
				{
					d.BeginInvoke(info, EndNewKey, null);
				}
			}
		}
		private static void EndNewKey(IAsyncResult iar)
		{
			var ar = (System.Runtime.Remoting.Messaging.AsyncResult)iar;
			var invokedMethod = (NewKeyEvent)ar.AsyncDelegate;

			try
			{
				invokedMethod.EndInvoke(iar);
			}
			catch
			{
			}
		}

		private static void OnNewLine(string line)
		{
			if (NewLine != null)
			{
				// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
				foreach (NewLineEvent d in NewLine.GetInvocationList())
				{
					d.BeginInvoke(line, EndNewLine, null);
				}
			}
		}
		private static void EndNewLine(IAsyncResult iar)
		{
			var ar = (System.Runtime.Remoting.Messaging.AsyncResult)iar;
			var invokedMethod = (NewLineEvent)ar.AsyncDelegate;

			try
			{
				invokedMethod.EndInvoke(iar);
			}
			catch
			{
			}
		}


	    private static void TReadKey_Run()
	    {

		    while (readKeyEnable.WaitOne())
		    {
				var key = System.Console.ReadKey(true);

				lastKeyLock.AcquireWriterLock(-1);
				lastKey = key;
			    lastKeyLock.ReleaseLock();

			    readKeyInput.Set();
				OnNewKey(key);

				UpdateCurrentLine(key);

		    }

		}
		private static void UpdateCurrentLine(ConsoleKeyInfo key)
		{
			if (key.Key == ConsoleKey.Enter)
			{
				currentLineLock.AcquireWriterLock(-1);
				var line = currentLine;
				currentLine = "";
				currentLineLock.ReleaseLock();

				currentLineIndex = 0;

				lastLineLock.AcquireWriterLock(-1);
				lastLine = line;
				lastLineLock.ReleaseLock();

				if (PrintNewLine)
				{
					Console.WriteLine();
				}


				OnNewLine(line);


				readLineInput.Set();

			}
			else if (key.Key == ConsoleKey.LeftArrow)
			{
				if (currentLineIndex > 0)
				{
					currentLineIndex--;
					if (PrintKeys)
						Console.CursorLeft--;
				}
				else
					currentLineIndex = 0;
			}
			else if (key.Key == ConsoleKey.RightArrow)
			{
				if (currentLineIndex < currentLine.Length)
				{
					currentLineIndex++;
					if (PrintKeys)
						Console.CursorLeft++;
				}
				else
					currentLineIndex = currentLine.Length;
			}
			else if (key.Key == ConsoleKey.Insert)
			{
				replace = !replace;
				if (PrintKeys)
				{
					Console.CursorSize = replace ? 100 : 25;
				}
			}
			else if (key.Key == ConsoleKey.UpArrow)
			{

			}
			else if (key.Key == ConsoleKey.DownArrow)
			{

			}
			else if (key.Key == ConsoleKey.Home)
			{
				int left = Console.CursorLeft;
				int offset = left - currentLineIndex;

				currentLineIndex = 0;

				if (PrintKeys)
					Console.CursorLeft = offset + currentLineIndex;
			}
			else if (key.Key == ConsoleKey.End)
			{
				int left = Console.CursorLeft;
				int offset = left - currentLineIndex;

				currentLineLock.AcquireReaderLock(-1);
				currentLineIndex = currentLine.Length;
				currentLineLock.ReleaseLock();

				if (PrintKeys)
					Console.CursorLeft = offset + currentLineIndex;
			}
			else if (key.Key == ConsoleKey.Backspace)
			{

				currentLineLock.AcquireReaderLock(-1);
				var builder = new StringBuilder(currentLine);
				currentLineLock.ReleaseLock();

				if (currentLineIndex <= 0)
					return;

				builder.Remove(currentLineIndex - 1, 1);

				currentLineLock.AcquireWriterLock(-1);
				currentLine = builder.ToString();
				currentLineLock.ReleaseLock();

				currentLineIndex--;

				if (PrintKeys)
				{
					int left = Console.CursorLeft;
					Console.CursorLeft--;
					Console.Write(currentLine.Substring(currentLineIndex));
					Console.Write(" ");
					Console.CursorLeft = left - 1;
				}

			}
			else if (key.Key == ConsoleKey.Delete)
			{
				currentLineLock.AcquireReaderLock(-1);
				var builder = new StringBuilder(currentLine);
				currentLineLock.ReleaseLock();

				if (currentLineIndex >= currentLine.Length)
					return;

				builder.Remove(currentLineIndex, 1);

				currentLineLock.AcquireWriterLock(-1);
				currentLine = builder.ToString();
				currentLineLock.ReleaseLock();

				if (PrintKeys)
				{
					int left = Console.CursorLeft;
					Console.Write(currentLine.Substring(currentLineIndex));
					Console.Write(" ");
					Console.CursorLeft = left;
				}
			}
			else if (key.KeyChar != '\u0000')
			{

				bool printToEnd = false;
				currentLineLock.AcquireWriterLock(-1);


				var builder = new StringBuilder(currentLine);
				if (currentLineIndex < currentLine.Length)
				{
					if (replace)
					{
						builder[currentLineIndex] = key.KeyChar;
					}
					else
					{
						builder.Insert(currentLineIndex, key.KeyChar);
						printToEnd = true;
					}
				}
				else
				{
					builder.Append(key.KeyChar);
				}

				currentLine = builder.ToString();
				currentLineIndex++;


				if (PrintKeys)
				{
					if (printToEnd)
					{
						int left = Console.CursorLeft;
						Console.Write(currentLine.Substring(currentLineIndex - 1));
						Console.CursorLeft = left + 1;
					}
					else
						Console.Write(key.KeyChar);
				}

				currentLineLock.ReleaseLock();
			}
		}

		public static void StartInput()
	    {
		    readKeyEnable.Set();
		}
		public static void StopInput()
		{
			readKeyEnable.Reset();
		}


	    public static ConsoleKeyInfo ReadKey()
	    {
		    var info = ReadKey(-1);
			if (!info.HasValue)
				throw new InvalidOperationException();
		    return info.Value;
	    }

	    public static ConsoleKeyInfo? ReadKey(int timeout)
		{
			readKeyInput.Reset();

			bool running = readKeyEnable.WaitOne(0);
			readKeyEnable.Set();


			ConsoleKeyInfo? key = null;
		    if (readKeyInput.WaitOne(timeout))
			{
				lastKeyLock.AcquireReaderLock(-1);
				key = lastKey;
				lastKeyLock.ReleaseLock();
			}

			if (!running)
				readKeyEnable.Reset();
			return key;
	    }

	    public static string ReadLine()
	    {
		    var line = ReadLine(-1);
		    return line;
	    }
		public static string ReadLine(int timeout)
		{
			readLineInput.Reset();

			bool running = readKeyEnable.WaitOne(0);
			readKeyEnable.Set();


			string line = null;
			if (readLineInput.WaitOne(timeout))
			{
				lastLineLock.AcquireReaderLock(-1);
				line = lastLine;
				lastLineLock.ReleaseLock();
			}

			if (!running)
				readKeyEnable.Reset();
			return line;
		}
	}
}
