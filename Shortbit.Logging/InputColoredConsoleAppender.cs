﻿using System;
using System.Diagnostics;
using log4net.Appender;
using log4net.Core;

namespace Shortbit.Logging
{
	public class InputColoredConsoleAppender : ColoredConsoleAppender
	{
		private int outputLine = 0;

		private int GetInputLine()
		{
			return outputLine + 1;
		}

		public override void ActivateOptions()
		{
			base.ActivateOptions();
			
			Console.CursorTop = GetInputLine();
			Console.Write(">");

			ConsoleReader.PrintKeys = true;
			ConsoleReader.PrintNewLine = true;
			ConsoleReader.StartInput();
		}

		protected override void Append(LoggingEvent loggingEvent)
		{
			Console.CursorTop = GetInputLine();
			Console.CursorLeft = 0;
			Console.Write(new string(' ', Console.WindowWidth));

			Debug.WriteLine("outputLine: "+ outputLine);
			Console.CursorTop = outputLine;
			Console.CursorLeft = 0;

			base.Append(loggingEvent);
			outputLine = Console.CursorTop;
			Debug.WriteLine("outputLine: " + outputLine);

			Console.CursorTop = GetInputLine();
			Console.CursorLeft = 0;
			Console.Write(">"+ConsoleReader.CurrentLine);
		}
	}
}
