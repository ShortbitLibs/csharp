﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Comm.Messages;
using Shortbit.Utils;

namespace Shortbit.Comm.LinkStateNetwork
{
	public class RoutingTable
	{

		public struct TargetEntry
		{
			public TargetEntry(Guid target, Guid gateway, int cost)
			{
				this.Target = target;
				this.Gateway = gateway;
				this.Cost = cost;
			}

			public Guid Target { get; }

			public Guid Gateway { get; }

			public int Cost { get; }

			/*public override bool Equals(object obj)
			{
				if (!(obj is TargetEntry))
					return false;

				var entry = (TargetEntry)obj;
				return this.Target.Equals(entry.Target);
			}*/

			public override int GetHashCode() => this.Target.GetHashCode();
		}

		public struct GatewayEntry
		{
			private readonly Dictionary<Guid, int> remotes;


			public Guid Gateway { get; }
			
			public ReadOnlyDictionary<Guid, int> Remotes { get; }


			public GatewayEntry(Guid gateway)
			{
				this.Gateway = gateway;
				this.remotes = new Dictionary<Guid, int>();
				this.Remotes = new ReadOnlyDictionary<Guid, int>(this.remotes);
			}

			internal void Add(Guid target, int cost)
			{
				this.remotes[target] = cost;
			}

			/*public override bool Equals(object obj)
			{
				if (!(obj is GatewayEntry))
					return false;

				var entry = (GatewayEntry)obj;
				return this.Gateway.Equals(entry.Gateway);
			}*/

			public override int GetHashCode() => this.Gateway.GetHashCode();
		}

		public IEnumerable<Guid> AccessibleNodes => this.targets.Keys;

		public IReadOnlyDictionary<Guid, TargetEntry> Targets { get; }
		public IReadOnlyDictionary<Guid, GatewayEntry> Gateways { get; }
		
		private readonly Dictionary<Guid, TargetEntry> targets = new Dictionary<Guid, TargetEntry>();
		private readonly Dictionary<Guid, GatewayEntry> gateways = new Dictionary<Guid, GatewayEntry>();
		

		internal RoutingTable()
		{

			this.Targets = new ReadOnlyDictionary<Guid, TargetEntry>(this.targets);
			this.Gateways = new ReadOnlyDictionary<Guid, GatewayEntry>(this.gateways);
		}

		internal void Clear()
		{
			this.targets.Clear();
			this.gateways.Clear();
		}

		internal void Add(Guid target, Guid gateway, int cost)
		{
			this.targets[target] = new TargetEntry(target, gateway, cost);

			if (!this.gateways.ContainsKey(gateway))
				this.gateways.Add(gateway, new GatewayEntry(gateway));
			this.gateways[gateway].Add(target, cost);
		}


		internal IEnumerable<GatewayEntry> GetGatewaysFor(MessagePackage package)
		{
			IEnumerable<Guid> remotes;
			if (package.RelayTo.Any())
				remotes = package.RelayTo.Select(id => id.ToGuid());
			else
			{
				var recipients = RecipientsList.FromMessageHeader(package.Header);
				if (recipients.IsBroadcast)
					remotes = this.AccessibleNodes;
				else
					remotes = recipients;
			}

			return this.GetGatewaysFor(remotes);
		}
		public IEnumerable<GatewayEntry> GetGatewaysFor(IEnumerable<Guid> recipientsOrRelayTargets)
		{

			var doneGateways = new HashSet<GatewayEntry>();

			foreach (var remote in recipientsOrRelayTargets)
			{
				if (!this.Targets.ContainsKey(remote))
					continue;

				var target = this.Targets[remote];
				var gateway = this.Gateways[target.Gateway];
				if (doneGateways.Contains(gateway))
					continue;
				doneGateways.Add(gateway);
				yield return gateway;
			}

		}

	}
}
