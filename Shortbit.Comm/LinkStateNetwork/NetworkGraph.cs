﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms.ShortestPath;
using Shortbit.Utils;

namespace Shortbit.Comm.LinkStateNetwork
{
	internal class NetworkGraph
	{
		internal class Node : IEquatable<Node>
		{
			public Guid NodeId { get; }
			private readonly NetworkGraph graph;

			public Node(NetworkGraph graph, Guid nodeId)
			{
				this.graph = graph;
				this.NodeId = nodeId;
			}

			/// <inheritdoc/>
			public bool Equals(Node other)
			{
				if (object.ReferenceEquals(null, other)) return false;
				if (object.ReferenceEquals(this, other)) return true;
				return object.Equals(this.graph, other.graph) && this.NodeId.Equals(other.NodeId);
			}

			/// <inheritdoc/>
			public override bool Equals(object obj)
			{
				return this.Equals(obj as Node);
			}

			/// <inheritdoc/>
			public override int GetHashCode()
			{
				unchecked
				{
					return ((this.graph != null ? this.graph.GetHashCode() : 0) * 397) ^ this.NodeId.GetHashCode();
				}
			}

			/// <summary>Returns a string that represents the current object.</summary>
			/// <returns>A string that represents the current object.</returns>
			public override string ToString() => $"NetworkGraphNode {this.NodeId.ToShortString()}";
		}

		internal struct Edge : IUndirectedEdge<Node>, IEquatable<Edge>
		{
			public Edge(Node source, Node target, int cost)
			{
				this.Source = source;
				this.Target = target;
				this.Cost = cost;
			}

			public int Cost { get; set; }

			/// <inheritdoc />
			public Node Source { get; }

			/// <inheritdoc />
			public Node Target { get; }



			public bool Equals(Edge other) => (this.Source.Equals(other.Source) &&
														   this.Target.Equals(other.Target)) ||
														  (this.Source.Equals(other.Target) &&
														   this.Target.Equals(other.Source));

			/// <inheritdoc/>
			public override bool Equals(object obj)
			{
				if (object.ReferenceEquals(null, obj)) return false;
				return obj is Edge edge && this.Equals(edge);
			}

			/// <inheritdoc/>
			public override int GetHashCode()
			{
				unchecked
				{
					var hashCode = this.Cost;
					hashCode = (hashCode * 397) ^ (this.Source != null ? this.Source.GetHashCode() : 0);
					hashCode = (hashCode * 397) ^ (this.Target != null ? this.Target.GetHashCode() : 0);
					return hashCode;
				}
			}
		}

		private readonly object rebuildSyncRoot = new object();

		//private IMutableUndirectedGraph<NetworkGraphNode, NetworkGraphEdge> graph;
		private readonly AdjacencyGraph<Node, Edge> graph = new AdjacencyGraph<Node, Edge>(false);
		private bool dirty;

		public LinkStateDatabase Database { get; }

		public bool IsDirty => this.dirty;


		private readonly RoutingTable routingTable;
		public RoutingTable RoutingTable {
			get
			{
				this.RebuildIfNecessary();
				return this.routingTable;
			}
		}

		internal RoutingTable NonUpdatingRoutingTable => this.routingTable;

		public NetworkGraph(LinkStateDatabase database)
		{
			this.Database = database;
			this.routingTable = new RoutingTable();
			this.dirty = true;
		}

		public void MarkDirty()
		{
			this.dirty = true;
		}

		public string GenerateGraphviz()
		{
			this.RebuildIfNecessary();

			var builder = new StringBuilder();


			builder.Append("digraph NetworkGraph {\n");

			foreach (var node in this.graph.Vertices)
				builder.Append($"\t\"{node.NodeId.ToShortString()}\";\n");
			foreach (var edge in this.graph.Edges)
				builder.Append($"\t\"{edge.Source.NodeId.ToShortString()}\" -> \"{edge.Target.NodeId.ToShortString()}\" [label=\"{edge.Cost}\"];\n");

			builder.Append("}\n");

			return builder.ToString();
		}

		public void RebuildIfNecessary()
		{
			if (!this.dirty)
				return;
			this.RebuildImpl();
		}
		public void RebuildForced() => this.RebuildImpl();

		private void RebuildImpl()
		{
			lock (this.rebuildSyncRoot)
			{

				this.graph.Clear();

				var nodes = new Dictionary<Guid, Node>();

				foreach (var entry in this.Database)
				{
					var node = new Node(this, entry.NodeId);
					this.graph.AddVertex(node);
					nodes.Add(node.NodeId, node);

				}
				foreach (var entry in this.Database)
				{
					var entryNode = nodes[entry.NodeId];
					
					foreach (var link in entry.Links.Values)
					{
						if (!this.Database.Contains(link.Neighbor))
							continue;

						var neighbor = this.Database[link.Neighbor];
						var neighborNode =
							nodes[link.Neighbor]; // Here we can assume that nodes is synchonized with the database

						if (!neighbor.Links.ContainsKey(entry.NodeId))
							continue;

						var backLink = neighbor.Links[entry.NodeId];

						var cost = Math.Max(link.Cost, backLink.Cost);

						var edge = new Edge(entryNode, neighborNode, cost);
						this.graph.AddEdge(edge);
					}
				}

				var ownNode = nodes[this.Database.OwnId];

				var predecessors = new Dictionary<Node, Node>();

				var bellmanFord = new BellmanFordShortestPathAlgorithm<Node, Edge>(this.graph, e => e.Cost);
				bellmanFord.TreeEdge += e => predecessors[e.Target] = e.Source;
				bellmanFord.Compute(ownNode);
				
				
				this.routingTable.Clear();

				foreach (var node in nodes.Values)
				{
					if (node == ownNode)
						continue;

					var gateway = this.FindGateway(predecessors, node, ownNode);

					if (gateway != null)
						this.routingTable.Add(node.NodeId, gateway.NodeId, (int) bellmanFord.Distances[node]);
				}
				this.dirty = false;
			}
		}

		private Node FindGateway(Dictionary<Node, Node> predecessors, Node node, Node ownNode)
		{
			while (predecessors.ContainsKey(node) && predecessors[node] != ownNode)
				node = predecessors[node];
			if (!predecessors.ContainsKey(node))
				return null;
			return node;
		}
		
	}
}
