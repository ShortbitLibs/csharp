﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Comm.LinkStateNetwork
{
	internal class LinkStateSequence
	{
		public const int Start = -100;
		public const int Reset = 0;
		public const int Max = int.MaxValue;
		public const int HalfN = int.MaxValue / 2;

		private int sequence;


		public LinkStateSequence()
		{
			this.sequence = LinkStateSequence.Start;
		}

		public int Next()
		{
			this.sequence++;
			if (this.sequence == LinkStateSequence.Max)
				this.sequence = LinkStateSequence.Reset;


			return this.sequence;
		}

		public bool IsSpoolUp => this.sequence < LinkStateSequence.Reset;

		public void Resume(int previous)
		{
			this.sequence = previous;
		}

		public static bool IsMoreRecentThan(int db, int message)
		{ 
			return (db < 0 && db < message) // db is from spoolup, message is not. So the db is older.
				   || (db >= 0 && db < message && ((message - db) < LinkStateSequence.HalfN))	// No spoolup. But message is higher (and not wrapped around)
				   || (db >= 0 && db > message && ((db - message) > LinkStateSequence.HalfN));	// Either message is in spoolup, or db is at edge of wrapping, and message is already wrapped. (Both are more than N/2 apart)
		}
	}
}
