﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Utils;

namespace Shortbit.Comm.LinkStateNetwork
{
	internal class LinkStateDatabase : IEnumerable<LinkStateDatabase.Entry>
	{
		public const int MaxAge = 3000;

		public class Link
		{
			public Link(LinkStatePackage.Link link)
				: this(link.Neighbor, link.Cost)
			{ }
			public Link(Guid neighbor, int cost)
			{
				this.Neighbor = neighbor;
				this.Cost = cost;
			}

			public Guid Neighbor { get; }
			public int Cost { get; }


			/// <summary>Returns a string that represents the current object.</summary>
			/// <returns>A string that represents the current object.</returns>
			public override string ToString() => $"{this.Neighbor.ToShortString()} @ {this.Cost}";
		}

		public class Entry
		{

			public Entry(Guid nodeId)
			{
				this.NodeId = nodeId;
			}

			public Guid NodeId { get; }

			public Dictionary<Guid, Link> Links { get; } = new Dictionary<Guid, Link>();

			public int Seqence { get; set; }

			public int Age { get; set; }

			public DateTime LastUpdate { get; set; }

			public bool Tick()
			{
				this.Age += 10;
				return this.Age < LinkStateDatabase.MaxAge;
			}

			public static Entry FromPackage(LinkStatePackage package)
			{
				var res = new Entry(package.Origin)
				{
					Seqence = package.Sequence,
					Age = package.Age,
					LastUpdate = DateTime.Now
				};
				res.Links.AddRange(package.Links.ToDictionary(l => (Guid)l.Neighbor, l => new Link(l)));
				return res;
			}

			public void Update(LinkStatePackage package)
			{
				this.Seqence = package.Sequence;
				this.Age = package.Age;
				this.LastUpdate = DateTime.Now;

				this.Links.Clear();
				this.Links.AddRange(package.Links.ToDictionary(l => (Guid)l.Neighbor, l => new Link(l)));
			}
		}


		private readonly object entriesLockSync = new object();
		private readonly Dictionary<Guid, Entry> entries = new Dictionary<Guid, Entry>();



		// ReSharper disable once InconsistentlySynchronizedField
		public Entry this[Guid id] => this.entries[id];


		private readonly Timer tickTimer;
		public NetworkGraph Graph { get; }

		public CommNode Node { get; }

		public Guid OwnId => this.Node.NodeId;
		

		public LinkStateDatabase(CommNode node)
		{
			this.tickTimer = new Timer(_ => this.Tick(), null, 0, 1000);
			this.Graph = new NetworkGraph(this);
			this.Node = node;
		}

		private void Tick()
		{
			var outdatedEntries = new List<Guid>();
			lock (this.entriesLockSync)
			{
				foreach (var entry in this.entries.Values)
				{
					if (!entry.Tick())
						outdatedEntries.Add(entry.NodeId);
				}

				foreach (var guid in outdatedEntries)
				{
					this.entries.Remove(guid);
				}
				if (outdatedEntries.Any())
					this.Graph.MarkDirty();
			}

		}

		// ReSharper disable once InconsistentlySynchronizedField
		public bool Contains(Guid id) => this.entries.ContainsKey(id);
		
		public bool Update(LinkStatePackage package)
		{
			lock (this.entriesLockSync)
			{
				if (this.Contains(package.Origin) &&
					!LinkStateSequence.IsMoreRecentThan(this[package.Origin].Seqence, package.Sequence))
					return false;

				if (!this.Contains(package.Origin))
					this.entries.Add(package.Origin, Entry.FromPackage(package));
				else
					this.entries[package.Origin].Update(package);

				this.Graph.MarkDirty();
			}
			return true;
		}

		public void ForceUpdate(LinkStatePackage package)
		{
			lock (this.entriesLockSync)
			{
				if (!this.Contains(package.Origin))
					this.entries.Add(package.Origin, Entry.FromPackage(package));
				else
					this.entries[package.Origin].Update(package);
				this.Graph.MarkDirty();
			}
		}

		public void Remove(Guid id)
		{
			lock (this.entriesLockSync)
			{
				if (this.entries.Remove(id))
					this.Graph.MarkDirty();
			}
		}
		

		/// <inheritdoc />
		// ReSharper disable once InconsistentlySynchronizedField
		public IEnumerator<Entry> GetEnumerator() => this.entries.Values.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
