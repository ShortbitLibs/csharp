// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: test_messages.proto
// </auto-generated>

#region Designer generated code
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Shortbit.Comm;
using Shortbit.Comm.Messages;
using _Proto = Shortbit.Comm.LinkStateNetwork.Proto;

namespace Shortbit.Comm.LinkStateNetwork {
	// python.message_module: Shortbit_Comm.link_state_network.lsn_messages
	// Class Info::
	//     python.mixin=
	//     package=Shortbit.Comm.LinkStateNetwork
	//     namespace=Shortbit.Comm.LinkStateNetwork
	//     proto_message_name=Heartbeat
	//     message_name=Heartbeat
	//     class_name=Heartbeat
	//     proto_class_name_including_parents=Heartbeat
	//     class_name_including_parents=Heartbeat
	//     full_proto_message_path=.Shortbit.Comm.LinkStateNetwork.Heartbeat
	//     full_proto_class_path=Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat
	//     full_class_path=Shortbit.Comm.LinkStateNetwork.Heartbeat
	public partial class Heartbeat : Message {
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static MessageDescriptor _descriptor = new MessageDescriptor(
			"Shortbit.Comm.LinkStateNetwork.Heartbeat",
			proto_msg => new Heartbeat((Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat)proto_msg),
			msg => ((Heartbeat)msg)._dataAccess(false),
			typeof(Heartbeat),
			Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat.Descriptor);
		
		public static MessageDescriptor Descriptor => _descriptor;
		public override MessageDescriptor GetDescriptor() => Descriptor;
		
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		protected Func<bool, Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat> _dataAccess;
		
		public Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat Data
		{
			get => this._dataAccess(false);
		}
		
		
		public override bool Equals(object obj) => this.Equals(obj as Heartbeat);
		public bool Equals(Heartbeat value) => value != null && this._dataAccess(false).Equals(value._dataAccess(false));
		public override int GetHashCode() => this._dataAccess(false).GetHashCode();
		public static implicit operator Heartbeat(Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat msg) => new Heartbeat(msg);
		public static implicit operator Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat(Heartbeat msg) => msg._dataAccess(false);
		public override string ToString() => this._dataAccess(false).ToString();
		public void MergeFrom(Heartbeat other) => this._dataAccess(true).MergeFrom(other?._dataAccess(false));
		
		
		public Heartbeat() 
			: base()
		{
			var data = new Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat();
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		public Heartbeat(Shortbit.Comm.LinkStateNetwork.Proto.Heartbeat data) 
			: base()
		{
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		partial void PostInit();
	}
	
	// Class Info::
	//     python.mixin=
	//     package=Shortbit.Comm.LinkStateNetwork
	//     namespace=Shortbit.Comm.LinkStateNetwork
	//     proto_message_name=LinkStatePackage
	//     message_name=LinkStatePackage
	//     class_name=LinkStatePackage
	//     proto_class_name_including_parents=LinkStatePackage
	//     class_name_including_parents=LinkStatePackage
	//     full_proto_message_path=.Shortbit.Comm.LinkStateNetwork.LinkStatePackage
	//     full_proto_class_path=Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage
	//     full_class_path=Shortbit.Comm.LinkStateNetwork.LinkStatePackage
	public partial class LinkStatePackage : Message {
		// Class Info::
		//     python.mixin=
		//     package=Shortbit.Comm.LinkStateNetwork
		//     namespace=Shortbit.Comm.LinkStateNetwork
		//     proto_message_name=Link
		//     message_name=Link
		//     class_name=Link
		//     proto_class_name_including_parents=LinkStatePackage.Types.Link
		//     class_name_including_parents=LinkStatePackage.Link
		//     full_proto_message_path=.Shortbit.Comm.LinkStateNetwork.LinkStatePackage.Link
		//     full_proto_class_path=Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link
		//     full_class_path=Shortbit.Comm.LinkStateNetwork.LinkStatePackage.Link
		public partial class Link {
			
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			protected Func<bool, Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link> _dataAccess;
			
			public Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link Data
			{
				get => this._dataAccess(false);
			}
			
			// Field:: name=neighbor, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.Messages.ProtoGuid, is_array=False, is_map=False
			//     type_name=Shortbit.Comm.Messages.MsgGuid
			//     accessor_type_name=Shortbit_Comm_Messages_MsgGuid_Access
			private class Shortbit_Comm_Messages_MsgGuid_Access : Shortbit.Comm.Messages.MsgGuid
			{
				public Shortbit_Comm_Messages_MsgGuid_Access (Func<bool, Shortbit.Comm.Messages.ProtoGuid> dataAccess)
					: base()
				{
					this._dataAccess = dataAccess;
				}
			}
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private object _locksync_Neighbor = new object();
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private Shortbit_Comm_Messages_MsgGuid_Access _backingField_Neighbor;
			public Shortbit.Comm.Messages.MsgGuid Neighbor
			{
				get
				{
					if (this._backingField_Neighbor == null)
					{
						lock(this._locksync_Neighbor)
						{
							if (this._backingField_Neighbor == null)
							{
								this._backingField_Neighbor = new Shortbit_Comm_Messages_MsgGuid_Access(
								writing => {
									if (this._dataAccess(writing).Neighbor == null)
									{
										var value = new Shortbit.Comm.Messages.ProtoGuid();
										if (writing)
											this._dataAccess(writing).Neighbor = value;
										else
											return value;
									}
									return this._dataAccess(writing).Neighbor;
								});
							}
						}
					}
					return this._backingField_Neighbor;
				}
			}
			
			// Field:: name=cost, type=TYPE_INT32, type_name=, is_array=False, is_map=False
			public int Cost
			{
				get => this._dataAccess(false).Cost;
				set => this._dataAccess(true).Cost = value;
			}
			
			
			public override bool Equals(object obj) => this.Equals(obj as Link);
			public bool Equals(Link value) => value != null && this._dataAccess(false).Equals(value._dataAccess(false));
			public override int GetHashCode() => this._dataAccess(false).GetHashCode();
			public static implicit operator Link(Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link msg) => new Link(msg);
			public static implicit operator Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link(Link msg) => msg._dataAccess(false);
			public override string ToString() => this._dataAccess(false).ToString();
			public void MergeFrom(Link other) => this._dataAccess(true).MergeFrom(other?._dataAccess(false));
			
			
			public Link() 
				: base()
			{
				var data = new Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link();
				this._dataAccess = writing => data;
				this.PostInit();
			}
			
			public Link(Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link data) 
				: base()
			{
				this._dataAccess = writing => data;
				this.PostInit();
			}
			
			partial void PostInit();
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static MessageDescriptor _descriptor = new MessageDescriptor(
			"Shortbit.Comm.LinkStateNetwork.LinkStatePackage",
			proto_msg => new LinkStatePackage((Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage)proto_msg),
			msg => ((LinkStatePackage)msg)._dataAccess(false),
			typeof(LinkStatePackage),
			Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Descriptor);
		
		public static MessageDescriptor Descriptor => _descriptor;
		public override MessageDescriptor GetDescriptor() => Descriptor;
		
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		protected Func<bool, Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage> _dataAccess;
		
		public Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage Data
		{
			get => this._dataAccess(false);
		}
		
		// Field:: name=origin, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.Messages.ProtoGuid, is_array=False, is_map=False
		//     type_name=Shortbit.Comm.Messages.MsgGuid
		//     accessor_type_name=Shortbit_Comm_Messages_MsgGuid_Access
		private class Shortbit_Comm_Messages_MsgGuid_Access : Shortbit.Comm.Messages.MsgGuid
		{
			public Shortbit_Comm_Messages_MsgGuid_Access (Func<bool, Shortbit.Comm.Messages.ProtoGuid> dataAccess)
				: base()
			{
				this._dataAccess = dataAccess;
			}
		}
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object _locksync_Origin = new object();
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Shortbit_Comm_Messages_MsgGuid_Access _backingField_Origin;
		public Shortbit.Comm.Messages.MsgGuid Origin
		{
			get
			{
				if (this._backingField_Origin == null)
				{
					lock(this._locksync_Origin)
					{
						if (this._backingField_Origin == null)
						{
							this._backingField_Origin = new Shortbit_Comm_Messages_MsgGuid_Access(
							writing => {
								if (this._dataAccess(writing).Origin == null)
								{
									var value = new Shortbit.Comm.Messages.ProtoGuid();
									if (writing)
										this._dataAccess(writing).Origin = value;
									else
										return value;
								}
								return this._dataAccess(writing).Origin;
							});
						}
					}
				}
				return this._backingField_Origin;
			}
		}
		
		// Field:: name=sequence, type=TYPE_INT32, type_name=, is_array=False, is_map=False
		public int Sequence
		{
			get => this._dataAccess(false).Sequence;
			set => this._dataAccess(true).Sequence = value;
		}
		
		// Field:: name=age, type=TYPE_INT32, type_name=, is_array=False, is_map=False
		public int Age
		{
			get => this._dataAccess(false).Age;
			set => this._dataAccess(true).Age = value;
		}
		
		// Field:: name=links, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.LinkStateNetwork.LinkStatePackage.Link, is_array=True, is_map=False
		//     type_name=Link
		//     accessor_type_name=Link_Access
		private class Link_Access : Link
		{
			public Link_Access (Func<bool, Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link> dataAccess)
				: base()
			{
				this._dataAccess = dataAccess;
			}
		}
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object _locksync_Links = new object();
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AccessorListWrapper<Link, Link_Access, Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link> _backingField_Links;
		public IList<Link> Links
		{
			get
			{
				if (this._backingField_Links == null)
				{
					lock(this._locksync_Links)
					{
						if (this._backingField_Links == null)
						{
							this._backingField_Links = new AccessorListWrapper<Link, Link_Access, Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage.Types.Link>(
								writing => this._dataAccess(writing).Links,
								entry => entry.Data,
								accessor => new Link_Access(accessor)
							);
						}
					}
				}
				return this._backingField_Links;
			}
		}
		
		
		public override bool Equals(object obj) => this.Equals(obj as LinkStatePackage);
		public bool Equals(LinkStatePackage value) => value != null && this._dataAccess(false).Equals(value._dataAccess(false));
		public override int GetHashCode() => this._dataAccess(false).GetHashCode();
		public static implicit operator LinkStatePackage(Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage msg) => new LinkStatePackage(msg);
		public static implicit operator Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage(LinkStatePackage msg) => msg._dataAccess(false);
		public override string ToString() => this._dataAccess(false).ToString();
		public void MergeFrom(LinkStatePackage other) => this._dataAccess(true).MergeFrom(other?._dataAccess(false));
		
		
		public LinkStatePackage() 
			: base()
		{
			var data = new Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage();
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		public LinkStatePackage(Shortbit.Comm.LinkStateNetwork.Proto.LinkStatePackage data) 
			: base()
		{
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		partial void PostInit();
	}
	
}
# endregion Designer generated code
