﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Comm.Transport;

namespace Shortbit.Comm
{
	public class ConnectionClosedEventArgs
	{
		public ConnectionClosedEventArgs(Connection connection, ConnectionClosedCause cause)
		{
			this.Connection = connection;
			this.Cause = cause;
		}

		public Connection Connection { get; }
		public ConnectionClosedCause Cause { get; }
	}
}
