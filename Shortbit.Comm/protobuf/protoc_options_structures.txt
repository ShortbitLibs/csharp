--plugin=protoc-gen-messages=SbC-message-gen.exe
--proto_path=../submodules/common
--csharp_out=.
--csharp_opt=base_namespace=Shortbit.Comm,file_extension=.p.g.cs
--messages_out=.
--messages_opt=language=csharp,csharp_base_namespace=Shortbit.Comm,csharp_file_extension=.g.cs
../submodules/common/Shortbit.Comm/Proto/structures.proto
