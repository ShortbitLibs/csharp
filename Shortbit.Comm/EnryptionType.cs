﻿// /*
//  * EnryptionType.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Comm
{
    public enum EncryptionType
    {
        None,
        RSA,
        Blowfish,
    }
}