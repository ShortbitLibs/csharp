﻿using System;
using System.Collections;
using System.Collections.Generic;
using Google.Protobuf.Collections;

namespace Shortbit.Comm.Messages
{
	public class AccessorDictionaryWrapper<TKey, TWrapper, TWrapperAccess, TData> : IDictionary<TKey, TWrapper>
		where TWrapperAccess : TWrapper
	{
		private readonly Func<bool, MapField<TKey, TData>> backboneAccess;
		private readonly Func<TWrapper, TData> dataAccess;
		private readonly Func<Func<bool, TData>, TWrapperAccess> constructor;

		//private readonly Dictionary<int, TAccess> accessors = new Dictionary<int, TAccess>();

		public AccessorDictionaryWrapper(Func<bool, MapField<TKey, TData>> backboneAccess, Func<TWrapper, TData> dataAccess, Func<Func<bool, TData>, TWrapperAccess> constructor)
		{
			this.backboneAccess = backboneAccess;
			this.dataAccess = dataAccess;
			this.constructor = constructor;
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TWrapper>> GetEnumerator()
		{
			foreach (var pair in this.backboneAccess(false))
			{
				yield return new KeyValuePair<TKey, TWrapper>(pair.Key, this.constructor(writing => pair.Value));
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <inheritdoc />
		public void Add(KeyValuePair<TKey, TWrapper> item)
		{
			throw new NotImplementedException();
		}

		/// <inheritdoc />
		public void Clear() => this.backboneAccess(true).Clear();

		/// <inheritdoc />
		public bool Contains(KeyValuePair<TKey, TWrapper> item)
		{
			var pair = new KeyValuePair<TKey, TData>(item.Key, this.dataAccess(item.Value));
			return (this.backboneAccess(false) as ICollection<KeyValuePair<TKey, TData>>).Contains(pair);
		}

		/// <inheritdoc />
		public void CopyTo(KeyValuePair<TKey, TWrapper>[] array, int arrayIndex)
		{
			var pairs = new KeyValuePair<TKey, TData>[this.Count];
			(this.backboneAccess(false) as ICollection<KeyValuePair<TKey, TData>>).CopyTo(pairs, 0);

			for (int i = 0; i < this.Count; i++)
			{
				var pair = pairs[i];
				array[arrayIndex + i] = new KeyValuePair<TKey, TWrapper>(pair.Key, this.constructor(writing => pair.Value));
			}
		}

		/// <inheritdoc />
		public bool Remove(KeyValuePair<TKey, TWrapper> item)
		{
			var pair = new KeyValuePair<TKey, TData>(item.Key, this.dataAccess(item.Value));
			return (this.backboneAccess(false) as ICollection<KeyValuePair<TKey, TData>>).Remove(pair);
		}

		/// <inheritdoc />
		public int Count => this.backboneAccess(false).Count;

		/// <inheritdoc />
		public bool IsReadOnly => this.backboneAccess(false).IsReadOnly;

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.backboneAccess(false).ContainsKey(key);

		/// <inheritdoc />
		public void Add(TKey key, TWrapper value) => this.backboneAccess(true).Add(key, this.dataAccess(value));

		/// <inheritdoc />
		public bool Remove(TKey key) => this.backboneAccess(true).Remove(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TWrapper value)
		{
			TData data;
			if (!this.backboneAccess(false).TryGetValue(key, out data))
			{
				value = default(TWrapper);
				return false;
			}

			value = this.constructor(writing => data);
			return true;
		}

		/// <inheritdoc />
		public TWrapper this[TKey key]
		{
			get => this.constructor(writing => this.backboneAccess(writing)[key]);
			set => this.backboneAccess(true)[key] = this.dataAccess(value);
		}

		/// <inheritdoc />
		public ICollection<TKey> Keys => this.backboneAccess(false).Keys;

		/// <inheritdoc />
		public ICollection<TWrapper> Values { get; }
	}
}
