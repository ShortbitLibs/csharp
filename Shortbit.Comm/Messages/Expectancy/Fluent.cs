﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Utils;

// ReSharper disable once CheckNamespace
namespace Shortbit.Comm.Messages.Expectancy.Fluent
{
	public class ExpectancyRoot
	{
		private readonly CommNode node;

		internal ExpectancyRoot(CommNode node)
		{
			this.node = node;
		}

		public SingleMessage OneMessage()
		{
			return new SingleMessage(this.node);
		}

		public MultiMessage MultipleMessages()
		{
			return new MultiMessage(this.node);
		}
	}

	#region Single message

	public class SingleMessage
	{
		private readonly CommNode node;

		public SingleMessage(CommNode node)
		{
			this.node = node;
		}

		public TypedSingleMessage<Message> OfType(params Type[] messageTypes)
		{
			return new TypedSingleMessage<Message>(this.node, messageTypes);
		}

		public TypedSingleMessage<T1> OfType<T1>()
			where T1 : Message
		{
			return new TypedSingleMessage<T1>(this.node, typeof(T1));
		}

		public TypedSingleMessage<Message> OfType<T1, T2>()
			where T1 : Message
			where T2 : Message
		{
			return new TypedSingleMessage<Message>(this.node, typeof(T1), typeof(T2));
		}

		public TypedSingleMessage<Message> OfType<T1, T2, T3>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
		{
			return new TypedSingleMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3));
		}

		public TypedSingleMessage<Message> OfType<T1, T2, T3, T4>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
		{
			return new TypedSingleMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3), typeof(T4));
		}

		public TypedSingleMessage<Message> OfType<T1, T2, T3, T4, T5>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
			where T5 : Message
		{
			return new TypedSingleMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3), typeof(T4),
												   typeof(T5));
		}
	}

	public class TypedSingleMessage<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;

		internal TypedSingleMessage(CommNode node, params Type[] messageTypes)
		{
			this.node = node;
			this.messageTypes = messageTypes;
		}

		public SingleMessageSingleSource<T> From(Guid origin)
		{
			return new SingleMessageSingleSource<T>(this.node, this.messageTypes, origin);
		}

		public SingleMessageMultiSource<T> From(IEnumerable<Guid> origins)
		{
			return new SingleMessageMultiSource<T>(this.node, this.messageTypes, origins);
		}

		public SingleMessageMultiSource<T> From(params Guid[] origins) => this.From((IEnumerable<Guid>)origins);
	}

	public class SingleMessageSingleSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly Guid origin;

		public SingleMessageSingleSource(CommNode node, Type[] messageTypes, Guid origin)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origin = origin;
		}


		public ResponseSingleMessageSingleSource<T> AsResponseTo(Message message)
		{
			return new ResponseSingleMessageSingleSource<T>(this.node, this.messageTypes, this.origin, message);
		}

		public T AndWait()
		{
			var filter = new ExpectSingleMessageSingleSourceSyncBase<T>(this.origin, null, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessage();
		}

		public Task<T> Async() => this.Async(CancellationToken.None);

		public Task<T> Async(CancellationToken cancellation)
		{
			var filter = new ExpectSingleMessageSingleSourceAsyncBase<T>(this.origin, cancellation, null, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessageAsync();
		}
	}

	public class ResponseSingleMessageSingleSource<TRet> where TRet : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly Guid origin;
		private readonly Message responseTo;

		internal ResponseSingleMessageSingleSource(CommNode node, Type[] messageTypes, Guid origin, Message responseTo)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origin = origin;
			this.responseTo = responseTo;
		}

		public TRet AndWait()
		{
			var filter = new ExpectSingleMessageSingleSourceSyncBase<TRet>(this.origin, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessage();
		}

		public Task<TRet> Async() => this.Async(CancellationToken.None);

		public Task<TRet> Async(CancellationToken cancellation)
		{
			var filter = new ExpectSingleMessageSingleSourceAsyncBase<TRet>(this.origin, cancellation, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessageAsync();
		}
	}

	public class SingleMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;

		public SingleMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
		}


		public ResponseSingleMessageMultiSource<T> AsResponseTo(Message message)
		{
			return new ResponseSingleMessageMultiSource<T>(this.node, this.messageTypes, this.origins, message);
		}

		public TerminatedSingleMessageMultiSource<T> CompletedWhen(
			Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator)
		{
			return new TerminatedSingleMessageMultiSource<T>(this.node, this.messageTypes, this.origins, null, terminator);
		}
	}

	public class ResponseSingleMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;
		private readonly Message responseTo;

		internal ResponseSingleMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins, Message responseTo)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
			this.responseTo = responseTo;
		}

		public TerminatedSingleMessageMultiSource<T> CompletedWhen(
			Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator)
		{
			return new TerminatedSingleMessageMultiSource<T>(this.node, this.messageTypes, this.origins, this.responseTo, terminator);
		}
	}

	public class TerminatedSingleMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;
		private readonly Message responseTo;
		private readonly Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator;

		internal TerminatedSingleMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins, Message responseTo, Func<ExpectSingleMessageMultiSourceBase<T>, bool> terminator)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
			this.responseTo = responseTo;
			this.terminator = terminator;
		}

		public IReadOnlyDictionary<Guid, T> AndWait()
		{
			var filter = new ExpectSingleMessageMultiSourceSyncBase<T>(this.terminator, this.origins, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessages();
		}

		public Task<IReadOnlyDictionary<Guid, T>> Async(CancellationToken cancellation)
		{
			var filter = new ExpectSingleMessageMultiSourceAsyncBase<T>(this.terminator, this.origins, cancellation, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessagesAsync();
		}

		public Task<IReadOnlyDictionary<Guid, T>> Async() => this.Async(CancellationToken.None);
	}

	#endregion

	#region Multi message
	
	public class MultiMessage
	{
		private readonly CommNode node;

		public MultiMessage(CommNode node)
		{
			this.node = node;
		}

		public TypedMultiMessage<Message> OfType(params Type[] messageTypes)
		{
			return new TypedMultiMessage<Message>(this.node, messageTypes);
		}

		public TypedMultiMessage<T1> OfType<T1>()
			where T1 : Message
		{
			return new TypedMultiMessage<T1>(this.node, typeof(T1));
		}

		public TypedMultiMessage<Message> OfType<T1, T2>()
			where T1 : Message
			where T2 : Message
		{
			return new TypedMultiMessage<Message>(this.node, typeof(T1), typeof(T2));
		}

		public TypedMultiMessage<Message> OfType<T1, T2, T3>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
		{
			return new TypedMultiMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3));
		}

		public TypedMultiMessage<Message> OfType<T1, T2, T3, T4>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
		{
			return new TypedMultiMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3), typeof(T4));
		}

		public TypedMultiMessage<Message> OfType<T1, T2, T3, T4, T5>()
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
			where T5 : Message
		{
			return new TypedMultiMessage<Message>(this.node, typeof(T1), typeof(T2), typeof(T3), typeof(T4),
												   typeof(T5));
		}
	}

	public class TypedMultiMessage<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;

		internal TypedMultiMessage(CommNode node, params Type[] messageTypes)
		{
			this.node = node;
			this.messageTypes = messageTypes;
		}

		public MultiMessageSingleSource<T> From(Guid origin)
		{
			return new MultiMessageSingleSource<T>(this.node, this.messageTypes, origin);
		}

		public MultiMessageMultiSource<T> From(IEnumerable<Guid> origins)
		{
			return new MultiMessageMultiSource<T>(this.node, this.messageTypes, origins);
		}

		public MultiMessageMultiSource<T> From(params Guid[] origins) => this.From((IEnumerable<Guid>)origins);
	}

	public class MultiMessageSingleSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly Guid origin;

		public MultiMessageSingleSource(CommNode node, Type[] messageTypes, Guid origin)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origin = origin;
		}


		public ResponseMultiMessageSingleSource<T> AsResponseTo(Message message)
		{
			return new ResponseMultiMessageSingleSource<T>(this.node, this.messageTypes, this.origin, message);
		}


		public TerminatedMultiMessageSingleSource<T> CompletedWhen(
			Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator)
		{
			return new TerminatedMultiMessageSingleSource<T>(this.node, this.messageTypes, this.origin, null, terminator);
		}
	}

	public class ResponseMultiMessageSingleSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly Guid origin;
		private readonly Message responseTo;

		internal ResponseMultiMessageSingleSource(CommNode node, Type[] messageTypes, Guid origin, Message responseTo)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origin = origin;
			this.responseTo = responseTo;
		}


		public TerminatedMultiMessageSingleSource<T> CompletedWhen(
			Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator)
		{
			return new TerminatedMultiMessageSingleSource<T>(this.node, this.messageTypes, this.origin, this.responseTo, terminator);
		}
	}

	public class TerminatedMultiMessageSingleSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly Guid origin;
		private readonly Message responseTo;
		private readonly Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator;

		public TerminatedMultiMessageSingleSource(CommNode node, Type[] messageTypes, Guid origin, Message responseTo, Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origin = origin;
			this.responseTo = responseTo;
			this.terminator = terminator;
		}
		public IReadOnlyList<T> AndWait()
		{
			var filter = new ExpectMultiMessageSingleSourceSyncBase<T>(this.terminator, this.origin, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessages();
		}

		public Task<IReadOnlyList<T>> Async() => this.Async(CancellationToken.None);

		public Task<IReadOnlyList<T>> Async(CancellationToken cancellation)
		{
			var filter = new ExpectMultiMessageSingleSourceAsyncBase<T>(this.terminator, this.origin, cancellation, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessagesAsync();
		}
	}

	public class MultiMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;

		public MultiMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
		}


		public ResponseMultiMessageMultiSource<T> AsResponseTo(Message message)
		{
			return new ResponseMultiMessageMultiSource<T>(this.node, this.messageTypes, this.origins, message);
		}

		public TerminatedMultiMessageMultiSource<T> CompletedWhen(
			Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator)
		{
			return new TerminatedMultiMessageMultiSource<T>(this.node, this.messageTypes, this.origins, null, terminator);
		}
	}

	public class ResponseMultiMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;
		private readonly Message responseTo;

		internal ResponseMultiMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins, Message responseTo)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
			this.responseTo = responseTo;
		}

		public TerminatedMultiMessageMultiSource<T> CompletedWhen(
			Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator)
		{
			return new TerminatedMultiMessageMultiSource<T>(this.node, this.messageTypes, this.origins, this.responseTo, terminator);
		}
	}

	public class TerminatedMultiMessageMultiSource<T> where T : Message
	{
		private readonly CommNode node;
		private readonly Type[] messageTypes;
		private readonly IEnumerable<Guid> origins;
		private readonly Message responseTo;
		private readonly Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator;

		internal TerminatedMultiMessageMultiSource(CommNode node, Type[] messageTypes, IEnumerable<Guid> origins, Message responseTo, Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator)
		{
			this.node = node;
			this.messageTypes = messageTypes;
			this.origins = origins;
			this.responseTo = responseTo;
			this.terminator = terminator;
		}

		public IReadOnlyDictionary<Guid, IReadOnlyList<T>> AndWait()
		{
			var filter = new ExpectMultiMessageMultiSourceSyncBase<T>(this.terminator, this.origins, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessages();
		}

		public Task<IReadOnlyDictionary<Guid, IReadOnlyList<T>>> Async(CancellationToken cancellation)
		{
			var filter = new ExpectMultiMessageMultiSourceAsyncBase<T>(this.terminator, this.origins, cancellation, this.responseTo, this.messageTypes);
			this.node.AddExpectancy(filter);
			return filter.GetMessagesAsync();
		}

		public Task<IReadOnlyDictionary<Guid, IReadOnlyList<T>>> Async() => this.Async(CancellationToken.None);
	}

	#endregion
}