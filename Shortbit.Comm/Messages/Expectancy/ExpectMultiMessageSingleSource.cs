﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Comm.Exceptions;

namespace Shortbit.Comm.Messages.Expectancy
{
	public abstract class ExpectMultiMessageSingleSourceBase<T> : MessageExpectancy
		where T : Message
	{
		private readonly Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator;
		private readonly Guid origin;
		private readonly Message responseMessage;
		private readonly Type[] messageTypes;


		private readonly ManualResetEvent terminated = new ManualResetEvent(false);
		public WaitHandle WaitHandle => this.terminated;


		private readonly List<T> receivedMessages = new List<T>();
		public IReadOnlyList<T> ReceivedMessages => this.receivedMessages;

		protected ExpectMultiMessageSingleSourceBase(Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator, Guid origin, Message responseMessage, Type[] messageTypes)
		{
			this.terminator = terminator;
			this.origin = origin;
			this.responseMessage = responseMessage;
			this.messageTypes = messageTypes;
		}

		public override bool IsExpected(Message message)
		{
			return this.messageTypes.Contains(message.GetType()) &&
				   (this.responseMessage == null || message.Header.IsResponseTo(this.responseMessage.Header)) &&
				   this.origin == message.Header.Origin;
		}

		public override bool IsSatisfied()
		{
			if (this.terminator(this))
			{
				this.terminated.Set();
				return true;
			}

			return false;
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			this.terminated.Set();
		}

		public override void ProcessMessage(Message message)
		{
			this.receivedMessages.Add((T)message);
		}

	}
	public class ExpectMultiMessageSingleSourceSyncBase<T> : ExpectMultiMessageSingleSourceBase<T>
		where T : Message
	{

		public ExpectMultiMessageSingleSourceSyncBase(Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator, Guid origin, Message responseMessage,
													  params Type[] messageTypes)
			: base(terminator, origin, responseMessage, messageTypes)
		{
		}


		public IReadOnlyList<T> GetMessages()
		{
			this.WaitHandle.WaitOne();
			if (this.ThrowOnTimeout && this.TimedOut)
				throw ExpectancyTimeoutException.Create(this, this.ReceivedMessages);
			return this.ReceivedMessages;
		}
	}

	public class ExpectMultiMessageSingleSourceAsyncBase<T> : ExpectMultiMessageSingleSourceBase<T>
		where T : Message
	{
		private readonly TaskCompletionSource<IReadOnlyList<T>> tcs = new TaskCompletionSource<IReadOnlyList<T>>();

		private readonly CancellationTokenSource ownCts = new CancellationTokenSource();
		private readonly CancellationTokenSource cts;

		public ExpectMultiMessageSingleSourceAsyncBase(Func<ExpectMultiMessageSingleSourceBase<T>, bool> terminator, Guid origin, CancellationToken cancellationToken, Message responseMessage,
													   params Type[] messageTypes)
			: base(terminator, origin, responseMessage, messageTypes)
		{
			this.cts = CancellationTokenSource.CreateLinkedTokenSource(this.ownCts.Token, cancellationToken);

			this.cts.Token.Register(() => this.tcs.TrySetCanceled(this.cts.Token));
		}

		public override bool IsSatisfied()
		{
			var res = base.IsSatisfied();
			if (res)
				this.tcs.SetResult(this.ReceivedMessages);
			return res;
		}

		protected override void AfterDetach()
		{
			base.AfterDetach();
			if (!this.WaitHandle.WaitOne(0))
				this.ownCts.Cancel();
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			if (this.ThrowOnTimeout)
				this.tcs.SetException(ExpectancyTimeoutException.Create(this, this.ReceivedMessages));
			else
				this.tcs.SetResult(this.ReceivedMessages);
		}

		public Task<IReadOnlyList<T>> GetMessagesAsync()
		{
			return this.tcs.Task;
		}

	}


	#region Typed Overloads
	public class ExpectMultiMessageSingleSource : ExpectMultiMessageSingleSourceSyncBase<Message>
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origin, responseTo, messageTypes)
		{ }
	}
	public class ExpectMultiMessageSingleSource<T1> : ExpectMultiMessageSingleSourceSyncBase<T1>
		where T1 : Message
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<T1>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, responseTo, typeof(T1))
		{ }

	}
	public class ExpectMultiMessageSingleSource<T1, T2> : ExpectMultiMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectMultiMessageSingleSource<T1, T2, T3> : ExpectMultiMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectMultiMessageSingleSource<T1, T2, T3, T4> : ExpectMultiMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
	}
	public class ExpectMultiMessageSingleSource<T1, T2, T3, T4, T5> : ExpectMultiMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{
		public ExpectMultiMessageSingleSource(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}

	public class ExpectMultiMessageSingleSourceAsync : ExpectMultiMessageSingleSourceAsyncBase<Message>
	{
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origin, CancellationToken.None, responseTo, messageTypes)
		{ }
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null, params Type[] messageTypes)
			: base(terminator, origin, cancellation, responseTo, messageTypes)
		{ }

	}
	public class ExpectMultiMessageSingleSourceAsync<T1> : ExpectMultiMessageSingleSourceAsyncBase<T1>
		where T1 : Message
	{
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<T1>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, CancellationToken.None, responseTo, typeof(T1))
		{ }
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<T1>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origin, cancellation, responseTo, typeof(T1))
		{ }

	}
	public class ExpectMultiMessageSingleSourceAsync<T1, T2> : ExpectMultiMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator,
												   Guid origin, Message responseTo = null)
			: base(terminator, origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2))
		{ }

		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origin, cancellation, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectMultiMessageSingleSourceAsync<T1, T2, T3> : ExpectMultiMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectMultiMessageSingleSourceAsync<T1, T2, T3, T4> : ExpectMultiMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }

	}
	public class ExpectMultiMessageSingleSourceAsync<T1, T2, T3, T4, T5> : ExpectMultiMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{

		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, Message responseTo = null)
			: base(terminator, origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }
		public ExpectMultiMessageSingleSourceAsync(Func<ExpectMultiMessageSingleSourceBase<Message>, bool> terminator, Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(terminator, origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}
	#endregion
}
