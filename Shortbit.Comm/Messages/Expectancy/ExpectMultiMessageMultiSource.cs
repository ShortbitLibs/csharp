﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Comm.Exceptions;
using Shortbit.Utils.Collections.Generic;

namespace Shortbit.Comm.Messages.Expectancy
{
	public abstract class ExpectMultiMessageMultiSourceBase<T> : MessageExpectancy
		where T : Message
	{
		private readonly Type[] messageTypes;
		private readonly Message responseMessage;
		private readonly HashSet<Guid> origins;
		private readonly HashSet<Guid> receivedOrigins = new HashSet<Guid>();

		private readonly Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator;

		private readonly ManualResetEvent terminated = new ManualResetEvent(false);
		public WaitHandle WaitHandle => this.terminated;

		public IReadOnlySet<Guid> ExpectedOrigins { get; }
		public IReadOnlySet<Guid> ReceivedOrigins { get; }

		public int TotalReceivedMessagesCount { get; private set; } = 0;
		public int MinOriginReceivedMessagesCount { get; private set; } = 0;
		public int MaxOriginReceivedMessagesCount { get; private set; } = 0;

		private readonly Dictionary<Guid, List<T>> receivedMessages = new Dictionary<Guid, List<T>>();
		private readonly Dictionary<Guid, IReadOnlyList<T>> receivedMessagesWrapper = new Dictionary<Guid, IReadOnlyList<T>>();
		public IReadOnlyDictionary<Guid, IReadOnlyList<T>> ReceivedMessages { get; }

		protected ExpectMultiMessageMultiSourceBase(Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, Message responseMessage, Type[] messageTypes)
		{
			this.terminator = terminator;
			this.origins = new HashSet<Guid>(origins);
			this.responseMessage = responseMessage;
			this.messageTypes = messageTypes;


			this.ExpectedOrigins = new ReadOnlySet<Guid>(this.origins);
			this.ReceivedOrigins = new ReadOnlySet<Guid>(this.receivedOrigins);
			this.ReceivedMessages = new ReadOnlyDictionary<Guid, IReadOnlyList<T>>(this.receivedMessagesWrapper);
		}

		public override bool IsExpected(Message message)
		{
			return this.messageTypes.Contains(message.GetType()) &&
				   (this.responseMessage == null || message.Header.IsResponseTo(this.responseMessage.Header)) &&
				   this.origins.Contains(message.Header.Origin);
		}

		public override bool IsSatisfied()
		{
			if (this.terminator(this))
			{
				this.terminated.Set();
				return true;
			}

			return false;
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			this.terminated.Set();
		}

		public override void ProcessMessage(Message message)
		{
			var origin = message.Header.Origin;
			if (!this.receivedMessages.ContainsKey(origin))
			{
				this.receivedMessages.Add(origin, new List<T>());
				this.receivedMessagesWrapper.Add(origin, this.receivedMessages[origin]);
			}
			this.receivedMessages[message.Header.Origin].Add((T)message);
			this.receivedOrigins.Add(message.Header.Origin);

			this.TotalReceivedMessagesCount++;
			this.MinOriginReceivedMessagesCount = this.ReceivedMessages.Select(p => p.Value.Count).Min();
			this.MaxOriginReceivedMessagesCount = this.ReceivedMessages.Select(p => p.Value.Count).Max();
		}

	}
	public class ExpectMultiMessageMultiSourceSyncBase<T> : ExpectMultiMessageMultiSourceBase<T>
		where T : Message
	{

		public ExpectMultiMessageMultiSourceSyncBase(Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, Message responseMessage,
													  params Type[] messageTypes)
			: base(terminator, origins, responseMessage, messageTypes)
		{
		}


		public IReadOnlyDictionary<Guid, IReadOnlyList<T>> GetMessages()
		{
			this.WaitHandle.WaitOne();
			if (this.ThrowOnTimeout && this.TimedOut)
				throw ExpectancyTimeoutException.Create(this, this.ReceivedMessages);
			return this.ReceivedMessages;
		}
	}

	public class ExpectMultiMessageMultiSourceAsyncBase<T> : ExpectMultiMessageMultiSourceBase<T>
		where T : Message
	{
		private readonly TaskCompletionSource<IReadOnlyDictionary<Guid, IReadOnlyList<T>>> tcs = new TaskCompletionSource<IReadOnlyDictionary<Guid, IReadOnlyList<T>>>();

		private readonly CancellationTokenSource ownCts = new CancellationTokenSource();
		private readonly CancellationTokenSource cts;

		public ExpectMultiMessageMultiSourceAsyncBase(Func<ExpectMultiMessageMultiSourceBase<T>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellationToken, Message responseMessage,
													   params Type[] messageTypes)
			: base(terminator, origins, responseMessage, messageTypes)
		{
			this.cts = CancellationTokenSource.CreateLinkedTokenSource(this.ownCts.Token, cancellationToken);

			this.cts.Token.Register(() => this.tcs.TrySetCanceled(this.cts.Token));
		}

		public override bool IsSatisfied()
		{
			var res = base.IsSatisfied();
			if (res)
				this.tcs.SetResult(this.ReceivedMessages);
			return res;
		}

		protected override void AfterDetach()
		{
			base.AfterDetach();
			if (!this.WaitHandle.WaitOne(0) && !this.TimedOut)
				this.ownCts.Cancel();
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			if (this.ThrowOnTimeout)
				this.tcs.SetException(ExpectancyTimeoutException.Create(this, this.ReceivedMessages));
			else
				this.tcs.SetResult(this.ReceivedMessages);
		}

		public Task<IReadOnlyDictionary<Guid, IReadOnlyList<T>>> GetMessagesAsync()
		{
			return this.tcs.Task;
		}



		#region Typed Overloads
		public class ExpectMultiMessageMultiSource : ExpectMultiMessageMultiSourceSyncBase<Message>
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null, params Type[] messageTypes)
				: base(terminator, origins, responseTo, messageTypes)
			{ }
		}
		public class ExpectMultiMessageMultiSource<T1> : ExpectMultiMessageMultiSourceSyncBase<T1>
			where T1 : Message
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, responseTo, typeof(T1))
			{ }

		}
		public class ExpectMultiMessageMultiSource<T1, T2> : ExpectMultiMessageMultiSourceSyncBase<Message>
			where T1 : Message
			where T2 : Message
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, responseTo, typeof(T1), typeof(T2))
			{ }

		}
		public class ExpectMultiMessageMultiSource<T1, T2, T3> : ExpectMultiMessageMultiSourceSyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3))
			{ }

		}
		public class ExpectMultiMessageMultiSource<T1, T2, T3, T4> : ExpectMultiMessageMultiSourceSyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
			{ }
		}
		public class ExpectMultiMessageMultiSource<T1, T2, T3, T4, T5> : ExpectMultiMessageMultiSourceSyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
			where T5 : Message
		{
			public ExpectMultiMessageMultiSource(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
			{ }

		}

		public class ExpectMultiMessageMultiSourceAsync : ExpectMultiMessageMultiSourceAsyncBase<Message>
		{
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null, params Type[] messageTypes)
				: base(terminator, origins, CancellationToken.None, responseTo, messageTypes)
			{ }
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null, params Type[] messageTypes)
				: base(terminator, origins, cancellation, responseTo, messageTypes)
			{ }

		}
		public class ExpectMultiMessageMultiSourceAsync<T1> : ExpectMultiMessageMultiSourceAsyncBase<T1>
			where T1 : Message
		{
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1))
			{ }
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<T1>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
				: base(terminator, origins, cancellation, responseTo, typeof(T1))
			{ }

		}
		public class ExpectMultiMessageMultiSourceAsync<T1, T2> : ExpectMultiMessageMultiSourceAsyncBase<Message>
			where T1 : Message
			where T2 : Message
		{
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator,
													   IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2))
			{ }

			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
				: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2))
			{ }

		}
		public class ExpectMultiMessageMultiSourceAsync<T1, T2, T3> : ExpectMultiMessageMultiSourceAsyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
		{
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3))
			{ }
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
				: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3))
			{ }

		}
		public class ExpectMultiMessageMultiSourceAsync<T1, T2, T3, T4> : ExpectMultiMessageMultiSourceAsyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
		{
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
			{ }
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
				: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
			{ }

		}
		public class ExpectMultiMessageMultiSourceAsync<T1, T2, T3, T4, T5> : ExpectMultiMessageMultiSourceAsyncBase<Message>
			where T1 : Message
			where T2 : Message
			where T3 : Message
			where T4 : Message
			where T5 : Message
		{

			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, Message responseTo = null)
				: base(terminator, origins, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
			{ }
			public ExpectMultiMessageMultiSourceAsync(Func<ExpectMultiMessageMultiSourceBase<Message>, bool> terminator, IEnumerable<Guid> origins, CancellationToken cancellation, Message responseTo = null)
				: base(terminator, origins, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
			{ }

		}
		#endregion
	}
}
