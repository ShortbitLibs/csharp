﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Comm.Exceptions;

namespace Shortbit.Comm.Messages.Expectancy
{

	public abstract class ExpectSingleMessageSingleSourceBase : MessageExpectancy
	{
		private readonly Type[] messageTypes;
		private readonly Message responseMessage;
		private readonly Guid origin;

		protected readonly ManualResetEvent terminated = new ManualResetEvent(false);

		public WaitHandle WaitHandle => this.terminated;

		internal ExpectSingleMessageSingleSourceBase(Guid origin, Message responseMessage, Type[] messageTypes)
		{
			if (messageTypes == null || messageTypes.Length == 0)
				throw new ArgumentNullException(nameof(messageTypes));

			this.messageTypes = messageTypes;
			this.origin = origin;
			this.responseMessage = responseMessage;
		}
		
		public override bool IsExpected(Message message)
		{
			return this.messageTypes.Contains(message.GetType()) &&
				   (this.responseMessage == null || message.Header.IsResponseTo(this.responseMessage.Header)) &&
				   (this.origin == message.Header.Origin);
			
		}

		public override void ProcessMessage(Message message)
		{
			this.terminated.Set();
		}

		public override bool IsSatisfied() => this.terminated.WaitOne(0);

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			this.terminated.Set();
		}
	}

	public class ExpectSingleMessageSingleSourceSyncBase<T> : ExpectSingleMessageSingleSourceBase
		where T : Message
	{
		private T message;

		public ExpectSingleMessageSingleSourceSyncBase(Guid origin, Message responseMessage, params Type[] messageTypes)
			: base(origin, responseMessage, messageTypes)
		{
		}

		public override void ProcessMessage(Message message)
		{
			base.ProcessMessage(message);
			this.message = (T)message;
		}

		public T GetMessage()
		{
			this.terminated.WaitOne();
			if (this.ThrowOnTimeout && this.TimedOut)
				throw ExpectancyTimeoutException.Create(this, this.message);
			return this.message;
		}
	}

	public class ExpectSingleMessageSingleSourceAsyncBase<T> : ExpectSingleMessageSingleSourceBase
		where T : Message
	{
		private readonly TaskCompletionSource<T> tcs = new TaskCompletionSource<T>();

		private readonly CancellationTokenSource ownCts = new CancellationTokenSource();
		private readonly CancellationTokenSource cts;

		public ExpectSingleMessageSingleSourceAsyncBase(Guid origin, CancellationToken cancellationToken, Message responseMessage, params Type[] messageTypes) :
			base(origin, responseMessage, messageTypes)
		{
			this.cts = CancellationTokenSource.CreateLinkedTokenSource(this.ownCts.Token, cancellationToken);

			this.cts.Token.Register(() => this.tcs.TrySetCanceled(this.cts.Token));
		}

		public override void ProcessMessage(Message message)
		{
			base.ProcessMessage(message);
			this.tcs.SetResult((T)message);
		}
		
		protected override void AfterDetach()
		{
			base.AfterDetach();
			if (!this.WaitHandle.WaitOne(0))
				this.ownCts.Cancel();
		}

		protected override void AfterTimeout()
		{
			base.AfterTimeout();
			if (this.ThrowOnTimeout)
				this.tcs.SetException(ExpectancyTimeoutException.Create<T>(this, null));
			else
				this.tcs.SetResult(null);
		}

		public Task<T> GetMessageAsync()
		{
			return this.tcs.Task;
		}
	}

	#region Typed Overloads
	public class ExpectSingleMessageSingleSource : ExpectSingleMessageSingleSourceSyncBase<Message>
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null, params Type[] messageTypes)
			: base(origin, responseTo, messageTypes)
		{ }
	}
	public class ExpectSingleMessageSingleSource<T1> : ExpectSingleMessageSingleSourceSyncBase<T1>
		where T1: Message
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null)
			: base(origin, responseTo, typeof(T1))
		{ }

	}
	public class ExpectSingleMessageSingleSource<T1, T2> : ExpectSingleMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null)
			: base(origin, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectSingleMessageSingleSource<T1, T2, T3> : ExpectSingleMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null)
			: base(origin, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectSingleMessageSingleSource<T1, T2, T3, T4> : ExpectSingleMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null)
			: base(origin, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
	}
	public class ExpectSingleMessageSingleSource<T1, T2, T3, T4, T5> : ExpectSingleMessageSingleSourceSyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{
		public ExpectSingleMessageSingleSource(Guid origin, Message responseTo = null)
			: base(origin, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}

	public class ExpectSingleMessageSingleSourceAsync : ExpectSingleMessageSingleSourceAsyncBase<Message>
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null, params Type[] messageTypes)
			: base(origin, CancellationToken.None, responseTo, messageTypes)
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null, params Type[] messageTypes)
			: base(origin, cancellation, responseTo, messageTypes)
		{ }
		
	}
	public class ExpectSingleMessageSingleSourceAsync<T1> : ExpectSingleMessageSingleSourceAsyncBase<T1>
		where T1 : Message
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null)
			: base(origin, CancellationToken.None, responseTo, typeof(T1))
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(origin, cancellation, responseTo, typeof(T1))
		{ }

	}
	public class ExpectSingleMessageSingleSourceAsync<T1, T2> : ExpectSingleMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null)
			: base(origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2))
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(origin, cancellation, responseTo, typeof(T1), typeof(T2))
		{ }

	}
	public class ExpectSingleMessageSingleSourceAsync<T1, T2, T3> : ExpectSingleMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null)
			: base(origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3))
		{ }

	}
	public class ExpectSingleMessageSingleSourceAsync<T1, T2, T3, T4> : ExpectSingleMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null)
			: base(origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4))
		{ }

	}
	public class ExpectSingleMessageSingleSourceAsync<T1, T2, T3, T4, T5> : ExpectSingleMessageSingleSourceAsyncBase<Message>
		where T1 : Message
		where T2 : Message
		where T3 : Message
		where T4 : Message
		where T5 : Message
	{
		public ExpectSingleMessageSingleSourceAsync(Guid origin, Message responseTo = null)
			: base(origin, CancellationToken.None, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }
		public ExpectSingleMessageSingleSourceAsync(Guid origin, CancellationToken cancellation, Message responseTo = null)
			: base(origin, cancellation, responseTo, typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5))
		{ }

	}
	#endregion
}
