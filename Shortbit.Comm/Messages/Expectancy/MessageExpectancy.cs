﻿using System;
using System.Collections.Generic;

namespace Shortbit.Comm.Messages.Expectancy
{
	public abstract class MessageExpectancy
	{
		public bool IsAttached => this.Node != null;
		public CommNode Node { get; private set; }

		public DateTime? AttachTime { get; private set; }

		public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(30);

		public bool ThrowOnTimeout { get; set; } = false;

		public bool TimedOut { get; private set; }

		internal MessageExpectancy()
		{

		}

		internal void CheckTimeout()
		{
			if (!this.AttachTime.HasValue || DateTime.Now < this.AttachTime + this.Timeout)
				return;

			this.TimedOut = true;
			this.AfterTimeout();
			this.Node.RemoveExpectancy(this);
		}

		protected virtual void AfterTimeout()
		{

		}


		internal void Attach(CommNode node)
		{
			this.BeforeAttach();
			this.Node = node;
			this.AttachTime = DateTime.Now;
			this.AfterAttach();
		}


		protected virtual void BeforeAttach()
		{
		}

		protected virtual void AfterAttach()
		{
		}


		internal void Detach()
		{
			this.BeforeDetach();
			this.Node = null;
			this.AttachTime = null;
			this.AfterDetach();
		}

		protected virtual void BeforeDetach()
		{
		}

		protected virtual void AfterDetach()
		{
		}

		public abstract bool IsExpected(Message message);

		public abstract bool IsSatisfied();

		public abstract void ProcessMessage(Message message);

	}
	
}
