﻿// /*
//  * MessageRegistry.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using Shortbit.Comm.CommLibMessages;
using Shortbit.Comm.LinkStateNetwork;
using Shortbit.Utils;

namespace Shortbit.Comm.Messages
{
	public class MessageRegistry
	{
		private static readonly MessageRegistry _default = new MessageRegistry();

		public static MessageRegistry Default => MessageRegistry._default;

		private readonly object writeLock = new object();

		private readonly Dictionary<string, MessageDescriptor> messagesByKey = new Dictionary<string, MessageDescriptor>();
		private readonly Dictionary<Type, MessageDescriptor> messagesByType = new Dictionary<Type, MessageDescriptor>();

		public MessageRegistry()
		{
			CommMessagesRegistry.Register(this);
			LsnMessagesRegistry.Register(this);
		}

		// ReSharper disable InconsistentlySynchronizedField
		public MessageDescriptor this[string key] => this.messagesByKey[key];
		public MessageDescriptor this[Type messageType] => this.messagesByType[messageType];
		public MessageDescriptor this[Message message] => this.messagesByType[message.GetType()];
		// ReSharper restore InconsistentlySynchronizedField

		public MessageRegistry Clone()
		{

			var res = new MessageRegistry();
			lock (this.writeLock)
			{
				res.messagesByKey.AddRange(this.messagesByKey);
				res.messagesByType.AddRange(this.messagesByType);
			}

			return res;
		}

		public void Register(MessageDescriptor descriptor)
		{
			lock (this.writeLock)
			{
				if (this.messagesByKey.ContainsKey(descriptor.Key))
					return;
					//throw new ArgumentException($"Message {descriptor.Key} already registered.");

				this.messagesByKey.Add(descriptor.Key, descriptor);
				this.messagesByType.Add(descriptor.MessageClass, descriptor);
			}
		}

		// ReSharper disable InconsistentlySynchronizedField
		public bool Contains(string key) => this.messagesByKey.ContainsKey(key);
		public bool Contains(Type messageType) => this.messagesByType.ContainsKey(messageType);
		public bool Contains(Message message) => this.messagesByType.ContainsKey(message.GetType());
		// ReSharper restore InconsistentlySynchronizedField

	}
}