﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Comm.Messages
{
	internal class MessagePackageDispatchHandle
	{
		private readonly ManualResetEvent messageProcessed;

		public MessagePackageDispatchHandle(MessagePackage package)
		{
			this.Package = package;
			this.messageProcessed = new ManualResetEvent(false);
		}

		public MessagePackage Package { get; }
		public bool IsDispatched { get; private set; }

		public WaitHandle WaitHandle => this.messageProcessed;

		public void MarkAsDisposed()
		{
			this.IsDispatched = false;
			this.messageProcessed.Set();
		}

		public void MarkAsDispatched()
		{
			this.IsDispatched = true;
			this.messageProcessed.Set();
		}

		public bool WaitOnProcessed(int timeout)
		{
			if (!this.messageProcessed.WaitOne(timeout))
				throw new TimeoutException("Timeout occured while waiting on Message dispatch.");
			return this.IsDispatched;
		}
	}
}
