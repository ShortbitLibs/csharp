﻿using System;
using Google.Protobuf;
using pbr = Google.Protobuf.Reflection;

namespace Shortbit.Comm.Messages
{
	public class MessageDescriptor
	{
		public MessageDescriptor(string key, Func<IMessage, Message> contructor, Func<Message, IMessage> dataAccessor, Type messageClass, pbr.MessageDescriptor protoDescriptor)
		{
			this.Key = key;
			this.Contructor = contructor;
			this.DataAccessor = dataAccessor;
			this.MessageClass = messageClass;
			this.ProtoDescriptor = protoDescriptor;
		}

		public string Key { get; }

		public Func<IMessage, Message> Contructor { get; }

		public Func<Message, IMessage> DataAccessor { get; }

		public Type MessageClass { get; }

		public pbr.MessageDescriptor ProtoDescriptor { get; }


	}
}
