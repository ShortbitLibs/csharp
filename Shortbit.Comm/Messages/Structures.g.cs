// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: test_messages.proto
// </auto-generated>

#region Designer generated code
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Shortbit.Comm;
using Shortbit.Comm.Messages;
using _Proto = Shortbit.Comm.Messages;

namespace Shortbit.Comm.Messages {
	// python.message_module: Shortbit_Comm.messages.structures
	// Class Info::
	//     python.mixin=
	//     package=Shortbit.Comm.Messages
	//     namespace=Shortbit.Comm.Messages
	//     proto_message_name=ProtoGuid
	//     message_name=Guid
	//     class_name=MsgGuid
	//     proto_class_name_including_parents=ProtoGuid
	//     class_name_including_parents=MsgGuid
	//     full_proto_message_path=.Shortbit.Comm.Messages.ProtoGuid
	//     full_proto_class_path=Shortbit.Comm.Messages.ProtoGuid
	//     full_class_path=Shortbit.Comm.Messages.MsgGuid
	public partial class MsgGuid {
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		protected Func<bool, Shortbit.Comm.Messages.ProtoGuid> _dataAccess;
		
		public Shortbit.Comm.Messages.ProtoGuid Data
		{
			get => this._dataAccess(false);
		}
		
		// Field:: name=value, type=TYPE_STRING, type_name=, is_array=False, is_map=False
		public string Value
		{
			get => this._dataAccess(false).Value;
			set => this._dataAccess(true).Value = value;
		}
		
		
		public override bool Equals(object obj) => this.Equals(obj as MsgGuid);
		public bool Equals(MsgGuid value) => value != null && this._dataAccess(false).Equals(value._dataAccess(false));
		public override int GetHashCode() => this._dataAccess(false).GetHashCode();
		public static implicit operator MsgGuid(Shortbit.Comm.Messages.ProtoGuid msg) => new MsgGuid(msg);
		public static implicit operator Shortbit.Comm.Messages.ProtoGuid(MsgGuid msg) => msg._dataAccess(false);
		public override string ToString() => this._dataAccess(false).ToString();
		public void MergeFrom(MsgGuid other) => this._dataAccess(true).MergeFrom(other?._dataAccess(false));
		
		
		public MsgGuid() 
			: base()
		{
			var data = new Shortbit.Comm.Messages.ProtoGuid();
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		public MsgGuid(Shortbit.Comm.Messages.ProtoGuid data) 
			: base()
		{
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		partial void PostInit();
	}
	
	// Class Info::
	//     python.mixin=
	//     package=Shortbit.Comm.Messages
	//     namespace=Shortbit.Comm.Messages
	//     proto_message_name=MessageHeader
	//     message_name=MessageHeader
	//     class_name=MsgMessageHeader
	//     proto_class_name_including_parents=MessageHeader
	//     class_name_including_parents=MsgMessageHeader
	//     full_proto_message_path=.Shortbit.Comm.Messages.MessageHeader
	//     full_proto_class_path=Shortbit.Comm.Messages.MessageHeader
	//     full_class_path=Shortbit.Comm.Messages.MsgMessageHeader
	public partial class MsgMessageHeader {
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		protected Func<bool, Shortbit.Comm.Messages.MessageHeader> _dataAccess;
		
		public Shortbit.Comm.Messages.MessageHeader Data
		{
			get => this._dataAccess(false);
		}
		
		// Field:: name=Origin, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.Messages.ProtoGuid, is_array=False, is_map=False
		//     type_name=MsgGuid
		//     accessor_type_name=MsgGuid_Access
		private class MsgGuid_Access : MsgGuid
		{
			public MsgGuid_Access (Func<bool, Shortbit.Comm.Messages.ProtoGuid> dataAccess)
				: base()
			{
				this._dataAccess = dataAccess;
			}
		}
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object _locksync_Origin = new object();
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private MsgGuid_Access _backingField_Origin;
		public MsgGuid Origin
		{
			get
			{
				if (this._backingField_Origin == null)
				{
					lock(this._locksync_Origin)
					{
						if (this._backingField_Origin == null)
						{
							this._backingField_Origin = new MsgGuid_Access(
							writing => {
								if (this._dataAccess(writing).Origin == null)
								{
									var value = new Shortbit.Comm.Messages.ProtoGuid();
									if (writing)
										this._dataAccess(writing).Origin = value;
									else
										return value;
								}
								return this._dataAccess(writing).Origin;
							});
						}
					}
				}
				return this._backingField_Origin;
			}
		}
		
		// Field:: name=Id, type=TYPE_UINT64, type_name=, is_array=False, is_map=False
		public ulong Id
		{
			get => this._dataAccess(false).Id;
			set => this._dataAccess(true).Id = value;
		}
		
		// Field:: name=ResponseToOrigin, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.Messages.ProtoGuid, is_array=False, is_map=False
		//     type_name=MsgGuid
		//     accessor_type_name=MsgGuid_Access
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object _locksync_ResponseToOrigin = new object();
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private MsgGuid_Access _backingField_ResponseToOrigin;
		public MsgGuid ResponseToOrigin
		{
			get
			{
				if (this._backingField_ResponseToOrigin == null)
				{
					lock(this._locksync_ResponseToOrigin)
					{
						if (this._backingField_ResponseToOrigin == null)
						{
							this._backingField_ResponseToOrigin = new MsgGuid_Access(
							writing => {
								if (this._dataAccess(writing).ResponseToOrigin == null)
								{
									var value = new Shortbit.Comm.Messages.ProtoGuid();
									if (writing)
										this._dataAccess(writing).ResponseToOrigin = value;
									else
										return value;
								}
								return this._dataAccess(writing).ResponseToOrigin;
							});
						}
					}
				}
				return this._backingField_ResponseToOrigin;
			}
		}
		
		// Field:: name=ResponseToId, type=TYPE_UINT64, type_name=, is_array=False, is_map=False
		public ulong ResponseToId
		{
			get => this._dataAccess(false).ResponseToId;
			set => this._dataAccess(true).ResponseToId = value;
		}
		
		// Field:: name=Recipients, type=TYPE_MESSAGE, type_name=.Shortbit.Comm.Messages.ProtoGuid, is_array=True, is_map=False
		//     type_name=MsgGuid
		//     accessor_type_name=MsgGuid_Access
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object _locksync_Recipients = new object();
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AccessorListWrapper<MsgGuid, MsgGuid_Access, Shortbit.Comm.Messages.ProtoGuid> _backingField_Recipients;
		public IList<MsgGuid> Recipients
		{
			get
			{
				if (this._backingField_Recipients == null)
				{
					lock(this._locksync_Recipients)
					{
						if (this._backingField_Recipients == null)
						{
							this._backingField_Recipients = new AccessorListWrapper<MsgGuid, MsgGuid_Access, Shortbit.Comm.Messages.ProtoGuid>(
								writing => this._dataAccess(writing).Recipients,
								entry => entry.Data,
								accessor => new MsgGuid_Access(accessor)
							);
						}
					}
				}
				return this._backingField_Recipients;
			}
		}
		
		
		public override bool Equals(object obj) => this.Equals(obj as MsgMessageHeader);
		public bool Equals(MsgMessageHeader value) => value != null && this._dataAccess(false).Equals(value._dataAccess(false));
		public override int GetHashCode() => this._dataAccess(false).GetHashCode();
		public static implicit operator MsgMessageHeader(Shortbit.Comm.Messages.MessageHeader msg) => new MsgMessageHeader(msg);
		public static implicit operator Shortbit.Comm.Messages.MessageHeader(MsgMessageHeader msg) => msg._dataAccess(false);
		public override string ToString() => this._dataAccess(false).ToString();
		public void MergeFrom(MsgMessageHeader other) => this._dataAccess(true).MergeFrom(other?._dataAccess(false));
		
		
		public MsgMessageHeader() 
			: base()
		{
			var data = new Shortbit.Comm.Messages.MessageHeader();
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		public MsgMessageHeader(Shortbit.Comm.Messages.MessageHeader data) 
			: base()
		{
			this._dataAccess = writing => data;
			this.PostInit();
		}
		
		partial void PostInit();
	}
	
}
# endregion Designer generated code
