﻿using System;
using System.Collections;
using System.Collections.Generic;
using Google.Protobuf.Collections;

namespace Shortbit.Comm.Messages
{
	public class AccessorListWrapper<TWrapper, TWrapperAccess, TData> : IList<TWrapper>
		where TWrapperAccess : TWrapper
	{
		private readonly Func<bool, RepeatedField<TData>> backboneAccess;
		private readonly Func<TWrapper, TData> dataAccess;
		private readonly Func<Func<bool, TData>, TWrapperAccess> constructor;

		//private readonly Dictionary<int, TAccess> accessors = new Dictionary<int, TAccess>();

		public AccessorListWrapper(Func<bool, RepeatedField<TData>> backboneAccess, Func<TWrapper, TData> dataAccess, Func<Func<bool, TData>, TWrapperAccess> constructor)
		{
			this.backboneAccess = backboneAccess;
			this.dataAccess = dataAccess;
			this.constructor = constructor;
		}

		/// <inheritdoc />
		public IEnumerator<TWrapper> GetEnumerator()
		{
			foreach (var data in this.backboneAccess(false))
			{
				yield return this.constructor(writing => data);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		/// <inheritdoc />
		public void Add(TWrapper item)
		{
			var data = this.dataAccess(item);
			var accessor = this.constructor(writing => data);
			this.backboneAccess(true).Add(data);
		}

		/// <inheritdoc />
		public void Clear()
		{
			this.backboneAccess(true).Clear();
		}

		/// <inheritdoc />
		public bool Contains(TWrapper item)
		{
			var data = this.dataAccess(item);
			return this.backboneAccess(false).Contains(data);
		}

		/// <inheritdoc />
		public void CopyTo(TWrapper[] array, int arrayIndex)
		{
			for (int i = 0; i < this.Count; i++)
			{
				array[arrayIndex + i] = this[i];
			}
		}

		/// <inheritdoc />
		public bool Remove(TWrapper item)
		{
			var data = this.dataAccess(item);
			return this.backboneAccess(false).Remove(data);
		}

		/// <inheritdoc />
		public int Count => this.backboneAccess(false).Count;

		/// <inheritdoc />
		public bool IsReadOnly => this.backboneAccess(false).IsReadOnly;

		/// <inheritdoc />
		public int IndexOf(TWrapper item)
		{
			var data = this.dataAccess(item);
			return this.backboneAccess(false).IndexOf(data);
		}

		/// <inheritdoc />
		public void Insert(int index, TWrapper item)
		{
			var data = this.dataAccess(item);
			this.backboneAccess(true).Insert(index, data);
		}

		/// <inheritdoc />
		public void RemoveAt(int index)
		{
			this.backboneAccess(true).RemoveAt(index);
		}

		/// <inheritdoc />
		public TWrapper this[int index]
		{
			get
			{
				var data = this.backboneAccess(false)[index];
				return this.constructor(writing => data);
			}
			set
			{
				var data = this.dataAccess(value);
				this.backboneAccess(true)[index] = data;
			}
		}
	}
}
