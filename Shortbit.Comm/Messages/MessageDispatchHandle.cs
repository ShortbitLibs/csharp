﻿// /*
//  * MessageDispatchHandle.cs
//  *
//  *  Created on: 15:58
//  *         Author: 
//  */

using System;
using System.Threading;

namespace Shortbit.Comm.Messages
{
	internal class MessageDispatchHandle
	{
		private readonly ManualResetEvent messageProcessed;

		public MessageDispatchHandle(Message m)
		{
			this.Message = m;
			this.messageProcessed = new ManualResetEvent(false);
		}

		public Message Message { get; private set; }
		public bool IsDispatched { get; private set; }

		public void MarkAsDisposed()
		{
			this.IsDispatched = false;
			this.messageProcessed.Set();
		}

		public void MarkAsDispatched()
		{
			this.IsDispatched = true;
			this.messageProcessed.Set();
		}

		public bool WaitOnProcessed(int timeout)
		{
			if (!this.messageProcessed.WaitOne(timeout))
				throw new TimeoutException("Timeout occured while waiting on Message dispatch.");
			return this.IsDispatched;
		}
	}
}