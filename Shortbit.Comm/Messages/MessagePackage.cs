﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Google.Protobuf;
using Shortbit.Comm.Exceptions;
using Shortbit.Utils.IO;

namespace Shortbit.Comm.Messages
{
	internal partial class MessagePackage
	{
		public bool IsKnownMessage => this.Body != null;

		public Message Body { get; set; }


		private static readonly SHA512 sha512 = SHA512.Create();

		public static MessagePackage Deserialize(Stream s, MessageRegistry registry)
		{
			byte[] headerContent;
			using (var protoStream = new CodedInputStream(s, true))
			{
				headerContent = protoStream.ReadBytes().ToByteArray();
			}

			var package = MessagePackage.Parser.ParseFrom(headerContent);

			var bodyContent = package.BodyContent.ToByteArray();
			byte[] readBodyChecksum = package.BodyChecksum.ToByteArray();
			byte[] calulatedBodyChecksum = MessagePackage.sha512.ComputeHash(bodyContent);
			
			//if (!MessagePackage.CompareHashes(readBodyChecksum, calulatedBodyChecksum))
			//	throw new ChecksumMissmatchException(
			//		"Calculated hash checksum of recieved data does not match recieved hash checksum.");

			if (registry.Contains(package.Type))
			{
				var descriptor = registry[package.Type];

				var protoMessage = descriptor.ProtoDescriptor.Parser.ParseFrom(bodyContent);
				package.Body = descriptor.Contructor(protoMessage);
				package.Body.Header = package.Header;
			}

			return package;
		}

		public void Serialize(Stream s)
		{
			byte[] content;
			using (var stream = new MemoryStream())
			{
				this.WriteTo(stream);
				content = stream.ToArray();
			}

			using (var protoStream = new CodedOutputStream(s, true))
			{
				protoStream.WriteBytes(ByteString.CopyFrom(content));
			}

		}
		public static MessagePackage Create(Message message, MessageRegistry registry, MessageHeader header = null)
		{
			if (!registry.Contains(message))
				throw new ArgumentException($"Message {message.GetType().FullName} is not registered.");

			var descriptor = registry[message];
			var protoMessage = descriptor.DataAccessor(message);

			if (header == null)
				header = message.Header;

			var package = new MessagePackage
			{
				Type = descriptor.Key,
				Header = header, 
				Ttl = 60
			};

			byte[] bodyContent = protoMessage.ToByteArray();
			byte[] bodyChecksum = MessagePackage.sha512.ComputeHash(bodyContent);
			package.BodyChecksum = ByteString.CopyFrom(bodyChecksum);
			package.BodyContent = ByteString.CopyFrom(bodyContent);

			return package;
		}

		private static bool CompareHashes(Byte[] hashA, Byte[] hashB)
		{
			if ((hashA == null) || (hashB == null))
				return false;
			if ((hashA.Length == 0) || (hashB.Length == 0))
				return false;
			if (hashA.Length != hashB.Length)
				return false;

			return !hashA.Where((t, i) => t != hashB[i]).Any();
		}
	}
}
