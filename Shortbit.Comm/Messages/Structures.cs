﻿using System.Collections.Generic;
using System.Linq;
using sys = System;

namespace Shortbit.Comm.Messages
{

	public static class GuidExtension
	{
		public static IEnumerable<sys.Guid> ToGuids(this IEnumerable<ProtoGuid> self)
		{
			return self.Select(id => id.ToGuid());
		}
		public static IEnumerable<ProtoGuid> ToProtoGuids(this IEnumerable<sys.Guid> self)
		{
			return self.Select(id => id.ToProtoGuid());
		}


		public static ProtoGuid ToProtoGuid(this sys.Guid id)
		{
			return id;
		}

		public static RecipientsList ToRecipientsList(this sys.Guid id)
		{
			return id;
		}
	}

	public partial class ProtoGuid
	{
		public static implicit operator sys.Guid(ProtoGuid id)
		{
			return sys.Guid.Parse(id.Value);
		}
		public static implicit operator ProtoGuid(sys.Guid id)
		{
			var res = new ProtoGuid();
			res.FromGuid(id);
			return res;
		}

		public sys.Guid ToGuid()
		{
			return sys.Guid.Parse(this.Value);
		}

		public void FromGuid(sys.Guid guid)
		{
			this.Value = guid.ToString();
		}

		public static bool operator == (ProtoGuid a, ProtoGuid b)
		{
			if (object.ReferenceEquals(null, a))
				return object.ReferenceEquals(null, b);
			if (object.ReferenceEquals(null, b))
				return false;

			return a.ToGuid() == b.ToGuid();
		}
	
		public static bool operator !=(ProtoGuid a, ProtoGuid b) => !(a == b);

		public static bool operator ==(sys.Guid a, ProtoGuid b)
		{
			if (object.ReferenceEquals(null, b))
				return a == sys.Guid.Empty;

			return a == b.ToGuid();
		}

		public static bool operator !=(sys.Guid a, ProtoGuid b) => !(a == b);

		public static bool operator ==(ProtoGuid a, sys.Guid b)
		{
			if (object.ReferenceEquals(null, a))
				return b == sys.Guid.Empty;

			return a.ToGuid() == b;
		}

		public static bool operator !=(ProtoGuid a, sys.Guid b) => !(a == b);
	}


	public partial class MsgGuid
	{
		public static implicit operator sys.Guid(MsgGuid id)
		{
			return id.ToGuid();
		}
		public static implicit operator MsgGuid(sys.Guid id)
		{
			var res = new MsgGuid();
			res.FromGuid(id);
			return res;
		}

		public sys.Guid ToGuid()
		{
			return sys.Guid.Parse(this.Value);
		}

		public void FromGuid(sys.Guid guid)
		{
			this.Value = guid.ToString();
		}
	}

	public partial class MessageHeader
	{
		public const int NoId = 0;

		public bool IsResponse => this.ResponseToOrigin != null && this.ResponseToId != MessageHeader.NoId;

		public bool IsResponseTo(MessageHeader header)
		{
			return this.ResponseToOrigin == header.Origin && this.ResponseToId == header.Id;
		}
	}
}
