﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Comm.Streams
{
    public interface IIncomingStream
    {
		bool DataAvailable { get; }

        int Read(byte[] buffer, int offset, int count);

        IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state);
        int EndRead(IAsyncResult asyncResult);
        
        Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken);
    }
}
