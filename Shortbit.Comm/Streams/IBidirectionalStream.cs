﻿namespace Shortbit.Comm.Streams
{
    public interface IBidirectionalStream : IIncomingStream, IOutgoingStream
    {
    }
}
