﻿namespace Shortbit.Comm.Streams
{
    public interface IBidirectionalStatelessPacketStream : IBidirectionalStream, IStatelessPacketStream
    {
    }
}
