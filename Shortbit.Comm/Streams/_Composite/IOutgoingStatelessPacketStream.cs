﻿namespace Shortbit.Comm.Streams
{
    public interface IOutgoingStatelessPacketStream : IOutgoingStream, IStatelessPacketStream
    {
    }
}
