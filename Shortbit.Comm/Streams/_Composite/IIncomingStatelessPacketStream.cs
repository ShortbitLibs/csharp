﻿namespace Shortbit.Comm.Streams
{
    public interface IIncomingStatelessPacketStream : IIncomingStream, IStatelessPacketStream
    {
    }
}
