﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Comm.Streams
{
	public class SocketPacketStream : IBidirectionalStatefullPacketStream
	{
		
		private IPStreamEndpoint localEndpoint;
		private IPStreamEndpoint remoteEndpoint;

		public bool DataAvailable => Socket.Available > 0;

		public bool Connected => Socket.Connected;

		public Socket Socket { get; }


		public IStreamEndpoint LocalEndpoint
		{
			get
			{
				if (Socket.IsBound)
					return localEndpoint ?? (localEndpoint = new IPStreamEndpoint((IPEndPoint) Socket.LocalEndPoint));
				localEndpoint = null;
				throw new InvalidOperationException("Can't access local endpoint on a not connected stream.");
			}
		}
		public IStreamEndpoint RemoteEndpoint
		{
			get
			{
				if (Socket.Connected)
					return remoteEndpoint ?? (remoteEndpoint = new IPStreamEndpoint((IPEndPoint) Socket.LocalEndPoint));
				remoteEndpoint = null;
				throw new InvalidOperationException("Can't access remote endpoint on a not connected stream.");
			}
		}

		public SocketPacketStream(Socket socket)
		{
			this.Socket = socket;
		}
		



		public int Read(byte[] buffer, int offset, int count)
		{
			return Socket.Receive(buffer, offset, count,SocketFlags.None);
		}

		public IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return Socket.BeginReceive(buffer, offset, count, SocketFlags.None, callback, state);
			
		}

		public int EndRead(IAsyncResult asyncResult)
		{
			return Socket.EndReceive(asyncResult);
		}

		public Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			var factory = new TaskFactory(cancellationToken);

			return factory.FromAsync(
				BeginRead(buffer, offset, count, null, null),
				EndRead
			);
		}

		public void Write(byte[] buffer, int offset, int count)
		{
			int sent = Socket.Send(buffer, offset, count, SocketFlags.None);
			if (sent != buffer.Length)
				throw new Exception();
		}

		public IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return Socket.BeginSend(buffer, offset, count, SocketFlags.None, callback, state);
		}

		public void EndWrite(IAsyncResult asyncResult)
		{
			Socket.EndSend(asyncResult);
		}

		public Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			var factory = new TaskFactory(cancellationToken);

			return factory.FromAsync(
				BeginWrite(buffer, offset, count, null, null),
				EndWrite
			);
		}

	}
}