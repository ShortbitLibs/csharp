﻿namespace Shortbit.Comm.Streams
{
    public interface IPacketStream
    {
		IStreamEndpoint LocalEndpoint { get; }
        IStreamEndpoint RemoteEndpoint { get; }
    }
}
