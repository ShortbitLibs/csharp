﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Comm.Streams
{
	public class OutgoingPacketStreamWrapper : Stream
	{
		private readonly IOutgoingPacketStream stream;

		public OutgoingPacketStreamWrapper(IOutgoingPacketStream stream)
		{
			this.stream = stream;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream.Write(buffer, offset, count);
		}

		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return stream.BeginWrite(buffer, offset, count, callback, state);
		}
		public override void EndWrite(IAsyncResult asyncResult)
		{
			stream.EndWrite(asyncResult);
		}

		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return stream.WriteAsync(buffer, offset, count, cancellationToken);
		}


		public override void Flush()
		{
			throw new InvalidOperationException();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new InvalidOperationException();
		}

		public override void SetLength(long value)
		{
			throw new InvalidOperationException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new InvalidOperationException();
		}


		public override bool CanRead => false;
		public override bool CanSeek => false;
		public override bool CanWrite => true;

		public override long Length
		{
			get { throw new InvalidOperationException(); }
		}

		public override long Position
		{
			get { throw new InvalidOperationException(); }
			set { throw new InvalidOperationException(); }
		}
	}
}