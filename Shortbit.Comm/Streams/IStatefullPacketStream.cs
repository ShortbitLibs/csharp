﻿namespace Shortbit.Comm.Streams
{
	public interface IStatefullPacketStream : IPacketStream
	{
		bool Connected { get; }
	}
}
