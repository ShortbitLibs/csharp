﻿using System.Net;

namespace Shortbit.Comm.Streams
{
	public class IPStreamEndpoint : IStreamEndpoint
	{
		public IPEndPoint Endpoint { get; }

		public IPStreamEndpoint(IPEndPoint endpoint)
		{
			Endpoint = endpoint;
		}
	}
}
