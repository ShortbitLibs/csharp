﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Shortbit.Comm.Streams
{
	public static class StreamTaskFactory
	{
		private class ReadWriteArgs
		{
			public byte[] Buffer { get; set; }
			public int Offset { get; set; }
			public int Count { get; set; }
		}

		public static Task<int> ReadAsync(IIncomingStream stream, byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			var factory = new TaskFactory<int>(cancellationToken);
			
			return factory.FromAsync(
				stream.BeginRead(buffer, offset, count, null, null),
				stream.EndRead
			);
		}

		public static Task WriteAsync(IOutgoingStream stream, byte[] buffer, int offset, int count,
			CancellationToken cancellationToken)
		{
			var factory = new TaskFactory(cancellationToken);

			return factory.FromAsync(
				stream.BeginWrite(buffer, offset, count, null, null),
				stream.EndWrite
			);
		}
	}
}
