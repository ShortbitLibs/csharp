﻿// /*
//  * CommClient.cs
//  *
//  *  Created on: 15:58
//  *		 Author: 
//  */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using Shortbit.Comm.Exceptions;
using Shortbit.Comm.CommLibMessages;
using Shortbit.Comm.Messages;
using Shortbit.Utils;
using Shortbit.Utils.AsyncCallbacks;

namespace Shortbit.Comm
{
	public class CommClient : IDisposable
	{
		private const int BuffersShutdownPollTimeout = 10;
			// This is also the sleep time that the buffer therad does between each iteration. Also determines the sleep time for each iteration

		private const int BuffersActivePollTimeout = 1000;
			// This is the time the buffer thread waits until it restarts the while and checks tBuffers_Shutdown again

		private const int KeepAliveShutdownPollTimeout = 5000;
		private const int KeepAliveActivePollTimeout = 500;

		private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		// Buffers

		private UInt64 currMessageIdCounter = 1;
		private readonly ManualResetEvent lockModeReleased;

		// Whitelist for lock mode. only these messages can be send/recieved in lock mode

		// Threads and ThreadLists
		private readonly Thread tBuffers;
		private readonly ManualResetEvent tBuffersActive;
		private readonly ManualResetEvent tBuffersShutdown;

		//private Thread TKeepAlive;

		// Cryptography stuff

		private readonly HashSet<string> lockModeMessageWhitelist;

		// Event handlers

		private readonly CallbackRegistry<Message, CommClient> messageHandlers;
		private readonly CallbackRegistry<Message, CommClient> orphanedMessageHandlers;
		private readonly CallbackRegistry<Exception, CommClient> exceptionHandlers;

		private readonly Lazy<CallbackRegistryWrapper<Message, CommClient>> messageHandlersWrapper;
		private readonly Lazy<CallbackRegistryWrapper<Message, CommClient>> orphanedMessageHandlersWrapper;
		private readonly Lazy<CallbackRegistryWrapper<Exception, CommClient>> exceptionHandlersWrapper;
		
		private int callbackTimeout;
		private WaitPolicy waitPolicy;


		private readonly ConcurrentQueue<MessageDispatchHandle> outBuffer;
		private readonly ConcurrentDictionary<Int64, ResponseQueue> responseBuffer;
		private Socket socket;
		private readonly MessageRegistry registry;

		public CommClient(MessageRegistry registry = null)
			: this(null, registry)
		{
			tBuffersActive.Reset();
		}

		internal CommClient(Socket socket, MessageRegistry registry = null)
		{
			if (registry == null)
				registry = MessageRegistry.Default;
			this.registry = registry;
			
			
			this.callbackTimeout = 2000;
			this.waitPolicy = WaitPolicy.DontWait;

			//	this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			this.socket = socket;
			this.SetupSocket();



			// Buffers
			//	log.Debug("Initializing Buffers...");
			outBuffer = new ConcurrentQueue<MessageDispatchHandle>();
			responseBuffer = new ConcurrentDictionary<long, ResponseQueue>();

			currMessageIdCounter = 1;

			// Threads
			//	log.Debug("Initializing Treads...");
			tBuffers = new Thread(TBuffers_Run) {Name = "Comm Buffer Thread", IsBackground = true};
			tBuffersShutdown = new ManualResetEvent(false);
			tBuffersActive = new ManualResetEvent(true);


			// Handlers
			//	log.Debug("Initializing event handlers...");
			messageHandlers = new CallbackRegistry<Message, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };
			orphanedMessageHandlers = new CallbackRegistry<Message, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };
			exceptionHandlers = new CallbackRegistry<Exception, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };

			messageHandlersWrapper = new Lazy<CallbackRegistryWrapper<Message, CommClient>>(() => new CallbackRegistryWrapper<Message, CommClient>(this.messageHandlers));
			orphanedMessageHandlersWrapper = new Lazy<CallbackRegistryWrapper<Message, CommClient>>(() => new CallbackRegistryWrapper<Message, CommClient>(this.orphanedMessageHandlers));
			exceptionHandlersWrapper = new Lazy<CallbackRegistryWrapper<Exception, CommClient>>(() => new CallbackRegistryWrapper<Exception, CommClient>(this.exceptionHandlers));

			// Lock mode
			lockModeReleased = new ManualResetEvent(true);

			//	log.Debug("Initializing message whitelist...");
			lockModeMessageWhitelist = new HashSet<string>();
			AddMessageTypeToWhitelist<Ack>();
			AddMessageTypeToWhitelist<SetLockMode>();
			
			AddMessageTypeToWhitelist<ByeBye>();

			// Finally register some intrenal message handlers
			//	log.Debug("Registering internal handlers...");
			this.MessageHandlers.Subscribe<SetLockMode>((m, client) =>
			{

				Log.Debug("Received SetLockMode message.");

				// Dispatch response and update lock mode
				// TODO: Dispatch(new Ack(m.Header.Id));

				if (m.LockMode)
					lockModeReleased.Reset();
				else
					lockModeReleased.Set();
			});
			this.MessageHandlers.Subscribe<ByeBye>(((m, client) => this.DisconnectLocalSocket(AbortionCause.RemoteClosed)));
			// TODO: this.MessageHandlers.Subscribe<KeepAlive>((m, client) => this.Dispatch(new Ack(m.Id)));
			
			// Done initializing everything. Start threads and go home...
			//	log.Debug("Starting buffer therad...");
			tBuffers.Start();
		}
		public void Dispose()
		{
			if (IsConnected)
				Disconnect();

			tBuffersShutdown.Set();
			tBuffers.Join(Timeout);
			
			// outBuffer.Clear(); // FIXME: Equivalent?
		}

		public bool IsInLockMode
		{
			get { return !lockModeReleased.IsSet(); }
		}

		public int Timeout { get; set; } = 100;

		public bool SilentlyRegister { get; set; } = true;

		public int CallbackTimeout { 
			get { return this.callbackTimeout; }
			set
			{
				this.callbackTimeout = value;
				this.messageHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
				this.orphanedMessageHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
				this.exceptionHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
			}
		}

		public WaitPolicy CallbackWaitPolicy
		{
			get { return this.waitPolicy; }
			set
			{
				this.waitPolicy = value;
				this.messageHandlers.WaitPolicy = value;
				this.orphanedMessageHandlers.WaitPolicy = value;
				this.exceptionHandlers.WaitPolicy = value;
			}
		}

		public bool IsAccepted { get; private set; }

		public bool IsClosed { get; private set; }

		public bool IsConnected
		{
			get
			{
				if ((this.IsClosed) || (socket == null))
					return false;

				try
				{

					if (socket.Poll(10, SelectMode.SelectRead) && socket.Available == 0)
						return false;

					if (!this.socket.Connected)
						return false;
				}
				catch (ObjectDisposedException)
				{
					return false;
				}

				return true;


				//	return ((socket.Poll(10, SelectMode.SelectRead)) && (socket.Available == 0) && (socket.Poll(10, SelectMode.SelectWrite)) &&
				//			(!socket.Poll(10, SelectMode.SelectError)));
			}
		}

		public IPEndPoint LocalEndPoint
		{
			get
			{
				if (!this.IsConnected) return null; 
				return (IPEndPoint) socket.LocalEndPoint;
			}
		}

		public IPEndPoint RemoteEndPoint
		{
			get
			{
				if (!this.IsConnected) return null; 
				return (IPEndPoint)socket.RemoteEndPoint;
			}
		}


		public ICallbackRegistry<Message, CommClient> MessageHandlers { get { return this.messageHandlersWrapper.Value; } }
		public ICallbackRegistry<Message, CommClient> OrphanedMessageHandlers { get { return this.orphanedMessageHandlersWrapper.Value; } }
		public ICallbackRegistry<Exception, CommClient> ExceptionHandlers { get { return this.exceptionHandlersWrapper.Value; } } 




		public event Callback<Message, CommClient> OnAnyMessage
		{
			add { this.MessageHandlers.Subscribe(value); }
			remove { this.MessageHandlers.Revoke(value); }
		}

		public event Callback<Message, CommClient> OnAnyOrphanedResponseMessage
		{
			add { this.OrphanedMessageHandlers.Subscribe(value); }
			remove { this.OrphanedMessageHandlers.Revoke(value); }
		}

		public event Callback<Exception, CommClient> OnAnyException
		{
			add { this.ExceptionHandlers.Subscribe(value); }
			remove { this.ExceptionHandlers.Revoke(value); }
		}

		public event ConnectionAbortedHandlerDelegate OnConnectionAborted;

		internal void SubscribeAll(CallbackRegistry<Message, CommClient> messageHandlers,
			CallbackRegistry<Message, CommClient> orphanedMessageHandlers,
			CallbackRegistry<Exception, CommClient> exceptionHandlers)
		{
			this.messageHandlers.SubscribeAll(messageHandlers);
			this.orphanedMessageHandlers.SubscribeAll(orphanedMessageHandlers);
			this.exceptionHandlers.SubscribeAll(exceptionHandlers);
		}


		private void SetupSocket()
		{
			if (this.socket == null)
			{
				this.IsAccepted = false;
				this.IsClosed = true;
				return;
			}


			if ((this.socket.IsBound) && (this.socket.Connected))
				this.IsAccepted = true;

			this.IsClosed = false;

			//	log.Debug("Initializing socket...");
			this.socket.ReceiveTimeout = this.Timeout;
			this.socket.SendTimeout = this.Timeout;
			this.socket.ReceiveBufferSize = Int32.MaxValue;
			this.socket.SendBufferSize = Int32.MaxValue;
			this.socket.Blocking = true;
		}


		public void Connect(string host, int port)
		{
			Connect(socket => socket.Connect(host, port));
		}

		public void Connect(EndPoint remote)
		{
			Connect(socket => socket.Connect(remote));
		}

		public void Connect(IPAddress address, int port)
		{
			Connect(socket => socket.Connect(address, port));
		}
		public void Connect(IPAddress[] addresses, int port)
		{
			Connect(socket => socket.Connect(addresses, port));
		}

		private void Connect(Action<Socket> connect)
		{
			if (IsAccepted)
				throw new InvalidOperationException("Accepted Clients annot be connected to new remote host.");

			Disconnect();

			this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			this.SetupSocket();

			connect(this.socket);

			tBuffersActive.Set();
		}



		public void Disconnect()
		{
			if (!this.IsConnected)
				return;
			
			try
			{
				//TODO: this.Dispatch(new Byebye());
			}
			catch (TimeoutException) { }
			catch (MessageDisposedException) { }
			this.DisconnectLocalSocket(AbortionCause.LocalClosed);
		}


		private void DisconnectLocalSocket(AbortionCause cause)
		{
			if (this.IsClosed)
				return;

			tBuffersActive.Reset();
			
			var args = new ConnectionAbortedEventArgs(cause, (IPEndPoint)socket.RemoteEndPoint,
				(IPEndPoint)socket.LocalEndPoint);


			if (this.socket != null)
			{
				lock (this.socket)
				{
					this.socket.Close();
					this.socket = null;
				}
			}
			IsClosed = true;

			foreach (var handle in this.outBuffer)
				handle.MarkAsDisposed();

			this.ProcessAbortedConnection(args);

		}

		/// <summary>
		///	 Enqueues the given message for transmission.
		/// </summary>
		/// <param name="m">The Message that should get dispatched.</param>
		/// <exception cref="System.TimeoutException">
		///	 Client was to long in lock mode - OR - methout could not aquire thread lock
		///	 on output buffer.
		/// </exception>
		public void Dispatch(Message m)
		{
			if (!this.IsConnected)
				throw new InvalidOperationException("Client is not connected.");

			if (!this.registry.Contains(m))
			{
				if (this.SilentlyRegister)
					this.registry.Register(m.GetDescriptor());
				else
					throw new ArgumentException($"Message {m.GetType().FullName} is not registered.");
			}

			var descriptor = this.registry[m];
			
			// Wait until lock mode is released
			if ((!lockModeMessageWhitelist.Contains(descriptor.Key)) && (!lockModeReleased.IsSet()))
			{
				Log.Debug("Message " + m.GetType().FullName + " not on whitelist. Waiting on LockMode release.");
				if (!lockModeReleased.WaitOne(Timeout))
					throw new TimeoutException("Unable to Dispatch Message. Client was to long in lock mode.");
			}

			if (!Monitor.TryEnter(outBuffer, Timeout)) 
				throw new TimeoutException("Unable to Dispatch Message. Can't get lock on Buffer.");

			// Set Message id so that caller can use it immidiatly for getResponse calls...

			var handle = new MessageDispatchHandle(m);

			// Enqueue it for output...
			outBuffer.Enqueue(handle);

			// Unlock buffer
			Monitor.Exit(outBuffer);

			if (!handle.WaitOnProcessed(Timeout))
				throw new MessageDisposedException("Connection was closed before the Message could be dispatched.");
		}

		/// <summary>
		///	 Retrieves the next response for the given message id. This method blocks until a reponse is available.
		/// </summary>
		/// <param name="m">The message id which the response should get retrieved.</param>
		/// <returns>The next message responded to the given id.</returns>
		/// <exception cref="System.ArgumentException">The given message id was invalid.</exception>
		/// <exception cref="System.TimeoutException">A Timeout occured while waiting on a response.</exception>
		public Message GetResponse(Message m)
		{
			//TODO: return GetResponse(m.Id);
			throw new NotImplementedException();
		}

		/// <summary>
		///	 Retrieves the next response for the given message id. This method blocks until a reponse is available.
		/// </summary>
		/// <param name="id">The message id to which the response should get retrieved.</param>
		/// <returns>The next message responded to the given id.</returns>
		/// <exception cref="System.ArgumentException">The given message id was invalid.</exception>
		/// <exception cref="System.TimeoutException">A Timeout occured while waiting on a response.</exception>
		public Message GetResponse(Int64 id)
		{
			//TODO: if (id == Message.NoId)
			//TODO: throw new ArgumentException("Given message id is invalid (null/not set).");

			ResponseQueue response = responseBuffer.GetOrAdd(id, k => new ResponseQueue(this));
			

			Message ret = response.Dequeue();
			if (response.Empty)
			{
				responseBuffer.TryRemove(id, out response);
			}

			return ret;
		}

		/// <summary>
		///	 Dispatches the given message and fetches the first response.
		///	 This is just a shortcut for Dispatch(Message m) and GetReponse(Int64 id).
		/// </summary>
		/// <param name="m">The message that should get dispatched.</param>
		/// <returns>Returns the first response to teh dispaced message.</returns>
		/// <exception cref="System.ArgumentException">The given message id was invalid.</exception>
		/// <exception cref="System.TimeoutException">
		///	 Client was to long in lock mode - OR - methout could not aquire thread lock
		///	 on output buffer - OR - a Timeout occured while waiting on a response.
		/// </exception>
		public Message Communicate(Message m)
		{
			Dispatch(m);
			return GetResponse(m);
		}

	/*	public async Task<Message> CommunicateAsync(Message m)
		{
			
		}*/

			protected void AddMessageTypeToWhitelist<T>() where T : Message
		{
			//TODO: AddMessageTypeIdToWhitelist(Message.GetKey<T>());
		}

		protected void AddMessageTypeIdToWhitelist(string key)
		{
			if (!lockModeMessageWhitelist.Contains(key))
				lockModeMessageWhitelist.Add(key);
		}


		protected void ProcessException(Exception e)
		{
			this.exceptionHandlers.Invoke(e, this);
		}

		internal void ProcessOrphanedResponseMessage(Message m)
		{
			this.orphanedMessageHandlers.Invoke(m, this);
		}

		protected void ProcessMessage(Message m)
		{
			/* //TODO: if ((IsInLockMode) && (!lockModeMessageWhitelist.Contains(Message.GetKey(m))))
			{
				Log.Warn("None-whitelist message \"" + m.GetType().FullName + "\" recieved while in lock mode.");
				ProcessException(new LockModeWhitelistException());
				return;
			}*/

			this.messageHandlers.Invoke(m, this);
		}

		protected void ProcessAbortedConnection(ConnectionAbortedEventArgs args)
		{
			if (OnConnectionAborted == null) // No handler found for given Message?
				return; // Hell, this is shit but what to do?
			

			ConnectionAbortedHandlerDelegate[] delegates =
				OnConnectionAborted.GetInvocationList().Cast<ConnectionAbortedHandlerDelegate>().ToArray();
			var tasks = new Task[delegates.Length];
			for (int i = 0; i < delegates.Length; i++)
				tasks[i] = Task.Factory.FromAsync(delegates[i].BeginInvoke, delegates[i].EndInvoke, this, args, null);

			switch (this.CallbackWaitPolicy)
			{
				case WaitPolicy.WaitForAny:
					Task.WaitAny(tasks, this.CallbackTimeout);
					break;
				case WaitPolicy.WaitForAll:
					Task.WaitAll(tasks, this.CallbackTimeout);
					break;
			}
		}



		private UInt64 NextMessageId()
		{
			UInt64 ret = currMessageIdCounter;

			if (currMessageIdCounter < UInt64.MaxValue)
				currMessageIdCounter++;
			else
				currMessageIdCounter = 1;

			return ret;
		}


		private void TBuffers_Run()
		{
			Log.Debug("Buffer thread started...");

			while (true)
			{
				if (tBuffersShutdown.WaitOne(BuffersShutdownPollTimeout))
				{
					Log.Debug("Buffer thread terminated.");
					return;
				}

				if (!tBuffersActive.WaitOne(BuffersActivePollTimeout))
				{
					continue;
				}

				/*	if (!this.IsConnected) // This never reaches is socket is thought to be disconnected.
				{
					log.Debug("Connection aborted!");
					this.processAbortedConnection(AbortionCause.RemoteClosed);
					continue;
				}
				log.Debug("BBBBBAAAAAZZZZZZ!!");*/

			if (!this.IsConnected)
				{
					this.DisconnectLocalSocket(AbortionCause.RemoteClosed);
					continue;
				}


				Stream s = new NetworkStream(socket); // TODO: Use ISocket.GetStream method.


				/**
				 * Check and try to read incomming messages
				 */
				Message m = null;
				if (socket.Available > 0)
				{
					try
					{
						Log.Debug("Deserializing incomming message");
						//m = MessageSerializer.Deserialize(s);
					}
					catch (Exception e)
					{
						ProcessException(e);
					}
				}
				
				if (m != null)
				{
					/*//TODO: if (m.IsResponse)
					{
						Log.Debug("Enqueueing response message " + Message.GetKey(m) + ":" + m.Id + " to " +
								  m.ResponseToId);
						responseBuffer.GetOrAdd(m.ResponseToId, k => new ResponseQueue(this)).Enqueue(m);
						
					}
					else
					{
						Log.Debug("Processing non-response message " + Message.GetKey(m) + ":" + m.Id);
						ProcessMessage(m);
					}*/
				}

				/**
				 * Try write outgoing messages
				 */
					MessageDispatchHandle handle;
				if (this.outBuffer.TryDequeue(out handle))
				{
					m = handle.Message;
					//TODO: Log.Debug("Serializing outgoing message " + Message.GetKey(m) + ":" + m.Id +
					//TODO: (m.IsResponse ? ", response to " + m.ResponseToId : ""));

					//TODO:  MessageSerializer.Serialize(s, m);
					handle.MarkAsDispatched();
				}
			}
		}

// ReSharper disable once UnusedMember.Local
		private void TKeepAlive_Run()
		{
			while (true)
			{
				if (tBuffersShutdown.WaitOne(KeepAliveShutdownPollTimeout))
					return;

				if (!tBuffersActive.WaitOne(KeepAliveActivePollTimeout))
					continue;

				try
				{
					Communicate(new KeepAlive());
				}
				catch (TimeoutException)
				{
					this.DisconnectLocalSocket(AbortionCause.TimedOut);
				}
			}
		}

		#region Deprecated methods
		// ReSharper disable UnusedTypeParameter


		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		public void addMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		internal void addMessageHandlerFor(Type t, MessageHandlerDelegate handler)
		{
		}

		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		public void removeMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		internal void removeMessageHandlerFor(Type t, MessageHandlerDelegate handler)
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		public void addOrphanedMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		internal void addOrphanedMessageHandlerFor(Type t, MessageHandlerDelegate handler)
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		public void removeOrphanedMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		internal void removeOrphanedMessageHandlerFor(Type t, MessageHandlerDelegate handler)
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		public void addExceptionHandlerFor<T>(ExceptionHandlerDelegate handler) where T : Exception
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		internal void addExceptionHandlerFor(Type t, ExceptionHandlerDelegate handler)
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		public void removeExceptionHandlerFor<T>(ExceptionHandlerDelegate handler) where T : Exception
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		internal void removeExceptionHandlerFor(Type t, ExceptionHandlerDelegate handler)
		{
		}
		// ReSharper restore UnusedTypeParameter
		#endregion
	}
}