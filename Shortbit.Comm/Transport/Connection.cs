﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using Shortbit.Comm.CommLibMessages;
using Shortbit.Comm.Exceptions;
using Shortbit.Comm.LinkStateNetwork;
using Shortbit.Comm.Messages;
using Shortbit.Utils;
using Shortbit.Utils.AsyncCallbacks;
using Shortbit.Utils.IO;

namespace Shortbit.Comm.Transport
{
	public class Connection
	{
		private static readonly HashSet<string> maintenanceMessageWhitelist = new HashSet<string>
		{
			{ Ack.Descriptor.Key },
			{ ByeBye.Descriptor.Key },
			{ Heartbeat.Descriptor.Key },
			{ LinkStatePackage.Descriptor.Key }
		};

		// ReSharper disable once InconsistentNaming
		// This is also the sleep time that the buffer therad does between each iteration. Also determines the sleep time for each iteration
		private const int TBuffersShutdownPollTimeout = 10;

		// ReSharper disable once InconsistentNaming
		// This is the time the buffer thread waits until it restarts the while and checks tBuffers_Shutdown again
		private const int TBuffersActivePollTimeout = 1000;
		
		private readonly Socket socket;

		// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
		private readonly Thread tBuffers;
		private readonly ManualResetEvent tBuffersActive = new ManualResetEvent(true);
		private readonly ManualResetEvent tBuffersShutdown = new ManualResetEvent(false);

		public CommNode Node { get; }

		private ConnectionState state = ConnectionState.Idle;
		public ConnectionState State
		{
			get => this.state;
			private set
			{
				var previousState = this.state;
				this.state = value;
				this.OnStateChanged(new ConnectionStateChangedEventArgs(this, this.state, previousState));
			}
		}

		public bool IsConnected
		{
			get
			{
				if (this.State == ConnectionState.Idle || this.State == ConnectionState.Closing ||
				    this.State == ConnectionState.Closing)
					return false;

				try
				{
					if (socket.Poll(10, SelectMode.SelectRead) && socket.Available == 0)
						return false;

					if (!this.socket.Connected)
						return false;
				}
				catch (ObjectDisposedException)
				{
					return false;
				}

				return true;
			}
		}

		public event EventHandler<ConnectionStateChangedEventArgs> StateChanged;


		private readonly ConcurrentQueue<MessagePackageDispatchHandle> outBuffer = new ConcurrentQueue<MessagePackageDispatchHandle>();
		private readonly ConcurrentQueue<MessagePackageDispatchHandle> delayedBuffer = new ConcurrentQueue<MessagePackageDispatchHandle>();

		public Guid RemoteId { get; private set; }

		public IPEndPoint RemoteEndPoint => (IPEndPoint)this.socket.RemoteEndPoint;
		public IPEndPoint LocalEndPoint => (IPEndPoint) this.socket.LocalEndPoint;
		
		internal Connection(CommNode node, Socket socket)
		{
			this.Node = node;
			this.socket = socket;

			//	log.Debug("Initializing socket...");
			this.socket.ReceiveTimeout = this.Node.Timeout;
			this.socket.SendTimeout = this.Node.Timeout;
			this.socket.ReceiveBufferSize = Int32.MaxValue;
			this.socket.SendBufferSize = Int32.MaxValue;
			this.socket.Blocking = true;

			this.State = ConnectionState.Handshaking;
			
			this.tBuffers = new Thread(this.TBuffers_Run)
			{
				Name = $"Comm Buffer Thread {this.RemoteEndPoint}",
				IsBackground = true
			};
			this.tBuffers.Start();

			this.StateChanged += this.OnStateHandshakeDone;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return $"Connection {this.Node.NodeId.ToShortString()} -> {this.RemoteId.ToShortString()} [{this.State}]";
		}

		public void Shutdown()
		{
			this.HandleShutdown(ConnectionClosedCause.LocalClosed);
		}

		private void OnStateHandshakeDone(object sender, ConnectionStateChangedEventArgs e)
		{
			if (e.State != ConnectionState.HandshakeDone)
				return;
			this.State = ConnectionState.SettingUp;


			this.MoveDelayedBuffer();

			this.State = ConnectionState.Active;
			this.Node.SignalConnectionEstablished(this);
		}

		private void MoveDelayedBuffer()
		{
			while (!this.delayedBuffer.IsEmpty)
			{
				if (!this.delayedBuffer.TryDequeue(out var handle))
					continue;
				this.outBuffer.Enqueue(handle);
			}
		}

		protected virtual void OnStateChanged(ConnectionStateChangedEventArgs e)
		{
			if (this.StateChanged == null)
				return;

			// ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
			foreach (EventHandler<ConnectionStateChangedEventArgs> del in this.StateChanged?.GetInvocationList())
				del.BeginInvoke(this, e, result => del.EndInvoke(result), null);
		}

		public void SetTimeout(int timeout)
		{
			this.socket.ReceiveTimeout = timeout;
			this.socket.SendTimeout = timeout;
		}

		private void HandleShutdown(ConnectionClosedCause cause)
		{
			if (this.State == ConnectionState.Closed || this.State == ConnectionState.Closing)
				return;
			this.State = ConnectionState.Closing;
			this.tBuffersShutdown.Set();
			this.socket.Shutdown(SocketShutdown.Both);
			if (Thread.CurrentThread != this.tBuffers)
				this.tBuffers.Join();
			this.socket.Close();
			this.State = ConnectionState.Closed;

			this.Node.RemoveConnection(this, cause);
		}
		

		internal MessagePackageDispatchHandle Dispatch(MessagePackage package)
		{
			var descriptor = this.Node.Registry[package.Type];

			var handle = new MessagePackageDispatchHandle(package);

			if (this.State == ConnectionState.Active || Connection.maintenanceMessageWhitelist.Contains(descriptor.Key)) 
			{
				this.outBuffer.Enqueue(handle);
			}
			else
			{
				this.delayedBuffer.Enqueue(handle);
			}

			return handle;
		}

		private Stream GetStream()
		{
			/*return new TimeoutStream<NetworkStream>(new NetworkStream(this.socket), s => s.DataAvailable)
			{
				Timeout = this.Node.Timeout
			};*/
			return new NetworkStream(this.socket);
		}

		private void TBuffers_Run()
		{
			while (!this.tBuffersShutdown.WaitOne(Connection.TBuffersShutdownPollTimeout))
			{
				if (!this.tBuffersActive.WaitOne(Connection.TBuffersActivePollTimeout))
					continue; // If thread is suspended (tBuffersActive.WaitOne times out), restart while loop to make thread termination thru tBuffersShutdown possible

				if (!this.IsConnected)
					this.HandleShutdown(ConnectionClosedCause.RemoteClosed);

				switch (this.State)
				{
				case ConnectionState.Handshaking:
				case ConnectionState.HandshakeSend:
					this.HandshakeCommunication();
					break;

				case ConnectionState.HandshakeDone:
				case ConnectionState.SettingUp:
				case ConnectionState.Maintenance:
				case ConnectionState.Active:
					this.MessageCommunication();
					break;
				}
			}
		}

		private void HandshakeCommunication()
		{
			var stream = this.GetStream();

			if (this.State == ConnectionState.Handshaking)
			{
				var ownHandshake = new Handshake
				{
					Id = this.Node.NodeId
				};
				
				ownHandshake.WriteDelimitedTo(stream);
				this.State = ConnectionState.HandshakeSend;
			}

			if (this.socket.Available > 0)
			{
				try
				{
					var otherHandshake = Handshake.Parser.ParseDelimitedFrom(stream);
					this.RemoteId = otherHandshake.Id;
					this.State = ConnectionState.HandshakeDone;
				}
				catch (TimeoutException)
				{
				}
			}

		}
		private void MessageCommunication()
		{

			var stream = this.GetStream();
			/**
			 * Check and try to read incomming messages
			 */
			if (socket.Available > 0)
			{
				try
				{
					//Log.Debug("Deserializing incomming message");
					var package = MessagePackage.Deserialize(stream, this.Node.Registry);
					
					this.Node.ProcessPackage(package, this);
				}
				catch (Exception e)
				{
					this.Node.ProcessException(e);
				}
			}

			/**
			 * Try write outgoing messages
			 */
			if (this.outBuffer.TryDequeue(out var handle))
			{
				//TODO: Log.Debug("Serializing outgoing message " + Message.GetKey(m) + ":" + m.Id +
				//TODO: (m.IsResponse ? ", response to " + m.ResponseToId : ""));

				try
				{
					handle.Package.Serialize(stream);
				
					handle.MarkAsDispatched();
				}
				catch (Exception e)
				{
					this.Node.ProcessException(e);
				}
			}
		}

	}
}
