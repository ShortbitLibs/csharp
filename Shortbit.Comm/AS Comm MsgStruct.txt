
===================
 Message Structure
===================


Header
	Type
		Unique identifier for the payload structure.
		Fully-qualified class name, ProtoBuf fullname, message key (current implementation) or something
	
	Id	
		(origin-)unique identifier for the message.
		Probably just an auto-increment integer (like in the current implementation)
	Origin
		Id (Guid?)  of the client the message originated from.
	Recipients
		List of Client Ids the message is targeted for.
		Maybe empty for broadcasts

	Payload length
		Binary length (byte count) of the payload. 
		Needed 