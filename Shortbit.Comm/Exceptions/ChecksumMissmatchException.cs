﻿// /*
//  * ChecksumMissmatchException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class ChecksumMissmatchException : CommException
    {
        public ChecksumMissmatchException()
        {
        }

        public ChecksumMissmatchException(String msg)
            : base(msg)
        {
        }

        public ChecksumMissmatchException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public ChecksumMissmatchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}