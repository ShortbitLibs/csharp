﻿using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
	public abstract class CommException : Exception
	{
		public CommException()
		{
		}

		public CommException(String msg)
			: base(msg)
		{
		}

		public CommException(String msg, Exception inner)
			: base(msg, inner)
		{
		}

		public CommException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
