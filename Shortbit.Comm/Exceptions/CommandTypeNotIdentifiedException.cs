﻿// /*
//  * CommandTypeNotIdentifiedException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class CommandTypeNotIdentifiedException : CommException
    {
        public CommandTypeNotIdentifiedException()
        {
        }

        public CommandTypeNotIdentifiedException(String msg)
            : base(msg)
        {
        }

        public CommandTypeNotIdentifiedException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public CommandTypeNotIdentifiedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}