﻿using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
	public class MessageDisposedException : CommException
	{
		public MessageDisposedException()
		{
		}

		public MessageDisposedException(String msg)
			: base(msg)
		{
		}

		public MessageDisposedException(String msg, Exception inner)
			: base(msg, inner)
		{
		}

		public MessageDisposedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
