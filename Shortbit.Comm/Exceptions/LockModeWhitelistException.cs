﻿// /*
//  * LockModeWhitelistException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class LockModeWhitelistException : CommException
    {
        public LockModeWhitelistException()
        {
        }

        public LockModeWhitelistException(String msg)
            : base(msg)
        {
        }

        public LockModeWhitelistException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public LockModeWhitelistException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}