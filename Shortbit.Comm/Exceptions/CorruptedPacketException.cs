﻿// /*
//  * CorruptedPacketException.cs
//  *
//  *  Created on: 15:59
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Comm.Exceptions
{
    public class CorruptedPacketException : CommException
    {
        public CorruptedPacketException()
        {
        }

        public CorruptedPacketException(String msg)
            : base(msg)
        {
        }

        public CorruptedPacketException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public CorruptedPacketException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}