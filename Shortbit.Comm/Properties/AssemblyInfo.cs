// /*
//  * AssemblyInfo.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden 
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die mit einer Assembly verknüpft sind.

[assembly: AssemblyTitle("Shortbit.Comm")]
[assembly: AssemblyDescription("No Description given")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabian Schreier Homeproduction")]
[assembly: AssemblyProduct("Shortbit.Comm")]
[assembly: AssemblyCopyright("Copyright © Fabian Schreier 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf "false" werden die Typen in dieser Assembly unsichtbar 
// für COM-Komponenten. Wenn Sie auf einen Typ in dieser Assembly von 
// COM zugreifen müssen, legen Sie das ComVisible-Attribut für diesen Typ auf "true" fest.

[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird

[assembly: Guid("65f6e949-7480-4282-b4b2-64e90a6721e4")]
[assembly: InternalsVisibleTo("Communication.Test")]


// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
// übernehmen, indem Sie "*" eingeben:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
