﻿// /*
//  * CommListener.cs
//  *
//  *  Created on: 15:58
//  *		 Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using log4net;
using Shortbit.Comm.Messages;
using Shortbit.Utils;
using Shortbit.Utils.AsyncCallbacks;

namespace Shortbit.Comm
{
	public class CommListener : IDisposable, IEnumerable<CommClient>
	{
		// This is also the sleep time that the buffer therad does between each iteration
		private const int ClientsShutdownPollTimeout = 10; 
		private const int ClientsAcceptPollTimeout = 10;

		// This is the time the buffer thread waits until it restarts the while and checks TBuffers_Shutdown again
		private const int ClientsActivePollTimeout = 1000; 

		private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


		// Events for Handler


		// Threads and ThreadLists
		private readonly Thread tClients;
		private readonly ManualResetEvent tClientsActive;
		private readonly ManualResetEvent tClientsShutdown;

		// Event handlers
		private ConnectionAbortedHandlerDelegate onConnectionAborted;
		private int timeout;

		private readonly List<CommClient> clients;

		private readonly CallbackRegistry<Message, CommClient> messageHandlers;
		private readonly CallbackRegistry<Message, CommClient> orphanedMessageHandlers;
		private readonly CallbackRegistry<Exception, CommClient> exceptionHandlers;

		private readonly Lazy<RegistryWrapper<Message>> messageHandlersWrapper;
		private readonly Lazy<RegistryWrapper<Message>> orphanedMessageHandlersWrapper;
		private readonly Lazy<RegistryWrapper<Exception>> exceptionHandlersWrapper;

		private int callbackTimeout;
		private WaitPolicy waitPolicy;


		private readonly Socket socket;

		public CommListener(int port)
			: this(new IPEndPoint(IPAddress.Any, port))
		{
		}

		public CommListener(EndPoint ep)
		{
			timeout = 5000;

			socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			socket.Bind(ep);
			socket.Listen(10);

			socket.Blocking = false;

			tClients = new Thread(TClients_Run) {IsBackground = true, Name = "Comm ClientAccept Thread"};
			tClientsShutdown = new ManualResetEvent(false);
			tClientsActive = new ManualResetEvent(false);

			clients = new List<CommClient>();

			// Handlers
			messageHandlers = new CallbackRegistry<Message, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };
			orphanedMessageHandlers = new CallbackRegistry<Message, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };
			exceptionHandlers = new CallbackRegistry<Exception, CommClient> { DefaulExecuteConstraint = ExecuteConstraint.OnlyAsFirstPriority, WaitPolicy = this.waitPolicy, WaitTimeout = TimeSpan.FromMilliseconds(this.callbackTimeout) };

			messageHandlersWrapper = new Lazy<RegistryWrapper<Message>>(() => new RegistryWrapper<Message>(this.MessageRegistries));
			orphanedMessageHandlersWrapper = new Lazy<RegistryWrapper<Message>>(() => new RegistryWrapper<Message>(this.OrphanedMessageRegistries));
			exceptionHandlersWrapper = new Lazy<RegistryWrapper<Exception>>(() => new RegistryWrapper<Exception>(this.ExceptionRegistries));

			// Add internal handler...
			OnConnectionAborted += Client_ConnectionAborted;

			tClients.Start();
		}
		public void Dispose()
		{
			tClientsShutdown.Set();
			tClients.Join(Timeout);
			
		//	socket.Shutdown(SocketShutdown.Both);
		//	socket.Disconnect(true);
			socket.Close();

			lock (clients)
			{
				foreach (var client in this.clients)
					client.Dispose();
			}
		}

		public int Timeout
		{
			get { return timeout; }
			set
			{
				timeout = value;
				foreach (CommClient c in clients)
					c.Timeout = value;
			}
		}

		public int CallbackTimeout
		{
			get { return this.callbackTimeout; }
			set
			{
				this.callbackTimeout = value;
				this.messageHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
				this.orphanedMessageHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
				this.exceptionHandlers.WaitTimeout = TimeSpan.FromMilliseconds(value);
			}
		}

		public WaitPolicy CallbackWaitPolicy
		{
			get { return this.waitPolicy; }
			set
			{
				this.waitPolicy = value;
				this.messageHandlers.WaitPolicy = value;
				this.orphanedMessageHandlers.WaitPolicy = value;
				this.exceptionHandlers.WaitPolicy = value;
			}
		}

		public ICallbackRegistry<Message, CommClient> MessageHandlers { get { return this.messageHandlersWrapper.Value; } }
		public ICallbackRegistry<Message, CommClient> OrphanedMessageHandlers { get { return this.orphanedMessageHandlersWrapper.Value; } }
		public ICallbackRegistry<Exception, CommClient> ExceptionHandlers { get { return this.exceptionHandlersWrapper.Value; } } 


		public IEnumerator<CommClient> GetEnumerator()
		{
			return clients.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public event Callback<Message, CommClient> OnAnyMessage
		{
			add { this.MessageHandlers.Subscribe(value); }
			remove { this.MessageHandlers.Revoke(value); }
		}

		public event Callback<Message, CommClient> OnAnyOrphaneResponseMessage
		{
			add { this.OrphanedMessageHandlers.Subscribe(value); }
			remove { this.OrphanedMessageHandlers.Revoke(value); }
		}

		public event Callback<Exception, CommClient> OnAnyException
		{
			add { this.ExceptionHandlers.Subscribe(value); }
			remove { this.ExceptionHandlers.Revoke(value); }
		}

		public event ConnectionEstablishedHandlerDelegate OnConnectionEstablished;

		public event ConnectionAbortedHandlerDelegate OnConnectionAborted
		{
			add
			{
				onConnectionAborted += value;
				foreach (CommClient c in clients)
					c.OnConnectionAborted += value;
			}
			remove
			{
				onConnectionAborted -= value;
				foreach (CommClient c in clients)
					c.OnConnectionAborted -= value;
			}
		}



		public void Start()
		{
			this.tClientsActive.Set();
		}

		public void Stop()
		{
			this.tClientsActive.Reset();
		}

		public void DisconnectAll()
		{
			List<CommClient> cs = null;
			lock (this.clients)
			{
				cs = new List<CommClient>(this.clients);
			}
			foreach (var client in cs)
				client.Disconnect();
		}

		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		public void addMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the MessageHandlers Property instead.", true)]
		public void removeMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		public void addOrphanedMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the OrphanedMessageHandlers Property instead.", true)]
		public void removeOrphanedMessageHandlerFor<T>(MessageHandlerDelegate handler) where T : Message
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		public void addExceptionHandlerFor<T>(ExceptionHandlerDelegate handler) where T : Exception
		{
		}

		[Obsolete("This method is deprecated. Please use the ExceptionHandlers Property instead.", true)]
		public void removeExceptionHandlerFor<T>(ExceptionHandlerDelegate handler) where T : Exception
		{
		}


		protected void ProcessConnectionEstablished(CommClient c)
		{
			if (OnConnectionEstablished == null) // No handler found for given Message?
				return; // Hell, this is shit but what to do?

			var results = new List<AsyncResult>();
			foreach (var @delegate in OnConnectionEstablished.GetInvocationList())
			{
				var h = (ConnectionEstablishedHandlerDelegate) @delegate;
				results.Add( (AsyncResult)h.BeginInvoke(c, null, null));
			}

			foreach (AsyncResult result in results)
			{
				var h = (ConnectionEstablishedHandlerDelegate) result.AsyncDelegate;
				h.EndInvoke(result);
			}
		}


		private void TClients_Run()
		{

			IAsyncResult result = null;

			while (true)
			{
				if (tClientsShutdown.WaitOne(ClientsShutdownPollTimeout))
					break;

				if (!tClientsActive.WaitOne(ClientsActivePollTimeout))
					continue;
						// If thread is suspended (TBuffers_Active.WaitOne times out), restart while loop to make thread termination thru TBuffers_Shutdown possible

				//	if (!socket.IsConnected)
				//		continue;

				if (result == null)
				{
					lock (socket)
					{
						result = socket.BeginAccept(null, null);
					}
				}
				if (!result.AsyncWaitHandle.WaitOne(ClientsAcceptPollTimeout))
					continue;
				
				if (tClientsShutdown.IsSet())
					break;

				Socket newSocket = EndAsyncAccept(result);
				result = null;

				if (newSocket == null)
					Log.Warn("Listen socket closed without setting tClientsShutdown WaitHandle!");

				var client = new CommClient(newSocket) { Timeout = Timeout };

				// Add all extisting event handlers
				client.SubscribeAll(this.messageHandlers, this.orphanedMessageHandlers, this.exceptionHandlers);

				foreach (var del in onConnectionAborted.GetInvocationList().Cast<ConnectionAbortedHandlerDelegate>())
				{
					client.OnConnectionAborted += del;
				}
				lock (clients)
				{
					clients.Add(client);
				}

				ProcessConnectionEstablished(client);
				
			}
			if (result != null)
				EndAsyncAccept(result);
		}

		private Socket EndAsyncAccept(IAsyncResult result)
		{
			lock (this.socket)
			{
				try
				{
					return this.socket.EndAccept(result);
				}
				catch (ObjectDisposedException)
				{
				}
			}
			return null;
		}

		private void Client_ConnectionAborted(CommClient sender, ConnectionAbortedEventArgs args)
		{
			lock (clients)
			{
				sender.Dispose();
				clients.Remove(sender);
			}
		}


		private IEnumerable<ICallbackRegistry<Message, CommClient>> MessageRegistries()
		{
			yield return this.messageHandlers;
			foreach (var client in this.clients)
			{
				yield return client.MessageHandlers;
			}
		}
		private IEnumerable<ICallbackRegistry<Message, CommClient>> OrphanedMessageRegistries()
		{
			yield return this.orphanedMessageHandlers;
			foreach (var client in this.clients)
			{
				yield return client.OrphanedMessageHandlers;
			}
		}
		private IEnumerable<ICallbackRegistry<Exception, CommClient>> ExceptionRegistries()
		{
			yield return this.exceptionHandlers;
			foreach (var client in this.clients)
			{
				yield return client.ExceptionHandlers;
			}
		}

		private class RegistryWrapper<T> : ICallbackRegistry<T, CommClient>
		{
			private readonly Func<IEnumerable<ICallbackRegistry<T, CommClient>>> registrySelector;

			public RegistryWrapper(Func<IEnumerable<ICallbackRegistry<T, CommClient>>> registrySelector)
			{
				this.registrySelector = registrySelector;
			}


			public void Subscribe<A>(Callback<A, CommClient> callback) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Subscribe(callback);
				}
			}

			public void Subscribe<A>(Callback<A, CommClient> callback, Priority priority) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Subscribe(callback, priority);
				}
			}

			public void Subscribe<A>(Callback<A, CommClient> callback, ExecuteConstraint constraint) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Subscribe(callback, constraint);
				}
			}

			public void Subscribe<A>(Callback<A, CommClient> callback, Priority priority, ExecuteConstraint constraint) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Subscribe(callback, priority, constraint);
				}
			}

			public void Revoke<A>(Callback<A, CommClient> callback) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Revoke(callback);
				}
			}

			public void Revoke<A>(Callback<A, CommClient> callback, Priority priority) where A : T
			{
				foreach (var registry in this.registrySelector())
				{
					registry.Revoke(callback, priority);
				}
			}
		}
	}
}