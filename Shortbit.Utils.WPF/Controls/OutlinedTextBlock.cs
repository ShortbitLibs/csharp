﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace Shortbit.Utils.WPF.Controls
{
	[ContentProperty("Text")]
	public class OutlinedTextBlock : FrameworkElement
	{
		private void UpdatePen() {
			this._Pen = new Pen(this.Stroke, this.StrokeThickness) {
				DashCap = PenLineCap.Round,
				EndLineCap = PenLineCap.Round,
				LineJoin = PenLineJoin.Round,
				StartLineCap = PenLineCap.Round
			};

			this.InvalidateVisual();
		}

		public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
			"Fill",
			typeof(Brush),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender));

		public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
			"Stroke",
			typeof(Brush),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender, OutlinedTextBlock.StrokePropertyChangedCallback));

		private static void StrokePropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs) {
			(dependencyObject as OutlinedTextBlock)?.UpdatePen();
		}

		public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
			"StrokeThickness",
			typeof(double),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsRender, OutlinedTextBlock.StrokePropertyChangedCallback));

		public static readonly DependencyProperty FontFamilyProperty = TextElement.FontFamilyProperty.AddOwner(
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty FontSizeProperty = TextElement.FontSizeProperty.AddOwner(
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty FontStretchProperty = TextElement.FontStretchProperty.AddOwner(
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty FontStyleProperty = TextElement.FontStyleProperty.AddOwner(
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty FontWeightProperty = TextElement.FontWeightProperty.AddOwner(
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
			"Text",
			typeof(string),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextInvalidated));

		public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register(
			"TextAlignment",
			typeof(TextAlignment),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty TextDecorationsProperty = DependencyProperty.Register(
			"TextDecorations",
			typeof(TextDecorationCollection),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty TextTrimmingProperty = DependencyProperty.Register(
			"TextTrimming",
			typeof(TextTrimming),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(OutlinedTextBlock.OnFormattedTextUpdated));

		public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register(
			"TextWrapping",
			typeof(TextWrapping),
			typeof(OutlinedTextBlock),
			new FrameworkPropertyMetadata(TextWrapping.NoWrap, OutlinedTextBlock.OnFormattedTextUpdated));

		private FormattedText _FormattedText;
		private Geometry _TextGeometry;
		private Pen _Pen;

		public Brush Fill
		{
			get { return (Brush)this.GetValue(OutlinedTextBlock.FillProperty); }
			set { this.SetValue(OutlinedTextBlock.FillProperty, value); }
		}

		public FontFamily FontFamily
		{
			get { return (FontFamily)this.GetValue(OutlinedTextBlock.FontFamilyProperty); }
			set { this.SetValue(OutlinedTextBlock.FontFamilyProperty, value); }
		}

		[TypeConverter(typeof(FontSizeConverter))]
		public double FontSize
		{
			get { return (double)this.GetValue(OutlinedTextBlock.FontSizeProperty); }
			set { this.SetValue(OutlinedTextBlock.FontSizeProperty, value); }
		}

		public FontStretch FontStretch
		{
			get { return (FontStretch)this.GetValue(OutlinedTextBlock.FontStretchProperty); }
			set { this.SetValue(OutlinedTextBlock.FontStretchProperty, value); }
		}

		public FontStyle FontStyle
		{
			get { return (FontStyle)this.GetValue(OutlinedTextBlock.FontStyleProperty); }
			set { this.SetValue(OutlinedTextBlock.FontStyleProperty, value); }
		}

		public FontWeight FontWeight
		{
			get { return (FontWeight)this.GetValue(OutlinedTextBlock.FontWeightProperty); }
			set { this.SetValue(OutlinedTextBlock.FontWeightProperty, value); }
		}

		public Brush Stroke
		{
			get { return (Brush)this.GetValue(OutlinedTextBlock.StrokeProperty); }
			set { this.SetValue(OutlinedTextBlock.StrokeProperty, value); }
		}

		public double StrokeThickness
		{
			get { return (double)this.GetValue(OutlinedTextBlock.StrokeThicknessProperty); }
			set { this.SetValue(OutlinedTextBlock.StrokeThicknessProperty, value); }
		}

		public string Text
		{
			get { return (string)this.GetValue(OutlinedTextBlock.TextProperty); }
			set { this.SetValue(OutlinedTextBlock.TextProperty, value); }
		}

		public TextAlignment TextAlignment
		{
			get { return (TextAlignment)this.GetValue(OutlinedTextBlock.TextAlignmentProperty); }
			set { this.SetValue(OutlinedTextBlock.TextAlignmentProperty, value); }
		}

		public TextDecorationCollection TextDecorations
		{
			get { return (TextDecorationCollection)this.GetValue(OutlinedTextBlock.TextDecorationsProperty); }
			set { this.SetValue(OutlinedTextBlock.TextDecorationsProperty, value); }
		}

		public TextTrimming TextTrimming
		{
			get { return (TextTrimming)this.GetValue(OutlinedTextBlock.TextTrimmingProperty); }
			set { this.SetValue(OutlinedTextBlock.TextTrimmingProperty, value); }
		}

		public TextWrapping TextWrapping
		{
			get { return (TextWrapping)this.GetValue(OutlinedTextBlock.TextWrappingProperty); }
			set { this.SetValue(OutlinedTextBlock.TextWrappingProperty, value); }
		}

		public OutlinedTextBlock() {
			this.UpdatePen();
			this.TextDecorations = new TextDecorationCollection();
		}

		protected override void OnRender(DrawingContext drawingContext) {
			this.EnsureGeometry();

			drawingContext.DrawGeometry(null, this._Pen, this._TextGeometry);
			drawingContext.DrawGeometry(this.Fill, null, this._TextGeometry);
		}

		protected override Size MeasureOverride(Size availableSize) {
			this.EnsureFormattedText();

			// constrain the formatted text according to the available size

			double w = availableSize.Width;
			double h = availableSize.Height;

			// the Math.Min call is important - without this constraint (which seems arbitrary, but is the maximum allowable text width), things blow up when availableSize is infinite in both directions
			// the Math.Max call is to ensure we don't hit zero, which will cause MaxTextHeight to throw
			this._FormattedText.MaxTextWidth = Math.Min(3579139, w);
			this._FormattedText.MaxTextHeight = Math.Max(0.0001d, h);

			// return the desired size
			return new Size(Math.Ceiling(this._FormattedText.Width), Math.Ceiling(this._FormattedText.Height));
		}

		protected override Size ArrangeOverride(Size finalSize) {
			this.EnsureFormattedText();

			// update the formatted text with the final size
			this._FormattedText.MaxTextWidth = finalSize.Width;
			this._FormattedText.MaxTextHeight = Math.Max(0.0001d, finalSize.Height);

			// need to re-generate the geometry now that the dimensions have changed
			this._TextGeometry = null;

			return finalSize;
		}

		private static void OnFormattedTextInvalidated(DependencyObject dependencyObject,
													   DependencyPropertyChangedEventArgs e) {
			var outlinedTextBlock = (OutlinedTextBlock)dependencyObject;
			outlinedTextBlock._FormattedText = null;
			outlinedTextBlock._TextGeometry = null;

			outlinedTextBlock.InvalidateMeasure();
			outlinedTextBlock.InvalidateVisual();
		}

		private static void OnFormattedTextUpdated(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e) {
			var outlinedTextBlock = (OutlinedTextBlock)dependencyObject;
			outlinedTextBlock.UpdateFormattedText();
			outlinedTextBlock._TextGeometry = null;

			outlinedTextBlock.InvalidateMeasure();
			outlinedTextBlock.InvalidateVisual();
		}

		private void EnsureFormattedText() {
			if (this._FormattedText != null) {
				return;
			}

			this._FormattedText = new FormattedText(
				this.Text ?? "",
				CultureInfo.CurrentUICulture,
				this.FlowDirection,
				new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch),
				this.FontSize,
				Brushes.Black);

			this.UpdateFormattedText();
		}

		private void UpdateFormattedText() {
			if (this._FormattedText == null) {
				return;
			}

			this._FormattedText.MaxLineCount = this.TextWrapping == TextWrapping.NoWrap ? 1 : int.MaxValue;
			this._FormattedText.TextAlignment = this.TextAlignment;
			this._FormattedText.Trimming = this.TextTrimming;

			this._FormattedText.SetFontSize(this.FontSize);
			this._FormattedText.SetFontStyle(this.FontStyle);
			this._FormattedText.SetFontWeight(this.FontWeight);
			this._FormattedText.SetFontFamily(this.FontFamily);
			this._FormattedText.SetFontStretch(this.FontStretch);
			this._FormattedText.SetTextDecorations(this.TextDecorations);
		}

		private void EnsureGeometry() {
			if (this._TextGeometry != null) {
				return;
			}

			this.EnsureFormattedText();
			this._TextGeometry = this._FormattedText.BuildGeometry(new Point(0, 0));
		}
	}
}