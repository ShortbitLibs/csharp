﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xaml;
using Shortbit.Utils.Properties;

namespace Shortbit.Utils.WPF
{
	class WPFPropertyConverter
	{
		public static void Register()
		{
			PropertyConverter.AddInstanceTypeSelector(DestinationTypeProvider);
			PropertyConverter.AddInstanceTypeSelector(ProvideValueTarget);
		}

		public static Type DestinationTypeProvider(ITypeDescriptorContext context)
		{
			var destinationProvider =
				(IDestinationTypeProvider)context.GetService(typeof(IDestinationTypeProvider));
			if (destinationProvider != null)
			{
				return destinationProvider.GetDestinationType();
			}
			return null;
		}

		public static Type ProvideValueTarget(ITypeDescriptorContext context)
		{
			var valueTarget =
				(IProvideValueTarget)context.GetService(typeof(IProvideValueTarget));
			if (valueTarget != null)
			{
				if (valueTarget.TargetProperty != null)
					return valueTarget.TargetProperty.GetType();
				if (valueTarget.TargetObject != null)
					return valueTarget.TargetObject.GetType();
			}
			return null;
		}

	}
}
