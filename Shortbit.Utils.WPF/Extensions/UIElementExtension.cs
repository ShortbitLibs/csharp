﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Shortbit.Utils.WPF
{
	public static class UIElementExtension
	{
		private static readonly Action EmptyDelegate = delegate { };

		public static void Refresh(this UIElement uiElement)
		{
			uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
		}
	}
}
