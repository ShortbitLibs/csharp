﻿using System;
using System.Windows;

namespace Shortbit.Utils.WPF
{
	public static class DependencyPropertyEx
	{
		public static DependencyProperty Register<OT, VT>(string name) where OT : DependencyObject
		{
			return DependencyProperty.Register(name, typeof(VT), typeof(OT));
		}
		public static DependencyProperty Register<OT, VT>(string name, Action<OT, VT> setter) where OT : DependencyObject
		{
			return DependencyProperty.Register(name, typeof(VT), typeof(OT), new PropertyMetadata(default(VT), Setter(setter)));
		}
		public static DependencyProperty Register<OT, VT>(string name, VT defaultValue, Action<OT, VT> setter) where OT : DependencyObject
		{
			return DependencyProperty.Register(name, typeof(VT), typeof(OT), new PropertyMetadata(defaultValue, Setter(setter)));
		}
		public static DependencyProperty Register<OT, VT>(string name, PropertyMetadata meta) where OT : DependencyObject
		{
			return DependencyProperty.Register(name, typeof(VT), typeof(OT), meta);
		}
		public static DependencyProperty Register<OT, VT>(string name, Action<OT, VT> setter, PropertyMetadata meta) where OT : DependencyObject
		{
			meta.PropertyChangedCallback = Setter(setter);
			return DependencyProperty.Register(name, typeof(VT), typeof(OT), meta);
		}

		public static PropertyChangedCallback Setter<OT, VT>(Action<OT, VT> setter) where OT : DependencyObject
		{
			return (d, e) => setter((OT)d, (VT)(e.NewValue));
		}
	}
}
