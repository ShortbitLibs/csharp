import de.shortbit.comm.CommClient;
import de.shortbit.comm.Message;
import de.shortbit.comm.MessageKey;
import de.shortbit.comm.MessageRegistry;
import de.shortbit.comm.listener.ConnectionAbortedListener;
import de.shortbit.utils.callbacks.IContextCallback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;

/**
 * Created by Fabian Schreier on 11.05.2015.
 */
public class Test
{
	private static final Logger log = LogManager.getLogger();

	public static void main(String[] args) throws Exception
	{
		URL resource = Thread.currentThread().getContextClassLoader()
				.getResource("log4j2.xml");


		new Test();
	}

	@MessageKey("Test_TestMsg")
	public static class TestMsg extends Message
	{
		public String getTest()
		{
			return test;
		}

		public void setTest(String test)
		{
			this.test = test;
		}

		private String test;

		public TestMsg(DataInput inputStream) throws IOException
		{
			super(inputStream);

			this.test = inputStream.readUTF();
		}

		public TestMsg(String test)
		{
			this(NO_ID, test);
		}


		public TestMsg(long responseToId, String test)
		{
			super(responseToId);
			this.test = test;
		}


		@Override
		protected void serialize(DataOutput outputStream)
		{
			try
			{
				outputStream.writeUTF(this.test);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}


	public Test () throws Exception
	{
		log.info("Creating classes");
		MessageRegistry.registerMessage(TestMsg.class);

		CommClient client = new CommClient();

		client.onAnyException().subscribe(new IContextCallback<Exception, CommClient>()
		{
			@Override
			public void call(Exception arg, CommClient context)
			{
				arg.printStackTrace();
			}
		});

		client.messageListeners().subscribe(TestMsg.class, new IContextCallback<TestMsg, CommClient>()
		{
			@Override
			public void call(TestMsg arg, CommClient context)
			{
				log.info("Message from server: "+arg.getTest());
			}
		});
		client.onConnectionAborted().subscribe(new ConnectionAbortedListener(){

			@Override
			public void onConnectionAborted(CommClient client, EventArgs args)
			{
				log.info("client disconnect: " + args.getRemoteAddress() + ", " + args.getCause());
			}
		});

		log.info(">>done");
		System.in.read();

		log.info("Connecting...");
		client.connect("localhost", 8123);

		log.info(">>done");
		System.in.read();

		if (client.isConnected())
		{
			log.info("Sending test message");

			client.dispatch(new TestMsg("Hello World from java client!"));

			log.info(">>done");
			System.in.read();
		}

		log.info("Disconnecting");
		client.disconnect();
		client.dispose();
		log.info(">>done");
	}
}
