﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Configuration;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Shortbit.Algorithms.CSP;
using Shortbit.Comm;
using Shortbit.Comm.CommLibMessages;
using Shortbit.Comm.LinkStateNetwork;
using Shortbit.Comm.Messages;
using Shortbit.Comm.Transport;
using Shortbit.Data.NBT;
using Shortbit.Data.Serialization;
using Shortbit.Logging;
using Shortbit.Utils;
namespace Test
{

	class Program
	{
		public static void Main(string[] args)
		{
			new Program();
		}

		private static readonly log4net.ILog log =
			log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private CommNode node1;
		private CommNode node2;
		private CommNode node3;
		private CommNode node4;
		private CommNode node5;
		private CommNode node6;
		private DateTime start;
		private DateTime last;
		public Program()
		{

			node1 = new CommNode(1234, nodeId: Guid.Parse("6db63484-c64c-4d88-b0c1-3175cc90c635"));
			node2 = new CommNode(1235, nodeId: Guid.Parse("c1fdb97a-86b3-494e-8b9c-95ea23677478"));
			node3 = new CommNode(1236, nodeId: Guid.Parse("5a7def21-3fd4-44cb-b357-86e3dfa0f688"));
			node4 = new CommNode(1237, nodeId: Guid.Parse("b317bc5f-bbff-4819-be02-8fa097a958c4"));
			node5 = new CommNode(1238, nodeId: Guid.Parse("fc9ed09b-7a28-46e9-b670-29f6d68788c2"));
			node6 = new CommNode(1239, nodeId: Guid.Parse("4a6f5b05-ff76-45d4-8225-0c11cae6c0c4"));

			this.node1.NodesAdded += Node1OnNodesAdded;
			this.node1.NodesRemoved += this.Node1OnNodesRemoved;
			

			this.start = DateTime.Now;

			node1.ConnectTo(IPAddress.Loopback, this.node2.ListenEndPoint.Port);
			node3.ConnectTo(IPAddress.Loopback, this.node2.ListenEndPoint.Port);
			node4.ConnectTo(IPAddress.Loopback, this.node3.ListenEndPoint.Port);
			
			node5.ConnectTo(IPAddress.Loopback, this.node3.ListenEndPoint.Port);
			node5.ConnectTo(IPAddress.Loopback, this.node1.ListenEndPoint.Port);
			node6.ConnectTo(IPAddress.Loopback, this.node3.ListenEndPoint.Port);

			
			Console.ReadKey();

			node6.Shutdown();
			node5.Shutdown();
			Thread.Sleep(2000);
			
			


			//return;

			var n4ExpectTask = this.node4.Expect().OneMessage().OfType<Ack>().From(this.node1.NodeId).Async();
			n4ExpectTask.ContinueWith((task, state) =>
			{
				if (!task.IsCompleted)
					return;
				var msg = task.Result;

				var r = RecipientsList.FromMessage(msg);
				Console.WriteLine($"{this.node4.NodeId.ToShortString()} received EXPECTED Ack from {msg.Header.Origin.ToGuid().ToShortString()}. Broadcast: {r.IsBroadcast}, Recipients: {string.Join(", ", r.Select(id => id.ToShortString()))}");

				this.node4.Respond(msg, new ByeBye(), true);

			}, CancellationToken.None);

			Message foo = new Ack();
			var recipients = new RecipientsList {this.node4.NodeId, this.node3.NodeId, this.node6.NodeId};


			this.node1.Dispatch(foo, recipients);

			Console.WriteLine("Dispatched message. Expecting response");

			var responses = this.node1.Expect().OneMessage().OfType<KeepAlive, ByeBye>().From(recipients).AsResponseTo(foo)
				.CompletedWhen(c => c.ExpectedOrigins.SetEquals(c.ReceivedOrigins)).AndWait();
			Console.WriteLine($"{this.node1.NodeId.ToShortString()} received all expected response messages");

			foreach (var pair in responses)
			{
				Console.WriteLine($"  Recieved {pair.Value.GetType().Name} from {pair.Key.ToShortString()}");
			}

			var asudh = new object();
			asudh.GetHashCode();

			Console.ReadKey();
		}

		private void Node1OnNodesAdded(object sender, NodesAddedEventArgs e)
		{
			Console.WriteLine($"Nodes {string.Join(", ", e.Nodes.Select(n => n.ToShortString()))} added on Node1");
		}
		private void Node1OnNodesRemoved(object sender, NodesRemovedEventArgs e)
		{
			Console.WriteLine($"Nodes {string.Join(", ", e.Nodes.Select(n => n.ToShortString()))} removed on Node1");
		}

		private void LSNCallback(LinkStatePackage arg, Connection context)
		{
			Console.WriteLine($"Received LSN package from {arg.Origin.ToGuid().ToShortString()} with sequence {arg.Sequence}");
			this.last = DateTime.Now;
		}

		private void Callback(Ack arg, Connection context)
		{
			var recipients = RecipientsList.FromMessage(arg);
			Console.WriteLine($"{context.Node.NodeId.ToShortString()} received Ack from {arg.Header.Origin.ToGuid().ToShortString()}. Broadcast: {recipients.IsBroadcast}, Recipients: {string.Join(", ", recipients.Select(id => id.ToShortString()))}");
			context.Node.Respond(arg, new KeepAlive(), true);
		}


		private void OnConnectionEstablished(object sender, Connection e)
		{
			Console.WriteLine($"Node {e.Node.NodeId.ToShortString()} connection established from {e.RemoteId.ToShortString()}");
		}
	}
}