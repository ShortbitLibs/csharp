﻿using System;
using System.Collections.Generic;
using System.IO;
//using NUnit.Mocks;


namespace Shortbit.Comm.Test
{


	[TestFixture]
	public class BaseClientTest
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		static BaseClientTest()
		{
			XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("testConfig.log4net"));
		}

		/*
		 * ToDo: Tests:
		 *  - System command reactions
		 *  - Data packet generation
		 *  - Background Worker thread activity
		 */

		TestClient Client;
		Mock<ISocket> SocketMock;
		Mock<ITcpClient> TcpClientMock;
		Mock<BaseClient_Old> ClientMock;

		MemoryStream NetStream;

		[SetUp]
		public void init()
		{
			NetStream = new MemoryStream();
			SocketMock = new Mock<ISocket>();

			TcpClientMock = new Mock<ITcpClient>();
			TcpClientMock.Setup (tcp => tcp.Client).Returns(SocketMock.Object);

			ClientMock = new Mock<BaseClient_Old>();

			Client = new TestClient(TcpClientMock.Object);
		}
		

		[Test]
		public void Transmit()
		{
			TestDataPacket pckt = new TestDataPacket(false, 1, SystemCommand.Ack, null);

			List<Byte> result = new List<byte>();

			SocketMock.Setup(skt => skt.Send(It.IsAny<byte[]>()))
				.Callback<byte[]>(bs => result.AddRange(bs));

			Client.TestTransmit(pckt);

			String hex = "";
			foreach (byte b in result) hex += Convert.ToString(b, 16);
			log.Debug(hex + ";");

		}

	}
}
