﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Shortbit.Utils
{
	public static class SecureStringExtension
	{

		public static unsafe void CosumeString(this SecureString self, Action<string> action)
		{
			int length = self.Length;
			var insecurePassword = new string('\0', length);

			var gch = new GCHandle();
			RuntimeHelpers.ExecuteCodeWithGuaranteedCleanup(
				delegate
				{
					RuntimeHelpers.PrepareConstrainedRegions();
					try
					{
					}
					finally
					{
						gch = GCHandle.Alloc(insecurePassword, GCHandleType.Pinned);
					}

					IntPtr passwordPtr = IntPtr.Zero;
					RuntimeHelpers.ExecuteCodeWithGuaranteedCleanup(
						delegate
						{
							RuntimeHelpers.PrepareConstrainedRegions();
							try
							{
							}
							finally
							{
								passwordPtr = Marshal.SecureStringToBSTR(self);
							}
							var pPassword = (char*) passwordPtr;
							var pInsecurePassword = (char*) gch.AddrOfPinnedObject();
							for (int index = 0; index < length; index++)
							{
								pInsecurePassword[index] = pPassword[index];
							}
						},
						delegate
						{
							if (passwordPtr != IntPtr.Zero)
							{
								Marshal.ZeroFreeBSTR(passwordPtr);
							}
						},
						null);

					// Use the password.
					action(insecurePassword);
				},
				delegate
				{
					if (gch.IsAllocated)
					{
						// Zero the string.
						var pInsecurePassword = (char*) gch.AddrOfPinnedObject();
						for (int index = 0; index < length; index++)
						{
							pInsecurePassword[index] = '\0';
						}
						gch.Free();
					}
				},
				null);
		}

	
	}
}