﻿// /*
//  * NotSupportedTypeException.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Runtime.Serialization;

namespace Shortbit.Data.Exceptions
{
    public class NotSupportedTypeException : Exception
    {
        public NotSupportedTypeException()
        {
        }

        public NotSupportedTypeException(String msg)
            : base(msg)
        {
        }

        public NotSupportedTypeException(String msg, Exception inner)
            : base(msg, inner)
        {
        }

        public NotSupportedTypeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}