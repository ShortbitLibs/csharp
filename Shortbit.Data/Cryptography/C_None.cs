﻿// /*
//  * C_None.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;

namespace Shortbit.Data.Cryptography
{
    public class C_None : C_Base
    {
        public Byte[] Key
        {
            get { return new Byte[0]; }
            set { }
        }

        public Byte[] Encrypt(Byte[] Data)
        {
            return Data;
        }

        public Byte[] Decrypt(Byte[] Data)
        {
            return Data;
        }

        public C_Base Clone()
        {
            C_Base r = new C_None();
            r.Key = Key;
            return r;
        }

        public void GenerateNewKey()
        {
        }
    }
}