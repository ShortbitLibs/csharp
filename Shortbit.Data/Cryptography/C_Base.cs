﻿// /*
//  * C_Base.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;

namespace Shortbit.Data.Cryptography
{
    public interface C_Base
    {
        Byte[] Key { get; set; }

        Byte[] Encrypt(Byte[] Data);
        Byte[] Decrypt(Byte[] Data);
        C_Base Clone();
        void GenerateNewKey();
    }
}