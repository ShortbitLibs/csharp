﻿// /*
//  * NBTList.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Shortbit.Data.Serialization;
using Shortbit.Utils;

namespace Shortbit.Data.NBT
{

    public class NBTList : NBTTag, IList<NBTTag>
    {
        private readonly List<NBTTag> tags;

        public NBTList(NBTType contentType)
        {
            tags = new List<NBTTag>();
            this.ContentType = contentType;
        }

        public NBTList(NBTType contentType, IEnumerable<NBTTag> items)
        {
            tags = new List<NBTTag>();
            this.ContentType = contentType;
            AddRange(items);
        }

        public NBTList(SerializationReader reader)
        {
            tags = new List<NBTTag>();

            ContentType = (NBTType) reader.ReadInt32();
            Int32 num = reader.ReadInt32();
            for (int i = 0; i < num; i++)
                Add(Deserialize(ContentType, reader));
        }

        public NBTList(XmlElement element)
        {
            tags = new List<NBTTag>();

            ContentType = (NBTType) Enum.Parse(typeof (NBTType), element.GetAttribute("contentType"));
            IEnumerable<XmlElement> children = element.ChildElements();
            //log.Debug("Decomposing NBTList with " + children.Count() + " entries");
            foreach (XmlElement entry in children)
            {
                Add(Deserialize(entry));
            }
        }

        public NBTType ContentType { get; private set; }

        public override object GetValue()
        {
            var ret = new Object[tags.Count];
            for (int i = 0; i < ret.Length; i++)
                ret[i] = tags[i].GetValue();
            return ret;
        }

        public override void Serialize(SerializationWriter writer)
        {
            writer.Write((Int32) ContentType);
            writer.Write(tags.Count);
            foreach (NBTTag tag in tags)
                tag.Serialize(writer);
        }

        public override XmlElement Serialize(XmlDocument doc)
        {
            XmlElement eBase = doc.CreateElement(Type.ToString());
            eBase.SetAttribute("contentType", ContentType.ToString());

            foreach (NBTTag child in this)
            {
                eBase.AppendChild(child.Serialize(doc));
            }

            return eBase;
        }
		

        public override string ToString()
        {
            return "NBT[" + String.Join(", ", this.Select(x => x.ToString())) + "]";
        }

        public static implicit operator NBTList(NBTTag[] array)
        {
            return new NBTList(GetNBTType(array.GetType().GetElementType()), array);
        }

        public static implicit operator NBTTag[](NBTList value)
        {
            return value.tags.ToArray();
        }

        #region IList Implementation

        public IEnumerator<NBTTag> GetEnumerator()
        {
            return tags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(NBTTag item)
        {
            if (item.Type != ContentType)
                throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " + ContentType +
                                            ", Tag type: " + item.Type);

            tags.Add(item);
        }

        public int IndexOf(NBTTag item)
        {
            if (item.Type != ContentType)
                throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " + ContentType +
                                            ", Tag type: " + item.Type);

            return tags.IndexOf(item);
        }

        public void Insert(int index, NBTTag item)
        {
            if (item.Type != ContentType)
                throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " + ContentType +
                                            ", Tag type: " + item.Type);

            tags.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            tags.RemoveAt(index);
        }

        public NBTTag this[int index]
        {
            get { return tags[index]; }
            set
            {
                if (value.Type != ContentType)
                    throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " +
                                                ContentType + ", Tag type: " + value.Type);

                tags[index] = value;
            }
        }


        public void Clear()
        {
            tags.Clear();
        }

        public bool Contains(NBTTag item)
        {
            if (item.Type != ContentType)
                throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " + ContentType +
                                            ", Tag type: " + item.Type);

            return tags.Contains(item);
        }

        public void CopyTo(NBTTag[] array, int arrayIndex)
        {
            tags.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return tags.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(NBTTag item)
        {
            if (item.Type != ContentType)
                throw new ArgumentException("Given tag does not fit into this Tag list. \n\nList type: " + ContentType +
                                            ", Tag type: " + item.Type);

            return tags.Remove(item);
        }

        public void AddRange(IEnumerable<NBTTag> items)
        {
            foreach (NBTTag tag in items)
                Add(tag);
        }

        #endregion
    }

    public class NBTList<T> : NBTTag, IList<T> where T : NBTTag
    {
        private readonly List<T> tags;

        public NBTList()
        {
            tags = new List<T>();
        }

        public NBTList(IEnumerable<T> list)
        {
            tags = new List<T>(list);
        }

        public NBTList(SerializationReader reader)
        {
            tags = new List<T>();

            //	this.ContentType = (NBTType)reader.ReadInt32();
            Int32 num = reader.ReadInt32();
            for (int i = 0; i < num; i++)
                Add((T) Deserialize(ContentType, reader));
        }

        public NBTList(XmlElement element)
        {
            tags = new List<T>();

            //this.ContentType = (NBTType)Enum.Parse(typeof(NBTType), element.GetAttribute("contentType"));
            IEnumerable<XmlElement> children = element.ChildElements();
            //log.Debug("Decomposing NBTList with " + children.Count() + " entries");
            foreach (XmlElement entry in children)
            {
                Add((T) Deserialize(entry));
            }
        }

	    public override NBTType Type => NBTType.List;

	    public NBTType ContentType => Map.TypeToNBTType(typeof (T));


	    public override string ToString()
        {
            return "NBT[" + String.Join(", ", this.Select(x => x.ToString())) + "]";
        }

        public override object GetValue()
        {
            var ret = new Object[tags.Count];
            for (int i = 0; i < ret.Length; i++)
                ret[i] = tags[i].GetValue();
            return ret;
        }

        public override void Serialize(SerializationWriter writer)
        {
            //	writer.Write((Int32)this.ContentType);
            writer.Write(tags.Count);
            foreach (T tag in tags)
                tag.Serialize(writer);
        }

        public override XmlElement Serialize(XmlDocument doc)
        {
            XmlElement eBase = doc.CreateElement(Type.ToString());

            foreach (T child in this)
            {
                eBase.AppendChild(child.Serialize(doc));
            }

            return eBase;
        }

        public static implicit operator NBTList<T>(T[] array)
        {
            return new NBTList<T>(array);
        }

        public static implicit operator T[](NBTList<T> value)
        {
            return value.ToArray();
        }

        public static implicit operator NBTList<T>(NBTList value)
        {
            var ret = new NBTList<T>();
            foreach (NBTTag tag in value)
                ret.Add((T) tag);

            return ret;
        }

        public static implicit operator NBTList(NBTList<T> value)
        {
            var ret = new NBTList(value.ContentType);
            foreach (T tag in value)
                ret.Add(tag);

            return ret;
        }

        #region IList implementation

        public IEnumerator<T> GetEnumerator()
        {
            return tags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            tags.Add(item);
        }


        public int IndexOf(T item)
        {
            return tags.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            tags.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            tags.RemoveAt(index);
        }

        public T this[int index]
        {
            get { return tags[index]; }
            set { tags[index] = value; }
        }


        public void Clear()
        {
            tags.Clear();
        }

        public bool Contains(T item)
        {
            return tags.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            tags.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return tags.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            return tags.Remove(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (T tag in items)
                Add(tag);
        }

        #endregion
    }
}