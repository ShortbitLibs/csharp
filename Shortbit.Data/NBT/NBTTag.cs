﻿// /*
//  * NBTTag.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using Shortbit.Data.Serialization;
using Shortbit.Utils;

namespace Shortbit.Data.NBT
{

    public abstract class NBTTag
	{
		#region Type mapping

		
		protected static readonly IDictionary<Type, NBTType> MapTypeToNBTType = new Dictionary<Type, NBTType>
		{
			{ typeof(Boolean), NBTType.Boolean },
			{ typeof(Byte), NBTType.Byte},
			{ typeof(SByte), NBTType.SByte},
			{ typeof(Char), NBTType.Char},
			{ typeof(Decimal), NBTType.Decimal},
			{ typeof(Single), NBTType.Single},
			{ typeof(Double), NBTType.Double},
			{ typeof(Int16), NBTType.Int16},
			{ typeof(Int32), NBTType.Int32},
			{ typeof(Int64), NBTType.Int64},
			{ typeof(UInt16), NBTType.UInt16},
			{ typeof(UInt32), NBTType.UInt32},
			{ typeof(UInt64), NBTType.UInt64},
			{ typeof(String), NBTType.String},
			{ typeof(IEnumerable<NBTTag>), NBTType.List},
			{ typeof(IDictionary<String, NBTTag>), NBTType.Compound},
		};

		protected static readonly IDictionary<NBTType, Type> MapNBTTypeToType = MapTypeToNBTType.ToDictionary(pair => pair.Value, pair => pair.Key);

		protected static readonly IDictionary<NBTType, Type> MapNBTTypeToImplementation = new Dictionary<NBTType, Type>
		{
			{ NBTType.Boolean, typeof(NBTPrimitiveType<Boolean>) },
			{ NBTType.Byte, typeof(NBTPrimitiveType<Byte>)},
			{ NBTType.SByte, typeof(NBTPrimitiveType<SByte>)},
			{ NBTType.Char, typeof(NBTPrimitiveType<Char>)},
			{ NBTType.Decimal, typeof(NBTPrimitiveType<Decimal>)},
			{ NBTType.Single, typeof(NBTPrimitiveType<Single>)},
			{ NBTType.Double, typeof(NBTPrimitiveType<Double>)},
			{ NBTType.Int16, typeof(NBTPrimitiveType<Int16>)},
			{ NBTType.Int32, typeof(NBTPrimitiveType<Int32>)},
			{ NBTType.Int64, typeof(NBTPrimitiveType<Int64>)},
			{ NBTType.UInt16, typeof(NBTPrimitiveType<UInt16>)},
			{ NBTType.UInt32, typeof(NBTPrimitiveType<UInt32>)},
			{ NBTType.UInt64, typeof(NBTPrimitiveType<UInt64>)},
			{ NBTType.String, typeof(NBTPrimitiveType<String>)},
			
			{ NBTType.List, typeof(NBTList)},
			{ NBTType.Compound, typeof(NBTCompound)},
		};

		protected static readonly IDictionary<Type, NBTType> MapImplementationToNBTType = MapNBTTypeToImplementation.ToDictionary(pair => pair.Value, pair => pair.Key);

		protected static readonly IDictionary<NBTType, Func<SerializationReader, NBTTag>> SerializationConstructors = MapNBTTypeToImplementation.ToDictionary(pair => pair.Key, pair => pair.Value.GetConstructorDelegate<Func<SerializationReader, NBTTag>>());
		protected static readonly IDictionary<NBTType, Func<XmlElement, NBTTag>> XmlConstructors = MapNBTTypeToImplementation.ToDictionary(pair => pair.Key, pair => pair.Value.GetConstructorDelegate<Func<XmlElement, NBTTag>>());
		protected static readonly IDictionary<NBTType, Delegate> TypedValueConstructors = new ConcurrentDictionary<NBTType, Delegate>();
		protected static readonly IDictionary<NBTType, Func<Object, NBTPrimitiveType>> UntypedValueConstructors = new ConcurrentDictionary<NBTType, Func<Object, NBTPrimitiveType>>();

		public static class Map
		{
			public static NBTType TypeToNBTType<T>()
			{
				return TypeToNBTType(typeof(T));
			}
			public static NBTType TypeToNBTType(Type type)
			{
				//if (type.IsGenericType)
				//s	type = type.GetGenericTypeDefinition();

				foreach (var pair in MapTypeToNBTType)
				{
					if (pair.Key.IsAssignableFrom(type))
						return pair.Value;
				}

				throw new ArgumentException();
			}
			public static Type NBTTypeToType(NBTType type)
			{
				return MapNBTTypeToType[type];
			}

			public static NBTType ImplementationToNBTType<T>() where T : NBTTag
			{
				return ImplementationToNBTType(typeof(T));
			}
			public static NBTType ImplementationToNBTType(Type type)
			{
				if (!MapImplementationToNBTType.ContainsKey(type))
					throw new ArgumentException("Unknown NBT type " + type.Name);

				return MapImplementationToNBTType[type];
			}
			public static Type NBTTypeToImplementation(NBTType type)
			{
				return MapNBTTypeToImplementation[type];
			}

			public static Func<SerializationReader, NBTTag> SerializationConstructor(NBTType type)
			{
				return NBTTag.SerializationConstructors[type];
			}
			public static Func<XmlElement, NBTTag> XmlConstructor(NBTType type)
			{
				return NBTTag.XmlConstructors[type];
			}


			public static Delegate RawValueConstructor(Type type)
			{
				var nbtType = TypeToNBTType(type);
				if (!NBTPrimitiveType.IsPrimitiveType(nbtType))
					throw new ArgumentException("Type " + type.Name + " is not a primitive NBT type.");

				return NBTTag.TypedValueConstructors[nbtType];
			}
			public static Func<T, INBTPrimitiveType<T>> ValueConstructor<T>()
			{
				return (Func<T, INBTPrimitiveType<T>>)RawValueConstructor(typeof(T));
			}
			public static Func<object, NBTPrimitiveType> ValueConstructor(Type type)
			{
				var nbtType = TypeToNBTType(type);
				if (!NBTPrimitiveType.IsPrimitiveType(nbtType))
					throw new ArgumentException("Type " + type.Name + " is not a primitive NBT type.");

				return NBTTag.UntypedValueConstructors[nbtType];
			}
		}
		static NBTTag()
		{
			foreach (var primitiveType in NBTPrimitiveType.PrimitiveTypes)
			{
				var type = Map.NBTTypeToType(primitiveType);
				var impl = Map.NBTTypeToImplementation(primitiveType);

				var ctor = impl.GetConstructor(type);

				var eTypedValueParam = Expression.Parameter(type, "value");
				var eTypedCtor = Expression.New(ctor, eTypedValueParam);
				var eTypedLambda = Expression.Lambda(eTypedCtor, eTypedValueParam);
				var typedLambda = eTypedLambda.Compile();

				TypedValueConstructors.Add(primitiveType, typedLambda);


				var eUntypedValueParam = Expression.Parameter(typeof(Object), "value");
				var eUntypedCast = Expression.Convert(eUntypedValueParam, type);
				var eUntypedCtor = Expression.New(ctor, eUntypedCast);
				var eUntypedLambda = Expression.Lambda<Func<Object, NBTPrimitiveType>>(eUntypedCtor, eUntypedValueParam);
				var untypedLambda = eUntypedLambda.Compile();

				UntypedValueConstructors.Add(primitiveType, untypedLambda);
			}
		}

		#endregion


		// ReSharper disable once UnusedParameter.Local
		protected NBTTag(SerializationReader reader)
		{
		}



        protected NBTTag()
        {
        }

        public virtual NBTType Type => GetNBTType(this);

		public static NBTType GetNBTType<T>() where T : NBTTag
		{
			return Map.ImplementationToNBTType<T>();
		}

        public static NBTType GetNBTType(NBTTag tag)
		{
			return Map.ImplementationToNBTType(tag.GetType());
		}

        public static NBTType GetNBTType(Type type)
		{
			return Map.ImplementationToNBTType(type);

		}


        public static NBTTag Deserialize(NBTType type, SerializationReader reader)
        {
			var ctor = Map.SerializationConstructor(type);
	        return ctor(reader);
        }

        public static NBTTag Deserialize(XmlElement element)
        {
            var type = (NBTType) Enum.Parse(typeof (NBTType), element.Name);

			var ctor = Map.XmlConstructor(type);
			return ctor(element);
		}

		public abstract void Serialize(SerializationWriter writer);

		public abstract XmlElement Serialize(XmlDocument doc);


		public static NBTTag FromValue(object value)
        {
            Type t = value.GetType();
            NBTType nbt = Map.TypeToNBTType(t);

            if (NBTPrimitiveType.IsPrimitiveType(nbt))
                return Map.ValueConstructor(t)(value);

            if (nbt == NBTType.List)
            {
                NBTType content = Map.TypeToNBTType(t.GetGenericArguments()[0]);
                return new NBTList(content, (IEnumerable<NBTTag>) value);
            }
            if (nbt == NBTType.Compound)
                return new NBTCompound((IDictionary<String, NBTTag>)value);

	        return NBTCompound.FromObject(value);

	        //throw new InvalidOperationException("Given object is a unknown NBT type.\n\nType:" + t.FullName);
        }


        public abstract object GetValue();

        public virtual T GetValue<T>()
        {
            return (T) GetValue();
        }
		
		
        public T As<T>() where T : NBTTag
        {
            return (T) this;
        }

        public static T As<T>(NBTTag tag) where T : NBTTag
        {
            return (T) tag;
        }

        #region Cast Operators

        public static implicit operator NBTTag(Boolean value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Byte value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(SByte value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Char value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Decimal value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Single value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Double value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Int16 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Int32 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(Int64 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(UInt16 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(UInt32 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(UInt64 value)
        {
            return (NBTPrimitiveType) value;
        }

        public static implicit operator NBTTag(String value)
        {
            return (NBTPrimitiveType) value;
        }


        public static implicit operator NBTTag(NBTTag[] array)
        {
            return (NBTList) array;
        }

        /*	public static implicit operator NBTTag(Object value)
		{
			return (NBTCompound)value;
		}*/

        #endregion
    }
}