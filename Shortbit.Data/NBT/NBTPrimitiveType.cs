﻿// /*
//  * NBTAtomicType.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml;
using Shortbit.Data.Serialization;
using Shortbit.Utils;

namespace Shortbit.Data.NBT
{
	public interface INBTPrimitiveType <T>
	{
		T Value { get; set; }
	}

	public class NBTPrimitiveType<T> : NBTPrimitiveType, INBTPrimitiveType<T>
	{
		static NBTPrimitiveType()
		{
			var type = typeof(T);
			var nbtType = NBTTag.Map.TypeToNBTType(type);
			if (!PrimitiveTypes.Contains(nbtType))
				throw new ArgumentException("Can't create generic NBTPrimitiveType of type " + type.Name + " since it's not an primitive NBT type.");
		}

		public NBTPrimitiveType (T value)
		{
			Value = value;
		}

		public NBTPrimitiveType(SerializationReader reader)
            : base(reader)
        {
			Value = CallRead<T>(reader);
		}

		public NBTPrimitiveType(XmlElement element)
		{
			String val = element.GetAttribute("value");
			if (val != String.Empty)
				Value = CallConvert<T>(val);
		}

		public override object GetValue()
		{
			return this.Value;
		}
		

		public T Value { get; set; }


		public static implicit operator NBTPrimitiveType<T>(T value)
		{
			return new NBTPrimitiveType<T>(value);
		}

		public static implicit operator T(NBTPrimitiveType<T> tag)
		{
			return tag.Value;
		}
	}

	public abstract class NBTPrimitiveType : NBTTag
	{
		public static readonly ImmutableArray<NBTType> PrimitiveTypes = new[]
		{
				NBTType.Boolean,
				NBTType.Byte,
				NBTType.Char,
				NBTType.Decimal,
				NBTType.Double,
				NBTType.Int16,
				NBTType.Int32,
				NBTType.Int64,
				NBTType.SByte,
				NBTType.Single,
				NBTType.String,
				NBTType.UInt16,
				NBTType.UInt32,
				NBTType.UInt64,
		}.ToImmutableArray();

		private static readonly Dictionary<Type, Delegate> readerMethods = new Dictionary<Type, Delegate>();
		private static readonly Dictionary<Type, Delegate> converterMethods = new Dictionary<Type, Delegate>();
		
		static NBTPrimitiveType()
		{

			var tConvert = typeof(Convert);
			var tReader = typeof(SerializationReader);

			var eConvertParam = Expression.Parameter(typeof(string));
			var eReaderParam = Expression.Parameter(typeof(SerializationReader));

			foreach (var primitiveType in PrimitiveTypes)
			{
				var type = MapNBTTypeToType[primitiveType];

				if (primitiveType == NBTType.String)
				{
					readerMethods.Add(type, (Func<SerializationReader, string>)(reader => reader.ReadString()));
					converterMethods.Add(type, (Func<string, string>)(s => s));
					continue;	
				}
				try
				{

					var readerMethod = tReader.GetMethod("Read" + type.Name, BindingFlags.Public | BindingFlags.Instance);
					if (readerMethod == null)
						throw new ArgumentNullException("Did not find SerializationReader.Read" + type.Name);
					var eReaderCall = Expression.Call(eReaderParam, readerMethod);
					var eReaderLambda = Expression.Lambda(eReaderCall, eReaderParam);
					var readerLambda = eReaderLambda.Compile();
					readerMethods.Add(type, readerLambda);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}

				try
				{
					var convertMethod = tConvert.GetMethod("To" + type.Name, typeof(string));
					if (convertMethod == null)
						throw new ArgumentNullException("Did not find Convert.To" + type.Name);
					var eConvertCall = Expression.Call(convertMethod, eConvertParam);
					var eConvertLambda = Expression.Lambda(eConvertCall, eConvertParam);
					var convertLambda = eConvertLambda.Compile();
					converterMethods.Add(type, convertLambda);
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
		}
		}

		public static T CallRead<T>(SerializationReader reader)
		{
			var lambda = (Func<SerializationReader, T>)readerMethods[typeof(T)];
			return lambda(reader);
		}
		public static T CallConvert<T>(string value)
		{
			var lambda = (Func<string, T>)converterMethods[typeof(T)];
			return lambda(value);
		}
		

        protected NBTPrimitiveType()
        {
        }

        protected NBTPrimitiveType(SerializationReader reader)
            : base(reader)
        {
        }


		public static bool IsPrimitiveType<T>()
		{
			return IsPrimitiveType(typeof(T));
		}
		public static bool IsPrimitiveType(Type type)
        {
            return IsPrimitiveType(Map.TypeToNBTType(type));
        }

        public static bool IsPrimitiveType(NBTType type)
        {
            switch (type)
            {
                case NBTType.Boolean:
                case NBTType.Byte:
                case NBTType.Char:
                case NBTType.Decimal:
                case NBTType.Double:
                case NBTType.Int16:
                case NBTType.Int32:
                case NBTType.Int64:
                case NBTType.SByte:
                case NBTType.Single:
                case NBTType.String:
                case NBTType.UInt16:
                case NBTType.UInt32:
                case NBTType.UInt64:
                    return true;

                default:
                    return false;
            }
        }

        public new static NBTPrimitiveType Deserialize(NBTType type, SerializationReader reader)
        {
            if (IsPrimitiveType(type))
                return (NBTPrimitiveType) NBTTag.Deserialize(type, reader);

            throw new ArgumentException("Given NBTType " + type + " is not a atomic type.");
        }


        public override string ToString()
        {
            return GetValue().ToString();
        }

	    public override void Serialize(SerializationWriter writer)
        {
            //Log.Debug("Serializing Atomic type " + Type);
        }

        public override XmlElement Serialize(XmlDocument doc)
        {
            XmlElement eBase = doc.CreateElement(Type.ToString());

            eBase.SetAttribute("value", GetValue().ToString());

            return eBase;
        }

		#region Cast Operators
		

		public static implicit operator NBTPrimitiveType(Boolean value)
        {
            return new NBTPrimitiveType<Boolean>(value);
        }

        public static implicit operator NBTPrimitiveType(Byte value)
        {
            return new NBTPrimitiveType<Byte>(value);
        }

        public static implicit operator NBTPrimitiveType(SByte value)
        {
            return new NBTPrimitiveType<SByte>(value);
        }

        public static implicit operator NBTPrimitiveType(Char value)
        {
            return new NBTPrimitiveType<Char>(value);
        }

        public static implicit operator NBTPrimitiveType(Decimal value)
        {
            return new NBTPrimitiveType<Decimal>(value);
        }

        public static implicit operator NBTPrimitiveType(Single value)
        {
            return new NBTPrimitiveType<Single>(value);
        }

        public static implicit operator NBTPrimitiveType(Double value)
        {
            return new NBTPrimitiveType<Double>(value);
        }

        public static implicit operator NBTPrimitiveType(Int16 value)
        {
            return new NBTPrimitiveType<Int16>(value);
        }

        public static implicit operator NBTPrimitiveType(Int32 value)
        {
            return new NBTPrimitiveType<Int32>(value);
        }

        public static implicit operator NBTPrimitiveType(Int64 value)
        {
            return new NBTPrimitiveType<Int64>(value);
        }

        public static implicit operator NBTPrimitiveType(UInt16 value)
        {
            return new NBTPrimitiveType<UInt16>(value);
        }

        public static implicit operator NBTPrimitiveType(UInt32 value)
        {
            return new NBTPrimitiveType<UInt32>(value);
        }

        public static implicit operator NBTPrimitiveType(UInt64 value)
        {
            return new NBTPrimitiveType<UInt64>(value);
        }

        public static implicit operator NBTPrimitiveType(String value)
        {
            return new NBTPrimitiveType<String>(value);
        }

        public static implicit operator Boolean(NBTPrimitiveType tag)
        {
	        return ((INBTPrimitiveType<Boolean>) tag).Value;
        }

        public static implicit operator Byte(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Byte>)tag).Value;
		}

        public static implicit operator SByte(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<SByte>)tag).Value;
		}

        public static implicit operator Char(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Char>)tag).Value;
		}

        public static implicit operator Decimal(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Decimal>)tag).Value;
		}

        public static implicit operator Single(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Single>)tag).Value;
		}

        public static implicit operator Double(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Double>)tag).Value;
		}

        public static implicit operator Int16(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Int16>)tag).Value;
		}

        public static implicit operator Int32(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Int32>)tag).Value;
		}

        public static implicit operator Int64(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<Int64>)tag).Value;
		}

        public static implicit operator UInt16(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<UInt16>)tag).Value;
		}

        public static implicit operator UInt32(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<UInt32>)tag).Value;
		}

        public static implicit operator UInt64(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<UInt64>)tag).Value;
		}

        public static implicit operator String(NBTPrimitiveType tag)
		{
			return ((INBTPrimitiveType<String>)tag).Value;
		}

        #endregion
    }
}