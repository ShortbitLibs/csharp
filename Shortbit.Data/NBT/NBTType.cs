﻿// /*
//  * NBTType.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

namespace Shortbit.Data.NBT
{
    public enum NBTType
    {
        End,
        Compound,
        List,

        Boolean,
        Byte,
        SByte,
        Char,
        Decimal,
        Single,
        Double,
        Int16,
        Int32,
        Int64,
        UInt16,
        UInt32,
        UInt64,
        String,
    }
}