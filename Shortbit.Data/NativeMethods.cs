﻿// /*
//  * NativeMethods.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Runtime.InteropServices;

namespace Shortbit.Data
{
    internal sealed class NativeMethods
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr LoadLibrary(string dllToLoad);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);


        [DllImport("kernel32.dll")]
        public static extern bool FreeLibrary(IntPtr hModule);
    }
}