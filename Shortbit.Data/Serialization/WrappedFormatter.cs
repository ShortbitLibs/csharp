﻿using System;

namespace Shortbit.Data.Serialization
{
    public abstract class WrappedFormat<TS, T> : IFormatter<TS>, IFormatter<TS,T>
    {
        public abstract TS Format(T value);

        public abstract T Parse(TS value);

		public virtual TS CreateEmpty()
		{
			return default(TS);
		}


		public virtual TS CreateEmpty(Type targetType)
		{
			if (!typeof(T).IsAssignableFrom(targetType))
				throw new ArgumentException("Can't create empty value of type " + targetType.FullName + ". Expected "+typeof(T).FullName);

			return this.CreateEmpty();
		}

        public TS Format(object value)
        {
            return this.Format((T)value);
        }

        public object Parse(TS value, Type targetType)
        {
            if (!typeof(T).IsAssignableFrom(targetType))
                throw new ArgumentException("Can't parse value of type " + targetType.FullName + ". Expected " + typeof(T).FullName);

            return Parse(value);
        }
    }
}
