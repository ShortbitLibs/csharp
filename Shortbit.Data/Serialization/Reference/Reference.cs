﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization.Reference
{
    public class Reference<TS>
    {
        public List<IRefererencePathSegment<TS>> Path { get; set; }

        public Reference()
        {
            this.Path = new List<IRefererencePathSegment<TS>>();
        }

        public Reference(Reference<TS> parent, IRefererencePathSegment<TS> segment)
            : this()
        {
            if (parent != null)
                this.Path.AddRange(parent.Path);
            this.Path.Add(segment);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (var segment in this.Path)
            {
                builder.Append(segment.ToString());
            }

            return builder.ToString();
        }
    }
}