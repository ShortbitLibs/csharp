﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization.Reference
{
    public class ReferenceException : Exception
    {
        public ReferenceException()
        {
        }

        public ReferenceException(string message) : base(message)
        {
        }

        public ReferenceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
