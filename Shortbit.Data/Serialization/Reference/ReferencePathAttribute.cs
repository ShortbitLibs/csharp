﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization.Reference
{
    public class ReferencePathAttribute<TS> : IRefererencePathSegment<TS>
    {
        public string Attribute { get; set; }

        public ReferenceValue Resolve(IComplexValue<TS> value, object instance)
        {
            foreach (var attribute in value.Attributes)
            {
                if (attribute.Name != this.Attribute)
                    continue;
                return new ReferenceValue
                {
                    Value = attribute,
                    Instance = attribute.Getter(instance)
                };
            }
            throw new ReferenceException("Expected attribute "+this.Attribute+" in type "+value.Type.FullName);
        }

        public ReferenceValue Resolve(ReferenceValue referenceValue)
        {
            if (referenceValue.Value is IComplexValue<TS>)
                return Resolve((IComplexValue<TS>) referenceValue.Value, referenceValue.Instance);

            throw new ReferenceException("Expected ComplexType value, got "+referenceValue.Value.GetType().Name);
        }

        public override string ToString()
        {
            return "." + this.Attribute;
        }
    }
}
