﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization.Reference
{
    public class ReferencePathIndex<TS> : IRefererencePathSegment<TS>
    {
        public int Index { get; set; }
         

        public ReferenceValue Resolve(ICollectionValue<TS> value, object instance)
        {
            var list = value.ItemsWrapper(instance);
            
            if (this.Index >= list.Count)
                throw new ReferenceException("Expected Collection item with index "+this.Index+". Found Collection with "+list.Count+" elements.");

            return new ReferenceValue
            {
                Value = value.Item,
                Instance = list[this.Index]
            };
        }


        public ReferenceValue Resolve(ReferenceValue referenceValue)
        {
            if (referenceValue.Value is ICollectionValue<TS>)
                return Resolve((ICollectionValue<TS>)referenceValue.Value, referenceValue.Instance);

            throw new ReferenceException("Expected CollectionType value, got " + referenceValue.Value.GetType().Name);
        }


        public override string ToString()
        {
            return "[" + this.Index + "]";
        }
    }
}
