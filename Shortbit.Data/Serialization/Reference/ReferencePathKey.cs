﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization.Reference
{
    public class ReferencePathKey<TS> : IRefererencePathSegment<TS>
    {
        public object Key { get; set; }

        public ReferenceValue Resolve(IDictionaryValue<TS> value, object instance)
        {
            var dict = value.ItemsWrapper(instance);

            if (!dict.Contains(this.Key))
                throw new ReferenceException("Expected Dictionary item with key " + this.Key + ".");

            return new ReferenceValue
            {
                Value = value.Value,
                Instance = dict[this.Key]
            };
        }


        public ReferenceValue Resolve(ReferenceValue referenceValue)
        {
            if (referenceValue.Value is IDictionaryValue<TS>)
                return Resolve((IDictionaryValue<TS>)referenceValue.Value, referenceValue.Instance);

            throw new ReferenceException("Expected DictionaryType value, got " + referenceValue.Value.GetType().Name);
        }

        public override string ToString()
        {
            return "{" + this.Key.ToString() + "}";
        }
    }
}
