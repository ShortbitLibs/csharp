﻿// /*
//  * SerializationWriter.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System.IO;
using System.Text;

namespace Shortbit.Data.Serialization
{
    public sealed class SerializationWriter
    {
        private readonly BinaryWriter writer;


        public SerializationWriter(Stream output)
        {
            writer = new BinaryWriter(output, Encoding.UTF8);
        }

        public void Flush()
        {
            writer.Flush();
        }

        //
        // Zusammenfassung:
        //     Schreibt einen 1-Byte-Boolean-Wert in den aktuellen Stream, wobei 0 (null)
        //     false und 1 true darstellt.
        //
        // Parameter:
        //   value:
        //     Der zu schreibende Boolean-Wert (0 (null) oder 1).
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(bool value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt ein Byte ohne Vorzeichen in den aktuellen Stream und erhöht die
        //     aktuelle Position im Stream um ein Byte.
        //
        // Parameter:
        //   value:
        //     Das zu schreibende Byte ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(byte value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt ein Bytearray in den zugrunde liegenden Stream.
        //
        // Parameter:
        //   buffer:
        //     Ein Bytearray, das die zu schreibenden Daten enthält.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.ArgumentNullException:
        //     buffer ist null.
        public void Write(byte[] buffer)
        {
            writer.Write(buffer);
        }

        //
        // Zusammenfassung:
        //     Schreibt ein Unicode-Zeichen in den aktuellen Stream und erhöht die aktuelle
        //     Position im Stream in Abhängigkeit von der verwendeten Encoding und der in
        //     den Stream geschriebenen Zeichen.
        //
        // Parameter:
        //   ch:
        //     Das zu schreibende Unicode-Zeichen (nicht-Ersatzzeichen).
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.ArgumentException:
        //     ch ist ein einzelnes Ersatzzeichen.
        public void Write(char ch)
        {
            writer.Write(ch);
        }

        //
        // Zusammenfassung:
        //     Schreibt ein Zeichenarray in den aktuellen Stream und erhöht die aktuelle
        //     Position im Stream in Abhängigkeit von der verwendeten Encoding und der in
        //     den Stream geschriebenen Zeichen.
        //
        // Parameter:
        //   chars:
        //     Ein Zeichenarray mit den zu schreibenden Daten.
        //
        // Ausnahmen:
        //   System.ArgumentNullException:
        //     chars ist null.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        public void Write(char[] chars)
        {
            writer.Write(chars);
        }

        //
        // Zusammenfassung:
        //     Schreibt einen Dezimalwert in den aktuellen Stream und erhöht die Position
        //     im Stream um 16 Bytes.
        //
        // Parameter:
        //   value:
        //     Der zu schreibende Dezimalwert.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(decimal value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt einen 8-Byte-Gleitkommawert in den aktuellen Stream und erhöht die
        //     Position im Stream um 8 Bytes.
        //
        // Parameter:
        //   value:
        //     Der zu schreibende 8-Byte-Gleitkommawert.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(double value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt einen 4-Byte-Gleitkommawert in den aktuellen Stream und erhöht die
        //     Position im Stream um 4 Bytes.
        //
        // Parameter:
        //   value:
        //     Der zu schreibende 4-Byte-Gleitkommawert.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(float value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 4-Byte-Ganzzahl mit Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 4 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 4-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(int value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 8-Byte-Ganzzahl mit Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 8 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 8-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(long value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt ein Byte mit Vorzeichen in den aktuellen Stream und erhöht die aktuelle
        //     Position im Stream um ein Byte.
        //
        // Parameter:
        //   value:
        //     Das zu schreibende Byte mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(sbyte value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 2-Byte-Ganzzahl mit Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 2 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 2-Byte-Ganzzahl mit Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(short value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine Zeichenfolge mit Längenpräfix in der aktuellen Codierung von
        //     System.IO.BinaryWriter in diesen Stream und erhöht die aktuelle Position
        //     im Stream in Abhängigkeit von der verwendeten Codierung und der in den Stream
        //     geschriebenen Zeichen.
        //
        // Parameter:
        //   value:
        //     Der zu schreibende Wert.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ArgumentNullException:
        //     value ist null.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(string value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 4-Byte-Ganzzahl ohne Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 4 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 4-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(uint value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 8-Byte-Ganzzahl ohne Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 8 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 8-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(ulong value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt eine 2-Byte-Ganzzahl ohne Vorzeichen in den aktuellen Stream und
        //     erhöht die Position im Stream um 2 Bytes.
        //
        // Parameter:
        //   value:
        //     Die zu schreibende 2-Byte-Ganzzahl ohne Vorzeichen.
        //
        // Ausnahmen:
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(ushort value)
        {
            writer.Write(value);
        }

        //
        // Zusammenfassung:
        //     Schreibt einen Bereich eines Bytearrays in den aktuellen Stream.
        //
        // Parameter:
        //   buffer:
        //     Ein Bytearray, das die zu schreibenden Daten enthält.
        //
        //   index:
        //     Der Anfangspunkt im buffer, an dem mit dem Schreiben begonnen wird.
        //
        //   count:
        //     Die Anzahl der zu schreibenden Bytes.
        //
        // Ausnahmen:
        //   System.ArgumentException:
        //     Die Länge des Puffers minus index ist kleiner als count.
        //
        //   System.ArgumentNullException:
        //     buffer ist null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index oder count ist negativ.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(byte[] buffer, int index, int count)
        {
            writer.Write(buffer, index, count);
        }

        //
        // Zusammenfassung:
        //     Schreibt einen Bereich eines Zeichenarrays in den aktuellen Stream und erhöht
        //     die aktuelle Position im Stream in Abhängigkeit von der verwendeten Encoding
        //     und ggf. der in den Stream geschriebenen Zeichen.
        //
        // Parameter:
        //   chars:
        //     Ein Zeichenarray mit den zu schreibenden Daten.
        //
        //   index:
        //     Der Anfangspunkt im buffer, an dem mit dem Schreiben begonnen wird.
        //
        //   count:
        //     Die Anzahl der zu schreibenden Zeichen.
        //
        // Ausnahmen:
        //   System.ArgumentException:
        //     Die Länge des Puffers minus index ist kleiner als count.
        //
        //   System.ArgumentNullException:
        //     chars ist null.
        //
        //   System.ArgumentOutOfRangeException:
        //     index oder count ist negativ.
        //
        //   System.IO.IOException:
        //     Ein E/A-Fehler ist aufgetreten.
        //
        //   System.ObjectDisposedException:
        //     Der Stream ist geschlossen.
        public void Write(char[] chars, int index, int count)
        {
            writer.Write(chars, index, count);
        }
    }
}