﻿using Shortbit.Data.Serialization.Reference;
using System;
using System.Collections.Generic;

namespace Shortbit.Data.Serialization
{
	public abstract class SerializedElement <TS>
	{

	    public abstract bool IsReference();
	    public abstract String LoadReferenceContent();
	    public abstract void StoreReferenceContent(string reference);


	    public abstract bool HasValue();
		public abstract void StoreValue(TS value);
		public abstract TS LoadValue(TS initialValue);

	    public abstract bool HasAttrib(string name);
		public abstract SerializedElement<TS> GetAttrib(string name);
		public abstract SerializedElement<TS> CreateAttrib(string name);
		
		public abstract IEnumerable<KeyValuePair<int, SerializedElement<TS>>> GetIndexedItems();
		public abstract Tuple<SerializedElement<TS>, int> CreateItemWithIndex();
        public abstract Tuple<SerializedElement<TS>, int> CreateNullItemWithIndex();

        public abstract IEnumerable<KeyValuePair<string, SerializedElement<TS>>> GetKeyedItems();
		public abstract SerializedElement<TS> CreateItemWithKey(string key);
        public abstract SerializedElement<TS> CreateNullItemWithKey(string key);
    }
}
