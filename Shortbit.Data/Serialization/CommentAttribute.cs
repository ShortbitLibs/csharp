﻿using System;

namespace Shortbit.Data.Serialization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property)]
    public class CommentAttribute : Attribute
    {
        public string[] Comments { get; set; }

        public CommentAttribute(params string[] comments)
        {
            this.Comments = comments;
        }
    }
}
