﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Shortbit.Data.Serialization.Format;
using Shortbit.Data.Serialization.Values;
using Shortbit.Utils;
using Shortbit.Data.Serialization.Reference;

namespace Shortbit.Data.Serialization
{
	public class SerializationContext<TS>
    {
        public StructureAnalyser<TS> Structure { get; }
        public SerializedElement<TS> StartElement { get; }
        public object StartInstance { get; }

        private readonly Dictionary<object, Reference<TS>> references = new Dictionary<object, Reference<TS>>();

        public SerializationContext(StructureAnalyser<TS> structure, SerializedElement<TS> startElement, object startInstance)
        {
            this.Structure = structure;
            this.StartElement = startElement;
            this.StartInstance = startInstance;
        }
        

        public bool HasReference(object instance)
        {
            return this.references.ContainsKey(instance);
        }

        public Reference<TS> GetReference(object instance)
        {
            return this.references[instance];
        }

        public void AssignReference(object instance, Reference<TS> reference)
        {
            this.references.Add(instance, reference);
        }

        public Reference<TS> LoadReference(SerializedElement<TS> element)
        {
            var content = element.LoadReferenceContent();
            var r = this.ParseReference(content);
            return r;
        }

        public void StoreReference(SerializedElement<TS> element, Reference<TS> reference)
        {
            var content = this.FormatReference(reference);
            element.StoreReferenceContent(content);
        }

        public object ResolveReference(Reference<TS> reference)
        {
            var value = this.ResolveReferenceSegments(reference.Path);
            return value.Instance;
        }
        public ReferenceValue ResolveReferenceSegments(IEnumerable<IRefererencePathSegment<TS>> segments)
        {
            var value = new ReferenceValue
            {
                Value = this.Structure.Root,
                Instance = this.StartInstance
            };

            foreach (var segment in segments)
            {
                value = segment.Resolve(value);
            }
            return value;
        }

        public string FormatReference(Reference<TS> reference)
        {

            var builder = new StringBuilder();

            foreach (var s in reference.Path)
            {
                var a = s as ReferencePathAttribute<TS>;
                if (a != null)
                {
                    builder.Append(".a");
                    var escaped = a.Attribute.Replace(".", "&dot;");
                    builder.Append(escaped);
                }

                var i = s as ReferencePathIndex<TS>;
                if (i != null)
                {
                    builder.Append(".i");
                    builder.Append(i.Index);
                }
                var k = s as ReferencePathKey<TS>;
                if (k != null)
                {
                    var format = StringFormatters.Get(k.Key.GetType());

                    builder.Append(".k");
                    var escaped = format.Format(k.Key).Replace(".", "&dot;");
                    builder.Append(escaped);
                }
            }

            return builder.ToString();
        }

        public Reference<TS> ParseReference(string representation)
        {
            var parts = representation.Split(new [] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            var reference = new Reference<TS>();
            reference.Path.Capacity = parts.Length;

            foreach (var part in parts)
            {
                var id = part[0];
                var info = part.Substring(1);

                if (id == 'a')
                {
                    var unescaped = info.Replace("&dot;", ".");
                    reference.Path.Add(new ReferencePathAttribute<TS> {Attribute = unescaped});
                }

                if (id == 'i')
                    reference.Path.Add(new ReferencePathIndex<TS> {Index = Convert.ToInt32(info)});

                if (id == 'k')
                {
                    var parent = this.ResolveReferenceSegments(reference.Path);
                    var value = (IDictionaryValue<TS>)(parent.Value);
                    var unescaped = info.Replace("&dot;", ".");
                    var key = StringFormatters.Get(value.KeyType).Parse(unescaped, value.KeyType);

                    reference.Path.Add(new ReferencePathKey<TS> {Key = key});
                }
            }
            return reference;
        }
        
    }
}
