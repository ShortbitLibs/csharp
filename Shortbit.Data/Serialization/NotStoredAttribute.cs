﻿using System;

namespace Shortbit.Data.Serialization
{
    public class NotStoredAttribute : Attribute
    {
    }
}
