﻿using Shortbit.Data.Serialization.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization
{
    public class Serializer <TS, T>
    {
        public Type Type => typeof(T);
        public StructureAnalyser<TS> Structure { get; set; }

        public Serializer()
        {
            this.Structure = StructureAnalyser<TS>.GetDefault(this.Type);
        }

        public void Serialize(SerializedElement<TS> startElement, T instance)
        {
            this.Structure.Analyse();

            var context = new SerializationContext<TS>(this.Structure, startElement, instance);
            
            this.Structure.Root.Save(context, context.StartElement, new Reference<TS>(), () => instance);
        }

        public T Deserialize(SerializedElement<TS> startElement)
        {
            var factory = StructureAnalyser<TS>.CreateFactory(this.Type);
            return this.Deserialize(startElement, () => (T) factory());
        }
        public T Deserialize(SerializedElement<TS> startElement, Func<T> factory)
        {
            this.Structure.Analyse();

            T instance = factory();
            var context = new SerializationContext<TS>(this.Structure, startElement, instance);

            this.Structure.Root.Load(context, context.StartElement, () => instance, o => instance = (T)o);

            return instance;
        }

    }
}
