﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization.Format
{
    public static class DefaultFormatters
    {
        public static Func<Type, IFormatter<string>> Strings = type => StringFormatters.Get(type);


        public static Func<Type, IFormatter<TS>> Get<TS>()
        {
            var t = typeof(TS);

            if (t == typeof(string))
                return (Func<Type, IFormatter<TS>>) Strings;

            throw new ArgumentException($"No known default formatter for store type {t.FullName}.");
        }
    }
}
