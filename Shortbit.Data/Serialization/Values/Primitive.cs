﻿using System;
using System.Xml;
using Shortbit.Data.Serialization.Reference;

namespace Shortbit.Data.Serialization.Values
{

	public interface IPrimitiveValue<TS> : IValue
	{
		IFormatter<TS> Formatter { get; set; }

	}

    public class PrimitiveRoot<TS> : Root<TS>, IPrimitiveValue<TS>
    {
        public IFormatter<TS> Formatter { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<object> setter)
        {
            if (!element.HasValue())
                return;

            setter(this.Formatter.ParseElement(element, this.Type));
        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            var value = getter();
            if (value == null)
                return;

            element.StoreValue(this.Formatter.Format(value));
        }

    }

    public class PrimitiveAttribute<TS> : Attribute<TS>, IPrimitiveValue<TS>
    {
        public IFormatter<TS> Formatter { get; set; }
		
	    public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance)
	    {
            if (!element.HasValue())
                return;
	        
			var value = this.Formatter.ParseElement(element, this.Type);
		    this.Setter(instance, value);
	    }

	    public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance)
	    {
		    var value = this.Getter(instance);
            if (value == null)
                return;


            element.StoreValue(this.Formatter.Format(value));
	    }
    }

    public class PrimitiveCollectionItem<TS> : CollectionItem<TS>, IPrimitiveValue<TS>
	{
		public IFormatter<TS> Formatter { get; set; }

		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, int index)
        {
            if (!element.HasValue())
                return;
            var value = this.Formatter.ParseElement(element, this.Type);
			this.Setter(instance, index, value);
		}

		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, int index)
		{
			var value = this.Getter(instance, index);
            if (value == null)
                return;
            element.StoreValue(this.Formatter.Format(value));
		}
        
	}

    public class PrimitiveDictionaryItem<TS> : DictionaryItem<TS>, IPrimitiveValue<TS>
	{
		public IFormatter<TS> Formatter { get; set; }

		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, object key)
        {
            if (!element.HasValue())
                return;
            var value = this.Formatter.ParseElement(element, this.Type);
			this.Setter(instance, key, value);
		}

		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, object key)
		{
			var value = this.Getter(instance, key);
            if (value == null)
                return;
            element.StoreValue(this.Formatter.Format(value));
		}
	}
}
