﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Shortbit.Data.Serialization.Values
{
    public class RootAttributeProvider : ICustomAttributeProvider
    {
        public object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            return new object[0];
        }

        public object[] GetCustomAttributes(bool inherit)
        {
            return new object[0];
        }

        public bool IsDefined(Type attributeType, bool inherit)
        {
            return false;
        }
    }
}
