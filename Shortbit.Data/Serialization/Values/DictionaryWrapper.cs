﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Shortbit.Data.Serialization.Values
{
    public class DictionaryWrapper<TKey, TValue> : IDictionary
	{
		private readonly IDictionary<TKey, TValue> wrapped;

		public DictionaryWrapper(IDictionary<TKey, TValue> wrapped)
		{
			this.wrapped = wrapped;
		}

		public bool Contains(object key)
		{
			return wrapped.ContainsKey((TKey) key);
		}

		public void Add(object key, object value)
		{
			wrapped.Add((TKey)key, (TValue)value);
		}

		public void Clear()
		{
			wrapped.Clear();
		}

		public IDictionaryEnumerator GetEnumerator()
		{
			return new DictionaryEnumerator(wrapped.GetEnumerator());
		}

		public void Remove(object key)
		{
			wrapped.Remove((TKey) key);
		}

		public object this[object key]
		{
			get { return wrapped[(TKey)key]; }
			set { wrapped[(TKey)key] = (TValue)value; }
		}

		public ICollection Keys => new CollectionWrapper<TKey>(wrapped.Keys);
		public ICollection Values => new CollectionWrapper<TValue>(wrapped.Values);
		public bool IsReadOnly => wrapped.IsReadOnly;
		public bool IsFixedSize { get { throw new NotImplementedException(); } }

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void CopyTo(Array array, int index)
		{
			wrapped.CopyTo((KeyValuePair<TKey, TValue>[])array, index);
		}

		public int Count => wrapped.Count;
		public object SyncRoot { get { throw new NotImplementedException();} }
		public bool IsSynchronized { get { throw new NotImplementedException(); } }


		private class CollectionWrapper<T> : ICollection
		{
			private readonly ICollection<T> wrapped;

			public CollectionWrapper(ICollection<T> wrapped)
			{
				this.wrapped = wrapped;
			}

			public IEnumerator GetEnumerator()
			{
				return wrapped.GetEnumerator();
			}

			public void CopyTo(Array array, int index)
			{
				wrapped.CopyTo((T[])array, index);
			}

			public int Count => wrapped.Count;
			public object SyncRoot { get { throw new NotImplementedException(); } }
			public bool IsSynchronized { get { throw new NotImplementedException(); } }
		}

		private class DictionaryEnumerator : IDictionaryEnumerator
		{
			private readonly IEnumerator<KeyValuePair<TKey, TValue>> wrapped;

			public DictionaryEnumerator(IEnumerator<KeyValuePair<TKey, TValue>> wrapped)
			{
				this.wrapped = wrapped;
			}

			public bool MoveNext()
			{
				return wrapped.MoveNext();
			}

			public void Reset()
			{
				wrapped.Reset();
			}

			public object Current => wrapped.Current;
			public object Key => wrapped.Current.Key;
			public object Value => wrapped.Current.Value;
			public DictionaryEntry Entry => new DictionaryEntry(wrapped.Current.Key, wrapped.Current.Value);
		}
	}
}
