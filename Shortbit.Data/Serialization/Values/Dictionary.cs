﻿using System;
using System.Collections;
using System.Xml;
using Shortbit.Data.Serialization.Format;
using Shortbit.Data.Serialization.Reference;
using Shortbit.Utils;

namespace Shortbit.Data.Serialization.Values
{
    public static class Dictionary<TS>
	{
		public static void Load(IDictionaryValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<Object> setter, Func<object> factory)
        {
            var instance = getter();

            if (instance == null)
            {
                instance = factory();
                setter(instance);
            }

            if (element.IsReference())
            {
                var r = serializationContext.LoadReference(element);
                instance = serializationContext.ResolveReference(r);
                setter(instance);
                return;
            }

            var dict = self.ItemsWrapper(instance);

			var keyFormatter = StringFormatters.Get(self.KeyType);

			foreach (var pair in element.GetKeyedItems())
			{
				var k = keyFormatter.Parse(pair.Key, self.KeyType);

			    if (pair.Value == null)
			    {
			        dict.Add(k, null);
			    }
			    else
                {
                    dict.Add(k, self.Value.Factory());
                    self.Value.Load(serializationContext, pair.Value, instance, k);
                }

			}
		}
		public static void Save(IDictionaryValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
		{
		    var instance = getter();

            if (instance == null)
                return;

            if (serializationContext.HasReference(instance))
            {
                var targetReference = serializationContext.GetReference(instance);
                serializationContext.StoreReference(element, targetReference);
                return;
            }

            serializationContext.AssignReference(instance, ownReference);

            var dict = self.ItemsWrapper(instance);

			var keyFormatter = StringFormatters.Get(self.KeyType); 

			foreach (var key in dict.Keys)
			{
			    var value = dict[key];
			    var formattedKey = keyFormatter.Format(key);

			    if (value == null)
			    {
			        element.CreateNullItemWithKey(formattedKey);
			    }
			    else
			    {
			        var r = new Reference<TS>(ownReference, new ReferencePathKey<TS>
			                                  {
			                                      Key = key
			                                  });
			        var childElement = element.CreateItemWithKey(formattedKey);
			        self.Value.Save(serializationContext, childElement, r, instance, key);
			    }
			}
		}
	}

    public interface IDictionaryValue<TS> : IValue
	{
		Type KeyType { get; set; }
		Type ValueType { get; set; }

		DictionaryItem<TS> Value { get; set; }

		Func<object, IDictionary> ItemsWrapper { get; set; }
	}

    public class DictionaryRoot<TS> : Root<TS>, IDictionaryValue<TS>
    {
        public Type KeyType { get; set; }
        public Type ValueType { get; set; }
        public DictionaryItem<TS> Value { get; set; }
        public Func<object, IDictionary> ItemsWrapper { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<object> setter)
        {
            Dictionary<TS>.Load(this, serializationContext, element, getter, setter, factory: getter);

        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            Dictionary<TS>.Save(this, serializationContext, element, ownReference, getter);
        }

    }

    public class DictionaryAttribute<TS> : Attribute<TS>, IDictionaryValue<TS>
	{
		public Type KeyType { get; set; }
		public Type ValueType { get; set; }

		public DictionaryItem<TS> Value { get; set; }

		public Func<object, IDictionary> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance)
		{
			Dictionary<TS>.Load(this, serializationContext, element, () => this.Getter(instance), value => this.Setter(instance, value), this.Factory);
        }
		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance)
		{
		    Dictionary<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance));
		}

	}

    public class DictionaryCollectionItem<TS> : CollectionItem<TS>, IDictionaryValue<TS>
	{
		public Type KeyType { get; set; }
		public Type ValueType { get; set; }

		public DictionaryItem<TS> Value { get; set; }

		public Func<object, IDictionary> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, int index)
		{
			Dictionary<TS>.Load(this, serializationContext, element, () => this.Getter(instance, index), value => this.Setter(instance, index, value), this.Factory);
		}
		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, int index)
		{
		    Dictionary<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, index));
		}

	}

    public class DictionaryDictionaryItem<TS> : DictionaryItem<TS>, IDictionaryValue<TS>
	{
		public Type KeyType { get; set; }
		public Type ValueType { get; set; }

		public DictionaryItem<TS> Value { get; set; }

		public Func<object, IDictionary> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, object key)
		{
			Dictionary<TS>.Load(this, serializationContext, element, () => this.Getter(instance, key), value => this.Setter(instance, key, value), this.Factory);
		}
		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, object key)
		{
		    Dictionary<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, key));
		}

	}
}
