﻿using System;
using System.Collections;
using System.Xml;
using Shortbit.Utils;
using Shortbit.Data.Serialization.Reference;

namespace Shortbit.Data.Serialization.Values
{
    public static class Collection<TS>
	{

		public static void Load(ICollectionValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<Object> setter, Func<object> factory)
        {
            var instance = getter();

            if (instance == null)
            {
                instance = factory();
                setter(instance);
            }

            if (element.IsReference())
            {
                var r = serializationContext.LoadReference(element);
                instance = serializationContext.ResolveReference(r);
                setter(instance);
                return;
            }

            var list = self.ItemsWrapper(instance);

			foreach (var pair in element.GetIndexedItems())
			{
			    if (pair.Value == null)
			        list.Insert(pair.Key, null);
			    else
			    {
			        list.Insert(pair.Key, self.Item.Factory());
			        self.Item.Load(serializationContext, pair.Value, instance, pair.Key);
			    }
			}
		}
		public static void Save(ICollectionValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            var instance = getter();

            if (instance == null)
                return;

            if (serializationContext.HasReference(instance))
            {
                var targetReference = serializationContext.GetReference(instance);
                serializationContext.StoreReference(element, targetReference);
                return;
            }

            serializationContext.AssignReference(instance, ownReference);


            var list = self.ItemsWrapper(instance);

			for (int i = 0; i < list.Count; i++)
			{
			    var value = list[i];

			    if (value == null)
			    {
			        var tuple = element.CreateNullItemWithIndex();
			    }
			    else
                {
                    var r = new Reference<TS>(ownReference, new ReferencePathIndex<TS>
                    {
                        Index = i
                    });


                    var tuple = element.CreateItemWithIndex();
                    self.Item.Save(serializationContext, tuple.Item1, r, instance, i);

                }
			}
		}
	}

    public interface ICollectionValue<TS> : IValue
	{
		Type ItemType { get; set; }

		CollectionItem<TS> Item { get; set; }
		
		Func<object, IList> ItemsWrapper { get; set; }
	}

    public class CollectionRoot<TS> : Root<TS>, ICollectionValue<TS>
    {

        public Type ItemType { get; set; }
        public CollectionItem<TS> Item { get; set; }
        public Func<object, IList> ItemsWrapper { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<object> setter)
        {
            Collection<TS>.Load(this, serializationContext, element, getter, setter, factory: getter);
        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            Collection<TS>.Save(this, serializationContext, element, ownReference, getter);
        }
    }

    public class CollectionAttribute<TS> : Attribute<TS>, ICollectionValue<TS>
	{
        public Type ItemType { get; set; }
        
        public CollectionItem<TS> Item { get; set; }

        public Func<object, IList> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance)
		{
			Collection<TS>.Load(this, serializationContext, element, () => this.Getter(instance), value => this.Setter(instance, value), this.Factory);
		}

		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance)
		{
			Collection<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance));
        }
        
	}
    public class CollectionCollectionItem<TS> : CollectionItem<TS>, ICollectionValue<TS>
	{
		public Type ItemType { get; set; }

		public CollectionItem<TS> Item { get; set; }

		public Func<object, IList> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, int index)
		{
			Collection<TS>.Load(this, serializationContext, element, () => this.Getter(instance, index), value => this.Setter(instance, index, value), this.Factory);
        }

		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, int index)
		{
			Collection<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, index));
        }
        
	}
    public class CollectionDictionaryItem<TS> : DictionaryItem<TS>, ICollectionValue<TS>
	{
		public Type ItemType { get; set; }

		public CollectionItem<TS> Item { get; set; }

		public Func<object, IList> ItemsWrapper { get; set; }


		public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, object key)
		{
			Collection<TS>.Load(this, serializationContext, element, () => this.Getter(instance, key), value => this.Setter(instance, key, value), this.Factory);
        }

		public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, object key)
		{
			Collection<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, key));
        }
        
	}

}
