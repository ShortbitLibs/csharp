﻿using System;
using System.Collections.Generic;
using System.Xml;
using Shortbit.Data.Serialization.Reference;
using Shortbit.Utils;

namespace Shortbit.Data.Serialization.Values
{
	public static class Complex<TS>
	{
		public static void Load(IComplexValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<Object> setter, Func<object> factory)
        {
            var instance = getter();

            if (instance == null)
            {
                instance = factory();
                setter(instance);
            }

            if (element.IsReference())
            {
                var r = serializationContext.LoadReference(element);
                instance = serializationContext.ResolveReference(r);
                setter(instance);
                return;
            }

            foreach (var attribute in self.Attributes)
            {
                if (!element.HasAttrib(attribute.Name))
                    continue;
				attribute.Load(serializationContext, element.GetAttrib(attribute.Name), instance);
			}
		}
		public static void Save(IComplexValue<TS> self, SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            var instance = getter();

            if (instance == null)
                return;

            if (serializationContext.HasReference(instance))
            {
                var targetReference = serializationContext.GetReference(instance);
                serializationContext.StoreReference(element, targetReference);
                return;
            }

            serializationContext.AssignReference(instance, ownReference);

            foreach (var attribute in self.Attributes)
            {
                var v = attribute.Getter(instance);
                if (v == null)
                    continue;

                var r = new Reference<TS>(ownReference, new ReferencePathAttribute<TS>
                {
                    Attribute = attribute.Name
                });


				var e = element.CreateAttrib(attribute.Name);
				attribute.Save(serializationContext, e, r, instance);
			}
		}


    }

    public interface IComplexValue<TS> : IValue
    {
        List<Attribute<TS>> Attributes { get; set; }
    }


    public class ComplexRoot<TS> : Root<TS>, IComplexValue<TS>
    {
        public List<Attribute<TS>> Attributes { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Func<object> getter, Action<object> setter)
        {
            Complex<TS>.Load(this, serializationContext, element, getter, setter, factory: getter); // Root allways has a instance, so no factory needed

        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, Func<object> getter)
        {
            Complex<TS>.Save(this, serializationContext, element, ownReference, getter);
        }
        
    }


    public class ComplexAttribute<TS> : Attribute<TS>, IComplexValue<TS>
    {
        public List<Attribute<TS>> Attributes { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance)
        {
            Complex<TS>.Load(this, serializationContext, element, () => this.Getter(instance), value => this.Setter(instance, value), this.Factory);
        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance)
        {
            Complex<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance));
        }
       
    }
    public class ComplexCollectionItem<TS> : CollectionItem<TS>, IComplexValue<TS>
    {
        public List<Attribute<TS>> Attributes { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, int index)
        {
            Complex<TS>.Load(this, serializationContext, element, () => this.Getter(instance, index), value => this.Setter(instance, index, value), this.Factory);
        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, int index)
        {
            Complex<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, index));
        }
       
    }
    public class ComplexDicionaryItem<TS> : DictionaryItem<TS>, IComplexValue<TS>
    {
        public List<Attribute<TS>> Attributes { get; set; }

        public override void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, object key)
        {
            Complex<TS>.Load(this, serializationContext, element, () => this.Getter(instance, key), value => this.Setter(instance, key, value), this.Factory);
        }

        public override void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, object key)
        {
            Complex<TS>.Save(this, serializationContext, element, ownReference, () => this.Getter(instance, key));
        }
        
    }

}
