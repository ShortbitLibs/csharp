﻿using System;
using System.Xml;
using Shortbit.Data.Serialization.Reference;

namespace Shortbit.Data.Serialization.Values
{
    public abstract class CollectionItem<TS> : IValue
	{
		public Type Type { get; set; }

		public Action<object, int, object> Setter { get; set; }
		public Func<object, int, object> Getter { get; set; }
		public Func<object> Factory { get; set; }

		public abstract void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance, int index);
		public abstract void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance, int index);
	}
}
