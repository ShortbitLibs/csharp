﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Shortbit.Data.Serialization.Values
{
    public class ListWrapper<T> : IList
    {

        public int Count => wrapped.Count;
        public object this[int index]
        {
            get { return wrapped[index]; }
            set { wrapped[index] = (T)value; }
        }


        public object SyncRoot { get { throw new NotImplementedException(); } }
        public bool IsSynchronized { get { throw new NotImplementedException(); } }
        public bool IsReadOnly => wrapped.IsReadOnly;
        public bool IsFixedSize { get { throw new NotImplementedException(); } }


        private readonly IList<T> wrapped;

        public ListWrapper(IList<T> wrapped)
        {
            this.wrapped = wrapped;
        }


        public IEnumerator GetEnumerator()
        {
            return wrapped.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            wrapped.CopyTo((T[])array, index);
        }

        public int Add(object value)
        {
			wrapped.Add((T)value);
	        return this.Count - 1;
        }


        public bool Contains(object value)
        {
            return wrapped.Contains((T) value);
        }

        public void Clear()
        {
            wrapped.Clear();
        }

        public int IndexOf(object value)
        {
            return wrapped.IndexOf((T) value);
        }

        public void Insert(int index, object value)
        {
            wrapped.Insert(index, (T)value);
        }

        public void Remove(object value)
        {
            wrapped.Remove((T) value);
        }

        public void RemoveAt(int index)
        {
            wrapped.RemoveAt(index);
        }


    }
}
