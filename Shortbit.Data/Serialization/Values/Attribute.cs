﻿using Shortbit.Data.Serialization.Reference;
using System;
using System.Xml;

namespace Shortbit.Data.Serialization.Values
{
    public abstract class Attribute<TS> : IValue
    {
		public Type Type { get; set; }
		

		public string Name { get; set; }
        public string FullName { get; set; }

        public Action<object, object> Setter { get; set; }
        public Func<object, object> Getter { get; set; }
		public Func<object> Factory { get; set; }



		public abstract void Load(SerializationContext<TS> serializationContext, SerializedElement<TS> element, object instance);
		public abstract void Save(SerializationContext<TS> serializationContext, SerializedElement<TS> element, Reference<TS> ownReference, object instance);

	}
}
