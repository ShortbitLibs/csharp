﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Shortbit.Utils;

namespace Shortbit.Data.Serialization
{
    public class XmlSerializedElement : SerializedElement<string>
    {
        public XElement Element { get; }
        
        
        public XmlSerializedElement(XElement element)
        {
            this.Element = element;
        }


        public override bool IsReference()
        {
            var children = this.Element.Descendants().ToListSafe();
            if (children.Count != 1)
                return false;

            return children[0].Name == "__Ref";
        }

        public override string LoadReferenceContent()
        {
            if (!this.IsReference())
                throw new InvalidOperationException("Current element is not a reference.");
            return this.Element.Descendant("__Ref").Value;
        }

        public override void StoreReferenceContent(string reference)
        {
            this.Element.Add(new XElement("__Ref", reference));
        }

        public override bool HasValue()
        {
            var children = this.Element.Descendants().ToListSafe();
            if (children.Count != 1)
                return false;

            return children[0].Name == "__Val";
        }

        public override void StoreValue(string value)
        {
            this.Element.Add(new XElement("__Val", value));
        }

        public override string LoadValue(string initialValue)
        {
            if (!this.HasValue())
                throw new InvalidOperationException("Current element has no value.");
            return this.Element.Descendant("__Val").Value;
        }

        public override bool HasAttrib(string name)
        {
            return this.Element.DescendantOrDefault(name) != null;
        }

        public override SerializedElement<string> GetAttrib(string name)
        {
            if (!this.HasAttrib(name))
                throw new InvalidOperationException("Current element has no attribute "+name+".");
            
            return new XmlSerializedElement(this.Element.Descendant(name));
        }

        public override SerializedElement<string> CreateAttrib(string name)
        {
            var e = new XElement(name);
            this.Element.Add(e);
            return new XmlSerializedElement(e);
        }

        public override IEnumerable<KeyValuePair<int, SerializedElement<string>>> GetIndexedItems()
        {
            int index = 0;
            foreach (var element in this.Element.Descendants().Where(e => e.Name == "iitem" || e.Name == "iitem_n"))
            {
                yield return new KeyValuePair<int, SerializedElement<string>>(index, new XmlSerializedElement(element));
                index++;
            }
        }

        public override Tuple<SerializedElement<string>, int> CreateItemWithIndex()
        {

            int num = this.Element.Descendants().Count(e => e.Name == "iitem" || e.Name == "iitem_n");

            var el = new XElement("iitem");
            this.Element.Add(el);
            return new Tuple<SerializedElement<string>, int>(new XmlSerializedElement(el), num);
        }
        public override Tuple<SerializedElement<string>, int> CreateNullItemWithIndex()
        {

            int num = this.Element.Descendants().Count(e => e.Name == "iitem" || e.Name == "iitem_n");

            var el = new XElement("iitem_n");
            this.Element.Add(el);
            return new Tuple<SerializedElement<string>, int>(new XmlSerializedElement(el), num);
        }

        public override IEnumerable<KeyValuePair<string, SerializedElement<string>>> GetKeyedItems()
        {
            foreach (var element in this.Element.Descendants().Where(e => e.Name == "kitem" || e.Name == "kitem_n"))
            {
                var key = element.Attribute("key").Value;

                XmlSerializedElement e = null;
                if (element.Name == "kitem")
                    e = new XmlSerializedElement(element);

                yield return new KeyValuePair<string, SerializedElement<string>>(key, e);
            }
        }

        public override SerializedElement<string> CreateItemWithKey(string key)
        {
            var e = new XElement("kitem", new XAttribute("key", key));
            this.Element.Add(e);
            return new XmlSerializedElement(e);
        }
        public override SerializedElement<string> CreateNullItemWithKey(string key)
        {
            var e = new XElement("kitem_n", new XAttribute("key", key));
            this.Element.Add(e);
            return new XmlSerializedElement(e);
        }
    }
}
