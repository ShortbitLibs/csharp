﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using Shortbit.Data.Serialization.Values;

namespace Shortbit.Data.Serialization
{
	public static class PrimitiveTypes
	{
		private static readonly HashSet<Type> Types = new HashSet<Type>()
		{
			typeof(bool),
			typeof(string),
			typeof(float),
			typeof(double),
			typeof(decimal),
			typeof(byte),
			typeof(sbyte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong)
		};

	    public static IImmutableSet<Type> All()
	    {
	        return Types.ToImmutableHashSet();
	    }

		public static bool Contains(Type type)
		{
			return Types.Contains(type);
		}
        
	}
}
