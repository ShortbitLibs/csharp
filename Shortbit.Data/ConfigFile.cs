﻿// /*
//  * ConfigFile.cs
//  *
//  *  Created on: 00:46
//  *         Author: 
//  */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Shortbit.Data
{
    /// <summary>
    ///     Klasse, um Dateien im Ini-Format
    ///     zu verwalten.
    /// </summary>
    public class ConfigFile
    {
        /// <summary>
        ///     Gibt an, welche Zeichen als Kommentarbeginn
        ///     gewertet werden sollen. Dabei wird das erste
        ///     Zeichen defaultmäßig für neue Kommentare
        ///     verwendet.
        /// </summary>
        private const String CommentCharacters = "#;";

        /// <summary>
        ///     Encoding der Datei
        /// </summary>
        private Encoding fileEncoding = Encoding.Default;

        /// <summary>
        ///     Voller Pfad und Name der Datei
        /// </summary>
        private String fileName = "";

        /// <summary>
        ///     Inhalt der Datei
        /// </summary>
        private readonly List<String> lines = new List<string>();

        /// <summary>
        ///     Regulärer Ausdruck für einen Bereichskopf
        /// </summary>
        private readonly Regex regCaption;

        /// <summary>
        ///     Regulärer Ausdruck für einen Kommentar in einer Zeile
        /// </summary>
        private const String RegCommentStr = @"(\s*[" + CommentCharacters + "](?<comment>.*))?";

        /// <summary>
        ///     Regulärer Ausdruck für einen Eintrag
        /// </summary>
        private readonly Regex regEntry;

        /// <summary>
        ///     Leerer Standard-Konstruktor
        /// </summary>
        public ConfigFile()
        {
            regEntry =
                new Regex(@"^[ \t]*(?<entry>([^=])+)=(?<value>([^=" + CommentCharacters + "])+)" + RegCommentStr + "$");
            regCaption = new Regex(@"^[ \t]*(\[(?<caption>([^\]])+)\]){1}" + RegCommentStr + "$");
        }

        /// <summary>
        ///     Konstruktor, welcher sofort eine Datei einliest
        /// </summary>
        /// <param name="filename">Name der einzulesenden Datei</param>
        public ConfigFile(string filename)
            : this(filename, Encoding.UTF8)
        {
        }

        /// <summary>
        ///     Konstruktor, welcher sofort eine Datei einliestund dabie eine bestimmte Kodierung benutzt
        /// </summary>
        /// <param name="filename">Name der einzulesenden Datei</param>
        /// <param name="encoding">Kodierung zum Einlesen</param>
        public ConfigFile(string filename, Encoding encoding)
            : this()
        {
            if (!File.Exists(filename)) throw new IOException("File " + filename + "  not found");
            fileName = filename;
            fileEncoding = encoding;

            lines.AddRange(File.ReadAllLines(fileName, fileEncoding));
            //	using (StreamReader sr = new StreamReader(I_FileName, I_FileEncoding))
            //		while (!sr.EndOfStream) lines.Add(sr.ReadLine().TrimEnd());

            //	foreach (String line in lines)
            //		Console.WriteLine("Line: " + line);
        }

        /// <summary>
        ///     Voller Name der Datei
        /// </summary>
        /// <returns></returns>
        public String FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        /// <summary>
        ///     Kodierung der Datei
        /// </summary>
        /// <returns></returns>
        public Encoding FileEncoding
        {
            get { return fileEncoding; }
            set { fileEncoding = value; }
        }

        /// <summary>
        ///     Verzeichnis der Datei
        /// </summary>
        /// <returns></returns>
        public String Directory
        {
            get { return Path.GetDirectoryName(fileName); }
        }

        /// <summary>
        ///     Datei sichern
        /// </summary>
        /// <returns></returns>
        public Boolean Save()
        {
            if (fileName == "") return false;
            try
            {
                File.WriteAllLines(fileName, lines.ToArray(), fileEncoding);
                //	using (StreamWriter sw = new StreamWriter(I_FileName, false, I_FileEncoding))
                //		foreach (String line in lines)
                //			sw.WriteLine(line);
            }
            catch (IOException ex)
            {
                throw new IOException("Fehler beim Schreiben der Datei " + FileName, ex);
            }
            catch
            {
                throw new IOException("Fehler beim Schreiben der Datei " + FileName);
            }
            return true;
        }

        /// <summary>
        ///     Sucht die Zeilennummer (nullbasiert)
        ///     eines gewünschten Eintrages
        /// </summary>
        /// <param name="caption">Name des Bereiches</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>Nummer der Zeile, sonst -1</returns>
        private int SearchCaptionLine(String caption, Boolean caseSensitive)
        {
            if (!caseSensitive) caption = caption.ToLower();
            for (int i = 0; i < lines.Count; i++)
            {
                String line = lines[i].Trim();
                if (line == "") continue;
                if (!caseSensitive) line = line.ToLower();
                // Erst den gewünschten Abschnitt suchen
                if (line == "[" + caption + "]")
                    return i;
            }
            return -1; // Bereich nicht gefunden
        }

        /// <summary>
        ///     Sucht die Zeilennummer (nullbasiert)
        ///     eines gewünschten Eintrages
        /// </summary>
        /// <param name="caption">Name des Bereiches</param>
        /// <param name="entry">Name des Eintrages</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>Nummer der Zeile, sonst -1</returns>
        private int SearchEntryLine(String caption, String entry, Boolean caseSensitive)
        {
            caption = caption.ToLower();
            if (!caseSensitive) entry = entry.ToLower();
            int captionStart = SearchCaptionLine(caption, false);
            if (captionStart < 0) return -1;
            for (int i = captionStart + 1; i < lines.Count; i++)
            {
                String line = lines[i].Trim();
                if (line == "") continue;
                if (!caseSensitive) line = line.ToLower();
                if (line.StartsWith("["))
                    return -1; // Ende, wenn der nächste Abschnitt beginnt
                if (Regex.IsMatch(line, @"^[ \t]*[" + CommentCharacters + "]"))
                    continue; // Kommentar
                if (line.StartsWith(entry))
                    return i; // Eintrag gefunden
            }
            return -1; // Eintrag nicht gefunden
        }

        /// <summary>
        ///     Kommentiert einen Wert aus
        /// </summary>
        /// <param name="caption">Name des Bereiches</param>
        /// <param name="entry">Name des Eintrages</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>true = Eintrag gefunden und auskommentiert</returns>
        public Boolean CommentValue(String caption, String entry, Boolean caseSensitive)
        {
            int line = SearchEntryLine(caption, entry, caseSensitive);
            if (line < 0) return false;
            lines[line] = CommentCharacters[0] + lines[line];
            return true;
        }

        /// <summary>
        ///     Löscht einen Wert
        /// </summary>
        /// <param name="caption">Name des Bereiches</param>
        /// <param name="entry">Name des Eintrages</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>true = Eintrag gefunden und gelöscht</returns>
        public Boolean DeleteValue(String caption, String entry, Boolean caseSensitive)
        {
            int line = SearchEntryLine(caption, entry, caseSensitive);
            if (line < 0) return false;
            lines.RemoveAt(line);
            return true;
        }

        /// <summary>
        ///     Liest den Wert eines Eintrages aus
        ///     (Erweiterung: case sensitive)
        /// </summary>
        /// <param name="caption">Name des Bereiches</param>
        /// <param name="entry">Name des Eintrages</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>Wert des Eintrags oder leer</returns>
        public String GetValue(String caption, String entry, Boolean caseSensitive)
        {
            int line = SearchEntryLine(caption, entry, caseSensitive);
            if (line < 0) return "";
            int pos = lines[line].IndexOf("=", StringComparison.Ordinal);
            if (pos < 0) return "";
            return lines[line].Substring(pos + 1).Trim();
            // Evtl. noch abschliessende Kommentarbereiche entfernen
        }

        /// <summary>
        ///     Setzt einen Wert in einem Bereich. Wenn der Wert
        ///     (und der Bereich) noch nicht existiert, werden die
        ///     entsprechenden Einträge erstellt.
        /// </summary>
        /// <param name="caption">Name des Bereichs</param>
        /// <param name="entry">name des Eintrags</param>
        /// <param name="value">Wert des Eintrags</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>true = Eintrag erfolgreich gesetzt</returns>
        public Boolean SetValue(String caption, String entry, String value, Boolean caseSensitive)
        {
            caption = caption.ToLower();
            if (!caseSensitive) entry = entry.ToLower();
            int lastCommentedFound = -1;
            int captionStart = SearchCaptionLine(caption, false);
            if (captionStart < 0)
            {
                lines.Add("[" + caption + "]");
                lines.Add(entry + "=" + value);
                return true;
            }
            int entryLine = SearchEntryLine(caption, entry, caseSensitive);
            for (int i = captionStart + 1; i < lines.Count; i++)
            {
                String line = lines[i].Trim();
                if (!caseSensitive) line = line.ToLower();
                if (line == "") continue;
                // Ende, wenn der nächste Abschnitt beginnt
                if (line.StartsWith("["))
                {
                    lines.Insert(i, entry + "=" + value);
                    return true;
                }
                // Suche aukommentierte, aber gesuchte Einträge
                // (evtl. per Parameter bestimmen können?), falls
                // der Eintrag noch nicht existiert.
                if (entryLine < 0)
                    if (Regex.IsMatch(line, @"^[ \t]*[" + CommentCharacters + "]"))
                    {
                        String tmpLine = line.Substring(1).Trim();
                        if (tmpLine.StartsWith(entry))
                        {
                            // Werte vergleichen, wenn gleich,
                            // nur Kommentarzeichen löschen
                            int pos = tmpLine.IndexOf("=", StringComparison.Ordinal);
                            if (pos > 0)
                            {
                                if (value == tmpLine.Substring(pos + 1).Trim())
                                {
                                    lines[i] = tmpLine;
                                    return true;
                                }
                            }
                            lastCommentedFound = i;
                        }
                        continue; // Kommentar
                    }
                if (line.StartsWith(entry))
                {
                    lines[i] = entry + "=" + value;
                    return true;
                }
            }
            if (lastCommentedFound > 0)
                lines.Insert(lastCommentedFound + 1, entry + "=" + value);
            else
                lines.Insert(captionStart + 1, entry + "=" + value);
            return true;
        }

        /// <summary>
        ///     Gibt zurück, ob ein Wert in dem Bereich exisiert.
        /// </summary>
        /// <param name="caption">Name des Bereichs</param>
        /// <param name="entry">name des Eintrags</param>
        /// <param name="caseSensitive">true = Gross-/Kleinschreibung beachten</param>
        /// <returns>true, wenn der eintrag exisiert und false, wenn nicht.</returns>
        public bool ValueExists(String caption, String entry, Boolean caseSensitive)
        {
            int line = SearchEntryLine(caption, entry, caseSensitive);
            if (line < 0) return false;
            //	int pos = lines[line].IndexOf("=");
            //	if (pos < 0) return false;
            return true;
        }

        /// <summary>
        ///     Liest alle Einträge uns deren Werte eines Bereiches aus
        /// </summary>
        /// <param name="caption">Name des Bereichs</param>
        /// <returns>Sortierte Liste mit Einträgen und Werten</returns>
        public SortedList<String, String> GetCaption(String caption)
        {
            var result = new SortedList<string, string>();
            Boolean captionFound = false;
            foreach (string l in lines)
            {
                String line = l.Trim();
                if (line == "") continue;
                // Erst den gewünschten Abschnitt suchen
                if (!captionFound)
                    if (line != "[" + caption + "]") continue;
                    else
                    {
                        captionFound = true;
                        continue;
                    }
                // Ende, wenn der nächste Abschnitt beginnt
                if (line.StartsWith("[")) break;
                if (Regex.IsMatch(line, @"^[ \t]*[" + CommentCharacters + "]")) continue; // Kommentar
                int pos = line.IndexOf("=", StringComparison.Ordinal);
                if (pos < 0)
                    result.Add(line, "");
                else
                    result.Add(line.Substring(0, pos).Trim(), line.Substring(pos + 1).Trim());
            }
            return result;
        }

        /// <summary>
        ///     Erstellt eine Liste aller enthaltenen Bereiche
        /// </summary>
        /// <returns>Liste mit gefundenen Bereichen</returns>
        public List<string> GetAllCaptions()
        {
            return (from line in lines select regCaption.Match(line) into mCaption where mCaption.Success select mCaption.Groups["caption"].Value.Trim()).ToList();
        }

        public bool CaptionExists(String caption)
        {
            return lines.Select(l => l.Trim()).Where(line => line != "").Any(line => line.ToLower() == "[" + caption + "]");
        }

        /// <summary>
        ///     Exportiert sämtliche Bereiche und deren Werte
        ///     in ein XML-Dokument
        /// </summary>
        /// <returns>XML-Dokument</returns>
        public XmlDocument ExportToXml()
        {
            var doc = new XmlDocument();
            XmlElement root = doc.CreateElement(
                Path.GetFileNameWithoutExtension(FileName));
            doc.AppendChild(root);
            XmlElement caption = null;
            foreach (string l in lines)
            {
                Match mEntry = regEntry.Match(l);
                Match mCaption = regCaption.Match(l);
                if (mCaption.Success)
                {
                    caption = doc.CreateElement(mCaption.Groups["caption"].Value.Trim());
                    root.AppendChild(caption);
                    continue;
                }
                if (mEntry.Success)
                {
                    XmlElement xe = doc.CreateElement(mEntry.Groups["entry"].Value.Trim());
                    xe.InnerXml = mEntry.Groups["value"].Value.Trim();
                    if (caption == null)
                        root.AppendChild(xe);
                    else
                        caption.AppendChild(xe);
                }
            }
            return doc;
        }
    }
}