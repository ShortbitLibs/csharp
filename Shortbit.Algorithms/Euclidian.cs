﻿namespace Shortbit.Algorithms
{
	public static class Euclidian
	{
		public static Factors Calculate(int a, int b)
		{
			var s = 0;
			var old_s = 1;
			var t = 1;
			var old_t = 0;
			var r = b;
			var old_r = a;

			while (r != 0)
			{
				var q = old_r / r;

				int temp;


				temp = old_r;
				old_r = r;
				r = temp - q * r;

				temp = old_s;
				old_s = s;
				s = temp - q * s;

				temp = old_t;
				old_t = t;
				t = temp - q * t;
			}
			return new Factors {A = a, B = b, FactorA = s, FactorB = t, Remainder = old_r};
		}

		public static int GreatesCommonDivisor(int a, int b)
		{
			return Calculate(a, b).Remainder;
		}

		public struct Factors
		{
			public int Remainder;

			public int A;
			public int FactorA;

			public int B;
			public int FactorB;
		}
	}
}