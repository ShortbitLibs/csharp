﻿using System;
using System.Collections.Generic;

namespace Shortbit.Algorithms.CSP
{
	public class BinaryConstraint<V, A> : Constraint<V, A>
	{
		private readonly Func<A, A, bool> predicate;


		private readonly V variableA;
		private readonly V variableB;

		public BinaryConstraint(V variableA, V variableB, Func<A, A, bool> predicate)
		{
			this.variableA = variableA;
			this.variableB = variableB;
			this.predicate = predicate;
		}

		public override IEnumerable<V> Variables
		{
			get { return new[] {variableA, variableB}; }
		}

		public static BinaryConstraint<V, A> Equal(V A, V B)
		{
			return new BinaryConstraint<V, A>(A, B, (a, b) =>
			{
				if (a == null)
					return b == null;
				return a.Equals(b);
			});
		}

		public static BinaryConstraint<V, A> NotEqual(V A, V B)
		{
			return new BinaryConstraint<V, A>(A, B, (a, b) =>
			{
				if (a == null)
					return b != null;
				return !a.Equals(b);
			});
		}

		public override bool Satisfies(Dictionary<V, A> assignments)
		{
			if (!assignments.ContainsKey(variableA) || !assignments.ContainsKey(variableB))
				return true;
			return predicate(assignments[variableA], assignments[variableB]);
		}
	}
}