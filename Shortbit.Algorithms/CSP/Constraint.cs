﻿using System.Collections.Generic;

namespace Shortbit.Algorithms.CSP
{
	public abstract class Constraint<V, A>
	{
		public abstract IEnumerable<V> Variables { get; }

		public abstract bool Satisfies(Dictionary<V, A> assignments);
	}
}