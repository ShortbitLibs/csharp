﻿using System;
using System.Collections.Generic;

namespace Shortbit.Algorithms.CSP
{
	public class UnaryConstraint<V, A> : Constraint<V, A>
	{
		private readonly Func<A, bool> predicate;

		private readonly V variable;

		public UnaryConstraint(V variable, Func<A, bool> predicate)
		{
			this.variable = variable;
			this.predicate = predicate;
		}

		public override IEnumerable<V> Variables
		{
			get { return new[] {variable}; }
		}


		public override bool Satisfies(Dictionary<V, A> assignments)
		{
			if (!assignments.ContainsKey(variable))
				return true;
			return predicate(assignments[variable]);
		}
	}
}