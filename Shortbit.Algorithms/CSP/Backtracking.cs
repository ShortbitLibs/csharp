﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shortbit.Utils;

namespace Shortbit.Algorithms.CSP
{
	public static class Backtracking<V, A>
	{
		public static Dictionary<V, A> Solve(Dictionary<V, IEnumerable<A>> domains, IEnumerable<Constraint<V, A>> constraints)
		{
			return Solve(domains, constraints, new Dictionary<V, A>());
		}

		public static Dictionary<V, A> Solve(Dictionary<V, IEnumerable<A>> domains, IEnumerable<Constraint<V, A>> constraints,
			Dictionary<V, A> assignments)
		{
			domains = new Dictionary<V, IEnumerable<A>>(domains);
				// Dictionary is a ref type. By this here, the outer object doe not get changed...
			assignments = new Dictionary<V, A>(assignments);

			var constraintsList = constraints.ToListSafe();
			FilterUnary(domains, ref constraintsList);

			if (!Precheck(domains, constraintsList, assignments))
				return null;

			FilterBinary(domains, ref constraintsList);

			if (!Precheck(domains, constraintsList, assignments))
				return null;

			foreach (var v in assignments.Keys)
				domains.Remove(v);

			if (Backtack(domains, assignments, constraintsList))
				return assignments;

			return null;
		}

		private static bool Precheck(Dictionary<V, IEnumerable<A>> domains, List<Constraint<V, A>> constraints,
			Dictionary<V, A> assignments)
		{
			return domains.All(d => d.Value.Any());
		}

		private static void FilterUnary(Dictionary<V, IEnumerable<A>> domains, ref List<Constraint<V, A>> constraints)
		{
			var unary = constraints.Where(x => x.Variables.Count() == 1);
			constraints = constraints.Where(x => x.Variables.Count() != 1).ToList();

			foreach (var c in unary)
			{
				var variable = c.Variables.First();
				var satisfies = new List<A>();
				var assignment = new Dictionary<V, A>();
				assignment.Add(variable, default(A));
				foreach (var value in domains[variable])
				{
					assignment[variable] = value;
					if (c.Satisfies(assignment))
						satisfies.Add(value);
				}

				//	domains[variable] = satisfies;
			}
		}

		private static void FilterBinary(Dictionary<V, IEnumerable<A>> domains, ref List<Constraint<V, A>> constraints)
		{
			var binary = constraints.Where(x => x.Variables.Count() == 2).ToListSafe();
			var queue = new Queue<Tuple<V, V>>();
			var neighbors = new Dictionary<V, HashSet<V>>();
			foreach (var c in binary)
			{
				var varA = c.Variables.First();
				var varB = c.Variables.Skip(1).First();
				queue.Enqueue(Tuple.Create(varA, varB));

				var existing = neighbors.GetOrDefault(varA, new HashSet<V>());
				existing.Add(varB);
				neighbors.AddOrSet(varA, existing);

				existing = neighbors.GetOrDefault(varB, new HashSet<V>());
				existing.Add(varA);
				neighbors.AddOrSet(varB, existing);
			}

			while (queue.Any())
			{
				var current = queue.Dequeue();
				var constraintsForCurrent = constraints.Where(c =>
				{
					var varA = c.Variables.First();
					var varB = c.Variables.Skip(1).First();
					return varA.Equals(current.Item1) && varB.Equals(current.Item2);
				}).ToList();

				if (ReviseBinary(current.Item1, current.Item2, domains, constraintsForCurrent))
				{
					foreach (var neighbor in neighbors[current.Item1])
						if (!neighbor.Equals(current.Item2))
							queue.Enqueue(Tuple.Create(current.Item1, neighbor));

					foreach (var neighbor in neighbors[current.Item2])
						if (!neighbor.Equals(current.Item1))
							queue.Enqueue(Tuple.Create(current.Item2, neighbor));
				}
			}
		}

		private static bool ReviseBinary(V a, V b, Dictionary<V, IEnumerable<A>> domains, List<Constraint<V, A>> constraints)
		{
			var domainA = new List<A>(domains[a]);
			var domainB = new List<A>(domains[b]);

			var assignment = new Dictionary<V, A>();
			var removeA = new HashSet<A>();
			var removeB = new HashSet<A>();
			foreach (var vA in domainA)
			foreach (var vB in domainB)
			{
				assignment.AddOrSet(a, vA);
				assignment.AddOrSet(b, vB);
				if (!constraints.All(c => c.Satisfies(assignment)))
				{
					removeA.Add(vA);
					removeB.Add(vB);
				}
			}
			domainA.RemoveAll(removeA.Contains);
			domainB.RemoveAll(removeB.Contains);

			domains[a] = domainA;
			domains[b] = domainB;

			return removeA.Any() || removeB.Any();
		}

		private static bool Backtack(Dictionary<V, IEnumerable<A>> remainingDomains,
			Dictionary<V, A> assignments, List<Constraint<V, A>> constraints)
		{
			if (!remainingDomains.Any())
				return true;


			var currDomain = remainingDomains.First();
			var variable = currDomain.Key;
			remainingDomains.Remove(variable);

			Debug.WriteLine("Variable: " + variable);

			Debug.WriteLine("  Domain: " + remainingDomains.ToAssignmentString());
			Debug.WriteLine("  Assignments: " + assignments.ToAssignmentString());

			assignments.Add(variable, default(A));

			foreach (var value in currDomain.Value)
			{
				Debug.WriteLine("    Testing " + variable + "=" + value);
				assignments[variable] = value;


				if (!constraints.All(c => c.Satisfies(assignments)))
					continue;

				if (Backtack(remainingDomains, assignments, constraints))
					return true;
			}
			assignments.Remove(variable);
			remainingDomains.Add(currDomain.Key, currDomain.Value);
			return false;
		}
	}
}