﻿using System;
using System.IO;
using System.Linq;
using log4net.Config;
using NUnit.Framework;
using Shortbit.Data.NBT;
using Shortbit.Data.Serialization;

namespace Shortbit.Data
{
	[TestFixture]
	public class SerializationTest
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		static SerializationTest()
		{
			AssemblyLoader.Initialize(true);

			FileInfo file = new System.IO.FileInfo("testConfig.log4net");
			if (file.Exists)
				XmlConfigurator.ConfigureAndWatch(file);
			else
			{
				BasicConfigurator.Configure();
			}
		}


		[SetUp]
		public void init()
		{
			log.Info("Initializing dummy data");
		}

		[Test]
		public void NBT()
		{
			log.Info("Testing NBT Serialization");

			MemoryStream writerStream = new MemoryStream();
			SerializationWriter writer = new SerializationWriter(writerStream);


			NBTCompound compound = new NBTCompound();

			compound.Add("TestInt", 42);
			compound.Add("TestString", "Foo Baa Baz");
			compound.Add("TestCompound", new NBTCompound(){
			{"Foo", 32},
			{"Baa", 2.3}
			});

			compound.Serialize(writer);

			writer.Flush();
			log.Info("Written " + writerStream.ToArray().Length + " bytes");

			MemoryStream readerStream = new MemoryStream(writerStream.ToArray());

			SerializationReader reader = new SerializationReader(readerStream);


			NBTCompound res = (NBTCompound)NBTTag.Deserialize(NBTType.Compound, reader);

			Assert.That(res.GetKeys().Contains("TestInt"));
			Assert.AreEqual(res.GetValue<Int32>("TestInt"), 42);
			Assert.That(res.GetKeys().Contains("TestString"));
			Assert.AreEqual(res.GetValue<String>("TestString"), "Foo Baa Baz");

			Assert.That(res.GetKeys().Contains("TestCompound"));
			Assert.AreEqual(res.Get("TestCompound").GetType(), typeof(NBTCompound));
			NBTCompound content = res.Get<NBTCompound>("TestCompound");

			Assert.That(content.GetKeys().Contains("Foo"));
			Assert.AreEqual(content.GetValue<Int32>("Foo"), 32);
			Assert.That(content.GetKeys().Contains("Baa"));
			Assert.AreEqual(content.GetValue<Double>("Baa"), 2.3);

		}
	}
}
