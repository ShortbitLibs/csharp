﻿using System;
using System.IO;
using System.Security.Cryptography;
using log4net.Config;
using NUnit.Framework;
using Shortbit.Data.Cryptography;

namespace Shortbit.Data
{
	[TestFixture]
	public class EncryptionTest
	{
		const int DataSize = 45;

		const int numRndData = 25;


		RNGCryptoServiceProvider randomSource;
		Random random;


		byte[] encr = null;
		byte[] decr = null;
		
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		static EncryptionTest ()
		{
			AssemblyLoader.Initialize(true);

			FileInfo file = new System.IO.FileInfo("testConfig.log4net");
			if (file.Exists)
				XmlConfigurator.ConfigureAndWatch(file);
			else
			{
				BasicConfigurator.Configure();
			}
		}

		public Byte[][] randomContent()
		{
			log.Info("Initializing random generators...");
			this.randomSource = new RNGCryptoServiceProvider();
			this.random = new Random();

			Byte[][] ret = new Byte[numRndData][];
			for (int i = 0; i < numRndData; i++)
			{
				ret[i] = new byte[this.random.Next(500) + 1];
				this.randomSource.GetBytes(ret[i]);
			}

			return ret;

		}

		private void runTest(C_Base encrypter, C_Base decrypter, byte[] raw)
		{
			log.Debug("  initial data: " + raw.Length);

			encr = encrypter.Encrypt(raw);

			log.Debug("encrypted data: "+encr.Length);

			decr = decrypter.Decrypt(encr);

			log.Debug("    final data: " + decr.Length);

			CollectionAssert.AreEqual(raw, decr);
		}
		private String B2H(byte[] bytes)
		{
			String hex = "";
			for (int i = 0; i < bytes.Length; i++)
			{
				hex += Convert.ToString(bytes[i], 16);
			}
			return hex;
		}

		[SetUp]
		public void init()
		{
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void SingleBlowfish(byte[] raw)
		{
			log.Info("Testing single blowfish instance");
			C_Base fish = new C_BlowFish();

			runTest(fish, fish, raw);
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void DoubleBlowfish(byte[] raw)
		{
			log.Info("Testing double blowfish instance");
			C_Base encrypter = new C_BlowFish();
			C_Base decrypter = new C_BlowFish();

			encrypter.Key = decrypter.Key;

			runTest(encrypter, decrypter, raw);
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void SingleCaesar(byte[] raw)
		{
			log.Info("Testing single caesar instance");
			C_Base fish = new C_Caesar();

			runTest(fish, fish, raw);
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void DoubleCaesar(byte[] raw)
		{
			log.Info("Testing double caesar instance");
			C_Base encrypter = new C_Caesar();
			C_Base decrypter = new C_Caesar();

			encrypter.Key = decrypter.Key;

			runTest(encrypter, decrypter, raw);
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void SingleRSA(byte[] raw)
		{
			log.Info("Testing single RSA instance");
			C_Base fish = new C_SingleRSA();

			runTest(fish, fish, raw);
		}

		[Test]
		[TestCaseSource("randomContent")]
		public void DoubleRSA(byte[] raw)
		{
			log.Info("Testing double RSA instance");
			C_Base encrypter = new C_SingleRSA();
			C_Base decrypter = new C_SingleRSA();

			encrypter.Key = decrypter.Key;

			runTest(encrypter, decrypter, raw);
		}
	}
}
